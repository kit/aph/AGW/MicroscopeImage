close all
zStack = importdata("zStack.dat",' ');

%zValues = zStack.VarName1;
zValues     = zStack(:,1);
InputRange  = 1000; %mV %actual range is +- this value
Intensity   = InputRange/2^15 * (zStack(:,2) - 2^15);

% figure(1)
% plot(zValues,Intensity)
% xlabel('Axial position in um')
% ylabel('Signal in mV')
% title('Axial Line Scan')

%%
%gainM = GainScan.VarName1;
%IntensityM = GainScan.VarName2;

%plot(gainM,IntensityM)
%xlabel('APD Gain M')
%ylabel('Signal in V')
%title('APD3 Gain Scan')

zFit = fit_z_scan(zValues,Intensity);
xlabel('Axial position in um')
ylabel('Signal in mV')
ylim([0 InputRange])
title('Axial Line Scan')
text(5,100,['FWHM = ',num2str(2*sqrt(2*log(2))*zFit.c1/sqrt(2)),char(181),'m'])
text(5,150,['z0 = ',num2str(zFit.b1),char(181),'m'])

FWHM = 2*sqrt(2*log(2))*zFit.c1/sqrt(2)