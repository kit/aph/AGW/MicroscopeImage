#include "mySerialPort.h"

/*
https://orxor.wordpress.com/2014/10/03/rs232-programmierung-mit-c-auf-die-schnelle/
*/

#include <fstream>
#include <iostream>
#include <string>

//#include <stdlib.h>
//#include <stdio.h>

#ifdef _WIN32
#include <Windows.h>
#else
#include <unistd.h>
#endif

#include "rs232.h"

using namespace std;

mySerialPort::mySerialPort() {
    bdrate = 115200;
    cport_nr = RS232_GetPortnr("COM4");;
    modeString = "8N1";
    flowcontrol = 0;
};

mySerialPort::mySerialPort(std::string COMportStr, std::string modeStr, int baudR, int flwCntrl) {
    bdrate = baudR;
    cport_nr = RS232_GetPortnr(COMportStr.c_str());
    modeString = modeStr;
    //mode = modeString.c_str();
    flowcontrol = 0;
};

bool mySerialPort::ConfigPort(std::string COMportStr, std::string modeStr, int baudR, int flwCntrl) {
    bdrate = baudR;
    cport_nr = RS232_GetPortnr(COMportStr.c_str());
    modeString = modeStr; // copy value of modeStr to local private variable
    //mode = modeString.c_str();
    flowcontrol = 0;
    return 0;
};

bool mySerialPort::SendCommand(string command, bool CR , bool LF, bool silent) {
    string sendCMD = command;
    
    if (CR)
        sendCMD = sendCMD + "\r";
    if (LF)
        sendCMD = sendCMD + "\n";

    if (RS232_OpenComport(cport_nr, bdrate, modeString.c_str(), flowcontrol) != 0 and !silent) {
        cerr << "Error while opening serial interface" << endl;
        return 1;
    }
    else if (!silent) {
        cout << "Com port opened." << endl;
    }

    RS232_cputs(cport_nr, sendCMD.c_str());
    if (!silent) {
        cout << "Sent command was " << sendCMD.substr(0, sendCMD.length()) << endl; // cut CR and LF Bytes
    }

#ifdef _WIN32
    Sleep(100);
#else
    usleep(100000);  /* sleep for 0.1 Second */
#endif

     // Listen for answer from port
    int i;
    int n;

    ClearBuffer(); // clears buffer to receive new debug response
    n = RS232_PollComport(cport_nr, buf, 128);

    if (n > 0) {
        buf[n] = 0;   /* always put a "null" at the end of a string! */

        for (i = 0; i < n; i++) {
            if (buf[i] < 32) {  /* replace unreadable control-codes by dots */
                buf[i] = '.';
            }
        }
        if (!silent) {
            printf("port responding with %i received bytes: %s\n", n, (char*)buf);
        }
    }
    if (RS232_CloseComport(cport_nr) != 0 and !silent) {
        cout << "COM port closed" << endl;
    }
    return 0;
};

bool mySerialPort::ClearBuffer() {
    for (int i = 0; i < 128; i++) {
        *(buf + i) = 0; // reset all buffer elements to zero
    }
    return 1;
};