#pragma once

// includes
#include <windows.h>
//#include <conio.h>
//#include <time.h>
//#include "PI_GCS2_DLL.h"
//#include <iostream>
//#include <fstream>

class LinStage
{

private:
	// Attributes / variables from PI sample code
	char szDeviceIDNs[2000]; // buffer to receive the IDN strings of he controllers (see PI_qIDN()).

	BOOL servoOn;
	BOOL referencingFinished;

	double travelRangeMinimum;
	double travelRangeMaximum;

public:
	// Attributes / variables from PI sample code
	char controllerSerialNumber[100];
	char deviceIdentificationString[200];
	char availableAxes[200];
	char currentStageType[256];

	int piDeviceId;


	// My Variables
	//Stores currently active axis
	char activeAxis[2];
	// Stores current position of the controller
	double currentPos;
	
	// Default constructor
	LinStage(); 

	// Connects the Controller of the LinStage. Requires a pointer to the serial number <<serianNo>> of the controller
	//bool ConnectAndStartUp(char * serialNo);// Starts up Stage and selects/sets active <<axis1>> with user input
	bool ConnectAndStartUp(const char * serialNo);// Starts up Stage and selects/sets active <<axis1>> with user input
	bool ConnectAndStartUpFixedAxis(const char* serialNo, char * setActiveAxis, bool silent = false); //start stage with hardcoded active axis (no user input)

	// Close connection to stage...
	bool DisconnectStage(); //... requires keyboard hit
	bool DisconStage();//... without keyboard hit and without cout..

	// Switch on Servo, Reference Stage and determine movement range of stage
	bool ReferenceStage();

	// Query stage position
	bool QuerryStagePos();

	// Move Stage to absolute position
	bool MoveStageTo(double absPos);

	// Move Stage relative to current position;
	bool MoveStageRel(double moveStep);

	
	
	// PI functions copied from sample code:
	// Helper Functions
	bool ReferenceIfNeeded(int piDeviceId, char* axis);
	void ReportError(int piDeviceId);
	void CloseConnectionWithComment(int piDeviceId, const char* comment);
	bool WaitForMovementStop(int piDeviceId, char* axis1);
	//void ShowAvailablePiControllers();

	// Additional Sample Functions
	bool ReadRecordedData(int iD, char* axis, double Position, double MaxPosValue);
	// ACHTUNG MIT RICHTIGEM STAGE TYPE!! KANN STAGE BESCHÄDIGEN WENN FALSCH GESETZT!!!
	// int ChangeConnectedStage(int piDeviceId, char* axis);
	//bool MoveTwoAxes ( int piDeviceId );
};

