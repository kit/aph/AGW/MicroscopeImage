clear all; close all; clc;

flag_plot_drift = 1;
flag_calib      = 1;
flag_valid      = ~flag_calib;

freqRange = [];
freqRange = 0:10:300;%kHz
pixelsize = 500; %nm

fileListing = dir;
driftpicnamelist = [];

if flag_calib == 1
    identifier = 'cal_';
else
    identifier = 'val_';
end

for i = 1:length(fileListing)% Gehe alle Elemente in Ordner durch
    locName = fileListing(i).name;
    if length(locName) > 6 % checke alle Elemente mit mehr als 6 Symbolen im Dateinamen
        if strcmp(locName(1:4),identifier) & strcmp(locName(end-5:end),'Hz.tif'); % identifiziere XXXXXXXHz.tif Files
            driftpicnamelist = [driftpicnamelist; locName];
            %Haenge Name der Bilddatei an Liste an
            locFreq = str2num(locName(1:6));
            freqRange = [freqRange locFreq];
        end
    end
end
save('driftpicnamelist.mat', 'driftpicnamelist')

%%
[r,c]=size(driftpicnamelist);                   % this will determine the number of images we have to loop through
% Half width of images
BASESIZE = length(mean(double(imread([driftpicnamelist(1,:)])),3))/2;
CORRSIZE = 20;
kernel   = [BASESIZE-CORRSIZE+1,BASESIZE-CORRSIZE+1,2*CORRSIZE-1,2*CORRSIZE-1]; %% rectangle used to cut out from input

figure(1)
imshow([driftpicnamelist(1,:)])           % show the first image
title('DIC Image')        % put a title

firstimage  = 1;
xdrift      = zeros(1,r);
ydrift      = zeros(1,r);
base        = mean(double(imread([driftpicnamelist(1,:)])),3);
sub_base    = imcrop(base,kernel);

% determine offset by correlating base picture with kernel taken from
% itself
norm_cross_corr_base    = normxcorr2(sub_base,base);
subpixel_base           = true;
%     [xpeakb, ypeakb, amplitudeb] = findpeak_dic(norm_cross_corr_base,subpixel_base);
[xpeakb, ypeakb] = findpeak_dic(norm_cross_corr_base,subpixel_base);

for i=firstimage:r               % run through all images
    % load input and crop
    input       = mean(double(imread([driftpicnamelist(i,:)])),3);
    sub_input   = imcrop(input,kernel);
    
    %perform xcorr
%     norm_cross_corr = normxcorr2(sub_input,base);
    norm_cross_corr = normxcorr2(sub_base,input);
    subpixel        = true;
    [xpeak, ypeak] = findpeak_dic(norm_cross_corr,subpixel);

    %% Coordinate sign convention applied here
    % The convention is chosen as such, that a !sample-movement
    % 1) to the left (negative x) yields a negative displacement
    % 2) to the bottom (negative y) yields a negative displacement
    % Together with the convention for the vibrometry displacement
    % results, this forms a right handed coordinate system.
    % This convention was formed on 11.02.2022 for the confocal Lab2 setup
    % data can be found in:
%     % S:\AG Wegener\MechaMeta\Adaptive Optics\04 Messungen\Setup Characterization\20220211_Calibrate_xy_Axis_directions
%     xdrift(i) = -1.*(xpeak-xpeakb);
%     ydrift(i) = ypeak-ypeakb; %  
        % update 19.9.2022 I exchanged the normxcorr2(sub_input,base) to
    % normxcorr2(sub_base,input). This flips the sign on x and y direction!
    % This way, I ensure that the feature I want to track is always
    % centered in the middle of the kernel (sub_base) and I use the same
    % kernel to search for the feature in any other image. I find this more
    % intuitive.
%     imagesc(input); % for debugging
    % S:\AG Wegener\MechaMeta\Adaptive Optics\04 Messungen\Setup Characterization\20220211_Calibrate_xy_Axis_directions
    xdrift(i) = (xpeak-xpeakb);
    ydrift(i) = -1.*(ypeak-ypeakb); %  
end

% safe displacements in pixels
% xdriftPath = [pwd '\' 'xdrift.dat'];
% writematrix(xdrift,xdriftPath,'Delimiter','tab')
% ydriftPath = [pwd '\' 'ydrift.dat'];
% writematrix(ydrift,ydriftPath,'Delimiter','tab')

% rescale to mm
xdriftMM = pixelsize .* xdrift .* 1e-6;
ydriftMM = pixelsize .* ydrift .* 1e-6;

% xdriftMMPath = [pwd '\' 'xdriftMM.dat'];
% writematrix(xdriftMM,xdriftMMPath,'Delimiter','tab')
% ydriftMMPath = [pwd '\' 'ydriftMM.dat'];
% writematrix(ydriftMM,ydriftMMPath,'Delimiter','tab')

%%
if flag_calib == 1
    fileID = fopen('xdriftMM.dat', 'w');
    fprintf(fileID,'%6d %9.6f\n',[freqRange; xdriftMM]);
    fclose(fileID);

    fileID = fopen('ydriftMM.dat', 'w');
    fprintf(fileID,'%6d %9.6f\n',[freqRange; ydriftMM]);
    fclose(fileID);

    zdriftMM = zeros(size(xdriftMM));
    fileID = fopen('driftCalib.dat','w');
    fprintf(fileID,'%8.6f %8.6f %8.6f\n',[xdriftMM; ydriftMM; zdriftMM])
    fclose(fileID);
end

%%

if flag_plot_drift == 1
    % rescale to nm
    xdriftNM = pixelsize .* xdrift;
    ydriftNM = pixelsize .* ydrift;

    rdriftNM = sqrt(xdriftNM.^2 + ydriftNM.^2);
    extr = max(rdriftNM);
    if extr < 1550
        extr = 1550;
    end

    xrang = 1.1*[-extr extr]; yrang = xrang;

    f = figure(2); clf; 
    p = plot(xdriftNM,ydriftNM,'o-','LineWidth',1.5);
    pbaspect([1 1 1]); 
    grid on; box on; 
    xlabel("x-drift in nm"); ylabel("y-drift in nm"); 
    xlim(xrang); ylim(yrang);

    drawnow
    % modified jet-colormap
    n  = length(ydriftNM);
    cd = [uint8(jet(n)*255) uint8(ones(n,1))].';
    set(p.Edge, 'ColorBinding','interpolated', 'ColorData',cd)

    cbh = colorbar;
    colormap jet
    set(cbh,'Ticks',linspace(0,1,length(freqRange)));
    set(cbh,'XTickLabel',freqRange); 
    cbh.Title.String = "kHz";
    
    if flag_calib == 1
        title('Sample Drift Cal.');
    else
        title('Sample Drift Val.');
    end

    saveas(gcf,[identifier 'drift.bmp'])
end


