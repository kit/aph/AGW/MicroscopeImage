
// simple tiff reader-writer for grey bitmaps (8 or 16 bit)
// Author: Rudi Weinacker
// 1. Version 07.05.2020
// Rev. 04.08.2020:
//  - now able to read tiff images, with:
//    - up to 5000000 bytes per file
//    - no compression
//    - not tiled, just single or multiple strips
//    - only 8 or 16 bit grey
//    - palettes are ignored
//  - Attributes added
//  - Timestamp added. Every call of saveData(...) updates timestamp


#include "TiffGrey.h"

#include <cstring>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string>
#include <time.h>


#ifndef BYTE
typedef unsigned char BYTE;
#endif
#ifndef WORD
typedef unsigned short WORD;
#endif
#ifndef DWORD
typedef unsigned long DWORD;
#endif


///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// class CFtoA
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////

namespace
{
	class CFtoA
	{
		enum { BufLength = 32 };
		char m_Buf [ BufLength ];
		char* getBuf () { return &m_Buf [ BufLength - 2 ]; }
		char* getBufZ () { return &m_Buf [ BufLength - 1 ]; }
	public:
		~CFtoA () {}
		//CFtoA () { m_Buf [ BufLength - 1 ] = 0 ; }
		CFtoA () { *getBufZ () = 0; }
		const char* ftoa ( double v , unsigned int numdec = 9 ); ///< Supress ending zeros. Supress decimal point if not necessary
		const char* ftoaf ( double v , unsigned int numdec ); ///< Supress decimal point if not necessary
		const char* ftoa3 ( double v ); ///< fixed 3 decimals.
	};
	static const double Pow [] =
	{
		1 ,
		10 ,
		100 ,
		1000 ,
		10000 ,
		100000 ,
		1000000 ,
		10000000 ,
		100000000 ,
		1000000000 ,
	};
	template < class T >
	static char nextDigit ( T& n )
	{
		const T rem = n % 10;
		n = n / 10;
		return ( char ) rem;
	}
	template < class T >
	static char nextDigitA ( T& n )
	{
		const T rem = n % 10;
		n = n / 10;
		return '0' + ( char ) rem;
	}

	const char* CFtoA::ftoa3 ( double v )
	{
		char* const s = getBufZ ();
		//const double rv = roundD ( v * 1000.0 );
		//unsigned __int64 ifull = ( __int64 ) fabs ( rv ) ;
		double Add = 0.5 ;
		if ( v < 0 )
		{
			v = - v ;
			Add = -0.5 ;
		}
		unsigned __int64 ifull = ( __int64 ) ( v * 1000.0 + Add ) ;
		s [ -1 ] = nextDigitA ( ifull );
		s [ -2 ] = nextDigitA ( ifull );
		s [ -3 ] = nextDigitA ( ifull );
		s [ -4 ] = '.';
		s [ -5 ] = nextDigitA ( ifull );
		int l = -5;
		while ( ifull > 9 )
		{
			s [ --l ] = nextDigitA ( ifull );
		}
		if ( ifull ) s [ --l ] = '0' + ( char ) ifull;
		if ( Add < 0 ) s [ --l ] = '-';
		return s + l;
	}

	const char* CFtoA::ftoa ( double v , unsigned int numdec )
	{
		char* const s = getBufZ ();
		int l = 0;
		//const double rv = roundD ( v * Pow [ std::min ( numdec , 9 ) ] );
		//unsigned __int64 ifull = ( __int64 ) fabs ( rv );
		if ( numdec > 9 ) numdec = 9 ;
		double Add = 0.5 ;
		if ( v < 0 )
		{
			v = - v ;
			Add = -0.5 ;
		}
		unsigned __int64 ifull = ( __int64 ) ( v * Pow [ numdec ] + Add ) ;
		if ( numdec )
		{
			bool DecWritten = false ;
			unsigned int i ;
			for ( i = 0 ; i < numdec ; i++ )
			{
				const char c = nextDigit ( ifull ) ;
				if ( c )
				{
					DecWritten = true ;
					s [ --l ] = '0' + c ;
					i++ ;
					break ;
				}
			}
			for ( ; i < numdec; i++ ) s [ --l ] = nextDigitA ( ifull ) ;
			if ( DecWritten ) s [ --l ] = '.' ;
		}
		s [ --l ] = nextDigitA ( ifull ) ; // first digit must be written

		while ( ifull > 9 ) // every other digit only if ifull is not zero
		{
			s [ --l ] = nextDigitA ( ifull ) ;
		}
		if ( ifull ) s [ --l ] = '0' + ( char ) ifull ;
		if ( Add < 0 ) s [ --l ] = '-' ;
		return s + l ;
	}
	const char* CFtoA::ftoaf ( double v , unsigned int numdec )
	{
		char* const s = getBufZ () ;
		int l = 0 ;
		//const double rv = roundD ( v * Pow [ std::min ( numdec , 9 ) ] );
		//unsigned __int64 ifull = ( __int64 ) fabs ( rv );
		if ( numdec > 9 ) numdec = 9 ;
		double Add = 0.5 ;
		if ( v < 0 )
		{
			v = - v ;
			Add = -0.5 ;
		}
		unsigned __int64 ifull = ( __int64 ) ( v * Pow [ numdec ] + Add ) ;
		if ( numdec )
		{
			for ( unsigned int i = 0 ; i < numdec ; i++ ) s [ --l ] = nextDigitA ( ifull ) ;
			s [ --l ] = '.' ;
		}
		s [ --l ] = nextDigitA ( ifull ) ; // first digit must be written

		while ( ifull > 9 ) // every other digit only if ifull is not zero
		{
			s [ --l ] = nextDigitA ( ifull ) ;
		}
		if ( ifull ) s [ --l ] = '0' + ( char ) ifull ;
		if ( Add < 0 ) s [ --l ] = '-' ;
		return s + l ;
	}
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// class CDateTime
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

namespace
{
	class CDateTime
	{
		char m_string [ 20 ];
	public:
		void actTime ();
		void setup ( WORD year , WORD month , WORD day , WORD hour , WORD min , WORD sec );
		const char* getString () const { return m_string; }
		CDateTime ();
	};

	void CDateTime::setup ( WORD year , WORD month , WORD day , WORD hour , WORD min , WORD sec )
	{
		sprintf_s ( m_string , 20 , "%04i:%02i:%02i %02i:%02i:%02i" , year , month , day , hour , min , sec );
	}

	void CDateTime::actTime ()
	{
		time_t ActTime = time ( 0 ) ;
		tm nt ;
		localtime_s ( &nt , &ActTime ) ;
		sprintf_s ( m_string , 20 , "%04i:%02i:%02i %02i:%02i:%02i" , nt.tm_year + 1900 , nt.tm_mon + 1 , nt.tm_mday , nt.tm_hour , nt.tm_min , nt.tm_sec );
	}

	CDateTime::CDateTime ()
	{
		actTime ();
	}
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// class CFlags
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////

namespace
{
	template < class E , typename _enumtype = std::underlying_type < E > ::type >
	class CFlag
	{
	public:
		typedef _enumtype enumtype ;
	//protected:
		enumtype m_value ;
	public:

		bool isFlagSet      ( E f )            const { return 0 != ( m_value & ( 1 << f ) ) ; }
		void setFlag        ( E f )                  { m_value |= ( 1 << f ) ; }
		void setOnly        ( E f )                  { m_value  = ( 1 << f ) ; }
		void clearFlag      ( E f )                  { m_value &= ~( 1 << f ) ; }
		void setFlag        ( E f , bool val )       { if ( val ) setFlag ( f ) ; else clearFlag ( f ) ; }
		void zeroAll        ( )                      { m_value = 0 ; }
		void setAll         ( )                      { m_value = ~ ( enumtype ( 0 ) ) ; }
		bool isAnySet       ( )                const { return 0 != m_value ; }
		int getNumSet       ( )                const { return getNumBitsSet ( m_value ) ; }
		//bool areAllFlagsSet ( CEFlag_Base < E , _enumtype > f ) const { return ( f.m_value & m_value ) == f.m_value ; }
		//bool isAnyFlagSet   ( CEFlag_Base < E , _enumtype > f ) const { return ( f.m_value & m_value ) != 0 ; }
		//void maskOut        ( CEFlag_Base < E , _enumtype > f )       { m_value &=  f.m_value ; }
		//void clearFlags     ( CEFlag_Base < E , _enumtype > f )       { m_value &= ~f.m_value ; }
		//void addFlags       ( CEFlag_Base < E , _enumtype > f )       { m_value |=  f.m_value ; }

		//bool areAllFlagsSet ( CEFlag_Base  f ) const { return ( f.m_value & m_value ) == f.m_value ; }
		//bool isAnyFlagSet   ( CEFlag_Base  f ) const { return ( f.m_value & m_value ) != 0 ; }
		//void maskOut        ( CEFlag_Base  f )       { m_value &=  f.m_value ; }
		//void clearFlags     ( CEFlag_Base  f )       { m_value &= ~f.m_value ; }
		//void addFlags       ( CEFlag_Base  f )       { m_value |=  f.m_value ; }

		//CEFlag_Base < E , _enumtype > getMaskedOut ( CEFlag_Base  f ) const { CEFlag_Base t = *this ; t.maskOut ( f ) ; return t ; }

		//CEFlag_Base < E , _enumtype > & operator  = ( E f )            { m_value  = f ; return *this ; }
		//CEFlag_Base < E , _enumtype > & operator |= ( E f )            { m_value |= f ; return *this ; }
		//CEFlag_Base < E , _enumtype > & operator &= ( E f )            { m_value &= f ; return *this ; }
		//CEFlag_Base < E , _enumtype > & operator  = ( CEFlag_Base < E , _enumtype > f ) { m_value  = f.m_value ; return *this ; }
		//CEFlag_Base < E , _enumtype > & operator |= ( CEFlag_Base < E , _enumtype > f ) { m_value |= f.m_value ; return *this ; }
		//CEFlag_Base < E , _enumtype > & operator &= ( CEFlag_Base < E , _enumtype > f ) { m_value &= f.m_value ; return *this ; }

		//explicit CEFlag_Base ( enumtype d ) : m_value ( d ) {}
		//enumtype getData() const { return m_value ; }
		//CEFlag_Base < E , _enumtype > ( E d ) : m_value ( d ) {}
		CFlag < E , _enumtype > () : m_value ( 0 ) {}

		//operator enumtype () const { return m_value ; }

		//static friend CFlag operator | ( CEFlag_Base l , E r ) { return CEFlag_Base ( l ) |= r ; }
		//static friend CEFlag_Base operator & ( CEFlag_Base l , E r ) { return CEFlag_Base ( l ) &= r ; }
		//static friend inline CEFlag_Base operator | ( E l , E r ) { return CEFlag_Base ( l ) | r ; }
		//static friend inline CEFlag_Base operator | ( E l , _enumtype r ) { return CEFlag_Base ( l ) | r ; }
		bool operator == ( CFlag < E , _enumtype > other ) const { return m_value == other.m_value ; }
		//static CEFlag_Base < E , _enumtype > getZero () { return enumtype ( 0 ) ; }
	};
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Header structs
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////


//#pragma pack(push,1)
#pragma pack(2)
namespace
{
	enum ETiffType
	{
		TT_Tiff ,
		TT_BigTiff ,
		TT_Unknown ,
	};

	enum EMarvin : WORD
	{
		Marvin32 = 42 ,
		Marvin64 = 43 ,
	};

	enum EBYTEORDER : WORD
	{
		byteorder_Intel = 0x4949 ,
		byteorder_Motorola = 0x4D4D ,
	};

	struct TIFF_HEADER
	{
	private:
		EBYTEORDER Byteorder ; // Byteorder: 0x4949=Intel, 0x4D4D=Motorola
		EMarvin Marvin ;	   // Tiff-Kennwert
		DWORD pIFD ;           // Pointer auf erstes IFD bei Tiff, ist 8 bei BigTiff
	public:
		TIFF_HEADER ()
			: Byteorder ( byteorder_Intel ) // intel Byteorder
			, Marvin ( Marvin32 )
			, pIFD ( sizeof ( *this ) )
		{}
		void setIntel ()    { Byteorder = byteorder_Intel; }
		void setMotorola () { Byteorder = byteorder_Motorola; }
		void setDefaultValues ()
		{
			Byteorder = byteorder_Intel ;
			Marvin = Marvin32 ;
			pIFD = sizeof ( *this ) ;
		}
		bool isValid () const ;
		DWORD getIFDOffset () const { return pIFD ; }
	};
	bool TIFF_HEADER::isValid () const
	{
		if ( byteorder_Intel != Byteorder ) return false ;
		if ( Marvin32        != Marvin    ) return false ;
		return true ;
	}

	enum FieldType : WORD
	{
		FT_ZERO = 0 ,
		FT_BYTE = 1 , //  8-bit unsigned integer.
		FT_ASCII = 2 , //  8-bit byte that contains a 7-bit ASCII code; the last byte must be NUL (binary zero).
		FT_SHORT = 3 , //  16-bit (2-byte) unsigned integer.
		FT_LONG = 4 , //  32-bit (4-byte) unsigned integer.
		FT_RATIONAL = 5 , // 
		FT_SBYTE = 6 , //  An 8-bit signed (twos-complement) integer.
		FT_UNDEFINED = 7 , //  An 8-bit byte that may contain anything, depending on the definition of the field.
		FT_SSHORT = 8 , //  A 16-bit (2-byte) signed (twos-complement) integer.
		FT_SLONG = 9 , //  A 32-bit (4-byte) signed (twos-complement) integer.
		FT_SRATIONAL = 10 , //  Two SLONG�s: the first represents the numerator of a fraction, the second the denominator.
		FT_FLOAT = 11 , //  Single precision (4-byte) IEEE format.
		FT_DOUBLE = 12 ,  // 
		TIFF_LONG8 = 16 , // being unsigned 8byte integer
		TIFF_SLONG8 = 17 , // being signed 8byte integer
		TIFF_IFD8 = 18 , // being a new unsigned 8byte IFD offset
	};
	static int getTypeSize ( FieldType ft )
	{
		const BYTE numbytes[] = 
		{
			0 , // not defined
			1 , // FT_BYTE      =  1, //  8-bit unsigned integer.
			1 , // FT_ASCII     =  2, //  8-bit byte that contains a 7-bit ASCII code; the last byte must be NUL (binary zero).
			2 , // FT_SHORT     =  3, //  16-bit (2-byte) unsigned integer.
			4 , // FT_LONG      =  4, //  32-bit (4-byte) unsigned integer.
			8 , // FT_RATIONAL  =  5, // 
			1 , // FT_SBYTE     =  6, //  An 8-bit signed (twos-complement) integer.
			1 , // FT_UNDEFINED =  7, //  An 8-bit byte that may contain anything, depending on the definition of the field.
			2 , // FT_SSHORT    =  8, //  A 16-bit (2-byte) signed (twos-complement) integer.
			4 , // FT_SLONG     =  9, //  A 32-bit (4-byte) signed (twos-complement) integer.
			8 , // FT_SRATIONAL = 10, //  Two SLONG�s: the first represents the numerator of a fraction, the second the denominator.
			4 , // FT_FLOAT     = 11, //  Single precision (4-byte) IEEE format.
			8 , // FT_DOUBLE    = 12  // 
			0 , // unknown = 13
			0 , // unknown = 14
			0 , // unknown = 15
			8 , // 16 : TIFF_LONG8   being unsigned 8byte integer
			8 , // 17 : TIFF_SLONG8  being signed 8byte integer
			8 , // 18 : TIFF_IFD8    being a new unsigned 8byte IFD offset
		};
		return numbytes [ ft ] ;
	}
	static DWORD getValueMask ( FieldType ft , DWORD count )
	{
		// sad it's necessary ...
		if ( count > 1 ) return 0xffffffff ;
		const DWORD Masks[] = 
		{
			0x00000000 , // not defined
			0x000000ff , // FT_BYTE      =  1, //  8-bit unsigned integer.
			0x000000ff , // FT_ASCII     =  2, //  8-bit byte that contains a 7-bit ASCII code; the last byte must be NUL (binary zero).
			0x0000ffff , // FT_SHORT     =  3, //  16-bit (2-byte) unsigned integer.
			0xffffffff , // FT_LONG      =  4, //  32-bit (4-byte) unsigned integer.
			0xffffffff , // FT_RATIONAL  =  5, // 
			0x000000ff , // FT_SBYTE     =  6, //  An 8-bit signed (twos-complement) integer.
			0x000000ff , // FT_UNDEFINED =  7, //  An 8-bit byte that may contain anything, depending on the definition of the field.
			0x0000ffff , // FT_SSHORT    =  8, //  A 16-bit (2-byte) signed (twos-complement) integer.
			0xffffffff , // FT_SLONG     =  9, //  A 32-bit (4-byte) signed (twos-complement) integer.
			0xffffffff , // FT_SRATIONAL = 10, //  Two SLONG�s: the first represents the numerator of a fraction, the second the denominator.
			0xffffffff , // FT_FLOAT     = 11, //  Single precision (4-byte) IEEE format.
			0xffffffff , // FT_DOUBLE    = 12  // 
			0 , // unknown = 13
			0 , // unknown = 14
			0 , // unknown = 15
			0xffffffff , // 16 : TIFF_LONG8   being unsigned 8byte integer
			0xffffffff , // 17 : TIFF_SLONG8  being signed 8byte integer
			0xffffffff , // 18 : TIFF_IFD8    being a new unsigned 8byte IFD offset
		} ;
		return Masks [ ft ] ;
	}
	enum TIFFTags : WORD
	{
		TIFFTAG_NewSubfileType = 254 , // FE LONG 1
		TIFFTAG_SubfileType = 255 , // FF SHORT 1
		TIFFTAG_ImageWidth = 256 , // 100 SHORT or LONG 1
		TIFFTAG_ImageLength = 257 , // 101 SHORT or LONG 1
		TIFFTAG_BitsPerSample = 258 , // 102 SHORT SamplesPerPixel
		TIFFTAG_Compression = 259 , // 103 SHORT 1
		TIFFTAG_PhotometricInterpretation = 262 , // 106 SHORT 1
		TIFFTAG_Threshholding = 263 , // 107 SHORT 1
		TIFFTAG_CellWidth = 264 , // 108 SHORT 1
		TIFFTAG_CellLength = 265 , // 109 SHORT 1
		TIFFTAG_FillOrder = 266 , // 10A SHORT 1
		TIFFTAG_DocumentName = 269 , // 10D ASCII
		TIFFTAG_ImageDescription = 270 , // 10E ASCII
		TIFFTAG_Make = 271 , // 10F ASCII
		TIFFTAG_Model = 272 , // 110 ASCII
		TIFFTAG_StripOffsets = 273 , // 111 SHORT or LONG StripsPerImage
		TIFFTAG_Orientation = 274 , // 112 SHORT 1
		TIFFTAG_SamplesPerPixel = 277 , // 115 SHORT 1
		TIFFTAG_RowsPerStrip = 278 , // 116 SHORT or LONG 1
		TIFFTAG_StripByteCounts = 279 , // 117 LONG or SHORT StripsPerImage
		TIFFTAG_MinSampleValue = 280 , // 118 SHORT SamplesPerPixel
		TIFFTAG_MaxSampleValue = 281 , // 119 SHORT SamplesPerPixel
		TIFFTAG_XResolution = 282 , // 11A RATIONAL 1
		TIFFTAG_YResolution = 283 , // 11B RATIONAL 1
		TIFFTAG_PlanarConfiguration = 284 , // 11C SHORT 1
		TIFFTAG_PageName = 285 , // 11D ASCII
		TIFFTAG_XPosition = 286 , // 11E RATIONAL
		TIFFTAG_YPosition = 287 , // 11F RATIONAL
		TIFFTAG_FreeOffsets = 288 , // 120 LONG
		TIFFTAG_FreeByteCounts = 289 , // 121 LONG
		TIFFTAG_GrayResponseUnit = 290 , // 122 SHORT 1
		TIFFTAG_GrayResponseCurve = 291 , // 123 SHORT 2**BitsPerSample
		TIFFTAG_T4Options = 292 , // 124 LONG 1
		TIFFTAG_T6Options = 293 , // 125 LONG 1
		TIFFTAG_ResolutionUnit = 296 , // 128 SHORT 1
		TIFFTAG_PageNumber = 297 , // 129 SHORT 2
		TIFFTAG_TransferFunction = 301 , // 12D SHORT {1 or SamplesPerPixel}* 2** BitsPerSample
		TIFFTAG_Software = 305 , // 131 ASCII
		TIFFTAG_DateTime = 306 , // 132 ASCII 20
		TIFFTAG_Artist = 315 , // 13B ASCII
		TIFFTAG_HostComputer = 316 , // 13C ASCII
		TIFFTAG_Predictor = 317 , // 13D SHORT 1
		TIFFTAG_WhitePoint = 318 , // 13E RATIONAL 2
		TIFFTAG_PrimaryChromaticities = 319 , // 13F RATIONAL 6
		TIFFTAG_ColorMap = 320 , // 140 SHORT 3 * (2**BitsPerSample)
		TIFFTAG_HalftoneHints = 321 , // 141 SHORT 2
		TIFFTAG_TileWidth = 322 , // 142 SHORT or LONG 1
		TIFFTAG_TileLength = 323 , // 143 SHORT or LONG 1
		TIFFTAG_TileOffsets = 324 , // 144 LONG TilesPerImage
		TIFFTAG_TileByteCounts = 325 , // 145 SHORT or LONG TilesPerImage
		TIFFTAG_InkSet = 332 , // 14C SHORT 1
		TIFFTAG_InkNames = 333 , // 14D ASCII total number of characters in all ink name strings, including zeros
		TIFFTAG_NumberOfInks = 334 , // 14E SHORT 1
		TIFFTAG_DotRange = 336 , // 150 BYTE or SHORT 2, or 2*NumberOfInks
		TIFFTAG_TargetPrinter = 337 , // 151 ASCII any
		TIFFTAG_ExtraSamples = 338 , // 152 BYTE number of extra components per pixel
		TIFFTAG_SampleFormat = 339 , // 153 SHORT SamplesPerPixel
		TIFFTAG_SMinSampleValue = 340 , // 154 Any SamplesPerPixel
		TIFFTAG_SMaxSampleValue = 341 , // 155 Any SamplesPerPixel
		TIFFTAG_TransferRange = 342 , // 156 SHORT 6
		TIFFTAG_JPEGProc = 512 , // 200 SHORT 1
		TIFFTAG_JPEGInterchangeFormat = 513 , // 201 LONG 1
		TIFFTAG_JPEGInterchangeFormatLngth = 514 , // 202 LONG 1
		TIFFTAG_JPEGRestartInterval = 515 , // 203 SHORT 1
		TIFFTAG_JPEGLosslessPredictors = 517 , // 205 SHORT SamplesPerPixel
		TIFFTAG_JPEGPointTransforms = 518 , // 206 SHORT SamplesPerPixel
		TIFFTAG_JPEGQTables = 519 , // 207 LONG SamplesPerPixel
		TIFFTAG_JPEGDCTables = 520 , // 208 LONG SamplesPerPixel
		TIFFTAG_JPEGACTables = 521 , // 209 LONG SamplesPerPixel
		TIFFTAG_YCbCrCoefficients = 529 , // 211 RATIONAL 3
		TIFFTAG_YCbCrSubSampling = 530 , // 212 SHORT 2
		TIFFTAG_YCbCrPositioning = 531 , // 213 SHORT 1
		TIFFTAG_ReferenceBlackWhite = 532 , // 214 LONG 2*SamplesPerPixel
		TIFFTAG_Copyright = 33432 , // 8298 ASCII Any


		TIFFTAG_ModelPixelScaleTag = 33550 , // (SoftDesk)
		TIFFTAG_ModelTransformationTag = 34264 , // (JPL Carto Group)
		TIFFTAG_ModelTiepointTag = 33922 , // (Intergraph)
		TIFFTAG_GeoKeyDirectoryTag = 34735 , // (SPOT)
		TIFFTAG_GeoDoubleParamsTag = 34736 , // (SPOT)
		TIFFTAG_GeoAsciiParamsTag = 34737 , // (SPOT)
		TIFFTAG_GDAL_NODATA = 42113 , // (Ascii)
	};
	class CTag
	{
		const WORD m_Value ;
	public:
		CTag ( TIFFTags tag ) : m_Value ( tag ) {}
		CTag ( TIFFTag_Attribute tag ) : m_Value ( tag ) {}
		operator TIFFTags () const { return ( TIFFTags ) m_Value ; }
	} ;
	enum TiffOrientation : WORD
	{
		to_TopDown  = 1 ,
		to_BottomUp = 4 ,
	};
	struct SIFDEntry
	{
		//static const int DoNotWriteDirectly = 1 ;
		union
		{
			DWORD TagType ;
			struct
			{
				TIFFTags  Tag   ;
				FieldType Type  ;
			};
		};
		DWORD     Count ;
		union
		{
			DWORD Value  ;
			float ValueF ;
		};
		DWORD getPos() const { return Value ; }
		DWORD getValue () const { return Value & getValueMask ( Type , Count ) ; }
		int  getValueNumBytes() const { return getTypeSize ( Type ) ; }
		int getDataSize () const { return Count * getValueNumBytes() ; }
		static int  getValueNumBytes ( FieldType type ) { return getTypeSize ( type ) ; }
		static bool isSimple ( FieldType type , DWORD count ) ;
		static int getDataSize ( FieldType type , DWORD count ) { return count * getValueNumBytes ( type ) ; }
		bool isSimple() const ;
		bool isValid () const { return TIFFTAG_NewSubfileType <= Tag ; }
		SIFDEntry() {}
		//SIFDEntry ( TIFFTags tag , FieldType type , DWORD count , DWORD value )
		//	: Tag   ( tag   )
		//	, Type  ( type  )
		//	, Count ( count )
		//	, Value ( value )
		//{}
		SIFDEntry ( CTag tag , FieldType type , DWORD count , DWORD value )
			: Tag   ( tag   )
			, Type  ( type  )
			, Count ( count )
			, Value ( value )
		{}
	};
	bool SIFDEntry::isSimple ( FieldType type , DWORD count )
	{
		if ( count != 1 ) return false ;
		return getValueNumBytes ( type ) <= 4 ;
	}

	bool SIFDEntry::isSimple() const
	{
		return isSimple ( Type , Count ) ;
	}
	struct SResolution
	{
		const int z , n ;
		SResolution() : z ( 960000 ) , n ( 10000 ) {}
		SResolution ( int dpi ) : z ( 10000 * dpi ) , n ( 10000 ) {}
		operator DWORD () const { return z ; }
	};
}

//#pragma pack(pop)
#pragma pack()

////////////////////////////////////////////////////////////////////////////////////////////////////////
// struct SAttribute
////////////////////////////////////////////////////////////////////////////////////////////////////////

namespace
{
	struct SAttribute
	{
		std::string Label ;
		std::string ValueS ;
		double ValueD ;
		int NumDecimales ;
		TIFFTags Tag ;
		enum : WORD
		{
			T_Float ,
			T_Ascii ,
		} Type ;
		static TIFFTags getTiffTag ( ETiffAttributeID id ) { return TIFFTags ( TT_StartNumber + id ) ; }
	};
}

////////////////////////////////////////////////////////////////////////////////////////////////////////
// struct CGreyBMP::SImpl
////////////////////////////////////////////////////////////////////////////////////////////////////////

namespace
{
	enum EStatus
	{
		st_Undefined = 0 ,
		st_Reader ,
		st_Writer ,
	};
}

struct CTiffGrey::SImpl
{
	DWORD wx , wy ;
	DWORD m_bpp ;
	TiffOrientation m_Orientation ;
	DWORD m_NumBytesPerPixel ;
	DWORD m_RowLength ;
	DWORD m_RowsPerStrip ;
	DWORD m_FileSize ;
	DWORD m_IfdStartPointer ;
	DWORD m_IfdEntryPointer ;
	DWORD m_NumIfdEntries ;
	DWORD m_DataPointer ;

	DWORD m_PointerWidth ;
	DWORD m_PointerHeight ;
	DWORD m_PointerBPP ;
	DWORD m_PointerOrientation ;
	DWORD m_PointerRowsPerStrip ;
	DWORD m_PointerImageByteCount ;
	DWORD m_PointerDateTime ;

	DWORD m_ImageStart ;
	DWORD m_ImageByteCount ;
	BYTE* m_Bits ;
	BYTE* m_FileImage ;
	EStatus m_Status ;

	// attributes


	// (this list is not up to date - 05/03/2021 Micha Gro�)
	//double m_FieldOfView     ;
	//double m_PixelSize       ;
	//double m_InputRange      ;
	//double m_SamplingRate    ;
	//double m_InputImpedance  ;
	//double m_MeasurementTime ;

	//int m_NumDecimals_FieldOfView     ;
	//int m_NumDecimals_PixelSize       ;
	//int m_NumDecimals_InputRange      ;
	//int m_NumDecimals_SamplingRate    ;
	//int m_NumDecimals_InputImpedance  ;
	//int m_NumDecimals_MeasurementTime ;

	bool m_AttributesChanged ;
	std::string m_Description ;
	std::string m_MeasurementMode ;
	SAttribute m_Attributes [ ID_NumDefinitions ] ;
	CFlag < ETiffAttributeID > m_AttFlags ;

	CFtoA FtoA ;
	CDateTime DateTime ;

	~SImpl () ;
	SImpl ( int x , int y ) ;
	SImpl ( const char* filename ) ;
	// common methods
	void clear () ;
	void releaseImage () ;
	bool isValid () const { return st_Undefined != m_Status ; }
	// reader methods
	bool init ( FILE *pf ) ;
	DWORD getNumIfdEntries () const { return getFieldInImage < WORD > ( m_IfdStartPointer ) ; }
	const SIFDEntry* findIfdEntry ( TIFFTags tag ) const ;
	const BYTE* getLine ( int line ) const ;
	bool getData ( void* buffer ) const ;
	ETiffAttributeType hasAttribute ( ETiffAttributeID id ) const ; // T_Unknown if not available
	double getAttributeValue ( ETiffAttributeID id ) const ; // 0 if doesn't match
	const char* getAttributeString ( ETiffAttributeID id ) const ; // 0 if doesn't match
	const char* getImageDescription () const ; // 0 if not available
	template < class T > T getFieldInImage ( DWORD offset ) const { return * ( T* ) ( m_FileImage + offset ) ; }
	template < class T > const T* getArrayInImage ( DWORD offset ) const { return ( T* ) ( m_FileImage + offset ) ; }
	// writer methods
	//bool init () ;
	bool createHeader () ;
	bool createHeaderPass () ;
	void setBitmapSize ( int width , int height ) ;
	void setBPP ( int bpp ) ;
	void setOrientation ( TiffOrientation o ) ;
	void setAttribute ( ETiffAttributeID id , const char* label , const char* value ) ;
	//void setAttribute ( ETiffAttributes attribute , int value , int numdecimals ) ;
	void setAttribute ( ETiffAttributeID id , const char* label , double value , int numdecimals ) ;
	void deleteAttributes () ; // deletes all attributes and description string

	DWORD addToImage ( const void* data , DWORD offset , DWORD size ) ; // returns offset + size
	template < class T > DWORD addToImage ( const T& object , DWORD offset ) { return addToImage ( &object , offset , sizeof ( object ) ) ; }
	DWORD addHeader ( DWORD offset ) ; // returns offset after Header, i.e. start of IFD
	DWORD addIFD ( DWORD offset ) ; // returns offset to first IFD entry
	DWORD addIfdEntry ( CTag tag , FieldType type , DWORD count , DWORD value ) ; // returns offset to next IFD entry
	DWORD addIfdEntry ( CTag tag , FieldType type , DWORD count , const void* data ) ;
	template < class T >
	DWORD addIfdEntry ( CTag tag , FieldType type , DWORD count , const T& value )
	{
		if ( SIFDEntry::isSimple ( type , count ) )
		{
			return addIfdEntry ( tag , type , count , ( DWORD ) value ) ;
		}
		else
		{
			return addIfdEntry ( tag , type , count , ( const void* ) &value ) ;
		}
	}
	DWORD addIfdEntry ( CTag tag , const std::string& str )
	{
		return addIfdEntry ( tag , FT_ASCII , ( DWORD ) str.length () + 1 , ( const void* ) str.c_str () ) ;
	}
	DWORD createDescriptionString () ;
	DWORD addDescription () ;
	DWORD addAttributes () ;
	void align ( DWORD& offset ) { if ( offset & 1 ) ++offset ; }
	static DWORD getAligned ( DWORD offset ) { return offset + ( offset & 1 ) ; }
	template < class T > T& updateImageField ( DWORD offset ) { return * ( T* ) ( m_FileImage + offset ) ; }
	bool save ( const void* buffer , const char* filename ) ;
	bool save ( const void* buffer , FILE *pf ) const ;
	static DWORD clacPixelImageSize ( int wx , int wy , int bpp ) ;
};

CTiffGrey::SImpl::~SImpl () { if ( m_FileImage ) free ( m_FileImage ) ; }

void CTiffGrey::SImpl::releaseImage ()
{
	if ( m_FileImage ) free ( m_FileImage ) ;
	m_FileImage = 0 ;
}

void CTiffGrey::SImpl::clear ()
{
	releaseImage () ;
	std::memset ( this , 0 , offsetof ( CTiffGrey::SImpl , m_Description ) ) ;
	m_Status = st_Undefined ;
	m_AttFlags.zeroAll () ;
	m_Description.clear () ;
}

// reader methods

CTiffGrey::SImpl::SImpl ( const char* filename )
{
	std::memset ( this , 0 , offsetof ( CTiffGrey::SImpl , m_Description ) ) ;
	//std::memset ( this , 0 , sizeof ( *this ) ) ;
	//m_Status = st_Undefined ;
	clear () ;
	FILE *pf ;
	if ( fopen_s ( &pf , filename , "rb" ) ) return ;
	bool sucess = init ( pf ) ;
	fclose ( pf ) ;
	m_Status = st_Reader ;
	if ( !sucess ) clear () ;
}

bool CTiffGrey::SImpl::init ( FILE *pf )
{
	if ( fseek ( pf , 0 , SEEK_END ) ) return false ;
	m_FileSize = ftell ( pf ) ;
	// check it is too large cause we'll load complete file into memory
	if ( m_FileSize > 5000000ul ) return false ;
	// load file into memory
	m_FileImage = ( BYTE* ) malloc ( m_FileSize ) ;
	if ( fseek ( pf , 0 , SEEK_SET ) ) return false ;
	if ( m_FileSize != fread ( m_FileImage , 1 , m_FileSize , pf ) ) return false ;
	// check header
	const TIFF_HEADER* Header = ( TIFF_HEADER* ) m_FileImage ;
	if ( !Header->isValid () ) return false ;
	m_IfdStartPointer = Header->getIFDOffset () ;
	m_IfdEntryPointer = m_IfdStartPointer + 2 ;
	if ( m_IfdStartPointer > m_FileSize ) return false ;
	m_NumIfdEntries = getNumIfdEntries () ;
	// set defaults
	m_Orientation = TiffOrientation::to_TopDown ;
	DWORD Comp = 1 ;
	DWORD Spp = 1 ;
	// check IFD
	const SIFDEntry* e ;
	e = findIfdEntry ( TIFFTAG_ImageWidth      ) ; if ( e ) wx               = e->getValue () ; else return false ; 
	e = findIfdEntry ( TIFFTAG_ImageLength     ) ; if ( e ) wy               = e->getValue () ; else return false ; 
	e = findIfdEntry ( TIFFTAG_BitsPerSample   ) ; if ( e ) m_bpp            = e->getValue () ; else return false ; 
	e = findIfdEntry ( TIFFTAG_Compression     ) ; if ( e ) Comp             = e->getValue () ; else return false ;
	e = findIfdEntry ( TIFFTAG_StripOffsets    ) ; if ( e ) m_ImageStart     = e->getValue () ; else return false ; 
	e = findIfdEntry ( TIFFTAG_Orientation     ) ; if ( e ) m_Orientation    = ( TiffOrientation ) e->getValue () ;
	e = findIfdEntry ( TIFFTAG_SamplesPerPixel ) ; if ( e ) Spp              = e->getValue () ;
	e = findIfdEntry ( TIFFTAG_RowsPerStrip    ) ; if ( e ) m_RowsPerStrip   = e->getValue () ; else return false ;
	e = findIfdEntry ( TIFFTAG_StripByteCounts ) ; if ( e ) m_ImageByteCount = e->getValue () ; else return false ;
	// unwanted tags
	if ( findIfdEntry ( TIFFTAG_TileWidth      ) ) return false ; // cannot handle tiled images
	// check values
	if ( Comp != 1 ) return false ; // cannot handle compression
	if ( Spp  != 1 ) return false ; // cannot handle colors
	// calculate other values
	if ( ( m_bpp != 8 ) && ( m_bpp != 16 ) ) return false ; // can handle only 8bit or 16bit grey
	m_NumBytesPerPixel = m_bpp / 8 ;
	m_RowLength = m_NumBytesPerPixel * wx ;
	if ( m_RowsPerStrip >= wy )
	{
		// single strip
		m_Bits = m_FileImage + m_ImageStart;
		if ( m_ImageByteCount != m_RowLength * wy ) return false ; // must be the same if no compression
	}
	else
	{
		// multiple strips
		m_ImageByteCount = m_RowLength = wy ;
	}
	return true ;
}

const SIFDEntry* CTiffGrey::SImpl::findIfdEntry ( TIFFTags tag ) const
{
	const SIFDEntry* Entries = ( const SIFDEntry* ) ( m_FileImage + m_IfdEntryPointer ) ;
	for ( DWORD i = 0 ; i < m_NumIfdEntries; i++ )
	{
		if ( Entries[i] .Tag == tag ) return &Entries[i] ;
	}
	return 0 ;
}

const BYTE* CTiffGrey::SImpl::getLine ( int line ) const
{
	if ( m_RowsPerStrip >= wy ) return m_Bits + line * m_RowLength ; // all data is in one strip
	// stripped tiff, so m_ImageStart is start of strip offsets
	DWORD StripNr = line / m_RowsPerStrip ;
	DWORD StripOffset = getArrayInImage < DWORD > ( m_ImageStart ) [ StripNr ] ;
	DWORD NumLineInStrip = line - m_RowsPerStrip * StripNr ;
	DWORD LineOffset = StripOffset + NumLineInStrip * m_RowLength ;
	return m_FileImage + LineOffset ;
}

template < class T > static T getMin ( T v1 , T v2 ) { return ( v1 < v2 ? v1 : v2 ) ; }

bool CTiffGrey::SImpl::getData ( void* buffer ) const
{
	if ( st_Reader != m_Status ) return false ;
	if ( m_RowsPerStrip >= wy )
	{
		// all data is in one strip
		memcpy ( buffer , m_Bits , m_ImageByteCount );
		return true ;
	}
	// stripped image, so we have to go through strips
	BYTE* BuffOut = ( BYTE* ) buffer ;
	const DWORD* StripOffsets = getArrayInImage < DWORD > ( m_ImageStart ) ; // m_ImageStart is start of strip offsets
	DWORD NumRowsLeft = wy ;
	do
	{
		DWORD StripOffset = *StripOffsets++ ;
		DWORD NumRowsToCopy = getMin ( m_RowsPerStrip , NumRowsLeft ) ;
		DWORD NumBytesToCopy = NumRowsToCopy * m_RowLength ;
		memcpy ( BuffOut , m_FileImage + StripOffset , NumBytesToCopy ) ;
		BuffOut += NumBytesToCopy ;
		NumRowsLeft -= NumRowsToCopy ;
	} while ( NumRowsLeft ) ;
	return true ;
}

ETiffAttributeType CTiffGrey::SImpl::hasAttribute ( ETiffAttributeID id ) const
{
	if ( st_Reader != m_Status ) return T_Unknown ;
	const SIFDEntry* e = findIfdEntry ( SAttribute::getTiffTag ( id ) ) ;
	if ( !e ) return T_Unknown ;
	if ( e->Type == FT_DOUBLE ) return T_Double ;
	if ( e->Type == FT_ASCII ) return T_Ascii ;
	return T_Unknown ;
}

double CTiffGrey::SImpl::getAttributeValue ( ETiffAttributeID id ) const
{
	if ( st_Reader != m_Status ) return 0 ;
	const SIFDEntry* e = findIfdEntry ( SAttribute::getTiffTag ( id ) ) ;
	if ( !e ) return 0 ;
	if ( e->Type != FT_DOUBLE ) return 0 ;
	if ( e->Count != 1 ) return 0 ;
	return getFieldInImage < double > ( e->Value ) ;
}

const char* CTiffGrey::SImpl::getAttributeString ( ETiffAttributeID id ) const
{
	if ( st_Reader != m_Status ) return 0 ;
	const SIFDEntry* e = findIfdEntry ( SAttribute::getTiffTag ( id ) ) ;
	if ( !e ) return 0 ;
	if ( e->Type != FT_ASCII ) return 0 ;
	if ( e->Count < 1 ) return 0 ;
	return getArrayInImage < char > ( e->Value ) ;
}
const char* CTiffGrey::SImpl::getImageDescription () const
{
	if ( st_Reader != m_Status ) return 0 ;
	const SIFDEntry* e = findIfdEntry ( TIFFTAG_ImageDescription ) ;
	if ( !e ) return 0 ;
	if ( e->Type != FT_ASCII ) return 0 ;
	if ( e->Count < 1 ) return 0 ;
	return getArrayInImage < char > ( e->Value ) ;
}

// writer methods

CTiffGrey::SImpl::SImpl ( int x , int y )
{
	std::memset ( this , 0 , offsetof ( CTiffGrey::SImpl , m_Description ) ) ;
	clear () ;
	wx = x ;
	wy = y ;
	m_bpp = 16 ;
	m_Orientation = TiffOrientation::to_TopDown ;
	m_NumBytesPerPixel = m_bpp / 8 ;
	m_RowLength = m_NumBytesPerPixel * x ;
	m_ImageByteCount = m_RowLength * y ;
	m_Status = st_Writer ;
	createHeader () ;
}

bool CTiffGrey::SImpl::createHeader ()
{
	if ( m_Status != st_Writer ) return false ;
	releaseImage () ;
	createDescriptionString () ; // if attributes changed
	m_DataPointer = 0 ; // after first pass m_DataPointer will hold data size
	createHeaderPass () ; // first init for getting tiff pointers
	DWORD DataSize = m_DataPointer ;
	m_DataPointer = getAligned ( m_IfdEntryPointer + 4 ) ; // 4 bytes zero after ifd
	m_ImageStart = getAligned ( m_DataPointer + DataSize ) ;
	//m_FileSize = m_ImageStart + m_ImageByteCount ;
	m_FileSize = m_ImageStart ;
	m_FileImage = ( BYTE* ) malloc ( m_FileSize ) ;
	memset ( m_FileImage , 0 , m_FileSize ) ;
	m_Bits = m_FileImage + m_ImageStart ;
	createHeaderPass () ; // second init for setup tiff image
	return true ;
}

bool CTiffGrey::SImpl::createHeaderPass ()
{
	m_IfdStartPointer = addHeader ( 0 ) ;
	m_IfdEntryPointer = addIFD ( m_IfdStartPointer ) ;

	m_PointerWidth          = addIfdEntry ( TIFFTAG_ImageWidth                , FT_LONG     ,  1 , wx                 ) ;
	m_PointerHeight         = addIfdEntry ( TIFFTAG_ImageLength               , FT_LONG     ,  1 , wy                 ) ;
	m_PointerBPP            = addIfdEntry ( TIFFTAG_BitsPerSample             , FT_SHORT    ,  1 , m_bpp              ) ; // bits per pixel, 8 or 16
	                          addIfdEntry ( TIFFTAG_Compression               , FT_SHORT    ,  1 , 1ul                ) ; // uncompressed
	                          addIfdEntry ( TIFFTAG_PhotometricInterpretation , FT_SHORT    ,  1 , 1ul                ) ; // 0 is black
							  addDescription () ; // if any
	                          addIfdEntry ( TIFFTAG_StripOffsets              , FT_LONG     ,  1 , m_ImageStart       ) ; // start of pixel data
	m_PointerOrientation    = addIfdEntry ( TIFFTAG_Orientation               , FT_SHORT    ,  1 , m_Orientation      ) ; // top-down or bottom-up
	m_PointerRowsPerStrip   = addIfdEntry ( TIFFTAG_RowsPerStrip              , FT_LONG     ,  1 , wy                 ) ; // single strip
	m_PointerImageByteCount = addIfdEntry ( TIFFTAG_StripByteCounts           , FT_LONG     ,  1 , m_ImageByteCount   ) ; // size of pixel data in bytes
	                          addIfdEntry ( TIFFTAG_XResolution               , FT_RATIONAL ,  1 , SResolution ( 96 ) ) ; // 96 dpi
	                          addIfdEntry ( TIFFTAG_YResolution               , FT_RATIONAL ,  1 , SResolution ( 96 ) ) ;
	                          addIfdEntry ( TIFFTAG_ResolutionUnit            , FT_SHORT    ,  1 , 2ul                ) ; // inch
	m_PointerDateTime       = addIfdEntry ( TIFFTAG_DateTime                  , FT_ASCII    , 20 , ( const void* ) DateTime.getString() ) ;
							  addAttributes () ; // if any
	return true ;
}

void CTiffGrey::SImpl::setBitmapSize ( int width , int height )
{
	if ( st_Writer != m_Status ) return ;
	wx = width ;
	wy = height ;
	m_RowLength = m_NumBytesPerPixel * wx ;
	m_ImageByteCount = m_RowLength * wy ;
	if ( !m_FileImage ) return ;
	updateImageField < DWORD > ( m_PointerWidth          ) = width  ;
	updateImageField < DWORD > ( m_PointerHeight         ) = height ;
	updateImageField < DWORD > ( m_PointerRowsPerStrip   ) = height ;
	updateImageField < DWORD > ( m_PointerImageByteCount ) = m_ImageByteCount ;
}

void CTiffGrey::SImpl::setBPP ( int bpp )
{
	if ( st_Writer != m_Status ) return ;
	m_bpp = bpp ;
	m_NumBytesPerPixel = m_bpp / 8 ;
	m_RowLength = m_NumBytesPerPixel * wx ;
	m_ImageByteCount = m_RowLength * wy ;
	if ( !m_FileImage ) return ;
	updateImageField < DWORD > ( m_PointerBPP            ) = bpp              ;
	updateImageField < DWORD > ( m_PointerImageByteCount ) = m_ImageByteCount ;
}

void CTiffGrey::SImpl::setOrientation ( TiffOrientation o )
{
	if ( st_Writer != m_Status ) return ;
	if ( !m_FileImage ) return ;
	updateImageField < DWORD > ( m_PointerOrientation ) = o ;
}

DWORD CTiffGrey::SImpl::addToImage ( const void* data , DWORD offset , DWORD size )
{
	// adds data to image, starting at offset
	// returns offset to first byte after stored data
	if ( m_FileImage ) memcpy ( m_FileImage + offset , data , size ) ;
	return offset + size ;
}

void CTiffGrey::SImpl::setAttribute ( ETiffAttributeID id , const char* label , const char* value )
{
	if ( st_Writer != m_Status ) return ;
	m_AttributesChanged = true ;
	SAttribute& Attribute = m_Attributes [ id ] ;
	Attribute.Label = label ;
	Attribute.ValueS = value ;
	Attribute.Type = SAttribute::T_Ascii ;
	Attribute.Tag = SAttribute::getTiffTag ( id ) ;
	m_AttFlags.setFlag ( id ) ;
}

void CTiffGrey::SImpl::setAttribute ( ETiffAttributeID id , const char* label , double value , int numdecimals )
{
	if ( st_Writer != m_Status ) return ;
	m_AttributesChanged = true ;
	SAttribute& Attribute = m_Attributes [ id ] ;
	Attribute.Label = label ;
	Attribute.ValueD = value ;
	Attribute.NumDecimales = numdecimals ;
	Attribute.Type = SAttribute::T_Float ;
	Attribute.Tag = SAttribute::getTiffTag ( id ) ;
	m_AttFlags.setFlag ( id ) ;
}
void CTiffGrey::SImpl::deleteAttributes ()
{
	m_AttFlags.zeroAll () ;
	for ( WORD i = 0 ; i < ID_NumDefinitions ; i++ )
	{
		if ( m_AttFlags.isFlagSet ( ( ETiffAttributeID ) i ) )
		{
			m_Attributes [ i ] . ValueS.clear() ;
		}
	}
	m_Description.clear () ;
	m_AttributesChanged = true ;
}
DWORD CTiffGrey::SImpl::addHeader ( DWORD offset )
{
	TIFF_HEADER Header ;
	return addToImage ( Header , offset ) ;
}

DWORD CTiffGrey::SImpl::addIFD ( DWORD offset )
{
	WORD NumEntries = 0 ;
	return addToImage ( NumEntries , offset ) ;
}

DWORD CTiffGrey::SImpl::addIfdEntry ( CTag tag , FieldType type , DWORD count , DWORD value )
{
	DWORD Result = m_IfdEntryPointer + 8 ;
	SIFDEntry Entry ( tag , type , count , value ) ;
	m_IfdEntryPointer = addToImage ( Entry , m_IfdEntryPointer ) ;
	if ( m_FileImage ) updateImageField < WORD > ( m_IfdStartPointer ) ++ ;
	return Result ;
}

DWORD CTiffGrey::SImpl::addIfdEntry ( CTag tag , FieldType type , DWORD count , const void* data )
{
	const int DataSize = SIFDEntry::getDataSize ( type , count ) ;
	if ( DataSize <= 4 )
	{
		DWORD Value = 0 ;
		memcpy ( &Value , data , DataSize ) ;
		return addIfdEntry ( tag , type , count , Value ) ;
	}
	else
	{
		SIFDEntry Entry ( tag , type , count , m_DataPointer );
		//DWORD Result = m_IfdEntryPointer + 8;
		DWORD Result = m_DataPointer ;
		m_IfdEntryPointer = addToImage ( Entry , m_IfdEntryPointer );
		if ( m_FileImage ) updateImageField < WORD > ( m_IfdStartPointer )++;
		m_DataPointer = addToImage ( data , m_DataPointer , Entry.getDataSize () );
		align ( m_DataPointer );
		return Result;
	}
}

DWORD CTiffGrey::SImpl::addDescription ()
{
	if ( m_Description.empty () ) return 0 ;
	return addIfdEntry ( TIFFTAG_ImageDescription , m_Description ) ;
}

DWORD CTiffGrey::SImpl::addAttributes ()
{
	if ( !m_AttFlags.isAnySet () ) return 0 ;
	DWORD Result = 0 ;

	for ( WORD i = 0 ; i < ID_NumDefinitions ; i++ )
	{
		if ( m_AttFlags.isFlagSet ( ( ETiffAttributeID ) i ) )
		{
			const SAttribute& Attribute = m_Attributes [ i ] ;
			if ( Attribute.Type == SAttribute::T_Ascii ) Result = addIfdEntry ( Attribute.Tag , Attribute.ValueS ) ;
			else if ( Attribute.Type == SAttribute::T_Float ) Result = addIfdEntry ( Attribute.Tag , FT_DOUBLE , 1 , Attribute.ValueD ) ;
		}
	}
	return Result ;
}

DWORD CTiffGrey::SImpl::createDescriptionString ()
{
	if ( !m_AttributesChanged  ) return ( DWORD ) m_Description.length() + 1 ;
	m_Description.clear () ;
	if ( !m_AttFlags.isAnySet () ) return 0 ;
	for ( WORD i = 0 ; i < ID_NumDefinitions ; i++ )
	{
		if ( m_AttFlags.isFlagSet ( ( ETiffAttributeID ) i ) )
		{
			const SAttribute& Attribute = m_Attributes [ i ] ;
			m_Description += '\n' ;
			m_Description += Attribute.Label ;
			m_Description += " : " ;
			if ( Attribute.Type == SAttribute::T_Ascii ) m_Description += Attribute.ValueS ;
			else if ( Attribute.Type == SAttribute::T_Float ) m_Description += FtoA.ftoa ( Attribute.ValueD , Attribute.NumDecimales ) ;
		}
	}
	m_Description += '\n' ;
	m_AttributesChanged = false ;
	return ( DWORD ) m_Description.length() + 1 ;
}

bool CTiffGrey::SImpl::save ( const void* buffer , const char* filename )
{
	if ( st_Writer != m_Status ) return false ;
	if ( m_AttributesChanged ) createHeader () ;
	// update time stamp
	DateTime.actTime () ;
	addToImage ( DateTime.getString() , m_PointerDateTime , 20 ) ;
	FILE *pf ;
	if ( fopen_s ( &pf , filename , "wb" ) ) return false ;
	bool sucess = save ( buffer , pf ) ;
	fclose ( pf ) ;
	return sucess ;
}

bool CTiffGrey::SImpl::save ( const void* buffer , FILE *pf ) const
{
	if ( m_FileSize       != fwrite ( m_FileImage , 1 , m_FileSize       , pf ) ) return false ;
	if ( m_ImageByteCount != fwrite ( buffer      , 1 , m_ImageByteCount , pf ) ) return false ;
	return true ;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////
// class CGreyBMP
////////////////////////////////////////////////////////////////////////////////////////////////////////

CTiffGrey::~CTiffGrey () { delete m_pImpl ; }
CTiffGrey::CTiffGrey ( const char* filename ) : m_pImpl ( new SImpl ( filename ) ) {} // reader
CTiffGrey::CTiffGrey ( int width , int height ) : m_pImpl ( new SImpl ( width , height ) ) {} // writer
bool CTiffGrey::isValid () const { return m_pImpl->isValid () ; }
int CTiffGrey::getBPP () const { return m_pImpl->m_bpp ; }
int CTiffGrey::getNumBytesPerPixel () const { return m_pImpl->m_NumBytesPerPixel ; }
int CTiffGrey::getNumPixelsX () const { return m_pImpl->wx ; }
int CTiffGrey::getNumPixelsY () const { return m_pImpl->wy ; }
int CTiffGrey::getBufferSize () const { return m_pImpl->wx * m_pImpl->wy * getNumBytesPerPixel() ; }
bool CTiffGrey::getData ( void* buffer ) const { return m_pImpl->getData ( buffer ) ; }

double CTiffGrey::getAttributeValue ( ETiffAttributeID id ) const { return m_pImpl->getAttributeValue ( id ) ; }
const char* CTiffGrey::getAttributeString ( ETiffAttributeID id ) const { return m_pImpl->getAttributeString ( id ) ; }
const char* CTiffGrey::getImageDescription () const { return m_pImpl->getImageDescription () ; }


void CTiffGrey::setBitmapSize ( int width , int height ) { m_pImpl->setBitmapSize ( width , height ) ; }
void CTiffGrey::setTopDown ( bool td ) { m_pImpl->setOrientation ( ( td ? to_TopDown : to_BottomUp ) ) ; }
void CTiffGrey::setBPP ( int bpp ) { m_pImpl->setBPP ( bpp ) ; }
void CTiffGrey::setAttribute ( ETiffAttributeID id , const char* label , const char* value ) { m_pImpl->setAttribute ( id , label , value ) ; }
void CTiffGrey::setAttribute ( ETiffAttributeID id , const char* label , double value , int numdecimals ) { m_pImpl->setAttribute ( id , label , value , numdecimals ) ; }
void CTiffGrey::deleteAttributes () { m_pImpl->deleteAttributes() ; }
bool CTiffGrey::saveData ( const void* buffer , const char* filename ) { return m_pImpl->save ( buffer , filename ) ; }


////////////////////////////////////////////////////////////////////////////////////////////////////////
// simple test
////////////////////////////////////////////////////////////////////////////////////////////////////////


;/* comment this line out for activating test

#include "PerformTests.h"
#include "..//Timer.h"
#include "AllocatedString.h"

//static bool doTest ()
TEST_FUNCTION_BEGIN
{
	//{
	//	const int NumPixels = 512 * 512;
	//	WORD* Data = new WORD [ NumPixels ];
	//	for ( int i = 0; i < NumPixels; i++ ) Data [ i ] = ( WORD ) i;
	//	TreesVisLib::CFilenameString Str;
	//	CTiffGrey Tiff ( 512 , 512 );
	//	{
	//		CAutoTimer Timer ( "WritingTiffs" );
	//		for ( int i = 0; i < 1000; i++ )
	//		{
	//			//CTiffGrey Tiff ( 512 , 512 );
	//			Str.printf ( "C://JannisTiffTest//File_%i.tif" , i );
	//			Tiff.saveData ( Data , Str.c_str () );
	//		}
	//	}
	//}

	const char* filename = "H://TestTif.tif" ;
	{
		CTiffGrey TestTiff ( 3 , 4 ) ;
		const WORD TestData1a [] = 
		{
			0x1111 , 0x1222 , 0x1444 ,
			0x2111 , 0x2222 , 0x2444 ,
			0x3111 , 0x3222 , 0x3444 ,
			0x4111 , 0x4222 , 0x4444 ,
		} ;
		const WORD TestData1b [] = 
		{
			10000 , 11000 , 12000 ,
			40000 , 41000 , 42000 ,
			20000 , 21000 , 22000 ,
			30000 , 31000 , 32000 ,
		} ;
		const WORD TestData1c [] = 
		{
			10000 , 11000 , 12000 ,
			20000 , 21000 , 22000 ,
			40000 , 41000 , 42000 ,
			30000 , 31000 , 32000 ,
		} ;
		TestTiff.saveData ( TestData1a , "H://TestTif1a.tif" ) ;
		TestTiff.saveData ( TestData1b , "H://TestTif1b.tif" ) ;
		TestTiff.saveData ( TestData1c , "H://TestTif1c.tif" ) ;

		const BYTE TestData2 [] =
		{
			50  , 55  , 60  ,
			100 , 105 , 110 ,
			150 , 155 , 160 ,
			200 , 205 , 210
		} ;
		TestTiff.setBPP ( 8 ) ;
		TestTiff.setTopDown ( false ) ;
		TestTiff.saveData ( TestData2 , "H://TestTif2.tif" ) ;

		const WORD TestData3 [] = 
		{
			10000 , 11000 , 12000 , 13000 , 14000 ,
			20000 , 21000 , 22000 , 23000 , 24000 ,
			30000 , 31000 , 32000 , 33000 , 34000 ,
			40000 , 41000 , 42000 , 43000 , 44000 ,
			50000 , 51000 , 52000 , 53000 , 54000 ,
			60000 , 61000 , 62000 , 63000 , 64000 ,
		} ;
		TestTiff.setTopDown ( true ) ;
		TestTiff.setBitmapSize ( 5 , 6 ) ;
		TestTiff.setBPP ( 16 ) ;
		TestTiff.saveData ( TestData3 , "H://TestTif3.tif" ) ;

		// add some attributes
		TestTiff.setAttribute ( ID_PixelSize , "Pixel Size [�m]" , 45 ) ;
		TestTiff.setAttribute ( ID_SamplingRate , "Sampling Rate [Hz]" , 20000.12345 ) ;
		TestTiff.saveData ( TestData3 , "H://TestTif4.tif" ) ;
		TestTiff.saveData ( TestData3 , "H://TestTif5.tif" ) ;

		// delete attributes
		TestTiff.deleteAttributes () ;
		TestTiff.saveData ( TestData3 , "H://TestTif6.tif" ) ;
	}
	{
		CTiffGrey Tiff ( "H://TestTif6.tif" ) ;
		WORD TestData4 [ 30 ] ;
		Tiff.getData (TestData4) ;
		printf ( "%i x %i Pixels, %i bit\n" , Tiff.getNumPixelsX () , Tiff.getNumPixelsY () , Tiff.getBPP() ) ;
		const char* desc = Tiff.getImageDescription () ;
		if ( desc ) printf ( "Description:\n%s\n" , desc ) ;
		printf ("TA_PixelSize    = %f\n" , Tiff.getAttributeValue ( ID_PixelSize    ) ) ;
		printf ("TA_SamplingRate = %f\n" , Tiff.getAttributeValue ( ID_SamplingRate ) ) ;
		printf ("TA_FieldOfView  = %f\n" , Tiff.getAttributeValue ( ID_FieldOfView  ) ) ;
	}

	//{
	//	CTiffGrey Tiff ( "H:\\Holgi\\Daten\\Mooswald_2002\\dsm_mw_felis.tif" ) ;
	//	printf ( "%i x %i Pixels, %i bit\n" , Tiff.getNumPixelsX () , Tiff.getNumPixelsY () , Tiff.getBPP() ) ;
	//}
	//{
	//	WORD* Data = new WORD [ 65536 ] ;
	//	for ( int i = 0; i < 65536; i++ ) Data [i] = ( WORD ) i ;
	//	CTiffGrey Tiff ( 256 , 256 ) ;
	//	//Tiff.setBPP ( 8 ) ;
	//	Tiff.saveData ( Data , "H://TestTif4.tif" ) ;
	//	delete Data ;
	//}
	//{
	//	BYTE* Data = new BYTE [ 65536 ] ;
	//	CTiffGrey Tiff ( "H://TestTif4_str.tif" ) ;
	//	Tiff.getData ( Data ) ;
	//	delete Data ;

	//}
	//return true ;
}
TEST_FUNCTION_END

//static const bool bTest = doTest () ;

// */

//#endif
