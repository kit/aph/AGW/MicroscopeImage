#pragma once

#define int16   short int

#include "gnuplot-iostream.h"

class plotter
{
public:
	plotter();
	plotter(const char* gnuPath);
	plotter(const char* gnuPath, int dataSize);

	~plotter();

	bool set_dataSize(int dataSize);

	// do something functions
	//bool do_reset();
	//bool do_clearData();

	bool do_loadData(int* dataPtr, int dataSize);
	bool do_loadData(int16* dataPtr, int dataSize);
	//bool do_updateData(int* dataPtr);
	bool do_updateData(int16* dataPtr);
	bool do_updateData(double* dataPtr);
	bool do_updateData(std::vector<float>& inpDataVec);
	bool do_showData();
	bool do_showLegend(bool showL);

	bool do_setPlotRanges(const int xMin, const int xMax, const int yMin, const int yMax);
	bool do_setPlotXrange(const int xMin, const int xMax);
	bool do_setPlotYrange(const int yMin, const int yMax);
	bool do_setTitle(const std::string& plotTitle);

	// Direct channel to gnuplot console! Commands must be terminated with newline!
	bool do_sendCMD(const std::string& command);


private:

	//Gnuplot mygp; // Only usefull if I wanted to use initializer lists and 
	// hard code the path of gnuplot on the system
	std::unique_ptr<Gnuplot> ptr_gp = NULL;
	const double adc_pi = 3.14159265359;
	std::string xRange, yRange;
	std::vector<double> dataVec;

};

