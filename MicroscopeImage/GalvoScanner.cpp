#include "GalvoScanner.h"
#include <iostream>


using namespace std;

GalvoScanner::GalvoScanner()
{
	UINT ErrorCode = init_rtc6_dll();

	if (ErrorCode)
	{
		cout << "Error while initilizing RTC6-Card" << endl;
	}

	(void) select_rtc(1);
	stop_execution();

	ErrorCode = load_program_file(0); // pPath = 0: Pfad des aktuellen Arbeitsverzeichnisses
	if (ErrorCode)
	{
		printf("Program file loading error : % d\n", ErrorCode);
		free_rtc6_dll();
		return;
	}
	reset_error(-1);
	//config_list(4194304, 4194304);
	config_list(6388607, 1);
}
