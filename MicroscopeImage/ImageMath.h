// file ImageMath.h
// performs some math calculations on images
// author: Rudi Weinacker
// Version: 2020:08:11

#ifndef BYTE
typedef unsigned char BYTE ;
#endif

namespace NImageMath
{
	// all arrays must be 16 byte aligned
	// num must be multiple of 16
	// no checks of that, so make sure!
	void shiftPhase ( BYTE* arrout , const BYTE* arrin1 , const BYTE* arrin2 , int num , double wavelengthcorr );
}
