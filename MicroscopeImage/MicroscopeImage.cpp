// MicroscopeImage.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>
//#include "GalvoScanner.h"
//#include "DigitizerBoard.h"
#include <thread>
#include <chrono> 
#include <fstream>
#include "CMicroscope.h"


//using namespace std;

int main()
{
    Microscope M;

    while (M.RunningStatus())
    {
        M.AskTask();
    }

    return(0);
}

