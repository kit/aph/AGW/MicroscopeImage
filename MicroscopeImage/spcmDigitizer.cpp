#pragma once
#pragma warning(disable : 4996)
#include "spcmDigitizer.h"


// Constructor lol
spcmDigitizer::spcmDigitizer() {
    cardIndex = 0;
    initParams();
    //printf("Constructed..\n");
};
spcmDigitizer::spcmDigitizer(int cIndx) {
    cardIndex = cIndx;
    initParams();
    //printf("Constructed..\n");
};
spcmDigitizer::spcmDigitizer(int cIndx, bool connectCard) {
    cardIndex = cIndx;
    if (connectCard)
        openCard();
    initParams();
    //printf("Constructed..\n");

};
spcmDigitizer::~spcmDigitizer() {
    closeCard();
    //if(pvBuffer != NULL)
        //delete[] ((int16*)pvBuffer); // doesn't work.. no idea why.. data should be there..
};
void spcmDigitizer::initParams() {

    pvBuffer            = NULL;
    samplerate          = 0;
    single_rec_totMem   = 0;
    single_rec_postTrig = 0;

    if (stCard.hDrv != NULL) {
        lBytesPerSample = stCard.lBytesPerSample;
    }
};

void spcmDigitizer::openCard() {

    if (stCard.hDrv != NULL) 
    {
        printf("Card already connected\n");
        return;
    }
    // ------------------------------------------------------------------------
    // init card number 0 (the first card in the system), get some information and print it
    // uncomment the second line and replace the IP address to use remote
    // cards like in a digitizerNETBOX
    if (bSpcMInitCardByIdx(&stCard, cardIndex))
    //if (bSpcMInitCardByIdx(&stCard, "192.168.169.42", 0))
    {
        //printf_s(pszSpcMPrintDocumentationLink(&stCard, szBuffer, sizeof(szBuffer)));
        printf(pszSpcMPrintCardInfo(&stCard, szBuffer, sizeof(szBuffer)));
    }
    else
        nSpcMErrorMessageStdOut(&stCard, "Error: Could not open card\n", true);
    // check whether we support this card type in the example
    if (stCard.eCardFunction != AnalogIn && stCard.eCardFunction != DigitalIn && stCard.eCardFunction != DigitalIO)
        nSpcMErrorMessageStdOut(&stCard, "Error: Card function not supported by this example\n", false);
};

void spcmDigitizer::closeCard() {
    if (stCard.hDrv == NULL)
        printf("Driver already closed\n");

    if (stCard.hDrv)
    {
        freeUpTheBuffer();
        spcm_vClose(stCard.hDrv);
        stCard.hDrv = NULL;
        printf("Driver closed\n");
    }
};

void spcmDigitizer::resetDigitizer() {
    closeCard();
    openCard();
};

//samples = totalMem / 2byte, pre-trigger samples = (totalMem-postTrgMem)/2byte
int spcmDigitizer::confDatasize(uint64 totalMem, uint64 postTrgMem) {
    single_rec_totMem   = totalMem;
    single_rec_postTrig = postTrgMem;

    //// calculate the amount of data we need and allocate memory buffer
    //if (!stCard.bSetError)
    //{

    //    switch (stCard.eCardFunction)
    //    {
    //    case AnalogIn:
    //        qwMemInBytes = stCard.llSetMemsize * stCard.lBytesPerSample * stCard.lSetChannels;
    //        //printf("qmMemInBytes = %2d", qwMemInBytes);
    //        break;

    //    case DigitalIn:
    //    case DigitalIO:
    //        qwMemInBytes = stCard.lSetChannels / 8 * stCard.llSetMemsize;
    //        break;
    //    }

    //    pvBuffer = pvAllocMemPageAligned(qwMemInBytes);
    //    if (!pvBuffer)
    //        return nSpcMErrorMessageStdOut(&stCard, "Memory allocation error\n", false);
    //}


    //printf("Memory size set, buffer allocated.\n");
    printf("Memory size set.\n");
    return 1;
};

//confSamplerate: Set the sample rate of the card
void spcmDigitizer::confSamplerate(uint64 smplrt)
{
    samplerate = smplrt;
    // we try to set the samplerate to 10 max/4 internal PLL, no clock output
    //bSpcMSetupClockPLL(&stCard, stCard.llMaxSamplerate / 4, false);
    bSpcMSetupClockPLL(&stCard, 1000000*smplrt, false);
    printf("Sampling rate set to %.1lf MHz\n", (double)stCard.llSetSamplerate / 1000000);

};

void spcmDigitizer::confSampling(uint64 smplrt, uint64 totalMem, uint64 postTrgMem)
{
    confSamplerate(smplrt);
    single_rec_totMem = totalMem;
    single_rec_postTrig = postTrgMem;
}

/*
samples = totalMem / 2byte, pre-trigger samples = (totalMem-postTrgMem)/2byte
inputRange in mV (termination dependent!)
*/
void spcmDigitizer::confChannel(uint64 chnl, int32 acqPath, int32 inputRange, bool term50or1M, bool ACorDC, bool bwLim, bool diffInput)
{
    if (single_rec_totMem == 0 || single_rec_postTrig == 0) {
        printf("Memorysizes not set. Exiting\n");
        return;
    }
    else
        printf("Taking %2d samples\n", single_rec_totMem);


    if(acqPath == 0)
        printf("Buffered Path selected.\n");
    else
        printf("HF Path selected. Input termination fixed to 50Ohm\n");

    // samples = totalMem / 2byte, pre-trigger samples = (totalMem-postTrgMem)/2byte
    bSpcMSetupModeRecStdSingle(&stCard, chnl, single_rec_totMem, single_rec_postTrig);
    //bSpcMSetupModeRecStdSingle(&stCard, CHANNEL0 | CHANNEL1, single_rec_totMem, single_rec_postTrig);
        // type dependent card setup
    switch (stCard.eCardFunction)
    {

        // analog acquisition card setup
    case AnalogIn:
        if (stCard.bM3i || stCard.bM4i)
        {
            // if the active Channel = CHANNEL1, the == decays to true=1 which 
            // selects the parameters for channel1 and false=0 for channel0 otherwise
            bSpcMSetupPathInputCh(&stCard, chnl == CHANNEL1, acqPath, inputRange, 0, term50or1M, ACorDC, bwLim, diffInput);
        }
        // activate digital inputs if available
        if (stCard.bM2i)
        {
            if ((stCard.lBytesPerSample > 1) && (stCard.lFeatureMap & SPCM_FEAT_DIGITAL))
                spcm_dwSetParam_i32(stCard.hDrv, SPC_READDIGITAL, 1);
        }

        break;

        // digital acquisition card setup
    //case DigitalIn:
    //case DigitalIO:

    //    // set all input channel groups to 110 ohm termination (if it's available)
    //    for (i = 0; i < pstCard->uCfg.stDIO.lGroups; i++)
    //        bSpcMSetupDigitalInput(pstCard, i, true);
    //    break;
    }

};
void spcmDigitizer::confChannel(uint64 chnl, int32 acqPath, int32 inputRange, bool term50or1M, bool ACorDC)
{
    confChannel(chnl, acqPath, inputRange, term50or1M, ACorDC, false, false);
    //printf("channel %2d configured and activated\n", chnl);
};

int spcmDigitizer::prepareBuffer() {
        //// calculate the amount of data we need and allocate memory buffer
    if (!stCard.bSetError)
    {

        switch (stCard.eCardFunction)
        {
        case AnalogIn:
            qwMemInBytes = stCard.llSetMemsize * stCard.lBytesPerSample * stCard.lSetChannels;
            //printf("qmMemInBytes = %2d", qwMemInBytes);
            break;

        case DigitalIn:
        case DigitalIO:
            qwMemInBytes = stCard.lSetChannels / 8 * stCard.llSetMemsize;
            break;
        }

        pvBuffer = pvAllocMemPageAligned(qwMemInBytes);
        if (!pvBuffer)
            return nSpcMErrorMessageStdOut(&stCard, "Memory allocation error\n", false);
    }
}

void spcmDigitizer::confSoftTrigger() {
    // Use software trigger as standard
    bSpcMSetupTrigSoftware(&stCard, false);
}

void spcmDigitizer::confChanTrigger(int trigSrc, int trigSlope, int trgLv0, int trgLv1, bool trgOut)
{
    // trigger levels see page 107 of manual pdf
    bSpcMSetupTrigChannel(&stCard, trigSrc, trigSlope, trgLv0, trgLv1, 0, trgOut, true); 
    //SPC_TM_POS;
    //SPC_TM_NEG;
}

void spcmDigitizer::confExtTrigger(int extMode, bool trgTerm, int trgLv0, int trgLv1, int trgExtLine)
{
    bSpcMSetupTrigExternalLevel(&stCard, extMode, trgLv0, trgLv1, trgTerm, false, 0L, true, trgExtLine);
}

int spcmDigitizer::measureSingleRecord(uint32 timeOut) {

    if (pvBuffer == NULL) {
        printf("Error(mg): Buffer not prepared! Exiting!"); 
        return 1;
    }

    // ------------------------------------------------------------------------
// make acquisition and get data
    if (!stCard.bSetError)
    {

        // We'll start and wait untill the card has finished or until a timeout occurs
        spcm_dwSetParam_i32(stCard.hDrv, SPC_TIMEOUT, timeOut);
        //printf("\nStarting the card and waiting for ready interrupt\n");
        if (spcm_dwSetParam_i32(stCard.hDrv, SPC_M2CMD, M2CMD_CARD_START | M2CMD_CARD_ENABLETRIGGER | M2CMD_CARD_WAITREADY) == ERR_TIMEOUT)
        {
            vFreeMemPageAligned(pvBuffer, qwMemInBytes);
            return nSpcMErrorMessageStdOut(&stCard, "... Timeout\n", false);
        }
        else
        {

            // we define the buffer for transfer and start the DMA transfer
            //printf("Starting the DMA transfer and waiting until data is in PC memory\n");
            spcm_dwDefTransfer_i64(stCard.hDrv, SPCM_BUF_DATA, SPCM_DIR_CARDTOPC, 0, pvBuffer, 0, qwMemInBytes);
            spcm_dwSetParam_i32(stCard.hDrv, SPC_M2CMD, M2CMD_DATA_STARTDMA | M2CMD_DATA_WAITDMA);

            // check for error code
            if (spcm_dwGetErrorInfo_i32(stCard.hDrv, NULL, NULL, szBuffer))
            {
                vFreeMemPageAligned(pvBuffer, qwMemInBytes);
                return nSpcMErrorMessageStdOut(&stCard, szBuffer, false);
            }
            //printf("... acquisition ended, data has been transferred to PC memory\n");

        }
    }
    return 0;
}

int spcmDigitizer::measureSingleRecord(uint32 timeOut, int16* data, int getNsamples) {

    //printf("Not done yet");
    //return 0;
    int16* locDataPtr = NULL;
    locDataPtr = (int16*)pvBuffer;

    if (getNsamples > single_rec_totMem) {
        printf("Error(mg): Requested number of samples %6d is larger than acquired number of samples %6d\n", 
            getNsamples, single_rec_totMem);
        return 1;
    }

    // call function to record
    measureSingleRecord(timeOut);

    // copy data out
    for (int i = 0; i < getNsamples; i++)
    {
        data[i] = locDataPtr[i];
    }
   
    //vFreeMemPageAligned(pvBuffer, qwMemInBytes);
 
    return 0;
}


int spcmDigitizer::measureSingleRecord(uint32 timeOut, double* data, int getNsamples) {

    //printf("Not done yet");
    //return 0;
    int16* locDataPtr = NULL;
    locDataPtr = (int16*)pvBuffer;

    if (getNsamples > single_rec_totMem) {
        printf("Error(mg): Requested number of samples %6d is larger than acquired number of samples %6d\n",
            getNsamples, single_rec_totMem);
        return 1;
    }

    // call function to record
    measureSingleRecord(timeOut);

    // copy data out
    for (int i = 0; i < getNsamples; i++)
    {
        data[i] = (double)locDataPtr[i];
    }

    //vFreeMemPageAligned(pvBuffer, qwMemInBytes);

    return 0;
}



int spcmDigitizer::measureSingleRecordVolt(uint32 timeOut, double* data, int getNsamples, int32 lChannel) {

    //printf("Not done yet");
    //return 0;
    const int16* locDataPtr = (const int16*)pvBuffer;

    if (getNsamples > single_rec_totMem) {
        printf("Error(mg): Requested number of samples %6d is larger than acquired number of samples %6d\n",
            getNsamples, single_rec_totMem);
        return 1;
    }

    // call function to record
    measureSingleRecord(timeOut);

    // convert to volt and copy data out
    ConvRecordValuesToVoltage(locDataPtr, getNsamples, lChannel, data);

    //vFreeMemPageAligned(pvBuffer, qwMemInBytes);

    return 0;
}


bool spcmDigitizer::freeUpTheBuffer()
{
    vFreeMemPageAligned(pvBuffer, qwMemInBytes);
    return 0;
}

uint64 spcmDigitizer::get_recordLength() {
    return stCard.llSetMemsize;
}

const int16* spcmDigitizer::get_DataPtr() {
    return (const int16*)pvBuffer;
};

const int16* spcmDigitizer::get_DataPtr(int& Nsamples) {
    Nsamples = get_recordLength();
    return (const int16*)pvBuffer;
};

bool spcmDigitizer::get_DataPtr_s(const int16** data_ptr_ptr, int& Nsamples) {
    if (pvBuffer == NULL) {
        printf("Buffer is null\n");
        return 1;
    }
    *data_ptr_ptr = (const int16*)pvBuffer;
    Nsamples = get_recordLength();
    return 0;
}

bool spcmDigitizer::get_DataPtr_s(const int16*& data_ptr_ref, int& Nsamples) {
    if (pvBuffer == NULL) {
        printf("Buffer is null\n");
        return 1;
    }
    data_ptr_ref = (const int16*)pvBuffer;
    Nsamples = get_recordLength();
    return 0;
}

double spcmDigitizer::ConvSingleValueToVoltage(double dValue, int32 lChannel) {
    {
        double dVoltage_mv;

        if ((lChannel < 0) || (lChannel >= stCard.lMaxChannels))
        {
            sprintf(stCard.szError, "SpcMIntToVoltage: channel number %d not valid. Channels range from 0 to %d\n", lChannel, stCard.lMaxChannels);
            return 0;
        }

        // recalculate with input range
        dVoltage_mv = stCard.uCfg.stAI.lSetRange[lChannel] * dValue / stCard.uCfg.stAI.lMaxADCValue;

        // add the signal offset
        if (stCard.uCfg.stAI.astPath[stCard.uCfg.stAI.lSetPath[lChannel]].bOffsPercentMode)
            dVoltage_mv -= stCard.uCfg.stAI.lSetRange[lChannel] * stCard.uCfg.stAI.lSetOffset[lChannel] / 100;
        else
            dVoltage_mv -= stCard.uCfg.stAI.lSetOffset[lChannel];

        return (dVoltage_mv);
    }
}

bool spcmDigitizer::ConvRecordValuesToVoltage(const int16* dValues, int Nsamples, int32 lChannel, double* results) {
    if ((lChannel < 0) || (lChannel >= stCard.lMaxChannels))
    {
        sprintf(stCard.szError, "SpcMIntToVoltage: channel number %d not valid. Channels range from 0 to %d\n", lChannel, stCard.lMaxChannels);
        return 1;
    }
    // querry channel parameters once!
    auto setRange = stCard.uCfg.stAI.lSetRange[lChannel];
    auto maxADCcode = stCard.uCfg.stAI.lMaxADCValue;
    auto chnlOffset = stCard.uCfg.stAI.lSetRange[lChannel] * stCard.uCfg.stAI.lSetOffset[lChannel] / 100;

    for (int i = 0; i < Nsamples; i++) 
    {
        results[i] = setRange * dValues[i] / maxADCcode - chnlOffset;
    }

    return 0;
}

void spcmDigitizer::safeASCIItoDisk(void* data) {
    int16* localData = NULL;
    if (data == NULL)
        localData = (int16*)pvBuffer;
    else
        localData = (int16*)data;

    printf("Storing %6d samples\n", single_rec_totMem);
    //system("pause");


    //FILE* hFileData = fopen(SPCM_FILENAME, "wt");
    FILE* hFileData;
    fopen_s(&hFileData,SPCM_FILENAME, "wt");
    //FILE* hFileMeta = fopen(SPCM_METAFILENAME, "wt");
    FILE* hFileMeta;
    fopen_s(&hFileMeta,SPCM_METAFILENAME, "wt");
    //int64 llFileSize = single_rec_totMem;
    int64 llFileSize = stCard.llSetMemsize;
    int64 samplerate = stCard.llSetSamplerate;

    //printf("sizeof(localData) = %2d\n", sizeof(localData));
    //printf("sizeof(*localData) = %2d\n", sizeof(*localData));

    for (int lIndex = 0; lIndex < llFileSize; lIndex++)
    {
        //printf("Samples %4d / %4d stored\r", lIndex+1,llFileSize);
        //printf("index = %4d  ->  %6d \n", lIndex, localData[lIndex]);
        //fprintf(hFile, "%6d\n", *localData++);
        //fprintf(hFile, "0x%02x\n", *localData++ & 0xffff);
        //fprintf(hFileData, "%02d\n", *localData);
        fprintf_s(hFileData, "%02d\n", *localData);
        //fprintf(hFile2, "%02d\n", *localData & 0xffff);
        //fprintf(hFile3, "0x%02x\n", *localData & 0xffff);

  /*      printf("data is = %10d",*localData);*/
        localData++;
    }

    //fprintf(hFileMeta, "%d,%d", samplerate, single_rec_totMem);
    fprintf_s(hFileMeta, "%d,%d", samplerate, single_rec_totMem);

    fclose(hFileData);   fclose(hFileMeta);
}

void spcmDigitizer::safeASCIItoDisk(const std::string& metaf, const std::string& dataf, void* data) {
    int16* localData = NULL;
    if (data == NULL)
        localData = (int16*)pvBuffer;
    else
        localData = (int16*)data;

    printf("Storing %6d samples\n", single_rec_totMem);
    //system("pause");

    FILE* hFileData;
    fopen_s(&hFileData, dataf.c_str(), "wt");

    FILE* hFileMeta;
    fopen_s(&hFileMeta, metaf.c_str(), "wt");

    int64 llFileSize = stCard.llSetMemsize;
    int64 samplerate = stCard.llSetSamplerate;

    for (int lIndex = 0; lIndex < llFileSize; lIndex++)
    {
        fprintf_s(hFileData, "%02d\n", *localData);
        localData++;
    }

    //fprintf(hFileMeta, "%d,%d", samplerate, single_rec_totMem);
    fprintf_s(hFileMeta, "%d,%d", samplerate, single_rec_totMem);

    fclose(hFileData);   fclose(hFileMeta);
}

void spcmDigitizer::safeASCIItoDisk(const std::string& dataf, void* data) {
    int16* localData = NULL;
    if (data == NULL)
        localData = (int16*)pvBuffer;
    else
        localData = (int16*)data;

    printf("Storing %6d samples\n", single_rec_totMem);
    //system("pause");

    FILE* hFileData;
    fopen_s(&hFileData, dataf.c_str(), "wt");

    int64 llFileSize = stCard.llSetMemsize;
    int64 samplerate = stCard.llSetSamplerate;

    for (int lIndex = 0; lIndex < llFileSize; lIndex++)
    {
        fprintf_s(hFileData, "%02d\n", *localData);
        localData++;
    }

    fclose(hFileData);
}