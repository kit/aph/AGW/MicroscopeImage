#pragma once

#include "AlazarCmd.h"
#include "AlazarApi.h"
#include "AlazarError.h"

struct channel
{
	U8 channelID;
	U32 couplingID;
	ALAZAR_INPUT_RANGES inputRange;
	U32 impedance;
};

struct triggerEngine
{
	U32 TriggerEngine;		// TRIG_ENGINE_J or TRIG_ENGINE_K
	U32 Source;				// Source for use as Trigger (e.g. TRIG_CHAN_A, TRIG_EXTERNAL or TRIG_DISABLE)
	U32 TriggerLevelCode;	// TriggerLevelCode = 128 + 127 * TriggerLevelVolts / InputRangeVolts
	U32 Slope;				// TRIGGER_SLOPE_POSITIVE or TRIGGER_SLOPE_NEGATIVE
};

struct onBoardRecord
{
	U32 preTriggerSamples;
	U32 postTriggerSamples;
	U32 recordsPerCapture; // number of records - each trigger leads to one record
};

struct AutoDMA
{
	U32 preTriggerSamples;
	U32 postTriggerSamples;
	U32 RecordsPerBuffer;
	U32 BufferPerAcquisition;
	U32 bytesPerBuffer;
};

#define BUFFER_COUNT 4 // Number of Buffers for AutoDMA

class DigitizerBoard
{
public:
	DigitizerBoard(U32 systemId = 1, U32 boardId = 1, U32 sampleRateIdOrValue = SAMPLE_RATE_50MSPS); 
	// Constuctor

	HANDLE GetBoardHandle(); 
	// returns the private member data boardHandle

	U32 GetSampleRate();	
	//returns the private member data sampleRate

	void SetSampleRate(U32 sampleRateIdOrValue);

	void SetSampleRateInt(int sampleRateInt);
	// changes the private member datat sampleRate

	void TestLED(const DWORD &time); 
	// turns on the LED for "time" in ms and then off again

	void ConfigureChannel(U8 channel, U32 coupling, ALAZAR_INPUT_RANGES inputRange, U32 impedance);
	// apllies new steetings to member data channels and then configures this channel via the method AlazarConfigureChannel

	void SetCaptureClock(const U32 &source, const U32 &edgeID);
	// apllies Clock behaviour from source, edgeID ans sampleRate to the board via the function AlazarSetCaptureClock

	void ActivateExternalClock(unsigned int decimation);

	void DeactivateExternalClock();

	void SetTriggerOperation(U32); 
	// Changes private member data TriggerOperation

	void ConfigureTriggerEngine(U32 TriggerEngine, U32 Source, U32 TriggerLevelCode, U32 Slope);
	// Changes values in The private member Datas EngineJ or EngineK. Then apllies new values to the board via ActivateTriggerOperation (AlazarSetTriggerOperation)

	void ConfigureTriggerEngine(U32 TriggerEngine, U32 Source, float TriggerLevelVolts, U32 Slope);
	// Same as ConfigureTriggerEngine above, but with mV Trigger Level instead of U32 Code

	void ConfigureBufferRecord(U32 preTriggersamples, U32 postTriggerSamples, U32 recordsPerCapture);
	// Changes the values of the private member data bufferRecord 

	void SinglePortAcquisition(bool SingleChannel = false, U8 ChannelID = CHANNEL_A, bool wait = true);
	// Performs a DAQ in Single port mode. If SingleChannel = true only voltage from channel "ChannelID" is measured. 
	// Use ConfigureBufferRecord before this to define parameters for measurement
	// Aquire data via the methode ReadDatafromBuffer

	U16* ReadDatafromBuffer(int &samplesPerRecord, U8 ChannelID, long record = 1);
	// Returns pointer on data measured in a Single Port Acquisition (see above). Use reference parameter samplesPerRecord to get the length of this array.
	// Parameter ChannelID determines from which channel datas will be returned
	// Parameter record determines which record will be returned if there is more than one

	double* ConvertToVoltage(U16* data, const int &n, U8 ChannelID);
	// Converts U16 datas into voltage values and returns pointer on it
	// Parameter data are the U16 raw datas, e.g. returned by the methode ReadDatafromBuffer
	// Parameter n is the length of this data array
	// Parameter ChannelID says from which channels data where required
	// Caution: Dont change Inputrange before datas where converted or you will get wrong conversion with the new input range

	double* GetTimes(int &samplesPerRecord, long record = 1);
	// Returns times from a Single port Acquisition 
	// Use referenc parameter samplesPerRecord to get the length of this array
	// Use Parameter record to determine from which record you want the time array if there is more than one
	// Caution: Do not change member bufferRecord via ConfigureBufferRecord before calculating time array or you might get wrong values!

	U16 RecordSingleValue(U8 ChannelID, U32 avg = 1);
	// Returns a single value at instant time
	// average over n points

	void SetTriggertimeout(U32);
	// set the private variable TriggerTimeout

	void SoftwareTrigger();
	// activates a software trigger event

	void ConfigureAuxIO(U32 mode, U32 parameter);
	// Saves Parameters to private member datas AUXIOMode and AUXIOParameter
	// Then Configures AuxIO via the Alazar function AlazarConfigureAuxIO()

	void StartNPTAutoDMAAcquisition(U32 samplesPerRecord, U32 RecordsPerBuffer, U32 BuffersPerAcquisition, U8 channel);
	// Configures Board to start a NPT AutoDMA Measurement at one given channel
	// If you want to use AusIO as trigger enable you must configure this before calling StartNPTAutoDMAAcquisition
	// If using AuxIO enable trigger remark, that each trigger enable will lead to a full buffer, so there will be 'RecordsPerBuffer' records
	// TODO: Funktion to read out data

	bool ReadDataNPTAutoDmA(U16* data, int timeout_ms);
	bool ReadDataNPTAutoDmA(U16* data, int timeout_ms, int px);
	// function to read out the next buffer in AutoDMA, will be stored in data array (allocate before!)
	// timeout_ms will be the time for the card to tkae the measurement
	// Be careful to call this funktion as soon as possible to avoid memory overflow

	bool ReadDataNPTAutoDmALine(U16* data, int timeout_ms, int line);

	bool AbortNPTAutoDMA();

	float translateInputRange(ALAZAR_INPUT_RANGES IR);
	// translates the enum ALAZAR_INPUT_RANGES into volatges (float) in mV

	U32 translateSampleRate();
	// Translates the enum ALAZAR_SAMPLE_RATES into U32 (S/s)

	U32 translateSampleRate(int SR);

private:
	HANDLE boardHandle ;
	U32 sampleRate;
	int bytesPerSample;
	U32 numberOfChannels;
	channel* channels;
	triggerEngine EngineJ;
	triggerEngine EngineK;
	U32 TriggerOperation;
	onBoardRecord bufferRecord;

	U32 buffersCompleted;

	U16* BufferArray[BUFFER_COUNT] = { NULL };
	//U16** BufferArray;
	int currentDMABufferIndex;
	AutoDMA NPT_DMA;

	U32 TriggerTimeout;

	U32 AuxIOMode;
	U32 AuxIOParameter;

	void AlazarConfigureChannel(channel CH);
	// configures channel by extracting ists elements ans using the function AlazarInputControl

	void ActivateTriggerOperation();
	// apllies all data saved in EngineJ, EngineK ans TriggerOperation via the function AlazarSetTriggerOperation to the board
};

