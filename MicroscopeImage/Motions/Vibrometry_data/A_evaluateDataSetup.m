close all; clear all;

if ((exist('metadatafilePath') >= 1) & (exist('signalfilePath') >= 1))
    clearvars -except metadatafile metadatafilePath signalfile signalfilePath Tstamp; 
else
    clear all;
    Tstamp          = '2022-02-25_13-10-09';
    signalfile      = [Tstamp '_ASCII_Data.txt'];
    metadatafile    = [Tstamp '_meta_Data.txt'];

    metadatafilePath = [pwd '\' metadatafile];
    signalfilePath = [pwd '\' signalfile];
%     metadatafilePath = [pwd '\data\' metadatafile];
%     signalfilePath = [pwd '\data\' signalfile];
%     metadatafilePath = [ metadatafile];
%     signalfilePath = [  signalfile];
    
%     signalfilePath2     = [ 'S:\AG Wegener\MechaMeta\Adaptive Optics\04 Messungen\Setup Characterization\20211129_Heterodyne_Signal_Analysis\data\2021112913267_signal.txt'];
%     metadatafilePath2   = [ 'S:\AG Wegener\MechaMeta\Adaptive Optics\04 Messungen\Setup Characterization\20211129_Heterodyne_Signal_Analysis\data\2021112913267_metaData.txt'];
end
%%
plotDir     = [pwd '\plots\'];

MetaData    = readmatrix(metadatafilePath);
Fs          = MetaData(1)
Ndata       = MetaData(2);
Ts          = 1/Fs;
fftRes      = Fs/Ndata;
time        = (0:Ndata-1) * Ts;
signal      = readmatrix(signalfilePath);
fc          = 80e6; %Hz

meanSig     = mean(signal);
Vpp         = max(signal)-min(signal);
rmsSig      = rms(signal);

freq        = ((-Ndata/2):(Ndata/2-1)) * Fs/Ndata;
signal_ft   = fft(signal);

% single sided amplitude spectrum according to
% https://de.mathworks.com/help/matlab/ref/fft.html
freq_single         = Fs/Ndata * (0:(Ndata/2));
signal_ft_single    = 2.*signal_ft(1:Ndata/2+1);
signal_ft_single    = signal_ft_single ./ Ndata; % Normalize
signal_ft           = fftshift(fft(signal));

%% time signal
figure(1)
clf
% plot(time*1e9,signal*1e3,'x-','Linewidth',1.5)
plot(time*1e9,signal,'x-','Linewidth',1.5)
title('Time Signal')
xlim([0 200])
% ylim(1e3.*[-max(abs(signal)) max(abs(signal))])
% ylim(1e3.*[-0.100 0.100])
ylim([-2^13 2^13])
xlabel('Time in ns')
% ylabel('Signal in mV')
ylabel('ADC code');
box on; grid on;
comment = "Parameters " + newline +...
            "Mean = " + num2str(meanSig) + "" + newline +...
            "pp = " + num2str(Vpp) + "" + newline +...
            "rms = " + num2str(rmsSig ) + "";
annotation('textbox', [0.62, 0.25, 0.1, 0.1],'BackgroundColor',...
            'w', 'String', comment,'FitBoxToText','on')
annotation('textbox', [0.62, 0.8, 0.1, 0.1],'BackgroundColor',...
            'w', 'String', ['TS: ' Tstamp],'FitBoxToText','on')
set(gca,'FontSize',16,'LineWidth',1)
saveas(gcf,[plotDir signalfile(1:end-4) '_signal.bmp'])

%% FFT
% figure(2)
% clf
% plot(freq/1e6,abs(signal_ft),'r','Linewidth',1.5)
% title('FFT Spectrum')
% xlim(3.5.*[-fc fc]./1e6)
% xlabel('Frequency in MHz')
% ylabel('abs. FFT amp in a.u.')
% box on; grid on;
% set(gca,'FontSize',16,'LineWidth',1)

figure(3)
clf
plot(freq_single/1e6,abs(signal_ft_single).*1e3,'r','Linewidth',1.5) %MHz, mV
title('Amplitude Spectrum')
xlim(3.5.*[0 fc]./1e6)
xlabel('Frequency in MHz')
ylabel('|U(f)| in mV')
annotation('textbox', [0.62, 0.8, 0.1, 0.1],'BackgroundColor',...
            'w', 'String', ['TS: ' Tstamp],'FitBoxToText','on')
% box on; 
grid on;
set(gca,'FontSize',16,'LineWidth',1)
saveas(gcf,[plotDir signalfile(1:end-4) '_ampSpec.bmp'])
xlim([79.5 80.5])
% xlim([69.5 70.5])
% xlim([59.5 60.5])
saveas(gcf,[plotDir signalfile(1:end-4) '_ampSpec_zoom.bmp'])
% xlim(3.5.*[0 fc]./1e6)
%% PSD

% [pxx, f] = pwelch(signal,1000,300,1000,Fs);
% [pxx, f] = pwelch(signal,flattopwin(1000),500,1000,Fs);
Nwindow     = Ndata/4;
Nfft        = Nwindow
[pxx, f]    = pwelch(signal,hann(Nwindow),Nwindow/2,Nfft,Fs);

figure(4)
clf
plot(f/1e6,10*log10(pxx),'r','Linewidth',1.5) %plotting as log means that basically 1W is the reference power!!
title('Welch PSD ')
xlim(3.5.*[0 fc]./1e6)
xlabel('Frequency in MHz')
ylabel('PSD (dB/Hz)')
box on; grid on;
comment = "Hann window: " + newline +...
            "Nwndw = " + num2str(Nwindow) + newline +...
            "Nfft = " + num2str(Nfft)
annotation('textbox', [0.2, 0.22, 0.1, 0.1], 'String', comment,...
            'Background','w','FitBoxToText','on')
annotation('textbox', [0.62, 0.8, 0.1, 0.1],'BackgroundColor',...
            'w', 'String', ['TS: ' Tstamp],'FitBoxToText','on')
set(gca,'FontSize',16,'LineWidth',1)
saveas(gcf,[plotDir signalfile(1:end-4) '_welchPSD.bmp'])
close(gcf)
%%


A_SignaDemodSetup6
% figure(5)
% clf
% plot(freq/1e6,abs(signal_ft).^2)
% title('PSD ')
% xlim(3.5.*[-fc fc]./1e6)
% xlabel('Frequency in MHz')
% ylabel('abs. FFT amp in a.u.')
% box on; grid on;
% set(gca,'FontSize',16,'LineWidth',1)
