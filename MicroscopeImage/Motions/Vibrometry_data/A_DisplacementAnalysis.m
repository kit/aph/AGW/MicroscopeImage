clear all; close all; clc
Tstamp          = '';
% Tstamp          = '2022-03-10-13-52-59';
dispfilefile    = [Tstamp '_ASCII_disp_Data.txt'];
metadatafile    = [Tstamp '_META_data.txt'];

metadatafilePath = [pwd '\dataDem\' metadatafile];
dispfilePath = [pwd '\dataDem\' dispfilefile];


%%
% Natural constants
hplanck         = 6.62607015e-34; %Js
c0              = 2.99792458e8; %m/s
lambda_LAS      = 532e-9; %m
freq_LAS        = c0/lambda_LAS; %Hz
% Read data
dispData    = readmatrix(dispfilePath)';
% Read metadata
MetaData    = importMeta(metadatafilePath);
Fs          = MetaData(1);
Ns          = MetaData(2);
smooth      = MetaData(3);
NsDec       = MetaData(4);
Ts          = 1/Fs;
Tsdec       = Ts*smooth;
fftRes      = Fs/Ns;
time        = (0:NsDec-1) * Tsdec;

freq      = Fs/Ns * (-NsDec/2:(NsDec/2)-1);
recFilter = ~((abs(freq) < 10e3) | (abs(freq) > 300e3));
% myWindow  = flattopwin(NsDec)';
myWindow  = hann(NsDec)';

%% Flags
flag_carrierCorr    = 1;
flag_window         = 1;
flag_dispFilt       = 0;

%% Carrier-frequency-mismatch correction
dispData  = dispData - mean(dispData);
if flag_carrierCorr == 1
    driftCorr = carrierCorr(time,dispData);
    dispData  = dispData(:)' - driftCorr(time)';
end

df = 500;
fex = 100e3 + df;
% dispData = 16*sin(2*pi*fex*time);

dispData0 = dispData; % copy for comparison later on

figure(1)
% subplot(2,1,2)
plot(time,dispData)
title('Disp data corr')
xlabel(['Time in s'])
ylabel(['Displ. in nm'])
ylim([min(dispData) max(dispData)])
% xlim([0 10/5e3])
% xlim((5+[0 10])./100e3)
grid on; box on;


%% windowing
if flag_window == 1
    dispData = dispData.*myWindow;
end

%% Calc FT and do filtering
dispFT          = fft(dispData);
dispFT0         = fft(dispData0); 

%% Filtering
if flag_dispFilt == 1
    dispFT = fftshift(dispFT);
    dispFT = dispFT.*recFilter;
    dispFT = ifftshift(dispFT);
    DispFiltered = real(ifft(dispFT));
    
    %%    
    figure(2)
    clf
    hold on;
    plot(time,DispFiltered,'Linewidth',1.5)
    title('Filtered Displacement')
    ylim([min(DispFiltered) max(DispFiltered)])
    % xlim((5+[0 10])./100e3)
    xlabel(['Time in s'])
    ylabel(['Displ. in nm'])
    box on; grid on; hold off;
end

freq_single     = Fs/Ns * (0:(NsDec/2));
dispFT_single   = 2.*dispFT(1:NsDec/2+1);
dispFT_single   = dispFT_single ./ NsDec; % Normalize
dispFT          = fftshift(dispFT);

dispFT0_single   = 2.*dispFT0(1:NsDec/2+1);
dispFT0_single   = dispFT0_single ./ NsDec; % Normalize
dispFT0          = fftshift(dispFT0);


%% Single-sided
% figure(3)
% clf
% hold on;
% plot(freq_single,abs(dispFT_single),'r','Linewidth',1.5)
% plot(freq_single,abs(dispFT0_single),'b','Linewidth',1.5)
% title('FFT Displacement-spectrum')
% 
% xlim([99000 101000])
% 
% xlabel('Frequency in Hz')
% ylabel(' FFT amp in nm')
% box on; grid on; hold off;

%% Double-sided
% figure(4)
% clf
% plot(freq,abs(dispFT),'r','Linewidth',1.5)
% title('FFT Displacement-spectrum')
% % xlim([Fc-100e3 Fc+100e3]./1e6)
% % xlim([4900 5100])
% % xlim([-10e3 10e3])
% xlim([-1e4 1e4])
% xlabel('Frequency in Hz')
% ylabel(' FFT amp in a.u.')
% box on; grid on;

%% with even (1Hz) FFT resolution
Ns1sec = 500e6/smooth;
Ns2sec = 2*500e6/smooth;
Fc = 80e6;

if NsDec >= Ns1sec
    dispDatashort      = dispData0(1:Ns1sec);
    dispDatashortW     = dispData0(1:Ns1sec);

    window = flattopwin(Ns1sec)';
%     window  = hann(Ns1sec)';

%     windowA = sum(abs(window));
%     windowHeight    = max(window)-min(window);
%     totalA          = Ns1sec * windowHeight;
%     normalizeW      = windowA / totalA
%     window              = window ./ (1-normalizeW);

    normalizeW          = length(window)/sum(window);
    window              = window .* normalizeW;
    dispDatashortW      = dispDatashortW.*window;

    freqShort           = (-Ns1sec/2:(Ns1sec/2)-1) * 1; % Ns1sec chosen as such that fft Resolution is 1Hz!
    freqShortSingle     = (0:Ns1sec/2);

    dispFTshort         = fft(dispDatashort);
    dispFTsingleShort   = 2.*dispFTshort(1:Ns1sec/2+1);
    dispFTsingleShort   = dispFTsingleShort./Ns1sec;
    dispFTshort         = fftshift(dispFTshort);
    
    dispFTshort2         = fft(dispDatashortW);
    dispFTsingleShort2   = 2.*dispFTshort2(1:Ns1sec/2+1);
    dispFTsingleShort2   = dispFTsingleShort2./Ns1sec;
    dispFTshort2         = fftshift(dispFTshort2);

    figure(5)
    clf
    hold on
    plot(freqShortSingle,abs(dispFTsingleShort),'x-','Linewidth',1.5)
    plot(freqShortSingle,abs(dispFTsingleShort2),'o-','Linewidth',1.5)
%     plot(freqShort,abs(dispFTshort),'r','Linewidth',1.5)
    title('FFT Displacement-spectrum')
    

%     xlim([100e3-1e3 100e3+1e3])
    xlim([fex-1e3 fex+1e3])
    
    xlabel('Frequency in Hz')
    ylabel(' FFT amp in nm')
    box on; grid on; hold off;
    legend('no window','windowed')
end

% if NsDec >= Ns2sec
%     dispDataCshort2      = dispData(1:Ns2sec);
% 
%     freqShort2           = (-Ns2sec/2:(Ns2sec/2)-1) * 1; % Ns1sec chosen as such that fft Resolution is 1Hz!
%     freqShortSingle2     = (0:Ns2sec/2);
% 
%     dispFTshort2         = fft(dispDataCshort2);
%     dispFTsingleShort2   = 2.*dispFTshort2(1:Ns2sec/2+1);
%     dispFTsingleShort2   = dispFTsingleShort2./Ns2sec;
%     dispFTshort2         = fftshift(dispFTshort2);
% 
%     figure(6)
%     clf
%     plot(freqShortSingle2,abs(dispFTsingleShort2),'r','Linewidth',1.5)
% %     plot(freqShort,abs(dispFTshort),'r','Linewidth',1.5)
%     title('FFT Displacement-spectrum')
%     % xlim([Fc-100e3 Fc+100e3]./1e6)
% %     xlim([4900 5100])
% %     xlim([0 10e3])
%     xlabel('Frequency in Hz')
%     ylabel(' FFT amp in nm')
%     box on; grid on;
% end
% 




%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function metaData = importMeta(filename, dataLines)
%IMPORTFILE Import data from a text file
%  METADATA = IMPORTFILE(FILENAME) reads data from text file FILENAME
%  for the default selection.  Returns the numeric data.
%
%  METADATA = IMPORTFILE(FILE, DATALINES) reads data for the specified
%  row interval(s) of text file FILENAME. Specify DATALINES as a
%  positive scalar integer or a N-by-2 array of positive scalar integers
%  for dis-contiguous row intervals.
%
%  Example:
%  metaData = importfile("C:\C++\MicroscopeImage\MicroscopeImage\Motions\Vibrometry_data\2022-03-04_15-45-31_meta_Data.txt", [1, 1]);
%
%  See also READTABLE.
%
% Auto-generated by MATLAB on 04-Mar-2022 15:47:59

%% Input handling

% If dataLines is not specified, define defaults
if nargin < 2
    dataLines = [1, 1];
end

%% Setup the Import Options and import the data
opts = delimitedTextImportOptions("NumVariables", 4);

% Specify range and delimiter
opts.DataLines = dataLines;
opts.Delimiter = ",";

% Specify column names and types
opts.VariableNames = ["VarName1", "VarName2", "VarName3", "VarName4"];
opts.VariableTypes = ["double", "double", "double", "double"];

% Specify file level properties
opts.ExtraColumnsRule = "ignore";
opts.EmptyLineRule = "read";

% Specify variable properties
opts = setvaropts(opts, "VarName1", "TrimNonNumeric", true);
opts = setvaropts(opts, "VarName1", "ThousandsSeparator", ",");

% Import the data
metaData = readtable(filename, opts);

%% Convert to output type
metaData = table2array(metaData);
end

function [fitresult, gof] = carrierCorr(time, dispData)
%CREATEFIT(TIME,DISPDATA)
%  Create a fit.
%
%  Data for 'untitled fit 1' fit:
%      X Input : time
%      Y Output: dispData
%  Output:
%      fitresult : a fit object representing the fit.
%      gof : structure with goodness-of fit info.
%
%  See also FIT, CFIT, SFIT.

%  Auto-generated by MATLAB on 04-Mar-2022 16:03:27


%% Fit: 'untitled fit 1'.
[xData, yData] = prepareCurveData( time, dispData );

% Set up fittype and options.
ft = fittype( 'poly1' );

% Fit model to data.
[fitresult, gof] = fit( xData, yData, ft );

% Plot fit with data.
% figure( 'Name', 'untitled fit 1' );
% h = plot( fitresult, xData, yData );
% legend( h, 'dispData vs. time', 'untitled fit 1', 'Location', 'NorthEast', 'Interpreter', 'none' );
% % Label axes
% xlabel( 'time', 'Interpreter', 'none' );
% ylabel( 'dispData', 'Interpreter', 'none' );
% grid on

end





