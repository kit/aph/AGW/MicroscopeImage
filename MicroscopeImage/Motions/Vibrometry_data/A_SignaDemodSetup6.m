clear all;
%% This script demodulates measurement data from the confocal vibrometry setup
% - The script comments out a lot of plotting which will not be required on
%   the long run.
% - The demodulation will in practise be performed thrice
% -- First run demodulates with the carrier set to Fc = 80MHz to determine
%    a possible mismatch between estimated carrier frequency and actual
%    carrer frequency.
% -- Second run demodulates with the now corrected carrier frequency to
%    determine residual effects i.e. a oscillation around 2-3kHz which may be
%    caused by clock/timing jitter.

% close all; 

if ((exist('metadatafilePath') >= 1) & (exist('signalfilePath') >= 1))
    clearvars -except metadatafile metadatafilePath signalfile signalfilePath Tstamp; 
else
    clear all;
    Tstamp          = '';
    Tstamp          = '2022-02-28_13-14-23';
    signalfile      = [Tstamp '_ASCII_data.txt'];
    metadatafile    = [Tstamp '_META_data.txt'];

    metadatafilePath = [pwd '\data\' metadatafile];
    signalfilePath = [pwd '\data\' signalfile];
%     metadatafilePath = [pwd '\data\' metadatafile];
%     signalfilePath = [pwd '\data\' signalfile];
%     metadatafilePath = [ metadatafile];
%     signalfilePath = [  signalfile];
    
%     signalfilePath2     = [ 'S:\AG Wegener\MechaMeta\Adaptive Optics\04 Messungen\Setup Characterization\20211129_Heterodyne_Signal_Analysis\data\2021112913267_signal.txt'];
%     metadatafilePath2   = [ 'S:\AG Wegener\MechaMeta\Adaptive Optics\04 Messungen\Setup Characterization\20211129_Heterodyne_Signal_Analysis\data\2021112913267_metaData.txt'];
end

% FLAGS
flag_save           = 1;
flag_window_signal  = 0;
flag_correct_drift  = 0;
flag_high_pass      = 0;
flag_show_IM_plots  = 1;
flag_do_signalFT    = 0;
flag_use_matlab_dec = 1;  

flag_use_dummy_data = 0;



%%
plotDir = [pwd '\plots\'];
% Metadata of setup and natural constants
hplanck         = 6.62607015e-34; %Js
c0              = 2.99792458e8; %m/s
Fc              = 80e6; %fixed carrier frequency
Fc = Fc -114.9449;
FcCorr          = 0;
% Fc              = (80)*1e6 - 124.0;
lambda_LAS      = 532e-9; %m
freq_LAS        = c0/lambda_LAS; %Hz


%% Load data
MetaData    = readmatrix(metadatafilePath);
Fs          = MetaData(1);
Ns          = MetaData(2);
Ts          = 1/Fs;
fftRes      = Fs/Ns;
time        = (0:Ns-1) * Ts;
doppSig     = readmatrix(signalfilePath)';
% doppSig     =  sin(2.*pi.*(Fc+1e3).*time);
% fm              = 50e3; %Hz

smooth              = 64;
decimate            = smooth;
% memRAW              = 2*Ns; % data size in bytes
fftFreq             = ((-Ns/2):(Ns/2-1)).*Fs/Ns;
% time2               = (0:100*Ns-1)/Fs/100;
% carrier             = sin(2.*pi.*Fc.*time2);

drift_freq_mismatch     = 0;
osc_freq_amp_mismatch   = 0;
osc_freq_mismatch       = 0;
osc_phase_mismatch      = 0;

% CarsonBW        = 2*(fm+fd);
% modIndex        = fd/fm;
CarsonBW          = 300e3;

%% generate data 
if flag_use_dummy_data == 1
    menu('ATTENTION! DUMMY DATA USED!','ok..','understood')
    
    Tstamp = 'generated-Data';
    amplitude       = 0.5*532e-9; %m Schwingungsamplitude in Z
    fm              = 100e3;
    v_max           = 2*pi*fm*amplitude;
%     fd              = 2*v_max*freq_LAS/c0
    fd              = 2*v_max/lambda_LAS
    phi0            = (0)*pi/180; %phase between carrier and doppler signal
    % Generate velocity / displacement trajectory for set sample rate
    time        = (0:Ns-1)*1/Fs;
%     doppSig     = 2^13*cos(2.*pi.*Fc.*time  + fd/fm .* sin(2.*pi.*fm.*time) + phi0);
    doppSig     = 2^13*cos(2.*pi.*(Fc-1e3).*time);

    % do this again for constant movement of sample, NOT for oscillation!
    % to do so, construct movement with constant velocity over measurement time
%     Tmes    = time(end);    % set total time of movement from measurement time
%     sPath   = lambda_LAS/1;   % move a fraction of a wavelength
%     v2      = sPath/Tmes; % get corresponding velocity
%     fd2     = 2*v2/lambda_LAS; % get corresponding doppler shift
%     doppSig = cos(2.*pi.*(Fc+fd2).*time);
end

% pre-allocate and prepare arrays
% Wikipedia convention of I (cos <-> x) and Q (sin <-> y) component in complex plane
LocOscI                 = 2^13.*cos(2*pi*Fc*time); % calculate representation of 80MHz in 250MSps sample rate
LocOscQ                 = 2^13.*sin(2*pi*Fc*time); % this is simply done by using the "time" array as it is determined by the sample rate
InphaseSmoothDec        = zeros(1,length(time)/smooth);
QuadratureSmoothDec     = zeros(1,length(time)/smooth);
bbPhaseDec              = zeros(1,length(time)/smooth);
timeDecimate            = time(1:decimate:end);
  

%% Start signal processing 
tic
if flag_window_signal == 1
    hannWindInput   = hann(length(doppSig))';
%     hannWindInput2  = hann(length(doppSig2))';
    doppSig         = doppSig.*hannWindInput;
%     doppSig2        = doppSig2.*hannWindInput2;
end

%% DDC - Demodulate doppler signal using a 80MHz quadrature signal

% Wikipedia convention of I (cos <-> x) and Q (sin <-> y) component in complex plane
Inphase     = LocOscI .* doppSig;
Quadrature  = LocOscQ .* doppSig;

% Inphase = cos(2.*pi.*(fd2).*time);
% Quadrature = sin(2.*pi.*(fd2).*time);
% Inphase = cos(2.*pi.*(2*v2/lambda_LAS).*time);
% Quadrature = sin(2.*pi.*(2*v2/lambda_LAS).*time);

if flag_show_IM_plots == 1
    if flag_do_signalFT == 1
        freq        = ((-Ns/2):(Ns/2-1)) * Fs/Ns;
        signal_ft   = fft(doppSig);
        % single sided amplitude spectrum according to
        % https://de.mathworks.com/help/matlab/ref/fft.html
        freq_single         = Fs/Ns * (0:(Ns/2));
        signal_ft_single    = 2.*signal_ft(1:Ns/2+1);
        signal_ft_single    = signal_ft_single ./ Ns; % Normalize
        signal_ft           = fftshift(signal_ft);
        
%         normFT = sum(abs(signal_ft_single));
        normFT = max(abs(signal_ft_single));
        
        figure(2)
        clf
        plot(freq_single/1e6,abs(signal_ft_single)./normFT,'r','Linewidth',1.5)
        title('FFT Spectrum')
        xlim([Fc-100e3 Fc+100e3]./1e6)
        xlabel('Frequency in MHz')
        ylabel('abs. FFT amp in a.u.')
        box on; grid on;
        set(gca,'FontSize',16,'LineWidth',1)
    end
    %%
figure(3)
clf
subplot(2,1,1)
% plot(time*1e6,Inphase./rms(Inphase),'o-',time*1e6,Quadrature./rms(Quadrature),'x-')
plot(time*1e6,Inphase,'o-',time*1e6,Quadrature,'x-')
title('Mixed signals')
% ylim([-1.4 1.4])
% ylim([-2 2])
xlim([0 20/Fc*1e6])
ylabel('Amp. arb. u.')
% ylabel('s(t) / s_rms')
xlabel('Time in us')
legend('I','Q')
% 
InphaseFt       = fftshift(fft(Inphase));
QuadratureFt    = fftshift(fft(Quadrature));
% % InphaseFt       = fftshift(fft(LocOscI.*LocOscI));
% % QuadratureFt    = fftshift(fft(LocOscI.*LocOscQ));

RectCenterPos   = Ns/2;
myRect          = heaviside(time*Fs-RectCenterPos+smooth/2).*heaviside(-time*Fs+RectCenterPos+smooth/2);
myRectFft       = fftshift(fft(myRect));
maxMyRectFft    = max(abs(myRectFft))
rectAmp         = 0;
% rectAmp         = 0.5*(max(abs(InphaseFt))+max(abs(QuadratureFt)))/max(abs(myRectFft));
maxIft          = max(abs(InphaseFt));
maxQft          = max(abs(QuadratureFt));
if maxIft > maxQft
    rectAmp = maxIft/maxMyRectFft;
end
if maxIft <= maxQft
    rectAmp = maxQft/maxMyRectFft;
end
set(gca,'FontSize',16,'LineWidth',1)

subplot(2,1,2)
hold on
plot(fftFreq/1e6,abs(InphaseFt)./1e3,'o-')
plot(fftFreq/1e6,abs(QuadratureFt)./1e3,'x-')
plot(fftFreq/1e6,abs(myRectFft).*rectAmp./1e3)
hold off
title('FFT spec of IQ mixed signals')
xlabel('Frequency in MHz')
ylabel('Amp. arb. u.')
annotation('textbox', [0.2, 0.3, 0.1, 0.1],'BackgroundColor',...
            'w', 'String', ['TS: ' Tstamp],'FitBoxToText','on')
% legend('I','Q')
legend('I','Q','filter')
set(gca,'FontSize',16,'LineWidth',1)
end

%% Downsampling
if flag_use_matlab_dec == 1
    % smooth
    InphaseSmooth       = movmean(Inphase,[0 smooth-1]);
    QuadratureSmooth    = movmean(Quadrature,[0 smooth-1]);
    % decimate
    InphaseSmoothDec    = InphaseSmooth(1:decimate:end);
    QuadratureSmoothDec = QuadratureSmooth(1:decimate:end);
else
    
    valI = 0; valQ = 0;
   
    for i = 0:length(InphaseSmoothDec)-1
        for j = 1:smooth
            valI = valI     + Inphase(i*smooth + j);
            valQ = valQ     + Quadrature(i*smooth+j);
        end
        InphaseSmoothDec(i+1)       = valI;
        QuadratureSmoothDec(i+1)    = valQ;
        valI = 0; valQ = 0;        
    end
end

if flag_show_IM_plots == 1
figure(6)
clf
% subplot(2,1,1)
title('Smoothed and decimated I&Q')
hold on
plot(timeDecimate*1e6,InphaseSmoothDec./rms(InphaseSmoothDec),'o-')
plot(timeDecimate*1e6,QuadratureSmoothDec./rms(QuadratureSmoothDec),'x-')
% plot(timeDecimate*1e6,InphaseSmoothDec,'o-');
% plot(timeDecimate*1e6,QuadratureSmoothDec,'x-');


legend('I','Q','Location','Best')
% ylim([-1. 1.])
% ylim([-2 2])
xlim([0 64])
% xlim([0 1/fm*1e6])
xlabel('Time in us')
% ylabel('s(t) / s_rms')
ylabel('Amp. in arb. u.')
box on; grid on;
hold off
set(gca,'FontSize',16,'LineWidth',1)

% subplot(2,1,2)
% hold on
% plot(fftFreq/1e6,abs(InphaseSmoothFt)./1e3,'o-')
% plot(fftFreq/1e6,abs(QuadratureSmoothFt)./1e3,'x-')
% xlabel('Frequency in MHz')
% ylabel('Amp. arb. u.')
% hold off
% title('FFT spec of smoothed signals')
% annotation('textbox', [0.2, 0.3, 0.1, 0.1],'BackgroundColor',...
%             'w', 'String', ['TS: ' Tstamp],'FitBoxToText','on')

end
%% recover phase / phase unwrap
% Wikipedia convention of I (cos <-> x) and Q (sin <-> y) component in complex plane
bbPhaseDec      = atan2(QuadratureSmoothDec,InphaseSmoothDec); 
% bbPhaseDec      = atan(QuadratureSmoothDec./InphaseSmoothDec); 
% bbPhaseDec      = unwrap(bbPhaseDec);
bbPhaseDec = myUnwrap(bbPhaseDec);

%% get displacement
bbDisp      = -1*(lambda_LAS/4/pi).*bbPhaseDec; 
% IMPORTANT! -1* IS DONE TO ASSOCIATE POSITIVE DISPLACEMENT SLOPE 
% WITH POSITIVE DOPPLER SHIFT APPROACHING SAMPLE
% THIS CONVENTION IS IN LINE WITH THE CONVENTION USED BY REMBE ET. AL ->   Laser-scanning confocal vibrometer microscope: Theory and experiments

bbDisp      = bbDisp - mean(bbDisp);

[fitRes, fitGof, fitXdata, fitYdata, fitFuncStr] = fitDisplacements(timeDecimate*1e6,bbDisp*1e9); %Fit in nm/us!!!!!
fitGof
% [fitRes, fitGof, fitXdata, fitYdata, fitFuncStr] = fitDisplacementsActual(timeDecimate*1e6,bbDisp*1e9); %Fit in nm/us!!!!!

validzPath  = [pwd '\loc_validz.dat'];
writematrix(bbDisp,validzPath,'Delimiter','tab')
toc
count = 1;
fftRes;

drift_freq_mismatch     = (fitRes.a*1e-9/1e-6) * 2 / lambda_LAS

osc_freq_amp_mismatch   = abs(2*pi*fitRes.d*1e6*fitRes.c*1e-9 * 2 / lambda_LAS);
osc_freq_mismatch       = fitRes.d*1e6;
osc_phase_mismatch      = fitRes.e;

%% Drift correction
if flag_correct_drift == 1
    FcCorr = Fc + drift_freq_mismatch

    LocOscI_dc     = cos(2*pi*FcCorr*time); 
    LocOscQ_dc     = sin(2*pi*FcCorr*time); 
    Inphase_dc     = LocOscI_dc .* doppSig;
    Quadrature_dc  = LocOscQ_dc .* doppSig;

    %2 Mov Mean
    % smooth
    InphaseSmooth_dc    = movmean(Inphase_dc,[0 smooth-1]);
    QuadratureSmooth_dc = movmean(Quadrature_dc,[0 smooth-1]);
    % decimate
    InphaseSmoothDec_dc     = InphaseSmooth_dc(1:decimate:end);
    QuadratureSmoothDec_dc  = QuadratureSmooth_dc(1:decimate:end);
%     timeDecimate            = time(1:decimate:end);

    %2 recover phase / phase unwrap
    bbPhaseDec_dc       = atan2(QuadratureSmoothDec_dc,InphaseSmoothDec_dc);
    bbPhaseDec_dc       = unwrap(bbPhaseDec_dc);
%     meanPhaseValue_dc   = mean(bbPhaseDec_dc);
%     maxPhaseValue_dc    = max(bbPhaseDec_dc);

    %2 get displacement
    bbDisp_dc      = -(lambda_LAS/4/pi).*(bbPhaseDec_dc);
    % IMPORTANT! -1* IS DONE TO ASSOCIATE POSITIVE DISPLACEMENT SLOPE 
    % WITH POSITIVE DOPPLER SHIFT APPROACHING SAMPLE
    [fitRes, fitGof, fitXdata, fitYdata, fitFuncStr] = fitDisplacements(timeDecimate*1e6,bbDisp_dc*1e9); %Fit in nm/us!!!!!
    
    count = 2
    drift_freq_mismatch = (fitRes.a*1e-9/1e-6) * 2 / lambda_LAS
    
    osc_freq_amp_mismatch   = abs(2*pi*fitRes.d*1e6*fitRes.c*1e-9 * 2 / lambda_LAS);
    osc_freq_mismatch       = fitRes.d*1e6;
    osc_phase_mismatch      = fitRes.e;
    
    bbDisp = bbDisp_dc;
end


%% high pass filtering (alternative to this weird carrier frequency modulation..)
if flag_high_pass == 1
    % bbHann = hann(length(bbDisp))';
    % bbGauss = gausswin(length(bbDisp),512)';
    ftBBdisp = fftshift(fft(bbDisp_dc));
    specMax = max(abs(ftBBdisp));
    % Fsdec = 1/(timeDecimate(2));
    Fsdec = Fs/smooth;
    Nsdec = Ns/smooth;

    Nsdec = length(timeDecimate);
    decFFTfreq = ((-Nsdec/2):(Nsdec/2-1)).*Fsdec/Nsdec;

    highPass = (abs(decFFTfreq) > 30e3);

    filt_ftBBdisp = highPass.*ftBBdisp;
    % filt_ftBBdisp = (1-bbGauss).*ftBBdisp;
    filt_BBdisp = ifft(ifftshift(filt_ftBBdisp));

    figure(7)
    clf
    subplot(2,1,1)
    plot(decFFTfreq,abs(ftBBdisp),decFFTfreq,(highPass)*specMax)
    xlim([-300e3 300e3])
    subplot(2,1,2)
    plot(timeDecimate,bbDisp,timeDecimate,filt_BBdisp)
    
    [fitRes, fitGof, fitXdata, fitYdata, fitFuncStr] = fitDisplacements(timeDecimate*1e6,filt_BBdisp*1e9); %Fit in nm/us!!!!!
end


%% plotting
fitCoeffs   = coeffvalues(fitRes);
aStr        = [num2str(fitRes.a,3)  'nm/us' ' '];
bStr        = [num2str(fitRes.b,3)  'nm' ' '];
cStr        = [num2str(fitRes.c,3)  'nm' ' '];
dStr        = [num2str(fitRes.d*1e3,3)  'kHz' ' '];
eStr        = [num2str(fitRes.e,3)  'rad' ' '];
% aStr        = [num2str(fitRes.a,3)  'm/s' ' '];
% bStr        = [num2str(fitRes.b,3)  'm' ' '];
% cStr        = [num2str(fitRes.c,3)  'm' ' '];
% dStr        = [num2str(fitRes.d*1e3,3)  'Hz' ' '];
% eStr        = [num2str(fitRes.e,3)  'rad' ' '];
% coeffsStr   = num2str(fitCoeffs);
coeffsStr    = [aStr bStr cStr dStr eStr]  
fitComment  = ['f(x) = ' fitFuncStr newline coeffsStr];


figure(8)
clf
hold on

h = plot(fitRes,fitXdata,fitYdata);
% h = plot(fitXdata,fitYdata,'.');
% h = plot(timeDecimate*1e6,bbDisp*1e9);
% h = plot(timeDecimate*1e6,bbDisp_dc*1e9,'x');
set(h,'Linewidth',1.5);

% myLeg = get(h,leg);
% set(h,'Position','best')
% set(h,'box','on')
% plot(fitXdata,fitYdata,'-','r','Linewidth',1)
% plot(time*1e6,fit1.a*sin(fit1.b*time+fit1.c)+fit1.d)
hold off
title('Recovered displacement')
% xlim([0 100])
xlim([0 10/5e3*1e6])
% ylim([-4 4])
% ylim([-25 25])
xlabel('Time in us')
ylabel('Displacement in nm')
% legend('Data','Fit','Location','Best')
box on;
grid on;
annotation('textbox', [0.15, 0.13, 0.1, 0.1],'BackgroundColor',...
            'w', 'String', ['TS: ' Tstamp],'FitBoxToText','on')
% annotation('textbox', [0.15, 0.8, 0.1, 0.1],'BackgroundColor',...
%             'w', 'String', [fitComment],'FitBoxToText','on')


figname = [signalfile(1:end-4) 'fig8_exp_decimate' num2str(smooth) '.bmp'];

set(gca,'FontSize',16,'LineWidth',1)
if flag_save == 1
    saveas(gcf,[plotDir figname])
end

if flag_show_IM_plots == 1
    figure(9)
    figure(clf)
    % norm = sqrt(max(InphaseSmoothDec)^2 + max(QuadratureSmoothDec)^2);
    normI = max(abs(InphaseSmoothDec)); normQ = max(abs(QuadratureSmoothDec));
    % plot(InphaseSmoothDec,-QuadratureSmoothDec,'x-')
    sz = 25;
    c = linspace(0,Ts*Ns*1e6,length(InphaseSmoothDec));
    scatter(InphaseSmoothDec./normI,QuadratureSmoothDec./normQ,sz,c)
    cbar = colorbar;
    cbar.Label.String = 'Time in us';
    xlabel('Inphase I(t)'); ylabel('Quadrature Q(t)'); title('IQ deicmated BB Signal')
    grid on
    box on
    plotRange = 1.4;
    xlim([-plotRange plotRange])
    ylim([-plotRange plotRange])
    pbaspect([1 1 1])
    annotation('textbox', [0.17, 0.13, 0.1, 0.1],'BackgroundColor',...
                'w', 'String', ['TS: ' Tstamp],'FitBoxToText','on')

    figname = [signalfile(1:end-4) 'fig9_exp.bmp'];

    set(gca,'FontSize',16,'LineWidth',1)
    if flag_save == 1
        saveas(gcf,[plotDir figname])
    end
end


%%
NsDec = Ns/smooth;
FsDec = Fs/smooth;

freqDec     = ((-NsDec/2):(NsDec/2-1)) * FsDec/NsDec;
bbDispFT    = fft(bbDisp);
% single sided amplitude spectrum according to
% https://de.mathworks.com/help/matlab/ref/fft.html
freq_singleDec         = FsDec/NsDec * (0:(NsDec/2));
bbDispFT_single    = 2.*bbDispFT(1:NsDec/2+1);
bbDispFT_single    = bbDispFT_single ./ NsDec; % Normalize
bbDispFT           = fftshift(bbDispFT);


% normFTdec = max(abs(bbDispFT_single));
normFTdec = 1;

% fd_demod = fit1.a * fit1.b/2/pi
% fd;

figure(10)
clf
plot(freq_singleDec,1e9.*abs(bbDispFT_single)./normFTdec,'r','Linewidth',1.5)
title('FFT Displacement-spectrum')
% xlim([Fc-100e3 Fc+100e3]./1e6)
xlim([4900 5100])
% xlim([0 10e3])
xlabel('Frequency in Hz')
ylabel(' FFT amp in nm')
box on; grid on;

dispMax = max(abs(bbDispFT_single));
txt = ['max. is ' num2str(1e9*dispMax) 'nm'];
text(5010,1e9*dispMax,txt)

annotation('textbox', [0.17, 0.2, 0.1, 0.1],'BackgroundColor',...
            'w', 'String', ['TS: ' Tstamp],'FitBoxToText','on')
set(gca,'FontSize',16,'LineWidth',1)

figname = [signalfile(1:end-4) 'fig10_exp.bmp'];

set(gca,'FontSize',16,'LineWidth',1)
if flag_save == 1
    saveas(gcf,[plotDir figname])
end

% abweichung = 100*(1-fd_demod/fd)

%% Functions
%%%%%%%%%%%%%%%

function res = unwrapPhase(previous_angle, new_angle)
    d = rem(new_angle - previous_angle,2*pi);
    if (d > pi)
        d = d - 2*pi;
    elseif(d < -pi)
        d = d + 2*pi;
    end
    res = (previous_angle + d);
end

function res = myUnwrap(data)
    loc_data = data;
    for i = 1:length(data)-1
        loc_data(i+1) = unwrapPhase(loc_data(i),loc_data(i+1));
    end
    res = loc_data;
end
