function [x_new, y_new, x_old, y_old] = marker_jumpremove(max_move_x, max_move_y, grid_x, grid_y, x, y)
% marker_jumpremove removes all markers moving more than specified
%   grid_x and grid_y are arrays containing the initial x- and y-coordinates 
%   of markers. x and y are matrices containing all further marker positions 
%   (x-dir.) for all images (y-dim.). 
%   marker_jumpremove calculates the relative movement of each marker and 
%   discards markers that move more (one one image to the next) than 
%   specified in max_move_x and max_move_y

if exist('max_move_x', 'var') == 0
    fprintf('Setting max_move_x to default (0.5)\n');
    max_move_x = 0.5;
end

if exist('max_move_y', 'var') == 0
    fprintf('Setting max_move_y to default (0.5)\n');
    max_move_y = 0.5;
end

if exist('grid_x', 'var') == 0
    % open x grid
    [fn_x_grid, path_x_grid] = uigetfile('*.dat', 'Open x grid file');
    cur = pwd;
    cd(path_x_grid);
    grid_x = importdata(fn_x_grid, '\t');
    cd(cur);
end

if exist('grid_y', 'var') == 0
    % open y grid
    [fn_y_grid, path_y_grid] = uigetfile('*.dat', 'Open y grid file');
    cur = pwd;
    cd(path_y_grid);
    grid_y = importdata(fn_y_grid, '\t');
    cd(cur);
end

if exist('x', 'var') == 0
    % open x
    [fn_x, path_x] = uigetfile('*.dat', 'Open x marker position file');
    cur = pwd;
    cd(path_x);
    x = importdata(fn_x, '\t');
    cd(cur);
end

if exist('y', 'var') == 0
    % open y
    [fn_y, path_y] = uigetfile('*.dat', 'Open y marker position file');
    cur = pwd;
    cd(path_y);
    y = importdata(fn_y, '\t');
    cd(cur);
end

%% data preparation
% concatenate grid and marker data
pos_x = [grid_x'; x'];
pos_y = [grid_y'; y'];

% calculate the difference between two images for each marker
move_x = diff(pos_x);
move_y = diff(pos_y);

% find jumping steps of all markers and sum over all images (= all lines)
% Afterwards, jumpers_* is a row vector with the number of steps bigger
% than max_move_* for each marker
jumpers_x = sum(abs(move_x) > max_move_x);
jumpers_y = sum(abs(move_y) > max_move_y);

% Get the indices of all jumpers (i.e. of all markers with an entry bigger
% than 0 in the jumpers_* vector
jumpidx_x = find(jumpers_x > 0);
jumpidx_y = find(jumpers_y > 0);

% We delete a marker if one of the directions is jumping
jumpidx_all = union(jumpidx_x, jumpidx_y);

% Calculate all marker indices that do not jump
nonjumpidx = setdiff(1:length(grid_x), jumpidx_all);

% Give back the only the marker positions of nonjumping markers
x_new = x(nonjumpidx,:);
y_new = y(nonjumpidx,:);
x_old = x;
y_old = y;

% Statistics
fprintf('Kept %d markers, deleted %d.\n', size(nonjumpidx,2), ...
    size(jumpidx_all,2));
answer = questdlg(...
    sprintf('Kept %d markers, deleted %d. Save the new marker data?',...
    size(nonjumpidx,2), size(jumpidx_all,2)),...
    'Save ...', 'Yes', 'No', 'Yes');
if strcmp(answer, 'Yes')
    [fn, folder]= uiputfile('*.dat', 'Save x data', 'validx_jmprmv.dat');
    folder_cur = pwd;
    cd(folder);
    save(fn, 'x_new', '-ascii', '-tabs');
    
    [fn, folder]= uiputfile('*.dat', 'Save x data', 'validy_jmprmv.dat');
    cd (folder);
    save(fn, 'y_new', '-ascii', '-tabs');
    
    cd(folder_cur);
end

end

