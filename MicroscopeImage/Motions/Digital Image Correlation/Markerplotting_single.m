% Captures frames with your images and an overlay of the tracking marker
% written by Chris

function [validx,validy]=Markerplotting_single(validx, validy, image)

% open marker files
if exist('validx', 'var') == 0
    [validx,Pathvalidx] = uigetfile('*.dat','Open validx.dat');
    cd(Pathvalidx);
    validx=importdata(validx,'\t');
end

if exist('validy', 'var') == 0
    [validy,Pathvalidy] = uigetfile('*.dat','Open validy.dat');
    cd(Pathvalidy);
    validy=importdata(validy,'\t');
end

if exist('image', 'var') == 0
    % open image to plot the markers on
    [fn_img, path_img]= uigetfile('*.tif;*.tiff', 'Open image file.');
    cd(path_img);
    
    % plot the image
    figure;
    imshow(fn_img);
else
    figure;
    imshow(image);
end
hold on;
plot(validx(:,1), validy(:,1),'.g','MarkerSize',10);