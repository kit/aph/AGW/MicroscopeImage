function  [filenamelist]=filenamelist_auto

pause(0.01);
ImageFolder =pwd;
listing = dir();

i = 1;
dummy = 0;

while dummy ==0;
    if length(listing(i).name) >8 
        if listing(i).name(1:3)== 'PIC'
            Firstimagename = listing(i).name;
            dummy = 1;
        end
    else
        i = i+1    
    end
    
end


if Firstimagename ~= 0;
    cd(ImageFolder);
    
    % Get the number of image name
    letters=isletter(Firstimagename);
    Pointposition=findstr(Firstimagename,'.');
    Firstimagenamesize=size(Firstimagename);
    counter=Pointposition-1;
    counterpos=1;
    letterstest=0;
    while letterstest==0
        letterstest=letters(counter);
        if letterstest==1
            break
        end
        Numberpos(counterpos)=counter;
        counter=counter-1;
        counterpos=counterpos+1;
        if counter==0
            break
        end
    end
    
    Filename_first = Firstimagename(1:min(Numberpos)-1);
    Firstfilenumber=Firstimagename(min(Numberpos):max(Numberpos));
    Lastname_first = Firstimagename(max(Numberpos)+1:Firstimagenamesize(1,2));
    Firstfilenumbersize=size(Firstfilenumber);
    onemore=10^(Firstfilenumbersize(1,2));
    filenamelist(1,:)=Firstimagename;
    
    Firstfilenumber=str2num(Firstfilenumber);
    u=1+onemore+Firstfilenumber;
    ustr=num2str(u);
    filenamelist(2,:)=[Filename_first ustr(2:Firstfilenumbersize(1,2)+1) Lastname_first];
    numberofimages=2;
    
    counter=1;
    
    while exist(filenamelist((counter+1),:),'file') ==2;
        counter=counter+1;
        u=1+u;
        ustr=num2str(u);
        filenamelist(counter+1,:)=[Filename_first ustr(2:Firstfilenumbersize(1,2)+1) Lastname_first];
        if exist(filenamelist((counter+1),:),'file') ==0;
            warning('Last image detected')
            filenamelist(counter+1,:)=[];
            break
        end
    end
end

PathNameBase = ImageFolder;
FileNameBase = 'filenamelist.mat';
save(FileNameBase,'filenamelist');


end

