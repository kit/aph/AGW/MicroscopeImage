function autogrid_dic(im)
% Finds grid points in an image based on edge analysis

if exist('image', 'var') == 0
    [fn_img, path_img]= uigetfile('*.tif;*.tiff', 'Open image file.');
    cd(path_img);
    
    im = imread(fn_img);
end

% convert to grayscale
im = uint8(mean(double(im),3));

% find edges
e = edge(im, 'log');

count = sum(sum(uint8(e)));

% Get scaling factor from user
dlg_title = 'Scale factor';
prompt = {sprintf('Found %d edge points, please specifiy target count:',...
    count)};
def_answer = {'3000'};
answer = inputdlg(prompt, dlg_title, 1, def_answer);

% Convert answer to double
target = str2double(answer{1});

thresh = target / count;
[idx_y, idx_x] = find(uint8(e) > 0);

% create random number for each edge point
rnd = random('unif', 0, 1, size(idx_x, 1), 1);

% discard all numbers above the threshold and keep the rest
idx_x = idx_x(rnd < thresh);
idx_y = idx_y(rnd < thresh);

% Plot the result
Markerplotting_single(idx_x, idx_y, im);
title(sprintf('%d markers left', size(idx_x, 1)));

% Save the results
choice = questdlg('Save the resulting marker positions?', ...
    'Save files?', 'Yes', 'No', 'No');
switch choice
    case 'Yes'
        curpn = pwd;
        
        [fn, pn] = uiputfile('*.dat', 'Save x grid coordinates', 'grid_x.dat');
        cd(pn);
        dlmwrite(fn, idx_x);
        
        [fn, pn] = uiputfile('*.dat', 'Save y grid coordinates', 'grid_y.dat');
        cd(pn);
        dlmwrite(fn, idx_y);
        
        cd(curpn);
end