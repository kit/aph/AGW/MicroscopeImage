close all;

load('filenamelist.mat')
xcorramps = load('xcorramplitudes.txt');

gridX = load('grid_x.dat'); % load grid file to determine amount of markers

% since 'xcorramplitudes.txt' grows somewhat organically with every run of
% the script, I tried to only plot the data points from the last run.
% Therefore I determine the number of datapoints from the number of files
% listed in the "filenamelist.mat" and the number of markers given by the
% number of x-coordinates in the "grid_x.dat". If one wants to be safe that
% all datapoints in 'xcorramplitudes.txt' are from the last excecution of
% the automate_image function, one should delete 'xcorramplitudes.txt'
% beforehand.
Nmarkers = length(gridX);
Nfiles = length(filenamelist);
Npoints = Nmarkers*Nfiles - Nmarkers; %e.g. 10000 pics with 4 ROI => 4 reference-ROI pictures resutling in 4*(10000-1) = 39996 pictures

fsample = 10e6; %Hz

sampleTimes = (0:(Nfiles-2)) .* 1/fsample;

% figure(1)
% plot(xcorramps(end-(Npoints-1):end))
% title('Amplitudes of cpcorr for all pictures and markers')
% ylim([0.975 1])
% xlim([0 1000])
% xlabel('Picture Number')
% ylabel('Xcorr Amplitude')


%%

% for i = 0:Nmarkers-1
%     figure(i+1)
%     plot(sampleTimes*1e6,xcorramps(end-(Npoints-1)+i:Nmarkers:end))
%     title(['Marker number ',num2str(i+1),' of ', num2str(Nmarkers)])
%     ylim([0.95 1])
%     xlim([0 100])
%     xlabel('Time t in ?s')
%     ylabel('Xcorr Amplitude')
% end

%%
figure(Nmarkers)
legendstring = {};
hold on
for i = 0:Nmarkers-1
    plot(sampleTimes*1e6,xcorramps(end-(Npoints-1)+i:Nmarkers:end))
    title('Amplitudes of cpcorr for all pictures')
    %ylim([0.85 1])
    ylim([0.85 1])
    xlim([0 100])
    xlabel('Time t in \mus')
    ylabel('Xcorr Amplitude')
    legendstring = [legendstring, ['Marker number ',num2str(i+1)]];
end
legend(legendstring, 'location', 'southeast')

hold off

saveas(gcf,'xcorr_amplitude_plot.png')
%%
% amplitudes = zeros(Nmarkers,length(sampleTimes));
% for i = 1:Nmarkers
%     amplitudes(i,:) = xcorramps(end-(Npoints-1)+i-1:Nmarkers:end)-mean(xcorramps(end-(Npoints-1)+i-1:Nmarkers:end));
% end
% 
% figure(99)
% plot(sampleTimes,amplitudes(1,:))

%%
% FT
% n = length(sampleTimes)-9; 
% freq = (-n/2:n/2-1) * fsample/n;
% 
% fft_amplitudes = zeros(Nmarkers,n);
% for i = 1:Nmarkers
%     fft_amplitudes = fftshift(fft(amplitudes(i,1:end-9)));
% end
% 
% 
% figure(98)
% plot(freq,abs(fft_amplitudes(1,:)))
% xlim([0 0.55e6])
% text(10000,23, ['Sampling frequency was  10 MHz'],'Color','black')





