clear all

figure(1)
clf
figure(2)
clf
figure(3)
clf
figure(4)
clf

% parameter important for low and high pass
fps=8;
beat_rate =1;
periods = 3;
smooth_int =fps/beat_rate*periods;



% filename needs to be specified here
namelist = ['3_3_6\bottom_1_50_200khz_4v_beat_1hz_16fps_60frames\'];

%%
for mes = 1:length(namelist(:,1))
    
    xdata = [];
    ydata = [];
    xsmooth = [];
    ysmooth = [];
    xfiltered = [];
    yfiltered =[];
    
    name = strtrim(namelist(mes, :));
%     xdata = importdata([name '\validx.dat']);
%     ydata = importdata([name '\validy.dat']);
     xdata = importdata('validx.dat');
     ydata = importdata('validy.dat');
    
    
    
    
    
    size1 = size(xdata);
    for i = size1(2):-1:1
        
        xdata(:,i) = xdata(:,i)-xdata(:,1);
        ydata(:,i) = ydata(:,i)-ydata(:,1);
        
    end
    for i = 1:size1(1)
        xsmooth(i,:) = smooth(xdata(i,:),smooth_int);
        ysmooth(i,:) = smooth(ydata(i,:),smooth_int);
        
        xfiltered(i,:) = xdata(i,:)-xsmooth(i,:);
        yfiltered(i,:) = ydata(i,:)-ysmooth(i,:);
    end
  
    figure(1)
    clf
    subplot(2,3,1)
    hold on
    plot(mean(xdata)')
    plot(mean(ydata)')
    plot(mean(xsmooth)')
    plot(mean(ysmooth)')
    ylabel('displacement in pixel')
    xlabel('picture number')
    title('averaged displacement over all tracked points');
    

    subplot(2,3,2)
    hold on

    plot((xdata)')
    plot((xsmooth)')

    ylabel('displacement in pixel')
    xlabel('picture number')
    title('raw data plus mean position of point x-direction');
    

    subplot(2,3,3)
    hold on
    plot((ydata)')
    plot((ysmooth)')

    ylabel('displacement in pixel')
    xlabel('picture number')
    title('raw data plus mean position of point y-direction');
    
    subplot(2,3,4)
    hold on
    plot(xfiltered','r')
    plot(yfiltered','b')
    ylabel('displacement in pixel')
    xlabel('picture number')
    title('filtered data')
    

    subplot(2,3,5)
    hold on
    plot(mean(xfiltered)','r')
    plot(mean(yfiltered)','b')
    ylabel('displacement in pixel')
    xlabel('picture number')
    title('mean filtered data');
    
    subplot(2,3,6)
    hold on
    plot(smooth(mean(xfiltered),3)','r')
    plot(smooth(mean(yfiltered),3)','b')
    ylabel('displacement in pixel')
    xlabel('picture number')
    title('mean filtered data + low pass');


%%
end
number_pics = length(xfiltered(:,1));

%low-pass filter
sorted_datax = smooth(mean(xfiltered),3)';
sorted_datay = smooth(mean(yfiltered),3)';

xdatafreq = zeros(floor(length(xdata)/number_pics), number_pics);
ydatafreq = zeros(floor(length(xdata)/number_pics), number_pics);

x = 1:number_pics;

% for i = 1:floor(length(xdata)/number_pics)
%     
%     xdatafreq(i,:) = (sorted_datax(1,(1:number_pics)+(i-1)*number_pics))';
%     ydatafreq(i,:) = (sorted_datay(1,(1:number_pics)+(i-1)*number_pics))';
%     
%     
%     %sin fit for evalution
%     
%     %fo = fitoptions('Method','NonlinearLeastSquares',...
%     %           'Lower',[0,0],...
%     %           'Upper',[Inf,max(cdate)],...
%     %           'StartPoint',[1 1]);
%     ft = fittype('a*sin(2*pi*b*x+c)');
%     options = fitoptions(ft);
%     options.StartPoint = [0.2 1/8 pi];
%     options.Lower = [0 1/10  0];
%     options.Upper = [1 1/6 pi]; 
%     %options.StartPoint = [0.2 pi/2];
%     %options.Lower = [0.0001  0];
%     %options.Upper = [1 pi]; 
%     fitx = fit(x(16:40)',xdatafreq(i,16:40)',ft,options);
%     figure(2)
%     clf
%     subplot(2,1,1)
%     plot(x,xdatafreq(i,:),'x')
%     hold on 
%     plot(x,xdatafreq(i,:))
%     
%     plot(fitx)
%     
%     ax(i) = fitx.a;
%     cx(i) = fitx.c;
%     if ax(i)< 0
%         
%         cx(i) = fitx.c+pi;
%         
%     end
%     
%     ax(i) = abs(ax(i));
%     
%     fity = fit(x(15:(end-20))',ydatafreq(i,15:(end-20))',ft,options);
%     ay(i) = fity.a;
%     cy(i) = fity.c;
%     if ax(i)< 0
%         
%         cy(i) = fity.c+pi;
%         
%     end
%     
%     ay(i) = abs(ay(i));
%     
%     subplot(2,1,2)
%     plot(x,ydatafreq(i,:),'x')
%     hold on 
%     plot(x,ydatafreq(i,:))
%     plot(fity)
%     
%     
% 
%     
% end

dlmwrite('filtered_data.dat',[mean(xfiltered)' mean(yfiltered)'],'delimiter','\t');
