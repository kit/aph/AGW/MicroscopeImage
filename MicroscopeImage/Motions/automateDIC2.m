function [validx,validy]=automateDIC1(filenamelist,validx,validy)
%diary 'cmd_log.txt'
% Code to start actual image correlation
% Programmed by Michael Gro?
% Last revision: 11.02.2022


if exist('filenamelist', 'var')==0
    load('filenamelist')            % file with the list of filenames to be processed
end

[r,c]=size(filenamelist);                   % this will determine the number of images we have to loop through
% Half width of images
BASESIZE = length(mean(double(imread(filenamelist(1,:))),3))/2;
%BASESIZE = 30;
CORRSIZE = 20;
kernel = [BASESIZE-CORRSIZE+1,BASESIZE-CORRSIZE+1,2*CORRSIZE-1,2*CORRSIZE-1]; %% rectangle used to cut out from input

% Open new figure so previous ones (if open) are not overwritten
h = figure();
imshow(filenamelist(1,:))           % show the first image


firstimage  = 1;
validx      = zeros(1,r);
validy      = zeros(1,r);
xcorramps    = zeros(1,r);
base        = mean(double(imread(filenamelist(1,:))),3);
sub_base    = imcrop(base,kernel);

% Start image 
g = waitbar(0,sprintf('Processing images'));        % initialize the waitbar
set(g,'Position',[275,50,275,50]) 

% determine offset by correlating base picture with kernel taken from
% itself
norm_cross_corr_base            = normxcorr2(sub_base,base);
subpixel_base                   = true;
[xpeakb, ypeakb, amplitudeb]    = findpeak_dic(norm_cross_corr_base,subpixel_base);
% [xpeakb, ypeakb]        = findpeak_dic(norm_cross_corr_base,subpixel_base);


% validxOffset = xpeakb;
% validyOffset = ypeakb;


for i=firstimage:(r)               % run through all images
    % load input and crop
    input = mean(double(imread(filenamelist(i,:))),3);
    sub_input = imcrop(input,kernel);

    %perform xcorr
%     norm_cross_corr = normxcorr2(sub_input,base);
    norm_cross_corr = normxcorr2(sub_base,input);
    %cross_corr = xcorr2(sub_input,base);
    subpixel = true;

    [xpeak, ypeak,amplitude] = findpeak_dic(norm_cross_corr,subpixel);
  
    %% Coordinate sign convention applied here
    % The convention is chosen as such, that a !sample-movement
    % 1) to the left (negative x) yields a negative displacement
    % 2) to the bottom (negative y) yields a negative displacement
    % Together with the convention for the vibrometry displacement
    % results, this forms a right handed coordinate system.
    % This convention was formed on 11.02.2022 for the confocal Lab2 setup
    % data can be found in:
    % S:\AG Wegener\MechaMeta\Adaptive Optics\04 Messungen\Setup Characterization\20220211_Calibrate_xy_Axis_directions
%     validx(i)    = -1.*(xpeak-xpeakb);
%     validy(i)    = ypeak-ypeakb; %
    % update 19.9.2022 I exchanged the normxcorr2(sub_input,base) to
    % normxcorr2(sub_base,input). This flips the sign on x and y direction!
    % This way, I ensure that the feature I want to track is always
    % centered in the middle of the kernel (sub_base) and I use the same
    % kernel to search for the feature in any other image. I find this more
    % intuitive.
%     imagesc(input); % for debugging
    validx(i)    =  (xpeak-xpeakb);
    validy(i)    =  -1.*(ypeak-ypeakb); %
    xcorramps(i) = amplitude;
    
    % update the waitbar
    waitbar(i/(r-1))                                                                       
end

validxPath = ['validx.dat'];
writematrix(validx,validxPath,'Delimiter','tab')

validyPath = ['validy.dat'];
writematrix(validy,validyPath,'Delimiter','tab')

xcorrampsPath = ['xcorramps.dat'];
writematrix(xcorramps,xcorrampsPath,'Delimiter','tab')

close(h); close(g);
%     save(validyPath, 'validy', '-ascii', '-tabs');




