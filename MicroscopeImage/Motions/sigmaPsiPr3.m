function [ sigma, psi, pr ] = sigmaPsiPr3( delta, Eyx )

%delta has to be between -180 and 180 degrees
if delta > 180;
   delta = (delta-360);
% elseif delta < -180
%     delta = -(delta+360)
end
epsDelta = 0.01;
epsEyx = 0.01;

if abs(delta)<=epsDelta || abs(delta-180)<=epsDelta % linear polarisiertes Licht
    sigma=1;
    pr=0;
    if abs(delta)<=epsDelta
        psi=atand(Eyx);
    else
        psi=180-atand(Eyx);
    end
elseif abs(abs(delta)-90)<= epsDelta && abs(Eyx-1)<=epsEyx % zirkular polarisiertes Licht
    sigma=sign(delta);
    pr=1;
    psi=0;
else % elliptisch polatisiertes Licht
    sigma=sign(delta);
    if Eyx~=1
        psi=0.5*atand(2*cosd(delta)*Eyx/(1-Eyx^2));
        if Eyx>1
            psi=psi+90;
        else 
            if abs(delta)>90 && abs(delta)<180
                psi=psi+180;
            end
        end
        pr=(sind(psi)^2-Eyx^2*cosd(psi)^2)/(Eyx^2*sind(psi)^2-cosd(psi)^2);
        if pr<0
            disp('Warning: pr<0');
            pr=0;
        end
    else
        if abs(delta)<90
            psi=45;
            pr=(1-cosd(delta))/(1+cosd(delta));
        else
            psi=135;
            pr=(1+cosd(delta))/(1-cosd(delta));
        end
    end
end