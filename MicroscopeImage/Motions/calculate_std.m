clear all ;

xval = importdata('validx.dat') ;
yval = importdata('validy.dat') ;
indecies = 1:length(xval) ;

xmean = zeros(1, length(xval)) ;
x_noise = zeros(length(xval(:,1)), length(xval)) ;

f = figure() ;
subplot(1, 2, 1)
hold on ;
for i = 1:length(xval(:,1))
    fitparam = fit(indecies', (xval(i,:) - xval(i,1))', 'poly1') ;
    xmean = xmean + xval(i,:) - xval(i,1) ;
    plot(fitparam, indecies, xval(i,:) - xval(i,1)) ;
    x_noise(i,:) = (xval(i,:) - xval(i,1))' - fitparam(indecies) ;
end
xlabel('picture number')
ylabel('Displacement in pixel')
title([{'Displacement for each' 'point of interest'}])
hold off
xmean = xmean / length(xval(:,1)) ;

sigma = std(x_noise') ;
sigma_res = sqrt(sum(sigma.^2)) / length(xval(:,1)) ;
legend off ;
pbaspect([1 1 1]) ;

subplot(1, 2, 2)
% figure()
plot(x_noise', 'o')
xlabel('picture number')
ylabel('Displacement in pixel')
grid on
title([{'Displacement around' 'mean value'} ['\sigma = ' num2str(round(sigma_res,4)) 'px']])
pbaspect([1 1 1]) ;

saveas(f, 'noise_std_2.pdf')
