%% my standard script to run xcorr analysis
clear all;
close all;
% set flags
flag_do_FFT         = 0;
flag_show_scatter   = 0;
%%

filelist_generator_tobi;
addpath('Digital Image Correlation','-end')
%%
automateDIC2;
%%
InputRange  = 1000; %mV
f0          = 5e3; %Hz %excitation frequency
currF       = f0;

I = double(imread('PIC00000.tif') - 2^15) * InputRange / 2^15; %mV

FOV         = 30; %um
pixelanzahl = 60;
pixelsize   = FOV/pixelanzahl;
ROI         = 40; %px

x_grid = importdata('grid_x.dat') * pixelsize;
y_grid = importdata('grid_y.dat') * pixelsize;  

x = (0:(pixelanzahl-1)) * pixelsize + pixelsize/2;
y = (0:(pixelanzahl-1)) * pixelsize + pixelsize/2;
y = fliplr(y);  % important, since imagesc flips y-axis.. 
                % otherwise image appears upside down!

A = importdata("validx.dat") * pixelsize;
B = importdata("validy.dat") * pixelsize;

samplerate = 1e5;
rescale = 1000;

n           = length(A(1,:));
Nmarkers    = length(A(:,1));
t           = (0:n-1) * 1/samplerate;

A_plot = zeros(length(A(1,:)), length(A(:,1)))';
B_plot = zeros(length(B(1,:)), length(B(:,1)))';

for i = 1:length(A(:,1))
     A_plot(i,:) = (A(i,:) - mean(A(i,:))) * rescale + mean(A(i,:));
     B_plot(i,:) = (B(i,:) - mean(B(i,:))) * rescale + mean(B(i,:));
end

% A_plot1 =  A_plot(1,:) - mean(A_plot(1,:));
% B_plot1 =  B_plot(1,:) - mean(B_plot(1,:));

figure(1)
clf
hold on
colormap('gray')
imagesc(x, y, I)
title("Displacements");
xlim([0 FOV])
ylim([0 FOV])
xlabel(['x-Coordinate in ',char(181),'m'])
ylabel(['y-Coordinate in ',char(181),'m'])
hc=colorbar;
title(hc,'mV');
pbaspect([1 1 1])

% Plot data
for i = 1:length(x_grid)
    rectangle('Position',[x_grid(i)-ROI/2*pixelsize FOV-y_grid(i)-ROI/2*pixelsize ROI*pixelsize ROI*pixelsize], 'EdgeColor','blue', 'linewidth', 3)
end

for i = 1:length(A(:,1))
    plot(A_plot(i,:)+FOV/2, B_plot(i,:)+FOV/2, 'yellow', 'linewidth', 2)
end

% Fit and plot
phi     = zeros(Nmarkers,1); % Store tilt of ellipse
sigmas  = zeros(Nmarkers,1); % Store rotation direction
xTraj   = zeros(1,5); % store fit result for x-trajectory
yTraj   = zeros(Nmarkers,5); % store fit result for y-trajectory

for i = 1:Nmarkers
u = A(i,:);
v = B(i,:);
% Fit trajectory and store results
xRes = sineFit(t,u,0); xTraj(i,:) = xRes;
yRes = sineFit(t,v,0); yTraj(i,:) = yRes;
% calculate phase difference and amplitude ratio
delta       = (yRes(4) - xRes(4)) * 180/pi;
AmpRatio    = yRes(2)/xRes(2);
% Determine ellipse orientation
[sigma,psi,pr]  = sigmaPsiPr3(delta,AmpRatio);
phi(i)          = psi;
sigmas(i)       = sigma;
% plot trajectories
% plot(xRes(1)+rescale*xRes(2)*sin(2*pi*xRes(3)*t+xRes(4))+FOV/2,...
%         yRes(1)+ rescale*yRes(2)*sin(2*pi*yRes(3)*t+yRes(4))+FOV/2,...
%         'r','Linewidth',1.5)
% xFT = xRes(1)+xRes(2)*sin(2*pi*xRes(3)*t+xRes(4));
% yFT = yRes(1)+yRes(2)*sin(2*pi*yRes(3)*t+yRes(4));
end

% % check if rotation flipped over markers
% if (abs(sum(sigmas)) ~= Nmarkers)
%     fprintf(['Warning! Possible rotation flip in Marker ',num2str(i),' detected!\n'])
% end

% % check if angle flipped over markers
% for i = 1:Nmarkers
%     if (phi(i) >= (mean(phi)+90))
%     fprintf(['Warning! Possible angle flip in Marker ',num2str(i),' detected!\n'])
%     fprintf(['Angle corrected by 180 degree.\n'])
%     phi(i) = phi(i)-180;
%     end
% end

% write parameters into graph
% text(5,4, ['Mean angle = ', num2str(mean(phi)) ,char(176)],'Color','red')
% text(5,4, ['Angle std = ', num2str(std(phi)),char(176)],'Color','red')
% text(5,2, ['Rescaled amplitude by ', num2str(rescale)],'Color','red')
set(gca,'FontSize',16,'LineWidth',1)
saveas(gcf,[pwd,'/Result Plots/2D_Plot.png']) 
hold off

%%
figure(2)
hold on
for c = 1:length(A(:,1))
%     plot(1e6*t,1e3*(A(c,:) - mean(A(c,:))),'x-',...
%         1e6*t,1e3*(B(c,:) - mean(B(c,:))),'+-')
    plot(1e6*t,1e3*(A(c,:)),'-',...
         1e6*t,1e3*(B(c,:)),'-')
%     xlim([0 100])
%     ylim([-25 25])
    xlabel(['Time in ',char(181),'s'])
    ylabel("Displacement in nm")
    legend('x disp.', 'y disp.','Location','best')
    title(['Marker No ',num2str(c)])
    text(5,45, ['Angle phi =  ', num2str(phi(c)),char(176)],'Color','black')
    set(gca,'FontSize',16,'LineWidth',1)
    grid on; 
    box on;
    saveas(gcf,[pwd,'/Result Plots/displ_marker_no_',num2str(c),'.png'])  
end
hold off
%%
if flag_show_scatter == 1
    figure(3)
    clf
    hold on;
    box on; grid on;
    pbaspect([1 1 1])
    for j = 1:length(A(:,1))
        sz = 25;
        c = 1:length((A(j,:)));
        scatter(1e3*A(j,:),1e3*B(j,:),sz,c)
        cbar = colorbar;
        cbar.Label.String = 'Datapoints';
    end
    xlim([1.05*min(1e3*A) 1.05*max(1e3*A)]); 
    ylim([1.05*min(1e3*B) 1.05*max(1e3*B)]); 
    title('Scatter plot of datapoints')
    xlabel('x-Displacement in nm'); ylabel('y-Displacement in nm');
    hold off;
    saveas(gcf,[pwd,'/Result Plots/scatter_plot.png'])
end
%%
% figure(12345)
% theta = 45.24;
% rotMatP = [cosd(theta) sind(theta); -sind(theta) cosd(theta)]
% 
% data = [A(1,:)-mean(A(1,:)) ; B(1,:)-mean(B(1,:))];
% rotData = rotMatP * data;
% 
% plot(1e6*t,1e3*rotData(1,:),1e6*t,1e3*rotData(2,:))


%%
% figure(2*Nmarkers+10)
% hold on;
% for c = 1:Nmarkers
%     plot(t,1e3*(A(c,:) - mean(A(c,:))),'.')
%     %text(5,45, ['Angle phi =  ', num2str(phi(c)),char(176)],'Color','black')
% end
% %xlim([0 100])
% ylim([-300 300])
% xlabel(['Time in s'])
% ylabel("Displacement in nm")
% %legend('x displacement', 'y displacement','x Fit','y Fit','Location','best')
% title(['x-Data from all markers'])
% hold off;
% saveas(gcf,[pwd,'/Result Plots/all_x.png'])
% 
% figure(2*Nmarkers+11)
% hold on;
% for c = 1:Nmarkers
%     plot(        t,1e3*(B(c,:) - mean(B(c,:))),'.')
%     %text(5,45, ['Angle phi =  ', num2str(phi(c)),char(176)],'Color','black')
% end
% %xlim([0 100])
% ylim([-300 300])
% xlabel(['Time in s'])
% ylabel("Displacement in nm")
% %legend('x displacement', 'y displacement','x Fit','y Fit','Location','best')
% title(['y-Data from all markers'])
% hold off;
% saveas(gcf,[pwd,'/Result Plots/all_y.png'])

%%
% Do FT but with a frequency resolution that includes 150kHz (excitation f)
% e.g. for 10MSps and Tpx = 1ms, one finds 10/9999 MHz/bin = 1.0001 kHz/bin
% by simply only using 5000 data points from A and B instead of 9999
% one finds 2kHz/bin 
if flag_do_FFT == 1
    n2 = 1000;
    %n2 = 4000; % for 10MSps sample rate
    %n2 = 10000; % for 25MSps sample rate
    %n2 = 10000; % for 25MSps sample rate
    f2 = (-n2/2 : n2/2-1) * samplerate/n2;

    f2_single = samplerate*(0:(n2/2))/n2;

    peakRatioX = 0; % ratio of zero order peak to first harmonic peak
    peakRatioY = 0;

    idx0 = find(f2_single == f0);
    idx1 = find(f2_single == 2*f0);
end


%%

% for c = 1:length(A(:,1))
%     %subtract DC peak
%     A2 = A(c,1:n2) - mean(A(c,1:n2));
%     B2 = B(c,1:n2) - mean(B(c,1:n2));
% 
%     x_FT2 = fft(A2);
%     y_FT2 = fft(B2);
%     
%     % single sided amplitude spectrum according to
%     % https://de.mathworks.com/help/matlab/ref/fft.html
%     
%     x_FT2_single = x_FT2(1:n2/2+1);
%     x_FT2_single(2:end-1) = 2.*x_FT2_single(2:end-1);
%     y_FT2_single = y_FT2(1:n2/2+1);
%     y_FT2_single(2:end-1) = 2.*y_FT2_single(2:end-1);
% 
%     x_FT2 = fftshift(x_FT2);
%     y_FT2 = fftshift(y_FT2);
%     % 
%     figure(3)
%     %             plot(f2, abs(x_FT2),f2,abs(y_FT2));
%     plot(f2_single, abs(x_FT2_single/n2*1e3),f2_single,abs(y_FT2_single/n2*1e3),'Linewidth',1.5);
%     title(['Spectrum Marker No ',num2str(c)])
%     grid on;
%     box on;
%     if(currF == f0)
%         peakRatioX = abs(x_FT2_single(idx0))/abs(x_FT2_single(idx1)); 
%         peakRatioY = abs(y_FT2_single(idx0))/abs(y_FT2_single(idx1));
% %         text(f0*1.1,2, ['transv: A(f0)/A(f1) = ', num2str(peakRatioX)],'Color','black')
% %         text(f0*1.1,1, ['long: A(f0)/A(f1) = ', num2str(peakRatioY)],'Color','black')                    
%     end
%     xlim([0 800e3])
%     ylim([0 10])
%     %xlim([0 600e3])
%     ylabel("FFT amplitude arb. u.")
%     xlabel("Frequency in Hz")
%     legend('x', 'y','Location','best')
%     set(gca,'FontSize',16,'LineWidth',1)
%     saveas(gcf,[pwd,'/Result Plots/','Spectr_No_',num2str(c),'.png'])
% end
% 




%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% plot xCorr amplitudes
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%close all;

load('filenamelist.mat')
xcorramps = load('xcorramps.dat');
fnlistsize  = size(filenamelist);
Nfiles      = fnlistsize(1);
Npoints     = Nmarkers*Nfiles;


sampleTimes = (0:(Nfiles-1)) .* 1/samplerate;

%%
figure(2*Nmarkers+20)
legendstring = {};
hold on
box on; grid on;
for i = 0:Nmarkers-1
    plot(sampleTimes*1e6,xcorramps,'x-','Linewidth',1.5)
    title('Amplitudes of cpcorr for all pictures')
    %ylim([0.85 1])
    ylim([0.85 1.05])
    xlim([0 100])
    xlabel(['Time in ',char(181),'s'])
    ylabel('Xcorr Amplitude')
    legendstring = [legendstring, ['Marker number ',num2str(i+1)]];
end
legend(legendstring, 'location', 'southeast')
set(gca,'FontSize',16,'LineWidth',1)
hold off

saveas(gcf,[pwd,'/Result Plots/xcorr_amplitude_plot.png'])
%%
% amplitudes = zeros(Nmarkers,length(sampleTimes));
% for i = 1:Nmarkers
%     amplitudes(i,:) = xcorramps(end-(Npoints-1)+i-1:Nmarkers:end)-mean(xcorramps(end-(Npoints-1)+i-1:Nmarkers:end));
% end
% 
% figure(99)
% plot(sampleTimes,amplitudes(1,:))

%%
% FT
% n = length(sampleTimes)-9; 
% freq = (-n/2:n/2-1) * fsample/n;
% 
% fft_amplitudes = zeros(Nmarkers,n);
% for i = 1:Nmarkers
%     fft_amplitudes = fftshift(fft(amplitudes(i,1:end-9)));
% end
% 
% 
% figure(98)
% plot(freq,abs(fft_amplitudes(1,:)))
% xlim([0 0.55e6])
% text(10000,23, ['Sampling frequency was  10 MHz'],'Color','black')



%close all;

