function filelist_generator_tobi
listing = dir;
filenamelist = [];

for i = 1:length(listing)% Gehe alle Elemente in Ordner durch
    if length(listing(i).name) > 6 % checke alle Elemente mit mehr als 6 Symbolen im Dateinamen
        if listing(i).name(1:3) == 'PIC'  & listing(i).name(end-2:end) == 'tif' % identifiziere PICXXXX.tif Files
      
            filenamelist = [filenamelist; listing(i).name];
            %H?nge Name der Bilddatei an Liste an
            
        end
    end
end

save('filenamelist.mat', 'filenamelist')
%Speichere Liste in Datei ab

end