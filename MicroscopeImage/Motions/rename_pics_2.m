clear all;

n = 5000;

for i = 0:n-1
   oldname = [num2str(i) '.tif']; 
   newname = ['PIC' num2str(i,'%04d') '.tif'];
   movefile(oldname,newname)
end