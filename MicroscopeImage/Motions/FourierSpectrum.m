clear all;
close all;

A = importdata("validx.dat") ;
B = importdata("validy.dat") ;

sampleRate = 10e6;
%sampleRate = 25e6;

pixelsize = 120e-6/150;

Index = int16(length(A(:,1))/2);

x = A(1,:);
y = B(1,:);

ttt = (0 : length(x)-1) * 1/sampleRate;
x = x - mean(x);
y = y - mean(y);


figure(1); 
plot(ttt,x*pixelsize,ttt,y*pixelsize)
pbaspect([1 1 1])

figure(2);
plot(x*pixelsize,y*pixelsize)
pbaspect([1 1 1])
xlim([-25e-9 25e-9]);
ylim([-25e-9 25e-9]);


n = length(x);

x_FT = fftshift(fft(x));
y_FT = fftshift(fft(y));

f = (-n/2 : n/2 - 1) * sampleRate / n;

figure(3)
plot(f/1000, abs(x_FT),f/1000, abs(y_FT));
legend('x-Component','y-Component')
xlim([0 250])
xlabel("Frequency in kHz")
ylabel("Amplitude of y component")
saveas(gcf,'Spectrum.png')
pbaspect([1 1 1])

fcut = 500e3;

x_FT_cut = x_FT;
x_FT_cut(abs(f) > fcut) = 0;

y_FT_cut = y_FT;
y_FT_cut(abs(f) > fcut) = 0;


x_cut = real(ifft(ifftshift(x_FT_cut)));
y_cut = real(ifft(ifftshift(y_FT_cut)));

figure(4); 
plot(ttt,x_cut*pixelsize,ttt,y_cut*pixelsize)
xlabel("Time in s")
ylabel("Displacement in m")
xlim([0 .5e-3])
title("Lowpass filtered (<500kHz)")
legend("x-Component", "y-Component")
saveas(gcf,'Displacement_fast.png')

figure(8); 
plot(ttt,x_cut*pixelsize,ttt,y_cut*pixelsize)
xlabel("Time in s")
ylabel("Displacement in m")
title("Lowpass filtered (<500kHz)")
legend("x-Component", "y-Component")
saveas(gcf,'Displacement_slow.png')

figure(5);
plot(x_cut*pixelsize,y_cut*pixelsize)
pbaspect([1 1 1])
xlim([-25e-9 25e-9]);
ylim([-25e-9 25e-9]);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% flower = 22e3; %Hz
% fupper = 25e3; %Hz
% filterbool = and((abs(f) > flower),(abs(f) < fupper));

x_FT_cut(abs(abs(f)-23e3)<= 1e3) = 0; %2kHz bandwidth truncated around 23kHz
y_FT_cut(abs(abs(f)-23e3)<= 1e3) = 0;

x_cut = real(ifft(ifftshift(x_FT_cut)));
y_cut = real(ifft(ifftshift(y_FT_cut)));

figure(6); 
plot(ttt,x_cut*pixelsize,ttt,y_cut*pixelsize)
pbaspect([1 1 1])

figure(7);
plot(x_cut*pixelsize,y_cut*pixelsize)
pbaspect([1 1 1])
xlim([-25e-9 25e-9]);
ylim([-25e-9 25e-9]);
