// file ImageMath.cpp
// performs some math calculations on images
// author: Rudi Weinacker
// Version: 2020:08:11 13:54

#include <tmmintrin.h>
#include "ImageMath.h"

#ifndef WORD
typedef unsigned short WORD;
#endif

void NImageMath::shiftPhase ( BYTE* arrout , const BYTE* arrin1 , const BYTE* arrin2 , int num , double wavelengthcorr )
{
	const __m128i* Input1 = ( const __m128i* ) arrin1 ;
	const __m128i* Input2 = ( const __m128i* ) arrin2 ;
	__m128i* Output = ( __m128i* ) arrout ;
	const int NumIterations = num / 16 ;

	const __m128i MaskLo = { -1 , 0 , -1 , 0 , -1 , 0 , -1 , 0 , -1 , 0 , -1 , 0 , -1 , 0 , -1 , 0 };

	const WORD WaveLengthFixScalar = ( WORD ) ( wavelengthcorr * 32768.0 ) ;
	const __m128i WaveLength = _mm_set1_epi16 ( WaveLengthFixScalar ) ;

	for ( int i = 0; i < NumIterations; i++ )
	{
		// load input
		__m128i V1 = Input1 [ i ] ;
		__m128i V2 = Input2 [ i ] ;

		// add up with overflow
		__m128i Sum = _mm_add_epi8 ( V1 , V2 ) ;

		// we use 16 bit operations, so we have to split into two parts
		__m128i Sum_Lo = _mm_and_si128 ( Sum , MaskLo ) ; // lower 8 bits
		__m128i Sum_Hi = _mm_srli_epi16 ( Sum , 8 ) ; // higher 8 bits, shifted to right

		// multiplication and round and shift, assuming 15 bit fixcomma
		__m128i PhaseCorr_Lo = _mm_mulhrs_epi16 ( Sum_Lo , WaveLength ) ;
		__m128i PhaseCorr_Hi = _mm_mulhrs_epi16 ( Sum_Hi , WaveLength ) ;

		// reconstruct output with two splitted values using left shift and bitwise or
		__m128i Result = _mm_or_si128 ( PhaseCorr_Lo , _mm_slli_epi16 ( PhaseCorr_Hi , 8 ) ) ;

		// store out
		Output [i] = Result ;
	}
}

//;/* comment this line out for activating test

//static bool doTest ()
//{
//	__declspec(align(16)) const BYTE Arr1 [] = { 1 , 2 , 3 , 4 , 5 , 6 , 7 , 8 , 9 , 100 , 110 , 120 , 130 , 140 , 150 , 160 } ;
//	__declspec(align(16)) const BYTE Arr2 [] = { 1 , 2 , 3 , 4 , 5 , 6 , 7 , 8 , 9 , 100 , 110 , 120 , 130 , 140 , 150 , 160 } ;
//	__declspec(align(16)) BYTE Result [ 16 ] ;
//	const double WaveLengthCorr = 0.321 ;
//
//	NImageMath::shiftPhase ( Result , Arr1 , Arr2 , 16 , WaveLengthCorr ) ;
//	return true ;
//}
//
//static const bool bTest = doTest () ;

// */
