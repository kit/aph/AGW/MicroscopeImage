#pragma once
#include "plotter.h"
#include <vector>
#include <iostream>
#include <chrono>
#include <thread>
#include <fstream>



/*
 plotter class that accepts a pointer to data from an ADC
 to show it in a gnuplot plot. To do so it should convert the data into a vector, 
 bc that is what gnuplot seems to work with.
*/

plotter::plotter()
{
	const char defGnuPath[] = "\"C:\\Program Files\\gnuplot\\bin\\gnuplot.exe\"";
	//ptr_gp = new Gnuplot(defGnuPath);
	ptr_gp.reset(new Gnuplot(defGnuPath));
	*ptr_gp << "set term qt noraise\n";
}


/*
This default constructor remains here for me as a reminder why I chose to work
with a pointer (ptr_gp) to a heap-allocated object of the Gnuplot class . Using 
a member-initializer list, as shown below, means that I have to hard-code the 
path to  where Gnuplot is located on the system. If the path changes, e.g. on 
another  machine, I will have to change the code of the plotter class. I am aware
that this is not too much effort, literally copying the new path here, but I 
still decided against it for "educational" purposes and so I have an example 
where an initializer list can be useful.
(Also I have no clue to what would be standard procedcure..)
*/
//plotter::plotter() :mygp("\"C:\\Program Files\\gnuplot\\bin\\gnuplot.exe\"")
//{
//	mygp << "set term qt noraise\n";
//}



plotter::plotter(const char* gnuPath)
{
	ptr_gp.reset(new Gnuplot(gnuPath));
	*ptr_gp << "set term qt noraise\n";
}

plotter::plotter(const char* gnuPath, int dataSize)
{
	ptr_gp.reset(new Gnuplot(gnuPath));
	*ptr_gp << "set term qt noraise\n";
	dataVec.reserve(dataSize);
	for (int i = 0; i < dataSize; i++)
	{
		dataVec.push_back(0);
	}

}


plotter::~plotter()
{
	dataVec.clear(); 
	dataVec.shrink_to_fit();
	std::cout << "Plotter died lol.." << std::endl;
}

bool plotter::set_dataSize(int dataSize)
{
	dataVec.resize(dataSize);
	//dataVec.reserve(dataSize);
	//for (int i = 0; i < dataSize; i++)
	//{
	//	dataVec.push_back(0);
	//}
	return 0;
}

bool plotter::do_loadData(int* dataPtr, int dataSize)
{
	if (dataPtr == NULL)
	{
		std::cout << "data pointer is null pointer\n";
		return 1;
	}

	dataVec.reserve(dataSize);
	for (int i = 0; i < dataSize; i++)
	{
		dataVec.push_back(dataPtr[i]);
	}
	return 0;
}

bool plotter::do_loadData(int16* dataPtr, int dataSize)
{
	if (dataPtr == NULL)
	{
		std::cout << "data pointer is null pointer\n";
		return 1;
	}

	dataVec.reserve(dataSize);
	for (int i = 0; i < dataSize; i++)
	{
		dataVec.push_back(((int)dataPtr[i]));
	}
	return 0;
}


bool plotter::do_updateData(double* dataPtr)
{
	if (dataVec.empty() || dataVec.capacity() == 0){
		std::cout << "Data size not specified!\n";
		return 1;
	}

	for (int i = 0; i < dataVec.size(); i++)
	{
		dataVec[i] = dataPtr[i];
	}
	return 0;
}

bool plotter::do_updateData(int16* dataPtr)
{
	if (dataVec.empty())
	{
		std::cout << "Data size not specified!\n";
		return 1;
	}

	for (int i = 0; i < dataVec.size(); i++)
	{
		dataVec[i] = ((double)dataPtr[i]);
	}
	return 0;
}

bool plotter::do_updateData(std::vector<float>& inpDataVec)
{
	if (dataVec.empty())
	{
		std::cout << "Data size not specified!\n";
		return 1;
	}

	for (int i = 0; i < dataVec.size(); i++)
	{
		dataVec[i] = ((double)inpDataVec[i]);
	}
	return 0;
}

bool plotter::do_showData()
{
	if (dataVec.empty() or ptr_gp == NULL)
	{
		std::cout << "No data loaded\n";
		return 1;
	}

	//*ptr_gp << "set term qt noraise\n";
	*ptr_gp << "plot '-' with linespoints title 'data' \n";
	//*ptr_gp << "plot '-' \n";

	ptr_gp->send1d(dataVec);
	return 0;
}


bool plotter::do_showLegend(bool showL)
{
	if (showL)
		*ptr_gp << "set key \n";
	else
		*ptr_gp << "unset key\n";
	return 0;
}


bool plotter::do_setPlotRanges(const int xMin, const int xMax, const int yMin, const int yMax)
{
	xRange = "[" + std::to_string(xMin) + ":" + std::to_string(xMax) + "]";
	yRange = "[" + std::to_string(yMin) + ":" + std::to_string(yMax) + "]";

	*ptr_gp << "set xrange " << xRange << "\n";
	*ptr_gp << "set yrange " << yRange << "\n";

	return 0;
}

bool plotter::do_setPlotXrange(const int xMin, const int xMax) {
	xRange = "[" + std::to_string(xMin) + ":" + std::to_string(xMax) + "]";
	*ptr_gp << "set xrange " << xRange << "\n";
	return 0;
}

bool plotter::do_setPlotYrange(const int yMin, const int yMax) {
	yRange = "[" + std::to_string(yMin) + ":" + std::to_string(yMax) + "]";
	*ptr_gp << "set yrange " << yRange << "\n";
	return 0;
}

bool plotter::do_setTitle(const std::string& plotTitle)
{
	*ptr_gp << "set title '" << plotTitle << "'\n";
	return 0;
}

bool plotter::do_sendCMD(const std::string& command)
{
	if (command.back() != '\n') {
		std::cout << "Command not newline terminated! Exiting\n";
		return 1;
	}
	std::string locCMD = command;
	*ptr_gp << locCMD;
	//std::cout << "command send was " << locCMD << std::endl;
	return 0;
}
