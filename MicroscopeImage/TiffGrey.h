

// simple tiff reader-writer for grey bitmaps (8 or 16 bit)
// Author: Rudi Weinacker
// 1. Version 07.05.2020
// Rev. 05.08.2020:
//  - now able to read tiff images, with:
//    - up to 5000000 bytes per file
//    - no compression
//    - not tiled, just single or multiple strips
//    - only 8 or 16 bit grey
//    - palettes are ignored
//  - Attributes added
//  - Timestamp added. Every call of saveData(...) updates timestamp

enum ETiffAttributeID : unsigned short
{
	ID_FieldOfView     = 0 ,
	ID_PixelSize       = 1 ,
	ID_InputRange      = 2 ,
	ID_SamplingRate    = 3 ,
	ID_InputImpedance  = 4 ,
	ID_MeasurementMode = 5 ,
	ID_MeasurementTime = 6 ,
	ID_APDGain         = 7 ,
	ID_ExcitationFreq  = 8 ,
	ID_xPos			   = 9 ,
	ID_yPos			   = 10 ,
	ID_zPos			   = 11 ,
	ID_NumDefinitions  = 12 ,
};

enum ETiffAttributeType : unsigned short
{
	T_Unknown = 0 ,
	T_Double ,
	T_Ascii ,
} ;

enum TIFFTag_Attribute : unsigned short
{
	TT_StartNumber      = 65000 , // benutzerdefinierbarer Bereich, 65000 - 65535
	TT_FieldOfView      = TT_StartNumber + ID_FieldOfView     ,
	TT_PixelSize        = TT_StartNumber + ID_PixelSize       ,
	TT_InputRange       = TT_StartNumber + ID_InputRange      ,
	TT_SamplingRate     = TT_StartNumber + ID_SamplingRate    ,
	TT_InputImpedance   = TT_StartNumber + ID_InputImpedance  ,
	TT_MeasurementMode  = TT_StartNumber + ID_MeasurementMode ,
	TT_MeasurementTime  = TT_StartNumber + ID_MeasurementTime ,
	TT_APDGain          = TT_StartNumber + ID_APDGain         ,
	TT_ExcitationFreq   = TT_StartNumber + ID_ExcitationFreq  ,
	TT_xPos				= TT_StartNumber + ID_xPos			  ,
	TT_yPos				= TT_StartNumber + ID_yPos			  ,
	TT_zPos				= TT_StartNumber + ID_zPos			  ,
};

class CTiffGrey
{
	struct SImpl ;
	SImpl* const m_pImpl ;
public:
	~CTiffGrey () ;
	CTiffGrey ( const char* filename ) ; // read
	CTiffGrey ( int width , int length ) ; // write
	// common methods
	bool isValid () const ;
	// reader methods
	int getBPP () const ;
	int getNumBytesPerPixel () const ;
	int getNumPixelsX () const ; // i.e. image width
	int getNumPixelsY () const ; // i.e. image length
	int getBufferSize () const ; // size needed for getData(void* buffer) 
	bool getData ( void* buffer ) const ; // size can be recieced with getBufferSize()
	ETiffAttributeType hasAttribute ( ETiffAttributeID id ) const ; // T_Unknown if not available
	double getAttributeValue ( ETiffAttributeID id ) const ; // 0 if doesn't match
	const char* getAttributeString ( ETiffAttributeID id ) const ; // 0 if doesn't match
	const char* getImageDescription () const ; // 0 if not available
	// writer methods
	void setBitmapSize ( int width , int height ) ; // only needed for changing
	void setTopDown ( bool td ) ; // optional, default is true
	void setBPP ( int bpp ) ; // allowed are 8 and 16. Optional, default is 16
	void setAttribute ( ETiffAttributeID id , const char* label , const char* value ) ;
	void setAttribute ( ETiffAttributeID id , const char* label , double value , int numdecimals = 3 ) ;
	void deleteAttributes () ; // deletes all attributes and description string
	bool saveData ( const void* buffer , const char* filename ) ; // data must be contigous without any gap
};

