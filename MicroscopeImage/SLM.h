// Header for SLM-class

#pragma once
#include <opencv2/core.hpp>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/highgui.hpp>
#include <fstream>
#include <iostream>
#include <string>

class SLM
{

private:
    int width, height;
    int segWidth, segHeight;
    cv::Mat* segments;

    cv::Mat flatCorr; // Phase map to correct for surface warp of slm active area
    cv::Mat grating; //Baseline grating to separate 0th order from 1st order beam
    cv::Mat gratAndFlatCorr; // holds sum%256 of flatCorr and grating
    cv::Mat phaseMask; // Holds the phase map for aberration correction
    cv::Mat image; //phase map that eventually is displayed on the slm (sum%256 of above matrices)
    cv::Mat matToSegment; // Matrix used for segmenting
    int LUTwavelengthCorr; // Correction value to convert grayscale to actual phase value at given wavelength by Hamamatsu LUT
    //flags: (all set to false by default in constructor)
    bool corrFileloaded; //Flag set if flattness correction file was loaded
    bool gratingLoaded;//Flag set if grating file was loaded
    bool gratAndFlatCorrExists; // Flag set if gratAndFlatCorr exists
    bool imageExists; // see above..

public:
    SLM(); // Default constructor
    //SLM(int a, int b); // Constructor for variable display resolution
    bool debugThisSLM(); // function for debugging
    bool setFlatCorr(std::string flatFileName); // Sets grating to separate 0th order from 1st order beam
    bool clearFlatCorr(); // Clears current flatness correction file.
    bool setGrating(std::string gratingName); // Sets grating to separate 0th order from 1st order beam
    bool clearGrating(); // Clears current grating.
    bool calcGratPlusCorr(); // generates gratAndFlatCorr matrix
    bool clearGratAndFlatCorr(); //Clears gratAndFlatCorr
    bool sumMasksToImage(cv::Mat sum1, cv::Mat sum2, bool wavelengthCorr = true); // sums up phase masks to fill cv::Mat image
    bool genAndDispImag(); // 1.Clears image 2.gratAndFlattCor + phaseMask = image 3. displays image 
    bool dispPhaseMaskFull(std::string imageName); //Displays phase mask in fullscreen mode
    bool savePhaseMask(std::string filename); // Safes PhaseMask as .bmp file
    bool clearPhaseMask(); // Clears phase mask i.e. resets to zeros array so new mask can be loaded
    bool clearImage(); // Clears image matrix - as above
    bool clearAllMasks(); // Clears alls phase masks - as above
    bool closeAllWindows(); //closes open windows 

    bool genMatSegment(); // Testing the segentation of a matrix i.e. getting to know openCV
    bool setSegmentValue(unsigned char newGrayVal, int segmNumber = 0); // Circle individual segments of SLM 

    int getWidth();
    int getHeight();
    int getSegWidth();
    int getSegHeight();

    int getNumSegments();
};

