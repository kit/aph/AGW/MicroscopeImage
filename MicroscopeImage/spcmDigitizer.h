#pragma once
// ----- include standard driver header from library -----
#include "spcm_c_header/dlltyp.h"
#include "spcm_c_header/regs.h"
#include "spcm_c_header/spcerr.h"
#include "spcm_c_header/spcm_drv.h"

// ----- include of common example librarys -----
#include "spcm_common/spcm_lib_card.h"
#include "spcm_common/spcm_lib_data.h"
#include "spcm_common/spcm_lib_thread.h"

#include "spcm_common/ostools/spcm_oswrap.h"
#include "spcm_common/ostools/spcm_ostools.h"

// ----- standard c include files -----
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <string>

#define SPCM_FILENAME "Single_ascii_test.txt"
#define SPCM_METAFILENAME "Single_ascii_META.txt"


class spcmDigitizer
{
	//////////////////////////////////////////////////////////////////////////////////////////
	// Functions
public:
	//constructs spcmDigitizer class object setting cardIndex = 0
	spcmDigitizer();
	//constructs spcmDigitizer class object with index = cIndx
	spcmDigitizer(int cIndx);
	//constructs spcmDigitizer class object with index = cIndx and connects the card if connectCard = true
	spcmDigitizer(
		int cIndx,
		bool connectCard);

	~spcmDigitizer();


	void initParams();

	//opens the card driver 
	void openCard();
	//closes the card driver
	void closeCard();
	//closes and opens the card driver
	void resetDigitizer();

	//samples = totalMem / 2byte, pre-trigger samples = (totalMem-postTrgMem)/2byte
	int confDatasize(
		uint64 totalMem,
		uint64 postTrgMem);

	// set samplerate in MHz
	void confSamplerate(
		uint64 smplrt);

	void confSampling(
		uint64 smplrt, 
		uint64 totalMem, 
		uint64 postTrgMem);

	//confChannel: samples = totalMem / 2byte, pre-trigger samples = (totalMem-postTrgMem)/2byte
	void confChannel(
		uint64 chnl,
		int32 acqPath,
		int32 inputRange,
		bool term50or1M,		//(1 <-> 50 Ohm, 0 <-> 1MOhm)
		bool ACorDC,			//(1 <-> AC, 0 <-> DC)
		bool bwLim,
		bool diffInput);

	//confChannel: samples = totalMem / 2byte, pre-trigger samples = (totalMem-postTrgMem)/2byte
	void confChannel(
		uint64 chnl,
		int32 acqPath,
		int32 inputRange,
		bool term50or1M,	//(1 <-> 50 Ohm, 0 <-> 1MOhm)
		bool ACorDC);		//(1 <-> AC, 0 <-> DC)

	int prepareBuffer();

	void confSoftTrigger();

	void confChanTrigger(
		int trigSrc, 
		int trigSlope, 
		int trgLv0, 
		int trgLv1 = 0, 
		bool trgOut = false);

	void confExtTrigger(
		int extMode,		// sets trigger mode to rising (SPC_TM_POS <-> 1) or falling edge (SPC_TM_NEG <-> 2)
		bool trgTerm = false, // sets Triger input termination to 50 Ohm (true) or HighZ (false). HighZ as default since trigger line works atm at HighZ
		int trgLv0 = 1500,	// first trigger level (e.g. for SPC_TM_POS trigger occurs when passing trgLv0 from below to above)
		int trgLv1 = 800,	// second trigger level
		int trgExtLine = 0); // Choses EXT0 (0) or EXT1 (1) as trigger source 
	//(trigger input coupling set to DC)


	// arms the card to acquire data depending ont he choice of trigger engine
	// !!! Attention, this method also allocates data !!!
	int measureSingleRecord(
		uint32 timeOut);

	// this version performs the measurement but clears the buffer afterwards!
	// "getNsamples" are written to the pointer "data".
	int measureSingleRecord(
		uint32 timeOut, 
		int16* data, 
		int getNsamples);

	int measureSingleRecord(
		uint32 timeOut,
		double* data,
		int getNsamples);

	int measureSingleRecordVolt(
		uint32 timeOut,
		double* data,
		int getNsamples,
		int32 lChannel = 0);

	bool freeUpTheBuffer();

	uint64 get_recordLength();

	//2do..?
	//uint64 get_inputRange(int chnl); 

	const int16* get_DataPtr();

	const int16* get_DataPtr(int& Nsamples);

	bool get_DataPtr_s( // pass pointer to pointer
		const int16** data_ptr_ptr, 
		int &Nsamples);

	bool get_DataPtr_s( // pass reference to pointer
		const int16*& data_ptr_ref,
		int& Nsamples);

	// converts SINGLE value into voltage value in mV. 
	// Based on dSpcMIntToVoltage(..) function but without the return(value / 1000) division..
	// return value should be in mV as is.. uses input range of selected lChannel for conversion
	double ConvSingleValueToVoltage(
		double dValue,
		int32 lChannel); 


	bool ConvRecordValuesToVoltage(
		const int16*	dValues,
		int				Nsamles,
		int32			lChannel,
		double*			results);

	void safeASCIItoDisk(void* data = NULL);
	void safeASCIItoDisk(
		const std::string& metaf,
		const std::string& dataf,
		void* data = NULL);
	void safeASCIItoDisk(
		const std::string& dataf, 
		void* data);


private:


	//////////////////////////////////////////////////////////////////////////////////////////////
	// Attributes
private:

	uint64				samplerate;
	uint64				lBytesPerSample;
	uint64				single_rec_totMem;
	uint64				single_rec_postTrig;

	int					cardIndex;
	char                szBuffer[1024];     // a character buffer for any messages

public:
	ST_SPCM_CARDINFO    stCard;             // info structure of my card
	uint64              qwMemInBytes;
	void*				pvBuffer;
	int32               lValue;

};