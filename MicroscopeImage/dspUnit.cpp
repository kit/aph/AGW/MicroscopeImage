#include "dspUnit.h"


#include <cmath>
#include <algorithm>


dspUnit::dspUnit() {
	v_LOfreq		= 0;
	v_smplR			= 0;
	v_dataLength	= 0;
	v_wWidth		= 0;
	v_LOamp			= 0;
};

dspUnit::dspUnit(float smplR, float carrF) {
	set_carrF(carrF);
	set_smplR(smplR);
};

dspUnit::~dspUnit() {
	//if (ptr_LOsin != NULL)
	//	delete[] ptr_LOsin;
	//if (ptr_LOcos != NULL)
	//	delete[] ptr_LOcos;
	//if (ptr_Inph != NULL)
	//	delete[] ptr_Inph;
	//if (ptr_Quad != NULL)
	//	delete[] ptr_Quad;
}


bool dspUnit::set_dataLength(int inLength) {
	if ((inLength != 0) && !((inLength & (inLength - 1)) == 0)) {
		std::cout << "Data length is no power of 2!" << std::endl;
		std::cout << "Data length = " << inLength << std::endl;
		v_dataLength = 0;
		return 1;
	}
	v_dataLength = inLength;
	return 0;
}

bool dspUnit::set_wWidth(int wWidth) {
	if (v_dataLength == 0) {
		std::cout << "Error: Data length not set yet! Set datalength first!\n";
		return 1;
	}
	if ((wWidth != 0) && !((wWidth & (wWidth - 1)) == 0)) {
		std::cout << "Window width is no power of 2!" << std::endl;
		std::cout << "Window width = " << wWidth << std::endl;
		wWidth = 0;
		return 1;
	}
	if (v_dataLength % wWidth  != 0) {
		std::cout << "Window length is no multiple of signal length\n";
		std::cout << "wWidth % v_dataLength = " << (v_dataLength % wWidth) << std::endl;
		return 1;
	}
	if (wWidth == 0) {
		std::cout << "Error: window width = 0 is invalid!\n";
		return 1;
	}
	if (wWidth == 1) {
		std::cout << "Warning: window width for downsampling = 1\n";
	}

	v_wWidth = wWidth;
	return 0;
}

bool dspUnit::set_smplR(float smplR) {
	if (smplR == 0) {
		std::cout << "Error: Chosen sample rate = 0!\n";
		v_smplR = 0;
		return 1;
	}
	v_smplR = smplR;
	return 0;
}

bool dspUnit::set_carrF(float carrF) {
	if (carrF == 0) {
		std::cout << "Error: Chosen carrier / LO frequency = 0!\n";
		v_LOfreq = 0;
		return 1;
	}
	v_LOfreq = carrF;
	return 0;
}

bool dspUnit::do_generateLO(std::vector<float>& LOinph, std::vector<float>& LOquad) {
	float timeStep = 1. / v_smplR;
	for (size_t i = 0; i < v_dataLength; i++) {
		LOinph[i] = v_LOamp * cos(2 * M_PI * v_LOfreq * timeStep * i);
		LOquad[i] = v_LOamp * sin(2 * M_PI * v_LOfreq * timeStep * i);
	}
	return 0;
}

bool dspUnit::do_generateLO() {
	return do_generateLO(vec_LOcos, vec_LOsin);
}

bool dspUnit::do_updateData(int inLength, int16* inputData) {
	if (inputData == NULL) {
		std::cout << "Error: Data pointer = NULL. Load valid data..\n";
		return 1;
	}
	if (vec_data.capacity() != inLength) {
		std::cout << "Warning! Mismatch in data capacity between internal vector and new data!\n";
		//std::cout << "Reinitializing all vectors with correct memory first!..\n";
		//return do_initDSP(inLength, inputData);
		return 1;
	}

	vec_data.resize(v_dataLength,0);

	for (size_t i = 0; i < inLength; i++) {
		vec_data[i] = (float)inputData[i];
	}
	return 0;
}

bool dspUnit::do_loadData(int inLength, int16* inputData) {


	return 0;
}

//bool dspUnit::do_initDSP(float smplR, float carrF, float lambda, int inLength, int16* inputData, int wWidth) {
bool dspUnit::do_initDSP(float smplR, float carrF, float lambda, int inLength, int wWidth) {

	// argument checks
	if (set_smplR(smplR) || set_carrF(carrF) || set_dataLength(inLength) || set_wWidth(wWidth)) {
		std::cout << "Error: Invalid parameters in initialization!";
		return 1;
	}

	v_LOamp			= pow(2, 13);
	v_laserLambda	= lambda;
	v_phaseScale	= lambda / 4 / M_PI;

	//std::cout << "Parameters set - initializing vectors.." << std::endl;

	// reserve vector memory here
	vec_data.resize(v_dataLength);
	vec_LOsin.resize(v_dataLength);//I dedicate two vectors to the LO so the cmath "sin" and "cos" functions don't
	vec_LOcos.resize(v_dataLength);//have to be called for every data set that is loaded. This would be the case
							         //if I were to directly mix the input data into the Inph and Quad vectors
	vec_Inph.resize(v_dataLength);
	vec_Quad.resize(v_dataLength);
	vec_InphDec.resize(v_dataLength / v_wWidth);
	vec_QuadDec.resize(v_dataLength / v_wWidth);
	vec_dataDem.resize(v_dataLength / v_wWidth);

	// Fill local oscillator vectors with actual values
	do_generateLO(vec_LOcos, vec_LOsin);

	return 0;
}

bool dspUnit::do_mixSignal(std::vector<float>& A, std::vector<float>& B, std::vector<float>& C) {
	for (size_t i = 0; i < A.size(); i++){
		C[i] = A[i] * B[i];
	}
	return 0;
}

bool dspUnit::do_decimate(std::vector<float>& initial, std::vector<float>& decimated) {
	if (initial.size() < decimated.size()) {
		std::cout << "Error: Input size is smaller than output! Check order of parameters!\n";
		return 1;
	}
	for (size_t i = 0; i < decimated.size(); i++){
		//decimated[i] = 0; // IMPORTANT!! RESET HERE!!! Otherwise data will pile up when running demodulation in a loop!!
		decimated[i] = initial[i * v_wWidth];// IMPORTANT!! Overwrite old value of "decimated" here! This makes sure no old values are in the "decimated" vector
		//for (size_t j = 0; j < v_wWidth; j++) { 
		for (size_t j = 1; j < v_wWidth; j++) { // j-for-loop shifted to run from 1 to v_wWidth instead of 0 to v_wWidth since zeroth operation is performed in the i-for-loop
			decimated[i] = decimated[i]+ initial[i*v_wWidth + j];
		}
	}
	return 0;
}


bool dspUnit::do_atan2d(std::vector<float>& I, std::vector<float>& Q, std::vector<float>& result) {

	for (int i = 0; i < result.capacity(); i++)	{
		result[i] = atan2(Q[i], I[i]);
	}
	return 0;
}

bool dspUnit::do_unwrapVec(std::vector<float>& inoutData) {
	for (size_t i = 0; i < inoutData.size() - 1; i++) {
		inoutData[i + (size_t)1] = unwrapPhase(inoutData[i], inoutData[i + (size_t)1]);
	}
	return 0;
}


bool dspUnit::do_scaleDispData(std::vector<float>& inoutData, float scale, bool average ) {
	float mean = 0;
	for (size_t i = 0; i < inoutData.size(); i++) {
		inoutData[i] = scale * (-1.f*inoutData[i]);	// IMPORTANT!- 1.f * IS DONE TO ASSOCIATE POSITIVE DISPLACEMENT SLOPE
		mean += inoutData[i];						// WITH POSITIVE DOPPLER SHIFT APPROACHING SAMPLE
	}
	if (average == true) {
		mean = mean / inoutData.size();
		for (size_t i = 0; i < inoutData.size(); i++) {
			inoutData[i] = inoutData[i] - mean;
		}
	}
	return 0;
}

bool dspUnit::do_pinToZero(std::vector<float>& inoutData) {
	for(size_t i = inoutData.size(); i > 0; i--){
		inoutData[i-1] -= inoutData[0];
	}
	return 0;
}

bool dspUnit::do_pinToMiddleVpp(std::vector<float>& inoutData) {
	float vecmax = *std::max_element(inoutData.begin(), inoutData.end());
	float vecmin = *std::min_element(inoutData.begin(), inoutData.end());
	float pin = (vecmax - vecmin) / 2.f;
	std::cout << "Vpp = " << 2.f * pin << std::endl;
	for (size_t i = 0; i < inoutData.size(); i++){
		inoutData[i] -=  pin;
	}
	return 0;
}


bool dspUnit::do_demodulate(std::vector<float>& dataOut, bool subAvg) {

	//1. mix signal with local oscillator
	do_mixSignal(vec_data, vec_LOcos, vec_Inph);
	do_mixSignal(vec_data, vec_LOsin, vec_Quad);

	//2. decimate with window width wWidth
	do_decimate(vec_Inph, vec_InphDec);
	do_decimate(vec_Quad, vec_QuadDec);

	//3. angle function
	do_atan2d(vec_InphDec, vec_QuadDec, vec_dataDem);

	//4. Unwrap phase and scale to nanometers
	do_unwrapVec(vec_dataDem);
	do_scaleDispData(vec_dataDem, v_phaseScale, subAvg);

	//6. Copy data out
	dataOut = vec_dataDem;


	return 0;
}

bool dspUnit::do_resetAllVectors() {
	std::fill(vec_data.begin(), vec_data.end(), 0);
	std::fill(vec_LOsin.begin(), vec_LOsin.end(), 0);
	std::fill(vec_LOcos.begin(), vec_LOcos.end(), 0);
	std::fill(vec_Inph.begin(), vec_Inph.end(), 0);
	std::fill(vec_Quad.begin(), vec_Quad.end(), 0);
	std::fill(vec_InphDec.begin(), vec_InphDec.end(), 0);
	std::fill(vec_QuadDec.begin(), vec_QuadDec.end(), 0);
	std::fill(vec_dataDem.begin(), vec_dataDem.end(), 0);
	return 0;
}

bool dspUnit::do_reset() {
	v_LOfreq = 0;		v_LOamp = 0;		v_smplR = 0; 
	v_laserLambda = 0;	v_phaseScale = 0;	v_dataLength = 0;	
	v_wWidth = 0;

	vec_LOsin.clear();		vec_LOsin.shrink_to_fit();
	vec_LOcos.clear();		vec_LOcos.shrink_to_fit();
	vec_Inph.clear();		vec_Inph.shrink_to_fit();
	vec_Quad.clear();		vec_Quad.shrink_to_fit();
	vec_data.clear();		vec_data.shrink_to_fit();
	vec_InphDec.clear();	vec_InphDec.shrink_to_fit();
	vec_QuadDec.clear();	vec_QuadDec.shrink_to_fit();
	vec_dataDem.clear();	vec_dataDem.shrink_to_fit();

	std::cout << "DSP unit all vectors cleared\n";
	return 0;
}

//bool dspUnit::do_resetVectors() {
	//std::fill(vec_data.begin(), vec_data.end(), 0);
	//std::fill(vec_LOsin.begin(), vec_LOsin.end(), 0);
	//std::fill(vec_LOcos.begin(), vec_LOcos.end(), 0);
	//std::fill(vec_Inph.begin(), vec_Inph.end(), 0);
	//std::fill(vec_Quad.begin(), vec_Quad.end(), 0);
	//std::fill(vec_InphDec.begin(), vec_InphDec.end(), 0); // HIER LIEGT DAS PROBLEM?!?!?!?!
	//std::fill(vec_QuadDec.begin(), vec_QuadDec.end(), 0);
	//std::fill(vec_dataDem.begin(), vec_dataDem.end(), 0);
//	return 0;
//}



/*
Private functions below
*/


//float unwrapPhase(float prev_angle, float new_angle);
//float unwrapArray(float* data, int length);

float dspUnit::unwrapPhase(float prev_angle, float new_angle) {
	float d = fmod(new_angle - prev_angle, 2 * M_PI);
	if (d > M_PI) {
		d -= 2 * M_PI;
	}
	else if(d < -M_PI){
		d += 2 * M_PI;
	}
	return (prev_angle + d);
}

