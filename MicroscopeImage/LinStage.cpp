#include "LinStage.h"

#include <conio.h>
#include <time.h>
#include "PI_GCS2_DLL.h"
#include <iostream>
#include <fstream>

// Constructs an object of class LinStage with the servo switched off
LinStage::LinStage() {
	// Servo swtiched off
	servoOn = false;
}

// Connects the Controller of the LinearStage - Requires pointer to Controller serial number variable
bool LinStage::ConnectAndStartUp(const char * serialNo){

	// Copy serial number
	for (int i = 0; i < 100; i++){
		controllerSerialNumber[i] = * serialNo;
		serialNo++;
	}

	piDeviceId = PI_ConnectUSB(controllerSerialNumber);

	if (piDeviceId < 0){
		std::cout << "Could not connect to controller.\n";
		_getch();
		return false;
	}

	// Query controller identification
	if (PI_qIDN(piDeviceId, deviceIdentificationString, 199) == false){
		CloseConnectionWithComment(piDeviceId, "qIDN failed. Exiting.\n");
		return false;
	}
	std::cout << "\nqIDN returned: " << deviceIdentificationString << "\n";

	// Query controller axes
	if (!PI_qSAI_ALL(piDeviceId, availableAxes, 199)){
		CloseConnectionWithComment(piDeviceId, "Could not send SAI?\n");
		return false;
	}
	std::cout << "Available axes:\n" << availableAxes << "\n\n";

	std::cout << "Chose axis (e.g. 1/2/..): " << std::endl;;
	std::cin >> activeAxis; 

	// Query which stage type the controller is configured for
	if (!PI_qCST(piDeviceId, activeAxis, currentStageType, 255)){
		CloseConnectionWithComment(piDeviceId, "Failed to query current stage type.\n");
		return false;
	}
	std::cout << "Axis " << activeAxis << " is configured to control " << currentStageType << std::endl;

	//////////////////////////



	// Switch on servo
	servoOn = true;
	if (!PI_SVO(piDeviceId, activeAxis, &servoOn)) {
		CloseConnectionWithComment(piDeviceId, "SVO failed. Exiting.\n");
		return false;
	}



	// Determine boundaries for stage movement
	if (!PI_qTMN(piDeviceId, activeAxis, &travelRangeMinimum)) {
		CloseConnectionWithComment(piDeviceId, "qTMN unable to query min. position of axis.\n");
		return false;
	}

	if (!PI_qTMX(piDeviceId, activeAxis, &travelRangeMaximum)) {
		CloseConnectionWithComment(piDeviceId, "qTMX, Unable to query max. position of axis.\n");
		return false;
	}

	std::cout << "Allowed range of movement: min: " << travelRangeMinimum << "\t max: " << travelRangeMaximum << "\n";


	////////////////////////


	//Querry and set current stage position
	if (!PI_qPOS(piDeviceId, activeAxis, &currentPos)) {
		std::cout << "Unable to query stage position\n";
		std::cout << "Check if stage is referenced." << std::endl;
	}

	return true;
}

// Connects the Controller of the LinearStage - Requires pointer to Controller serial number variable
bool LinStage::ConnectAndStartUpFixedAxis(const char* serialNo, char* setActiveAxis, bool silent) {

	// Copy serial number
	for (int i = 0; i < 100; i++) {
		controllerSerialNumber[i] = *serialNo;
		serialNo++;
	}

	piDeviceId = PI_ConnectUSB(controllerSerialNumber);

	if (piDeviceId < 0) {
		std::cout << "Could not connect to controller.\n";
		_getch();
		return false;
	}

	// Query controller identification
	if (PI_qIDN(piDeviceId, deviceIdentificationString, 199) == false) {
		CloseConnectionWithComment(piDeviceId, "qIDN failed. Exiting.\n");
		return false;
	}
	if (!silent) {
		std::cout << "\nqIDN returned: " << deviceIdentificationString << "\n";
	}
	

	// Query controller axes
	if (!PI_qSAI_ALL(piDeviceId, availableAxes, 199)) {
		CloseConnectionWithComment(piDeviceId, "Could not send SAI?\n");
		return false;
	}

    // Copy active axis number
	for (int i = 0; i < 2; i++) {
		activeAxis[i] = *setActiveAxis;
		setActiveAxis++;
	}

	// Query which stage type the controller is configured for
	if (!PI_qCST(piDeviceId, activeAxis, currentStageType, 255)) {
		CloseConnectionWithComment(piDeviceId, "Failed to query current stage type.\n");
		return false;
	}
	//////////////////////////
	// Switch on servo
	servoOn = true;
	if (!PI_SVO(piDeviceId, activeAxis, &servoOn)) {
		CloseConnectionWithComment(piDeviceId, "SVO failed. Exiting.\n");
		return false;
	}

	// Determine boundaries for stage movement
	if (!PI_qTMN(piDeviceId, activeAxis, &travelRangeMinimum)) {
		CloseConnectionWithComment(piDeviceId, "qTMN unable to query min. position of axis.\n");
		return false;
	}

	if (!PI_qTMX(piDeviceId, activeAxis, &travelRangeMaximum)) {
		CloseConnectionWithComment(piDeviceId, "qTMX, Unable to query max. position of axis.\n");
		return false;
	}
	////////////////////////
	//Querry and set current stage position
	if (!PI_qPOS(piDeviceId, activeAxis, &currentPos)) {
		std::cout << "Unable to query stage position\n";
		std::cout << "Check if stage is referenced." << std::endl;
	}

	return true;
}


bool LinStage::DisconnectStage() {
	if (piDeviceId < 0){
		std::cout << "Invalid piDeviceID / Could not find controller.\n";
		std::cout << "Connection cannot be closed.\n";
		_getch();
		return false;
	}
	PI_CloseConnection(piDeviceId);
	std::cout << "Connection closed.\n";
	_getch();
	return true;
}

bool LinStage::DisconStage() {
	if (piDeviceId < 0) {
		std::cout << "Invalid piDeviceID / Could not find controller.\n";
		std::cout << "Connection cannot be closed.\n";
		_getch();
		return false;
	}
	PI_CloseConnection(piDeviceId);
	//std::cout << "Connection closed.\n";
	//_getch();
	return true;
}

bool LinStage::ReferenceStage() {
	// Switch on servo
	const BOOL servoOn = true;
	if (!PI_SVO(piDeviceId, activeAxis, &servoOn)){
		CloseConnectionWithComment(piDeviceId, "SVO failed. Exiting.\n");
		return false;
	}

	// Reference stage
	std::cout << "Referencing axis " << activeAxis << "\n";
	if (!PI_FRF(piDeviceId, activeAxis)){
		return false;
	}

	// Wait until the reference move is done.
	BOOL referencingFinished;
	referencingFinished = false;
	while (TRUE != referencingFinished){
		if (!PI_IsControllerReady(piDeviceId, &referencingFinished)){
			return false;
		}
	}
	
	std::cout << "Stage is referenced.\n";

	// Determine and store current position
	if (!PI_qDFH(piDeviceId, activeAxis, &currentPos)){
		CloseConnectionWithComment(piDeviceId, "Unable to query current position of axis.\n");
		return false;
	}
	
	std::cout << "Current position is = " << currentPos << std::endl;
	
	// Determine boundaries for stage movement
	 if (!PI_qTMN(piDeviceId, activeAxis, &travelRangeMinimum)){
	 	CloseConnectionWithComment(piDeviceId, "qTMN unable to query min. position of axis.\n");
	 	return false;
	 }

	 if (!PI_qTMX(piDeviceId, activeAxis, &travelRangeMaximum)){
	 	CloseConnectionWithComment(piDeviceId, "qTMX, Unable to query max. position of axis.\n");
	 	return false;
	 }

	 std::cout << "Allowed range of movement: min: " << travelRangeMinimum << "\t max: " << travelRangeMaximum << "\n";

	return true;
}

// Querry stage position for currently chosen device and axis
bool LinStage::QuerryStagePos() {
	if (!PI_qPOS(piDeviceId, activeAxis, &currentPos)) {
		CloseConnectionWithComment(piDeviceId, "Unable to query stage position\n");
		return false;
	}
	return true;
}

bool LinStage::MoveStageTo(double absPos) {

	double targetPos = absPos;
	BOOL inputError = false;
	// Check if input position is valid and does not exceed travel range
	if (absPos < travelRangeMinimum) {
		std::cout << "Input position exceeds lower travel range of " << travelRangeMinimum << std::endl;
		std::cout << "Stage will not be moved.\n";
		targetPos = currentPos;
		inputError = true;
		return false;
	}

	if (absPos > travelRangeMaximum) {
		std::cout << "Input position exceeds upper travel range of " << travelRangeMaximum << std::endl;
		std::cout << "Stage will not be moved.\n";
		targetPos = currentPos;
		inputError = true;
		return false;
	}

	if (!PI_MOV(piDeviceId, activeAxis, &absPos)){
	 	CloseConnectionWithComment(piDeviceId, "MOV, unable to move to target position.\n");
	 	return false;
	}

	 // wait for motion to stop
	 BOOL isMoving = true;
	 while (isMoving){
	 	if (!PI_IsMoving(piDeviceId, activeAxis, &isMoving))
	 	{
	 		CloseConnectionWithComment(piDeviceId, "Unable to query movement status.\n");
	 		return false;
	 	}
	 	//Sleep(100);
	 }

	 // Query stage position
	 if (!PI_qPOS(piDeviceId, activeAxis, &currentPos)){
	 	CloseConnectionWithComment(piDeviceId, "Unable to query stage position\n");
	 	return false;
	 }

	 //if(inputError == false){
		//std::cout << "Stage is now at " << currentPos << " target was " << absPos << "\n";
	 //}
		
	 return true;
}

bool LinStage::MoveStageRel(double moveStep) {
	double step = moveStep;
	std::cout << "step is = " << step << std::endl;
	if (!PI_MVR(piDeviceId, activeAxis, &step)) {
		CloseConnectionWithComment(piDeviceId, "MVR, unable to move to target position.\n");
		return false;
	}
	step = moveStep;

	// wait for motion to stop
	BOOL isMoving = true;
	while (isMoving) {
		if (!PI_IsMoving(piDeviceId, activeAxis, &isMoving))
		{
			CloseConnectionWithComment(piDeviceId, "Unable to query movement status.\n");
			return false;
		}
	}

	// Query stage position
	if (!PI_qPOS(piDeviceId, activeAxis, &currentPos)) {
		CloseConnectionWithComment(piDeviceId, "Unable to query stage position\n");
		return false;
	}

	return true;
}


//////////////////////////////////////////////////////////////////////////////////////////////////
// Helper Functions																		
//////////////////////////////////////////////////////////////////////////////////////////////////

bool LinStage::ReferenceIfNeeded(int piDeviceId, char* axis)
{
	BOOL referenced;
	if (!PI_qFRF(piDeviceId, axis, &referenced))
	{
		return false;
	}

	// If stage is equipped with absolute sensors, Referenced will always be set to true.
	if (!referenced)
	{
		std::cout << "Referencing axis " << axis << "\n";
		if (!PI_FRF(piDeviceId, axis))
		{
			return false;
		}

		// Wait until the reference move is done.
		BOOL referencingFinished;
		referencingFinished = false;
		while (TRUE != referencingFinished)
		{
			if (!PI_IsControllerReady(piDeviceId, &referencingFinished))
				return false;
		}
	}
	return true;
}


void LinStage::ReportError(int piDeviceId)
{
	int err = PI_GetError(piDeviceId);
	char zErrMsg[300];
	if (PI_TranslateError(err, zErrMsg, 299))
	{
		std::cout << "Error " << err << " occurred: " << zErrMsg << "\n";
	}
}


void LinStage::CloseConnectionWithComment(int piDeviceId, const char* comment)
{
	std::cout << comment << "\n";
	ReportError(piDeviceId);
	PI_CloseConnection(piDeviceId);
	_getch();
}


bool LinStage::WaitForMovementStop(int piDeviceId, char* axis1)
{
	BOOL isMoving = true;

	while (isMoving)
	{
		if (!PI_IsMoving(piDeviceId, axis1, &isMoving))
		{
			CloseConnectionWithComment(piDeviceId, "Unable to query movement status.\n");
			return false;
		}
		Sleep(100);
	}
	return TRUE;
}


//////////////////////////////////////////////////////////////////////////////////////////////////
// Optional Sample Functions
//////////////////////////////////////////////////////////////////////////////////////////////////

bool LinStage::ReadRecordedData(int iD, char* axis, double Position, double MaxPosValue)
{
	// This sample function shows how to use the data recorder. The data
	// recorder is described in detail in the user manual. Please refer to it to
	// get the configuration you need.
	//
	// Valid for the following PI controllers:
	// ALL

	// Data recroder settings
	int piRecordTableIds[2] = { 1, 2 };		// Record tables to be used by data recroder
	int piRecordOption[2] = { 1, 2 };		// Record options (1=commanded position, 2=measured position)
	int RecordTableRate = 50;			// Record table rate (equal to the output rate of the wave generator)
	int piTriggerSource[] = { 1 };		// Default option
	char dummyValues[] = "0";			// Dummy value, only used for certain trigger sources (see manual for details).
	double relativeMove[1] = { 1 };			// Set relative move

	// Configure data recorder

	// Configure Record Table Rate
	if (!PI_RTR(iD, RecordTableRate))
	{
		CloseConnectionWithComment(iD, "Unable to set record table rate.\n");
		return FALSE;
	}

	// Configure which data is recorded by the data recorder
	if (!PI_DRC(iD, piRecordTableIds, axis, piRecordOption))
	{
		CloseConnectionWithComment(iD, "Unable to set data recorder configuration.\n");
		return FALSE;
	}

	int recTrigger[1] = { 0 };
	// Configure which event triggers the data recorder (starts the data acquisition)
	if (!PI_DRT(iD, recTrigger, piTriggerSource, dummyValues, 1))
	{
		CloseConnectionWithComment(iD, "Unable to set trigger sources for data record tables\n");
		return FALSE;
	}

	// Perform move
	// Make sure the relative move will not exeed the travel range
	if (MaxPosValue < Position + relativeMove[0])
	{
		relativeMove[0] = -1;
	}

	Sleep(100);
	if (!PI_MVR(iD, axis, relativeMove))
	{
		CloseConnectionWithComment(iD, "Unable to perform relative movement\n");
		return FALSE;
	}

	// Misc. variables used for data recorder readout
	BOOL OkFlag = TRUE;			//Indicates whether data recorder content request was successful.
	double* dTableIndex;								//Pointer to internal array to store the data
	char GcsHeader[1024];								//Character array to store GCS-Header
	int NReadChannels = 2;			//Number of channels to read out
	int Err;											//Error number
	int NumberOfSamplesToRead = 200;			//Number of samples to read from controller

	std::ofstream OutputFile;

	// Specify target folder and filename here
	OutputFile.open("test.dat");
	PI_qERR(iD, &Err);

	int valuesRecorded = 0;

	while (valuesRecorded < NumberOfSamplesToRead)
	{
		if (!PI_qDRL(iD, piRecordTableIds, &valuesRecorded, 1))
		{
			CloseConnectionWithComment(iD, "Unable to read number of recorded samples\n");
			return FALSE;
		}
		Sleep(100);
	}

	// Start reading asynchronously
	OkFlag = PI_qDRR(iD, piRecordTableIds, NReadChannels, 1, NumberOfSamplesToRead, &dTableIndex, GcsHeader, 1024);
	// This means, the controller is now sending its recorded data. The function returns the Header and
	// a pointer to memory allocated in the dll.
	// you could now analyze the Header information in GcsHeader

	std::cout << "GCS Header:\n" << GcsHeader << "\n";
	OutputFile << "GCS Header:\n" << GcsHeader << "\n";
	PI_qERR(iD, &Err);

	if (!OkFlag)
	{
		return false;
	}

	std::cout << "Reading...\n";
	int Index = -1;
	int OldIndex = 0;
	int Timeout = 20;

	do// Wait until the read pointer has reached the number of expected samples
	{
		OldIndex = Index;
		Sleep(100);
		// While the controller sends data, the buffer index is increasing.
		// This means the array pointed to by OkFlag is filled with valid data
		Index = PI_GetAsyncBufferIndex(iD);

		if (Index == OldIndex) // If the index does not change for about 2 seconds, there is a problem
		{
			Timeout--;

			if (Timeout < 0)
			{
				std::cout << "No more data after " << Index << " of " << NumberOfSamplesToRead * NReadChannels << " sampled\nStop reading now";
				NumberOfSamplesToRead = (Index - 1) / NReadChannels;
				break;
			}
		}

		std::cout << "\r " << Index;

	} while (Index < (NumberOfSamplesToRead * NReadChannels));
	// After OkFlag is filled with data, you should process it or stored it in a local variable,
	// as it will be cleared and/or overwritten with the next recording, e.g. started by a 
	// motion command
	std::cout << "\n Finished \n";
	int k;
	std::ios init(NULL);
	init.copyfmt(std::cout);

	for (Index = 0; Index < NumberOfSamplesToRead; Index++)
	{	// Print read data
		// The data columns
		// c1_1 c2_1 c3_1 c4_1
		// c1_2 c2_2 c3_2 c4_2
		// ...
		// c1_n c2_n c3_n c4_n
		// are aligned as follows:
		// OkFlag:
		// {c1_1,c2_1,c3_1,c4_1,c1_2,c2_2,...,c4_n}
		std::ios init(NULL);
		init.copyfmt(std::cout);
		std::cout.fill('0');
		std::cout.width(3);
		OutputFile.copyfmt(std::cout);
		std::cout << Index;
		OutputFile << Index;

		for (k = 0; k < NReadChannels; k++)
		{
			std::cout << std::fixed;
			std::cout.precision(5);
			OutputFile.copyfmt(std::cout);
			std::cout << "\t" << std::fixed << dTableIndex[Index * NReadChannels + k];
			OutputFile << "\t" << std::fixed << dTableIndex[Index * NReadChannels + k];
		}

		std::cout << "\n";
		OutputFile << "\n";
	}

	std::cout.copyfmt(init);
	OutputFile.copyfmt(std::cout);
	OutputFile.close();
	return true;
}


//void LinStage::ShowAvailablePiControllers()
//{
//	std::cout << "Available USB-controllers:" << std::endl;
//	char UsbController[1024];
//	int ResultUBS = PI_EnumerateUSB(UsbController, 1024, "C-891");
//	if (strlen(UsbController) == 0) strcpy(UsbController, "No USB controllers found.");
//	std::cout << UsbController << "\n";
//
//	std::cout << "Available TCP/IP-controllers:" << std::endl;
//	char TcpIpController[10000] = "";
//	int ResultTcpIp = PI_EnumerateTCPIPDevices(TcpIpController, 9999, "");
//	if (strlen(TcpIpController) == 0) strcpy(TcpIpController, "No TCP/IP controllers found.");
//	std::cout << TcpIpController << "\n";
//}


//int LinStage::ChangeConnectedStage(int piDeviceId, char* axis)
//{
//	// Query current stage type
//	char CurrentStageType[256];
//	if (!PI_qCST(piDeviceId, axis, CurrentStageType, 255))
//	{
//		CloseConnectionWithComment(piDeviceId, "Failed to query current stage type.\n");
//		return false;
//	}
//
//	// 1. Set the correct stage type. A WRONG STAGE TYPE CAN DAMAGE YOUR STAGE!
//	char StageType[256] = "M-686.D64";
//
//	// Check if current set stage type is the same as defined by user input
//	if (!strstr(CurrentStageType, StageType))
//	{
//		// If not, set stage type according to user input
//		if (!PI_CST(piDeviceId, axis, StageType))
//		{
//			CloseConnectionWithComment(piDeviceId, "Could not send CST.\n");
//			return false;
//		}
//	}
//}
