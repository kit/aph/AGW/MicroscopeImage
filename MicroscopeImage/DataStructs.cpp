#include "DataStructs.h"

std::ostream& operator<<(std::ostream& os, const DynMeasParameters& other) {

    os << "_________________________________________________________\n ";
    os << "Outputting measurement parameters :\n";
    os << "_________________________________________________________\n";

    if (!other.s_filled) {
        os << "No parameters found!\n";
    }
    else
    {
        os << "Data path:               " << other.s_dynMeasSeriesPath << "\n";
        os << "Flag clear dirs (1/0):   " << other.s_clearDirs << "\n";
        os << "Flag save meta (1/0):    " << other.s_saveMeta << "\n";
        os << "Flag save as dat (1/0):  " << other.s_saveAsDat << "\n";
        os << "line/col (1/0):          " << other.s_linewise << "\n";
        os << "Fftfilt (1/0):           " << other.s_fftFiltering << "\n";
        os << "APD gain:                " << other.s_apdGain << "\n";
        os << "Pixel in X               " << other.s_pixelsX << "\n";
        os << "Pixel in Y               " << other.s_pixelsY << "\n";
        os << "FOV micrometers:         " << other.s_FOV << "\n";
        os << "Datapoints (images) DIC: " << other.s_NsampPx << "\n";
        os << "Piezo-Act. Vpp V:        " << other.s_Vpp << "\n";
        os << "Fstart Hz:               " << other.s_startFreq << "\n";
        os << "Fstep Hz:                " << other.s_freqStep << "\n";
        os << "Number of Fsteps:        " << other.s_NfreqSteps << "\n";
        os << "Datapoints Nvib:         " << other.s_vibrSamples << "\n";
        os << "Sample rate vib (MSps):  " << other.s_vibSampleR << "\n";
        os << "Vib trglvl mV:           " << other.s_trigLvl << "\n";
        os << "Vib input range mV:      " << other.s_inptRng << "\n";
        os << "Imag. sample rate (kSps) " << other.s_imagSampleR <<"\n";
    }

    os << "_________________________________________________________\n ";
    os << "Done..\n";
    os << "_________________________________________________________\n";

    return os;
}

std::ostream& operator<<(std::ostream& os, const MicroscopeHardware& other) {

    os << "_________________________________________________________\n ";
    os << "Outputting Hardware Parameters :\n";
    os << "_________________________________________________________\n";

    if (!other.s_filled) {
        os << "No parameters found!\n";
    }
    else
    {
        os << "1: Wetzlar 50x, 2: Wetzlar 25x, 3: Wetzlar 8x, 4: Nikon 50x \n";
        os << "Obj. lens choice:        " << other.s_objLens << "\n";
        os << "Galvo jump speed:        " << other.s_jump_speed << "\n";
        os << "Galvo mark speed:        " << other.s_mark_speed << "\n";
        os << "Waveform gen VISA addr:  " << other.s_FG_VISA << "\n";
        os << "Piezo Vpp thresh. in V:  " << other.s_Vpp_thresh << "\n";
        os << "x-Stage serial:          " << other.s_xStageAddr << "\n";
        os << "y-Stage serial:          " << other.s_yStageAddr << "\n";
        os << "z-Stage serial:          " << other.s_zStageAddr << "\n";
        os << "Active axis x            " << other.s_xAxis << "\n";
        os << "Active axis y            " << other.s_yAxis << "\n";
        os << "Active axis z            " << other.s_zAxis << "\n";
        os << "Laser wavelength in nm:  " << other.s_lasWavel << "\n";
        os << "Gate duration in T<us>:  " << other.s_gateT << "\n";
        os << "Gate control COM port:   " << other.s_gateCOMport << "\n";
        os << "Gate control BAUD rate:  " << other.s_gateCOMbaud << "\n";
    }
    os << "_________________________________________________________\n";
    os << "Done..\n";
    os << "_________________________________________________________\n";

    return os;
}

MarkerCoordList::MarkerCoordList() {
    s_filled = false;
    Nmarkers = 0;
    xPosMarkers = nullptr;
    yPosMarkers = nullptr;
    zPosMarkers = nullptr;
}

MarkerCoordList::MarkerCoordList(int N, double* xP, double* yP, double* zP) {
    s_filled = true;
    Nmarkers = N;
    xPosMarkers = xP;
    yPosMarkers = yP;
    zPosMarkers = zP;
}

MarkerCoordList::MarkerCoordList(const MarkerCoordList& mcl) { // copy constructor
    s_filled = mcl.s_filled;
    Nmarkers = mcl.Nmarkers;

    xPosMarkers = new double[Nmarkers];
    yPosMarkers = new double[Nmarkers];
    zPosMarkers = new double[Nmarkers];
    for (size_t i = 0; i < mcl.Nmarkers; i++){
        xPosMarkers[i] = mcl.xPosMarkers[i];
        yPosMarkers[i] = mcl.yPosMarkers[i];
        zPosMarkers[i] = mcl.zPosMarkers[i];
    }
}

MarkerCoordList::~MarkerCoordList() {
    if (xPosMarkers != nullptr)
        delete[] xPosMarkers;
    if (yPosMarkers != nullptr)
        delete[] yPosMarkers;
    if (zPosMarkers != nullptr)
        delete[] zPosMarkers;
}

MarkerCoordList& MarkerCoordList::operator= (const MarkerCoordList& mcl) {

    // self-assignment check
    if (this == &mcl)
        return *this;

    //	//if data exists in the curretn string, delete it
    if (xPosMarkers) delete[] xPosMarkers;
    if (yPosMarkers) delete[] yPosMarkers;
    if (zPosMarkers) delete[] zPosMarkers;

    // copy the data from cml to the implicit object
    s_filled = mcl.s_filled;
    Nmarkers = mcl.Nmarkers;
    // reallocate
    xPosMarkers = new double[Nmarkers];
    yPosMarkers = new double[Nmarkers];
    zPosMarkers = new double[Nmarkers];
    for (size_t i = 0; i < mcl.Nmarkers; i++) {
        xPosMarkers[i] = mcl.xPosMarkers[i];
        yPosMarkers[i] = mcl.yPosMarkers[i];
        zPosMarkers[i] = mcl.zPosMarkers[i];
    }

    // return the existing object so we can chain this operator
    return *this;
}

DynMeasParameters::DynMeasParameters() {
    s_dynMeasSeriesPath = "";
    s_clearDirs         = 9;
    s_saveMeta          = 9;
    s_saveAsDat         = 9;
    s_linewise          = 9;
    s_fftFiltering      = 9;
    s_apdGain           = 0;
    s_pixelsX           = 0;
    s_pixelsY           = 0;
    s_FOV               = 0.f;
    s_NsampPx           = 0; // samples per pixel to be used for saving.. 
    s_Vpp               = (double)0;
    s_startFreq         = 0.f;
    s_freqStep          = 0.f;
    s_NfreqSteps        = 0;
    s_vibrSamples       = 0; // 65536 = 2^16 @ 500MSps <-> 1024 = 2^10 @ 10MSps
    s_vibSampleR        = 0; //Sps
    s_trigLvl           = 0; //Trigger levels for the external (analog) trigger to be programmed in mV
    s_inptRng           = 0;
    s_imagSampleR       = 0;

    s_filled = false;
}

MicroscopeHardware::MicroscopeHardware() {
    s_objLens       = 0;
    s_jump_speed    = 0;
    s_mark_speed    = 0;
    s_FG_VISA       = "";
    s_Vpp_thresh    = 0.f;

    s_xStageAddr = "";
    s_yStageAddr = "";
    s_zStageAddr = "";

    s_xAxis[0] = ' ';
    s_yAxis[0] = ' ';
    s_zAxis[0] = ' ';

    s_lasWavel = 0.f;

    s_gateT         = "";
    s_gateCOMport   = "";
    s_gateCOMbaud   = 0;

    s_filled = false;
}



