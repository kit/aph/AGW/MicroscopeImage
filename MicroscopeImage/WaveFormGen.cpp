#include "WaveFormGen.h"

WaveFormGen::WaveFormGen() {
	activeChannel = ViConstString("Channel1");
}

bool WaveFormGen::InitWFG(const char* VISAaddr) {
	RsrcName = ViRsrc(VISAaddr); // Keysight connection expert declares the ViRsrc as VISAaddress, hence the name
	error = Ag3352x_InitWithOptions(RsrcName, VI_FALSE, VI_FALSE, "simulate=false", &vi);
	error |= Ag3352x_SetAttributeViInt32(vi,"Channel1", AG3352X_ATTR_CHANNEL_OUTPUT_SYNC_SOURCE, 0); //doesn't matter if RepCapIdentifier is "Channel1" or "Channel2" so "Channel1" is hard coded here!
	return(CheckErr(error));
}

bool WaveFormGen::ResetWFG() {
	error = Ag3352x_reset(vi);
	return(CheckErr(error));
}

bool WaveFormGen::SelectChannel(int chnlNo) {
	if (chnlNo == 1) {
		activeChannel = ViConstString("Channel1");
		std::cout << "Frequency generator channel 1 control now active" << std::endl;
	}
	else if (chnlNo == 2) {
		activeChannel = ViConstString("Channel2");
		std::cout << "Frequency generator channel 2 control now active" << std::endl;
	}
	else {
		std::cout << "Invalid channel choice" << std::endl;
		return 1;
	}
	return 0;
}

bool WaveFormGen::OutPutEnable() {
	std::cout << "Enabling frequency generator output on " << activeChannel << ".." << std::endl;
	error = Ag3352x_ConfigureOutputEnabled(vi, activeChannel, VI_TRUE);
	return(CheckErr(error));
}

bool WaveFormGen::OutPutEnable(int chnlNo) {
	SelectChannel(chnlNo);
	std::cout << "Enabling frequency generator output on " << activeChannel << ".." << std::endl;
	error = Ag3352x_ConfigureOutputEnabled(vi, activeChannel, VI_TRUE);
	return(CheckErr(error));
}

bool WaveFormGen::OutPutDisable() {
	std::cout << "Disabling frequency generator output on " << activeChannel << ".." << std::endl;
	error = Ag3352x_ConfigureOutputEnabled(vi, activeChannel, VI_FALSE);
	return(CheckErr(error));
}

bool WaveFormGen::OutPutDisable(int chnlNo) {
	SelectChannel(chnlNo);
	std::cout << "Disabling frequency generator output on " << activeChannel << ".." << std::endl;
	error = Ag3352x_ConfigureOutputEnabled(vi, activeChannel, VI_FALSE);
	return(CheckErr(error));
}


bool WaveFormGen::ConfigureSine(double amplitude, double offset, double freq, double phase) {
	if (amplitude == 0) {
		std::cout << "Zero excitation amplitude.\n";
		return OutPutDisable();
	}
	if (freq == 0) {
		std::cout << "Zero excitation frequency.\n";
		return OutPutDisable();
	}
	error = Ag3352x_ConfigureStandardWaveform(vi, activeChannel, \
		ViInt32(1), ViReal64(amplitude), ViReal64(offset), ViReal64(freq), ViReal64(phase));
	return(CheckErr(error));
}

bool WaveFormGen::CloseDevice() {
	if (vi)
		error = Ag3352x_close(vi);
	return(CheckErr(error));
}

bool WaveFormGen::CheckErr(ViStatus err) {
	    //https://en.wikipedia.org/wiki/Comma_operator#Examples
	    (error = (error < 0) ? error : VI_SUCCESS);
	
	    if (error != VI_SUCCESS)
	    {
	        ViChar errStr[2048];
	        Ag3352x_GetError(vi, &error, 2048, errStr);
	        //printf("Error!", errStr);
	        std::cout << "errStr = " << errStr << std::endl;
	        return 1;
	    }
	    return 0;
	}