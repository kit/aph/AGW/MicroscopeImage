﻿#include "CMicroscope.h"

// General
#include <chrono> 
#include <iostream>
#include <fstream>
#include <thread>
#include <cmath>
//#include <string.h>
//#include <string>
#include <conio.h>
#include <filesystem>

// For use of function generator
// #include "WaveFormGen.h" // header already in CMicroscope.h

//#include "spcmDigitizer.h" // should not be here..

using namespace std;
using std::filesystem::exists;
//using namespace cv;

const double pi = 3.14159265358979323846;

// Constructor
Microscope::Microscope()
    :Tiff(1, 1),Board(1, 1, SAMPLE_RATE_50MSPS),spcmADC(0,true), gnuPlotter(), DSP(), MarkerPosStrct()
{
    std::cout << "\n"<<"Starting the microscope: \n";

    // check if config file exists
    if (!exists(hardwareConfFileName)) {
        std::cout << "No hardware config file found!\n";
        std::cout << "File name should be: "<< hardwareConfFileName << "\n";
        std::cout << "Check if file exists and restart software!\n";
        std::cin.get();
        //goto stop;
    }
    // read config file and check for error
    if (readHardwareConfigFile(hardwareConfFileName, hwParams)) {
        std::cout << "Error in reading hardware config file!\n";
        std::cout << "Undefined state may be realized! Recheck " << hardwareConfFileName << endl;
        std::cout << "Terminate the programm and restart!" << endl;
        std::cin.get();
        //goto stop;
    }
    std::cout << hwParams; // output parameters for debugging purposes!

    switch (hwParams.s_objLens) // objLensChoice is convertion factor for galvo position encoder into micrometers
    {
    case 1:
        galvoPixelCalib = 460; // Wetzlar Plan L50x/0.6
        std::cout <<"\n" << "Objective Lens: Wetzlar Plan L50x/0.6\n";
        break;
    case 2:
        galvoPixelCalib = 230; // Wetzlar Plan L25x/0.4
        std::cout << "\n" << "Objective Lens: Wetzlar Plan L25x/0.4\n";
        break;
    case 3:
        galvoPixelCalib = 74; // Wetzlar PL8x/0.18
        std::cout << "\n" << "Objective Lens: Wetzlar PL8x/0.18\n";
        break;
    case 4:
        galvoPixelCalib = 600; // Nikon CFI60 TU Plan Epi ELWD 50x/0.6
        std::cout << "\n" << "Objective Lens: Nikon CFI60 TU Plan Epi ELWD 50x/0.6\n";
        break;
    default:
    {
        std::cout << "This was an invalid objective lens choice!\n";
        std::cout << "Make sure the hardware config file is correct!\n";
        cout << "Press Enter to Continue\n";
        getch();
        break;
    };
    }

    // Configure Galvo Scanner
    isScanning = false;
	set_jump_speed_ctrl(hwParams.s_jump_speed);
	set_mark_speed_ctrl(hwParams.s_mark_speed);


    // Set parameters for dynamic measurements

    dynSampleRate = 10e6; // default value
    timeDynPx = 0.42; // ms // Illumination time per pixel (lowest value that gives good image results)

    // Set value for which a image counts as "showing saturation"..
    // 2^15 equals the levels from 0 to max voltage divided by the 400 gain levels of the apd.. hence, 32768/400 is "bit-levels per gain-step"
    // If the image intensity falls into this intervall, a reduction of the gain can be reasonable
    ImagSatThresh = 0xFFF0 - WORD(32768. / 400 + 0.5);  // 0xFFF0 = 65520 and is the value for which the APD saturates (with +-2V range)
    //ImagSatThresh = 0xA000;

    // Configure APD settings
    std::cout << std::endl << std::endl << "Configuring APD-Detector: \n" << std::endl;
    microscopeAPDM = APD0.QuerryCurrentGain();
    Tiff.setAttribute(ID_APDGain, "APD Gain M", microscopeAPDM);

    // Configure Alazartec ADC Board
    DetectorChannel = CHANNEL_A;
    TriggerChannel  = TRIG_CHAN_B;
    InputRange      = INPUT_RANGE_PM_1_V;
    //InputRange = INPUT_RANGE_PM_5_V;
	Board.ConfigureChannel(CHANNEL_A, DC_COUPLING, InputRange, IMPEDANCE_1M_OHM);
	Board.ConfigureChannel(CHANNEL_B, DC_COUPLING, INPUT_RANGE_PM_2_V, IMPEDANCE_1M_OHM);
    Board.ConfigureTriggerEngine(TRIG_ENGINE_J, TriggerChannel, (U32)0, TRIGGER_SLOPE_POSITIVE);

    Tiff.setAttribute(ID_InputRange, "Input range [mV]", Board.translateInputRange(InputRange));
    Tiff.setAttribute(ID_InputImpedance, "Input impedance [Ohm]", 1000000); // Todo: write translation function in digitizer class

    // Configure and start up function generator driver (same clunky string/char handling as below for PI stages..)
    // VISA address can be found using the keysight connection expert
    //myVISAaddr = "USB0::0x0957::0x4B07::MY59000201::0::INSTR"; 
    Fgen.InitWFG(hwParams.s_FG_VISA.c_str());
    //Fgen.ConfigureSine(0.12, 0, 140000, 0);
    //Fgen.OutPutDisable(1);

    // Configure Piezo Linear Stages
    std::cout << std::endl << std::endl << "Configuring Linear Stages:" << std::endl;


    allStagesConnected = true;

    if (xStage.ConnectAndStartUpFixedAxis(hwParams.s_xStageAddr.c_str(), hwParams.s_xAxis)) {
        xPosition = xStage.currentPos;
        std::cout << "x-Stage available and connected" << endl;
    }
    else {
        std::cout << "x-Stage UNavailable" << endl;
        allStagesConnected = false;
    }

    if (yStage.ConnectAndStartUpFixedAxis(hwParams.s_yStageAddr.c_str(), hwParams.s_yAxis)) {
        yPosition = yStage.currentPos;
        std::cout << "y-Stage available and connected" << endl;
    }
    else {
        std::cout << "y-Stage UNavailable" << endl;
        allStagesConnected = false;
    }

    if (zStage.ConnectAndStartUpFixedAxis(hwParams.s_zStageAddr.c_str(), hwParams.s_zAxis)) {
        zPosition = zStage.currentPos;
        std::cout << "z-Stage available and connected" << endl;
    }
    else {
        std::cout << "z-Stage UNavailable" << endl;
        allStagesConnected = false;
    }

    // configure serial port for gate control
    cout << std::endl;
    GateControl.ConfigPort(hwParams.s_gateCOMport, "8N1", hwParams.s_gateCOMbaud);
    GateControl.SendCommand(hwParams.s_gateT,1,0,0);
    cout << std::endl;

    AskParameters();
    std::cout << std::endl << "Programm initialized and configured." << std::endl << std::endl;
}

// Destructor 
Microscope::~Microscope() {

    if (gCoords != NULL)
        delete[] gCoords;
    if (gPosResult != NULL)
        delete[] gPosResult;
    if (picture != NULL)
        delete[] picture;

    spcmADC.closeCard();
}

bool Microscope::RunningStatus()
{
    return running;
}

bool Microscope::readHardwareConfigFile(string filename, MicroscopeHardware& hwParams) {
    string line;
    ifstream readMeta;
    readMeta.open(filename);
    if (!readMeta.is_open()) {
        std::cout << "ERROR OPENING FILE" << endl;
        return 1;
    }
    // count lines i.e. number of markers
    int lineN = 0;
    while (std::getline(readMeta, line) and readMeta.good())
    {
        ++lineN;
    }
    // reset filestream after counting lines
    readMeta.clear();
    readMeta.seekg(0);

    string* lines = new string[lineN];

    for (int i = 0; i < lineN; i++)
        std::getline(readMeta, lines[i]);

    readMeta.close();

    hwParams.s_objLens      = stoi( lines[0].substr(3));
    hwParams.s_jump_speed   = stoi( lines[1].substr(3));
    hwParams.s_mark_speed   = stoi( lines[2].substr(3));
    hwParams.s_FG_VISA      =       lines[3].substr(3);
    hwParams.s_Vpp_thresh   = stof( lines[4].substr(3));

    hwParams.s_xStageAddr   =       lines[5].substr(3);
    hwParams.s_yStageAddr   =       lines[6].substr(3);
    hwParams.s_zStageAddr   =       lines[7].substr(3);

    hwParams.s_xAxis[0]     =       lines[8].substr(3)[0];
    hwParams.s_yAxis[0]     =       lines[9].substr(3)[0];
    hwParams.s_zAxis[0]     =       lines[10].substr(3)[0];

    hwParams.s_lasWavel     = stof( lines[11].substr(3));
    hwParams.s_gateT        =       lines[12].substr(3);
    hwParams.s_gateCOMport  =       lines[13].substr(3);
    hwParams.s_gateCOMbaud  = stoi( lines[14].substr(3));

    hwParams.s_filled = true;

    delete[] lines;
    return 0;
}

std::string Microscope::create_timestamp() {
    auto current_time = std::time(nullptr);
    tm time_info{};
    const auto local_time_error = localtime_s(&time_info, &current_time);
    if (local_time_error != 0)
    {
        throw std::runtime_error("localtime_s() failed: " + std::to_string(local_time_error));
    }
    std::ostringstream output_stream;
    output_stream << std::put_time(&time_info, "%Y-%m-%d_%H-%M-%S");
    std::string timestamp(output_stream.str());

    return timestamp;
}

void Microscope::SetJumpSpeed(int JS)
{
    hwParams.s_jump_speed = JS;
    set_jump_speed_ctrl(hwParams.s_jump_speed);
}

void Microscope::SetMarkSpeed(int MS)
{
    hwParams.s_mark_speed = MS;
    set_mark_speed_ctrl(hwParams.s_mark_speed);
}

void Microscope::ResetADCboard() {
    Board.ConfigureChannel(CHANNEL_A, DC_COUPLING, InputRange, IMPEDANCE_1M_OHM);
    Board.ConfigureChannel(CHANNEL_B, DC_COUPLING, INPUT_RANGE_PM_2_V, IMPEDANCE_1M_OHM);
    Board.ConfigureTriggerEngine(TRIG_ENGINE_J, TriggerChannel, (U32)0, TRIGGER_SLOPE_POSITIVE);
}


void Microscope::AskParameters()
{
    std::cout << "Set Parameters of Microscope" << endl;
        
    string  input_str;    double  tempD;    int     tempI;    bool    valid;
    do
    {
        std::cout << "Field of View in um: ";
        std::cin >> input_str;
        if (isNumeric(input_str))
            tempD = stoi(input_str);
        else
            tempD = -999;
        valid = (tempD > 0);
    } while (!valid);
    FOV = tempD;

    do
    {
        std::cout << "Number of pixels in one axis: ";
        std::cin >> input_str;
        if (isNumeric(input_str))
            tempI = stoi(input_str);
        else
            tempI = -999;
        valid = (tempI > 0);
    } while (!valid);
    pixels = tempI;
    AskParameters(tempD, tempI);

    std::cout << "All parameters set..." << endl << endl;
}

void Microscope::AskParameters(double newFOV, int newPx)
{
    FOV             = newFOV;
    pixels          = newPx;
    pixelsizeMetric = FOV / pixels;
    pixelsizeGalvo  = pixelsizeMetric * galvoPixelCalib;

    std::cout << "Pixelsize is now" << 1000 * pixelsizeMetric << " nm" << endl;

    Tiff.setAttribute(ID_FieldOfView, "Field of View [µm]", newFOV);
    Tiff.setBitmapSize(pixels, pixels);
    Tiff.setAttribute(ID_PixelSize, "Pixel size [µm]", pixelsizeMetric);

    // initialize/allocate memory for galvo position return values
    if (gPosResult == NULL)
        gPosResult = new int[4];

    // initialize/allocate memory for galvo coordinates
    if (gCoords != NULL)
        delete[] gCoords;
    gCoords = new int[pixels];

    // initialize/allocate memory for an image
    if(picture != NULL)
        delete[] picture;
    picture = new WORD[pixels * pixels];

    // generate galvo coordinate list
    for (int i = 0; i < pixels; i++)    {
        gCoords[i] = (int)(1.*i * pixelsizeGalvo - pixels / 2. * pixelsizeGalvo +0.5); // calculate coords as double and then round to integer so galvo doesn't become stuck. This happens if the pixelsize is uneven!
    }

}

// Function that is basicly the main menu of the microscope software
void Microscope::AskTask()
{
    std::cout << "Welcome to the Confocal-O-Mat! What do you wish to do?" << endl;
    std::cout << "1: Take and save overview with stage position readout" << endl;
    std::cout << "2: Take and save image " << endl;
    std::cout << "3: Start interactive mode" << endl;
    std::cout << "4: Change Image parameters" << endl;
    std::cout << "5: Set Excitation Parameters" << endl;
    std::cout << "6: Take Stack Image" << endl;
    std::cout << "7: Move Piezo Stage" << endl;
    std::cout << "8: Do Dynamic Measurement from current slice" << endl;
    std::cout << "9: Take several images in a row" << endl;

    std::cout << "10: Perform an axial line scan" << endl;
    std::cout << "11: Image along axial direction" << endl;
    std::cout << "12: Change APD M factor (5 to 400)" << endl;
    std::cout << "13: Take Stack Image with dynamic Gain reduction" << endl;
    std::cout << "14: Mean over several slices" << endl;
    std::cout << "15: Do dynamic measurement with mean in z direction" << endl;
    std::cout << "16: Empty.." << endl;
    std::cout << "17: Querry Gate Box for Gate Duration!" << endl;

    std::cout << "22: Do a single vibrometry measurement" << std::endl;

    std::cout << "0:  Exit Programm"  << endl << endl;

    std::cout << "71: Perform piezo frequency drift calibration" << endl;
    std::cout << "81: Determine APD Saturation point " << endl;
    std::cout << "82: Empty" << endl;
    std::cout << "83: Test moving through markers" << endl;
    //std::cout << "88: Do Dynamic Measurement for several frequencies" << endl;
    std::cout << "89: Do Dyn. Measurement for several freq. at all Marker pos." << endl; 
    //std::cout << "99: Perform Calibration measurement" << endl;

    std::cout << endl;
    std::cout << "Your choice: \n";

    string choice_str;
    unsigned int choice;
    std::cin >> choice_str;

    if (isNumeric(choice_str))
        choice = stoi(choice_str);
    else
        choice = 999999;

    std::cout << endl;

    switch (choice)
    {
    case 1:
    {
        std::cout << "Imaging in slow mode.." << endl;
        bool linewise;
        std::cout << "Scan linewise? (0/1)" << endl;
        std::cin >> linewise;

        if (!UpdateStagePositions(xPosition, yPosition, zPosition)) {
            Tiff.setAttribute(ID_xPos, "x Position [mm]", xPosition,6);
            Tiff.setAttribute(ID_yPos, "y Position [mm]", yPosition,6);
            Tiff.setAttribute(ID_zPos, "z Position [mm]", zPosition,6);

            if (linewise)
                TakeImageSinglePixel();
            else
                TakeImageSinglePixelCol();

            microscopeAPDM = APD0.QuerryCurrentGain();
            SaveAsTiff(picture, "overview.tif");
        }
        else {
            std::cout << "Not all stages were available - imaging stopped" << endl;
        }
        break;
    }
    case 2:
    {
        std::cout << "Imaging w/o position loging.." << endl;

        bool linewise;
        bool fast;
        std::cout << "Scan linewise? (0/1)" << endl;
        std::cin >> linewise;
        std::cout << "Use fast mode? (0/1)" << endl;
        std::cin >> fast;

        if (linewise) {
            if(fast)
                TakeImageRows();
            else
                TakeImageSinglePixel();
        }
        if(!linewise){
            if (fast)
                TakeImageColumns();
            else
                TakeImageSinglePixelCol();
        }

        microscopeAPDM = APD0.QuerryCurrentGain();
        SaveAsTiff(picture,"output.tif");
        break;
    }
    case 3:
        std::cout << "Imaging in live mode.." << endl;
        InteractiveMode();
        break;
    case 4:
        AskParameters();
        break;
    case 5:
        std::cout << "Setting excitation parameters." << endl;
        SetExcitParam();
        break;
    case 6:
        std::cout << "Stack imaging selected.." << endl;
        bool fast;
        std::cout << "use fast mode (1) or slow mode (0)? ";
        std::cin >> fast;
        int n;
        std::cout << "Number of images: ";
        std::cin >> n;
        double stepsize;
        std::cout << "Stepsize (um): ";
        std::cin >> stepsize;
        TakeStack(stepsize, n, fast);
        break;
    case 7:
        std::cout << "Axial movement selected.." << endl;
        std::cout << "Number of um movement: ";
        double distance;
        std::cin >> distance;
        if(!allStagesConnected)
            zStage.ConnectAndStartUpFixedAxis(hwParams.s_zStageAddr.c_str(), hwParams.s_zAxis,true);
        zPosition = zStage.currentPos;
        SetzPosition(zPosition + distance / 1000);
        if (!allStagesConnected)
            zStage.DisconStage();
        //yStage.ConnectAndStartUpFixedAxis(linStageSerialY, yStageAxis, true);
        //yPosition = yStage.currentPos;
        //SetzPosition(yPosition + distance / 1000);
        //yStage.DisconStage();
        break;
    case 8:
        std::cout << "Single frequency Dynamic measurement selected.." << endl;
        DynamicMeasurement();
        //float Npx;
        //cout << "Enter datapoints per px" << endl;
        //cin >> Npx;
        //TakeAndSaveMotionImages(Npx);
        break;
    case 9:
    {
        std::cout << "Taking several images selected.." << endl;
        //int T;
        //std::cout << "Time for each picture in seconds: " << endl;
        //cin >> T;
        int number;
        std::cout << "Number of pictures: " << endl;
        std::cin >> number;
        bool fast;
        std::cout << "use fast mode? (1/0) " << endl;
        std::cin >> fast;
        //TakeSeveralImages(number, T, fast);
        bool scan;
        std::cout << "linewise / columnwise (1/0)?" << endl;
        std::cin >> scan;
        TakeConsecutiveImages(number, fast, scan);
        break;
    }
    case 10:
        std::cout << "Axial line scan selected.." << endl;
        int samplesZ;
        double dist;
        do
        {
            std::cout << "Which distance should be measured (um)? ";
            std::cin >> dist;
        } while (dist <= 0);
        do
        {
            std::cout << "How many samples should be taken? ";
            std::cin >> samplesZ;
        } while (samplesZ <= 0);
        TakeZLineScan(dist, samplesZ);
        break;
    case 11:
    {
        std::cout << "Axial image selected.." << endl;
        int orientation = 3;
        int axialPx = 0;
        do
        {
            std::cout << "Pixels in axial direction? ";
            std::cin >> axialPx;
        } while (axialPx <= 0);
        do
        {
            std::cout << "Orientation of image? XZ/YZ (1/0)? ";
            std::cin >> orientation;
        } while (orientation > 1);
        TakeAxialImage(axialPx, orientation);
        break;
    }
    case 12:
        std::cout << "Setting new APD gain selected.." << endl;
        if (APD0.SetGain(&microscopeAPDM) == 0)
            std::cout << "Setting gain failed - check COM-port vacancy" << endl;
        break;
    case 13:
    {
        std::cout << "Stack imaging with dynamic gain ajdustment selected.." << endl;
        bool fast;
        std::cout << "use fast mode (1) or slow mode (0)? ";
        std::cin >> fast;
        int n;
        std::cout << "Number of images: ";
        std::cin >> n;
        double stepsize2;
        std::cout << "Stepsize (um): ";
        std::cin >> stepsize2;
        TakeStackGainAdj(stepsize2, n, fast);//TakeStack(stepsize, n, fast);
        break;
    }
    case 14:
    {
        std::cout << "Averaging over several slices selected.." << endl;
        unsigned int NumberMean;
        float dz;
        std::cout << "Number of images: ";
        std::cin >> NumberMean;
        std::cout << "Distance between two images: ";
        std::cin >> dz;
        TakeImageMeanZ(NumberMean, dz);
        string fn = to_string(NumberMean) + "images_" + to_string(dz) + "um.tif";
        SaveAsTiff(picture,fn.c_str());
        break;
    }
    case 15:
        std::cout << "Dynamic measurement with mean over several slices selected.." << endl;
        unsigned int numberSlices;
        float Deltaz;
        double timeMean;
        std::cout << "Time to be measured in ms: ";
        std::cin >> timeMean;
        std::cout << "Number of slices to build mean: ";
        std::cin >> numberSlices;
        std::cout << "Distance between two slices (um): ";
        std::cin >> Deltaz;
        TakeAndSaveMotionImagesMeanZ(timeMean, numberSlices, Deltaz);
        break;
    case 16:
        std::cout << "Empty\n";
        break;

    case 17:
        std::cout << "Sending Querry Command To Gate Box: \n";
        GateControl.SendCommand("Q",1);
        system("pause");
        break;


    case 22:
    {
        int ex, N; char trigC; int trigEdge; bool show; 
        bool demod          = false; 
        bool validSmooth    = false;
        bool timeStamp      = false;
        int smooth          = 1;

        std::cout << "Include Timestamp in filenames? (y/n)(1/0)\n";
        std::cin >> timeStamp;

        std::cout << "Demodulate and decimate data? (y/n)(1/0)?\n";
        std::cin >> demod;

        trigEdge = SPC_TM_NEG;
        //trigC = 'e';
        std::cout << "Enter trigger source channel/external (c/e)" << endl;
        std::cin >> trigC;
        std::cout << "Single vibrometry measurement chosen" << std::endl;
        std::cout << "Enter n for 2^n samples to be acquired" << std::endl;
        std::cin >> ex;
        N = pow(2, ex);
        std::cout << N << " Samples will be acquired.\n";
        if (demod == true) {

            do {
                std::cout << "Set movAvg window length\n";
                std::cout << "Chose power of 2 and integer divider of " << N << "\n";
                std::cin >> smooth;
                // check for valid power of two - otherwise DSP will fail
                if (!((smooth != 0) && !((smooth & (smooth - 1)) == 0)))
                    validSmooth = true;
                else
                    validSmooth = false;
                // check for integer divisor of total record length - otherwise DSP will fail
                if (N % smooth != 0)
                    validSmooth = false;
            } while (!validSmooth);
        }

        //RunSingleVibro(N,'c',trigEdge,1);
        if (demod == false) {
            // make sure sample excitation is ON
            Fgen.OutPutEnable(1);
            RunSingleVibro(N, trigC, trigEdge, timeStamp);
        }
        else {
            RunSingleVibroDemod(N, trigC, trigEdge, smooth, timeStamp);
        }
        // make sure sample excitation is OFF
        Fgen.OutPutDisable(1);
        break;
    }


    case 0:
        bool backup;
        std::cout << "Do you really want to quit the Confocal-O-Mat? (1/0) ";
        std::cin >> backup;
        if (backup)
            running = false;
        break;
    case 81:
        int startM;
        int Msteps;
        bool UpDown;
        std::cout << "Enter starting gain value: " << endl;
        std::cin >> startM;
        std::cout << "Enter number of gain steps: " << endl;
        std::cin >> Msteps;
        std::cout << "Increase (1) or decrease (0) gain: " << endl;
        std::cin >> UpDown;
        determineAPDSaturation(startM, Msteps, UpDown);
        break;
    case 82:
    {
        std::cout << "Empty.." << endl;
        break;
    }
    case 83: {
        std::cout << "Testing moving through a few markers" << endl;
        MoveThroughMarkers();
        break;
    }
    case 71: {
        bool val  = false;
        bool linw = true;
        int  warmup = 10;
        std::cout << "Generate Piezo Calibration Data" << endl;
        std::cout << "Calibrate (0) or validate (1) \n";
        std::cin >> val;
        std::cout << "Chose settling time in seconds (default is 10)\n";
        std::cin >> warmup;
        if (warmup > 30) {
            warmup = 10; std::cout << "Are you crazy? warm up set to 10s!\n";
        }
        
        std::cout << "Linewise imaging? (y/n) (1/0)\n";
        std::cin >> linw;
        GeneratePiezoDriftCalib(val,warmup, linw);

        if (val == true) 
            std::cout << "Validation finished with settling time of " << warmup << "s\n";
        else 
            std::cout << "Calibration finished with settling time of " << warmup << "s\n";
        if (linw == true)
            std::cout << "and linewise imaging\n";
        else 
            std::cout << "and columnwise imaging\n";
        
        break;
    }

    case 89: {
        string  input_str;    int  input_int;
        bool acquireOverview, driftComp;
        bool validConf = false;
        int firstLayerNo;
        std::cout << "Performing dynamic measurement on sample at marker positions " << endl;
        std::cout << "Loading measurement config file\n";
        // check if config file exists
        if (!exists(measurementConfFileName)){
            std::cout << "No measurement config file found!\n";
            std::cout << "File name should be: " << measurementConfFileName << "\n";
            std::cout << "Check if file exists and restart process!\n";
            std::cin.get();
            break;
        }
        while (!validConf){
            std::cout << "Config file found!\n";
            readMeasurementConfigFile(measurementConfFileName, dynMeasParams);
            std::cout << dynMeasParams;
            //std::cout << "Parameters ok? (1/0)\n";
            std::cout << "Parameters ok? proceed: 1, reload parameters: 0, abort: a\n";

            std::cin >> input_str;
            if (!isNumeric(input_str)) {
                std::cout << "Aborting.." << std::endl;
                goto abort_label;
            }
            else if (isNumeric(input_str))
                validConf = (bool)stoi(input_str);
            else
                validConf = false;
        }

        std::cout << "Select dynamic measurement mode dynMode1/dynMode2 (0/1)" << endl;
        std::cin >> input_str;
        if (isNumeric(input_str))
            dynModeSelect = stoi(input_str);
        else {
            std::cout << "Invalid input! Exiting!\n";
            goto abort_label;
        }


        std::cout << "Enter first layer number" << endl;
        std::cin >> input_str;
        if (isNumeric(input_str))
            firstLayerNo = stoi(input_str);
        else {
            std::cout << "Invalid input! Exiting!\n";
            goto abort_label;
        }
        //std::cin >> firstLayerNo;

        std::cout << "Acquire Overview Images (1/0)?" << endl;
        std::cin >> input_str;
        if (isNumeric(input_str))
            acquireOverview = stoi(input_str);
        else {
            std::cout << "Invalid input! Exiting!\n";
            goto abort_label;
        }
        //std::cin >> acquireOverview;

        std::cout << "Compensate piezo drift (1/0)?" << endl;
        std::cin >> input_str;
        if (isNumeric(input_str))
            driftComp = stoi(input_str);
        else {
            std::cout << "Invalid input! Exiting!\n";
            goto abort_label;
        }
        //std::cin >> driftComp;

        if (driftComp and ReadMarkerCoords("PiezoDriftCalib\\driftCalib.dat", PiezoDriftCorr)) {
            std::cout << "Couldn't read piezo drift compensation data\n";
            goto abort_label;
        }

        if (!CreateDirectoryA(dynMeasParams.s_dynMeasSeriesPath.c_str(), NULL)) {
            std::cout << "Could not create folder.." << endl;
            std::cout << "Foulder may already exist.." << endl;
        }

        if (dynModeSelect == 0)
            DynMeasSample("", firstLayerNo, acquireOverview, driftComp);
        else if (dynModeSelect == 1) {
            DynMeasSample2("", firstLayerNo, acquireOverview, driftComp);
        }

        CopyDatFile(hardwareConfFileName, dynMeasParams.s_dynMeasSeriesPath + "\\" + hardwareConfFileName);
        CopyDatFile(measurementConfFileName, dynMeasParams.s_dynMeasSeriesPath + "\\" + measurementConfFileName);
  
        break;
    }
    //case 91:
    //    slm0.debugThisSLM();
    //    break;
    //case 92:
    //    TakeImageRowsAsynch();
    //    SaveAsTiff(picture,"output.tif");
    //    break;
    //case 93:
    //    FindCorrectionPatterns();
    //    break;
    //case 94:
    //    TakeImagePhaseMasks();
    //    SaveAsTiff(picture,"output.tif");
    //    break;
    //case 95:
    //    FindCorrectionPatterns();
    //    TakeImagePhaseMasks();
    //    SaveAsTiff(picture,"output.tif");
    //    break;
    //case 96:
    //    loadTestImageOnSLM();
    //    break;
    //case 97:
    //    optimizeSinglePixelSlow();
    //    break;
    //case 98:
    //    int segX, segY;
    //    std::cout << "Give segment coodinates (<= 22, <= 20): ";
    //    std::cin >> segX;
    //    std::cin >> segY;
    //    circleSegment(segX * slm0.getWidth() / slm0.getSegWidth() + segY);
    //    break;
    //case 99:
    //    CalibrationRoutine();
    //    break;
    default:
        abort_label:
        std::cout << "Process aborted!\n";
        cout << "Press Enter to Continue\n";
        getch();
        break;
    }
    std::cout << endl;
}


void Microscope::TakeImageSinglePixel(bool trig){
    Tiff.setAttribute(ID_MeasurementMode, "Measurement mode", "Slow Mode");

	// Configuring Board
    Board.SetSampleRate(SAMPLE_RATE_50MSPS);
    int dwellTime; // pixel dwell time in µs
    U32 samples = 26;

	Board.ConfigureBufferRecord(0, samples, U32(pixels * pixels)); // "pixels * pixels" records with "samples" samples each
	Board.SinglePortAcquisition(false, DetectorChannel, false);    // "wait" = false to continue in programm

    bool uniaxial = true;
    //std::cout << "Scan uniaxial (1) or bidirectional (0)? ";
    //std::cin >> uniaxial;

    // Create List in Galvo Scanner

    set_start_list(1);
    set_wait(0);

    jump_abs(gCoords[0], gCoords[0]);
    set_wait(0);
    if (uniaxial)
    {
        for (int i = 0; i < pixels; i++)
        {
            for (int j = 0; j < pixels; j++)
            {
                mark_abs(gCoords[j], gCoords[i]);
                set_wait(i * pixels + j + 1);
            }
        }
    }
    else
    {
        for (int i = 0; i < pixels; i++)
        {
            for (int j = 0; j < pixels; j++)
            {
                if (i % 2 == 0)
                {

                    jump_abs(gCoords[j], gCoords[i]);
                    set_wait(i * pixels + j + 1);
                }
                else
                {
                    jump_abs(gCoords[pixels - j - 1], gCoords[i]);
                    set_wait(i * pixels + j + 1);
                }
            }
        }
    }
    jump_abs(0, 0);
    set_end_of_list();
    execute_list(1);

    // scanning part

    // For readout of xy position
    // 05: SetMode (readout); 01: Is position
    control_command(1, 1, 0x0501); // x position
    control_command(1, 2, 0x0501); // y position

    int signal[4] = { 1, 2, 0, 0 }; // Tells which variables should be read out (here x, y, 0, 0)

    bool busy;
    UINT Status;
    UINT Pos;

    std::cout << "Scanning..." << endl;
    release_wait();
    this_thread::sleep_for(chrono::microseconds(100)); // for the lolz..
    auto start_scanning = chrono::steady_clock::now();
    for (int i = 0; i < pixels; i++)
    {
        //std::cout << "Row " << i + 1 << endl;
        release_wait();
        //this_thread::sleep_for(chrono::microseconds(100)); 
        int posCount = 0;
        do
        {   // Check actual position until continue
            //get_values((ULONG_PTR)signal, (ULONG_PTR)result);
            get_values((ULONG_PTR)signal, (ULONG_PTR)gPosResult);
            // Convert Adresses to ULONG 
            if (abs(gPosResult[0] - gCoords[0]) < 4)  // Since scanlab measures distances in "bit-values" 600 = 1mu (for Nikon50x), 1 = 1/600mu as tolerance for "galvo overswing" // 4 is the amplitude of the position noise
                posCount++;
        } while (posCount < 4); // Mirror is at the beginning of a line again. The "threshold" value for posCaount is defined rather arbitrarily to give good images. 
        // it is chosen > 1 to account for multiple overswings..
        posCount = 0; //Reset

        Board.SoftwareTrigger(); // Trigger to take Intensity
        //std::this_thread::sleep_for(std::chrono::microseconds(1000)); // Delay to simulate dynamic mode
        for (int j = 1; j < pixels; j++)
        {
            release_wait();
            do
            {   // Check actual position until continue
                get_values((ULONG_PTR)signal, (ULONG_PTR)gPosResult);
                // Convert Adresses to ULONG 
            } while (gPosResult[0] < gCoords[j]); // Mirror is moving from the previous pixel
            Board.SoftwareTrigger(); // Trigger to take Intensity
            //std::this_thread::sleep_for(std::chrono::microseconds(1000)); // Delay to simulate dynamic mode
        }
    }
    while (AlazarBusy(Board.GetBoardHandle())) {}
    release_wait();
    auto end_scanning = chrono::steady_clock::now();
    double time_scanning = chrono::duration_cast<chrono::milliseconds>(end_scanning - start_scanning).count();


    // readout data and write to Intensity array

    std::cout << "Reading Data..." << endl;
    auto start_readout = chrono::steady_clock::now();



    for (int i = 0; i < pixels * pixels; i++)
    {
        int     n; // this is really messy code and should not be done this way. I leave it in bc it works and is seldomly used.. I hope..
        float temp = 0;
        WORD* data = Board.ReadDatafromBuffer(n, DetectorChannel, i + 1);
        for (int j = 0; j < n; j++)
            temp += data[j];
        picture[i] = (WORD)(temp / n + 0.5);
        //picture[i] = (WORD)(data[0]);  // no averaging here
        free(data); // since ReadDatafromBuffer calls malloc, i need to call free here to plug memory leak
        data = NULL;
    }
    auto    end_readout     = chrono::steady_clock::now();
    double  time_readout    = chrono::duration_cast<chrono::milliseconds>(end_readout - start_readout).count();

    // Sort Array if bidirectional
    if (!uniaxial)
    {
        WORD temp;
        for (int i = 0; i < pixels; i++)
        {
            if (i % 2 != 0)
            {
                for (int j = 0; j < pixels / 2; j++)
                {
                    temp = picture[i * pixels + j];
                    picture[i * pixels + j] = picture[i * pixels + pixels - 1 - j];
                    picture[i * pixels + pixels - 1 - j] = temp;
                }
            }
        }
    }

    std::cout << "Time for scanning:     " << time_scanning << " ms" << endl;
    std::cout << "Time for reading data: " << time_readout << " ms" << endl;
}

void Microscope::TakeImageSinglePixelCol(bool trig){
    Tiff.setAttribute(ID_MeasurementMode, "Measurement mode", "Slow Mode");

    // Configuring Board
    Board.SetSampleRate(SAMPLE_RATE_50MSPS);
    int dwellTime; // pixel dwell time in µs
    U32 samples = 26;

    Board.ConfigureBufferRecord(0, samples, U32(pixels * pixels)); // "pixels * pixels" records with "samples" samples each
    Board.SinglePortAcquisition(false, DetectorChannel, false);      // "wait" = false to continue in programm

    bool uniaxial = true;
    //std::cout << "Scan uniaxial (1) or bidirectional (0)? ";
    //std::cin >> uniaxial;

    // Create List in Galvo Scanner

    set_start_list(1);
    set_wait(0);
    jump_abs(gCoords[0], gCoords[0]);
    set_wait(0);

    for (int i = 0; i < pixels; i++)
    {
          for (int j = 0; j < pixels; j++)
          {
             mark_abs(gCoords[i], gCoords[j]);
             set_wait(i * pixels + j + 1);
          }
    }

    jump_abs(0, 0);
    set_end_of_list();
    execute_list(1);

    // scanning part

    // For readout of xy position
    // 05: SetMode (readout); 01: Is position
    control_command(1, 1, 0x0501); // x position
    control_command(1, 2, 0x0501); // y position

    int signal[4] = { 1, 2, 0, 0 }; // Tells which variables should be read out (here x, y, 0, 0)
    //int* result = new int[4];       // Pointer for the results from readout

    bool busy;
    UINT Status;
    UINT Pos;

    std::cout << "Scanning..." << endl;
    release_wait();
    this_thread::sleep_for(chrono::microseconds(100)); // for the lolz..
    auto start_scanning = chrono::steady_clock::now();
    for (int i = 0; i < pixels; i++)
    {
        //std::cout << "Row " << i + 1 << endl;
        release_wait();
        //this_thread::sleep_for(chrono::microseconds(100)); 
        int posCount = 0;
        do
        {   // Check actual position until continue
            get_values((ULONG_PTR)signal, (ULONG_PTR)gPosResult); // Convert Adresses to ULONG 
            if (abs(gPosResult[1] - gCoords[0]) < 4)  // Since scanlab measures distances in "bit-values" 600 (for Nikon50x) = 1mu, 1 = 1/600mu as tolerance for "galvo overswing"
                posCount++;
        } while (posCount < 4); // Mirror is at the beginning of a line again. The "threshold" value for posCount is defined rather arbitrarily to give good images. 
        // it is chosen > 1 to account for multiple overswings..
        posCount = 0; //Reset

        Board.SoftwareTrigger(); // Trigger to take Intensity
        //std::this_thread::sleep_for(std::chrono::microseconds(1000)); // Delay to simulate dynamic mode
        for (int j = 1; j < pixels; j++)
        {
            release_wait();
            do
            {   // Check actual position until continue
                //get_values((ULONG_PTR)signal, (ULONG_PTR)result);
                get_values((ULONG_PTR)signal, (ULONG_PTR)gPosResult);
                // Convert Adresses to ULONG
            } while (gPosResult[1] < gCoords[j]); // Mirror is moving from the previous pixel
            Board.SoftwareTrigger(); // Trigger to take Intensity
            //std::this_thread::sleep_for(std::chrono::microseconds(1000)); // Delay to simulate dynamic mode
        }
    }
    while (AlazarBusy(Board.GetBoardHandle())) {}
    release_wait();
    auto end_scanning = chrono::steady_clock::now();
    double time_scanning = chrono::duration_cast<chrono::milliseconds>(end_scanning - start_scanning).count();


    // readout data and write to Intensity array

    std::cout << "Reading Data..." << endl;
    auto start_readout = chrono::steady_clock::now();


    for (int i = 0; i < pixels; i++) {
        for (int j = 0; j < pixels; j++) {
            int n;
            U16* data = Board.ReadDatafromBuffer(n, DetectorChannel, (i*pixels+j) + 1);
            float temp = 0;
            for (int k = 0; k < n; k++)
                temp += data[k];
            picture[j*pixels + i] = (WORD)(temp / n + 0.5);
            //pic[j * pixels + i] = (WORD)data[0]; //No averaging here..
        }
    }

    auto end_readout = chrono::steady_clock::now();
    double time_readout = chrono::duration_cast<chrono::milliseconds>(end_readout - start_readout).count();


    std::cout << "Time for scanning:     " << time_scanning << " ms" << endl;
    std::cout << "Time for reading data: " << time_readout << " ms" << endl;
}

void Microscope::TakeOverviewImage(bool linewise, double oV_FOV, int oV_px, char * sideNo, std::string path) { // path is empty string as default value
    // store current values
    double          tempFOV = GetFOV();
    unsigned int tempPixels = GetPixels();

    std::cout << "For overview: ";
    AskParameters(oV_FOV, oV_px);

    //cout << "tempFOV = " << tempFOV << endl;
    //cout << "tempPixels = " << tempPixels << endl;

    // replace with values for overview image
    //FOV     = oV_FOV;
    //pixels  = oV_px;

    Tiff.setAttribute(ID_FieldOfView, "Field of View [µm]", FOV);
    Tiff.setBitmapSize(pixels, pixels);
    Tiff.setAttribute(ID_PixelSize, "Pixel size [µm]", pixelsizeMetric);

    //delete[] picture;
    //picture = new WORD[pixels * pixels];
    //delete[] gCoords;
    //gCoords = new int[pixels];
    //AskParameters(FOV, pixels);

    //cout << "New values for overview" << endl;
    //cout << "FOV = " << FOV << endl;
    //cout << "pixels = " << pixels << endl;

    //string filename = path + "\\" + sideNo + "_Overview.tif";
    string filename = path + "\\" + "Overview_" + sideNo + ".tif";

    //cout << "filename for overview image is = " << filename << endl;

    if (!UpdateStagePositions(xPosition, yPosition, zPosition)) {
        Tiff.setAttribute(ID_xPos, "x Position [mm]", xPosition,6);
        Tiff.setAttribute(ID_yPos, "y Position [mm]", yPosition,6);
        Tiff.setAttribute(ID_zPos, "z Position [mm]", zPosition,6);

        std::cout << "Imaging started" << endl;

        if (linewise)
            TakeImageRows();
            //TakeImageSinglePixel();
        else
            TakeImageColumns();
            //TakeImageSinglePixelCol();

        std::cout << "Tiff saving now" << endl;
        SaveAsTiff(picture, filename.c_str());
    }
    else {
        std::cout << "Not all stages were available - imaging stopped" << endl;
    }

    // reset to previous values
    std::cout << "Resetting image and pixel size: ";
    AskParameters(tempFOV, tempPixels);
    //FOV = tempFOV;
    //pixels = tempPixels;
    // set Tiff attributes accordingly
    
    Tiff.setAttribute(ID_FieldOfView, "Field of View [µm]", FOV);
    Tiff.setBitmapSize(pixels, pixels);
    Tiff.setAttribute(ID_PixelSize, "Pixel size [µm]", FOV / pixels);

    //delete[] picture;
    //picture = new WORD[pixels * pixels];
    //delete[] gCoords;
    //gCoords = new int[pixels];
    //AskParameters(FOV, pixels);

    //cout << "reset values " << endl;
    //cout << "FOV = " << FOV << endl;
    //cout << "pixels = " << pixels << endl;

    //// reset board
    //ResetADCboard();
    //cout << "ADC Board reset again" << endl;

    std::cout << "Overview Image acquired" << endl;
}

void Microscope::TakeImageRows()
{
    int waitCounter = 0;
    int releaseCounter = 0;
    Tiff.setAttribute(ID_MeasurementMode, "Measurement mode", "Fast Mode");
     
    // Configuring Board
    Board.SetSampleRate(SAMPLE_RATE_200KSPS);
    int offset = 36; // correct shift 
    int pretrigger = 0;
    Board.ConfigureBufferRecord(pretrigger, pixels + offset - pretrigger, pixels); // "pixels" records with "pixels+offset-pretrigger" samples each
    Board.SinglePortAcquisition(false, DetectorChannel, false);      // "wait" = false to continue in programm
    Board.ConfigureTriggerEngine(TRIG_ENGINE_J, TriggerChannel, (U32)150, TRIGGER_SLOPE_POSITIVE); // Trigger for synchronisation

    // calculate time for one line with given sample rate (TODO: arbritrary sample rate)
    int samplerate = 200000; // S/s
    int Time = pixels * 1e6 / samplerate + 9; //(+9 from jump delay default value) // 1e6/samplerate gives time in microseconds
    if (FOV * galvoPixelCalib * samplerate / pixels > 112350000) // jump speed higher than possible for Scanner
        std::cout << "Warning: Velocity too high. Increase pixelsize" << endl;    //????????????? Reduce? Rather increase pixelsize or reduce amount of pixels, right?

    // Create List for Galvo Scanner
    set_start_list(1);
    for (int i = 0; i < pixels; i++)
    {
        micro_vector_abs(gCoords[0], gCoords[i], -1, -1); // fastest way to get back to beginning of a line (so far)
        set_wait(i+1); waitCounter++;
        timed_jump_abs(gCoords[pixels - 1], gCoords[i], Time); // Try timed_mark_abs if precision is neccessary 
    }
    jump_abs(0, 0);
    set_end_of_list();
    execute_list(1);

    // For readout of xy position
    // 05: SetMode (readout); 01: Is position
    control_command(1, 1, 0x0501); // x position
    control_command(1, 2, 0x0501); // y position

    int signal[4] = { 1, 2, 0, 0 }; // Tells which variables should be read out (here x, y, 0, 0)
    //int* result = new int[4];       // Pointer for the results from readout

    bool busy;
    UINT Status;
    UINT Pos;

    //std::cout << "Scanning..." << endl;
    //std::this_thread::sleep_for(std::chrono::microseconds(100000)); // time that needs to be waited or the images are weirdly distorted..
    std::this_thread::sleep_for(std::chrono::microseconds(10000)); //
    auto start_scanning = chrono::steady_clock::now();
    for (int i = 0; i < pixels; i++) // Loop over all lines
    {
        do
        {   // Check actual position until continue
            //get_values((ULONG_PTR)signal, (ULONG_PTR)result); // Convert Adresses to ULONG 
            get_values((ULONG_PTR)signal, (ULONG_PTR)gPosResult); // Convert Adresses to ULONG 
        //} while (gPosResult[0] > -FOV / 2 * galvoPixelCalib); // Mirror is at the beginning of a line again
        } while (gPosResult[0] >= gCoords[0]); // Mirror is at the beginning of a line again
        release_wait(); releaseCounter++;// Start scanning the line
        do
        {
            get_status(&Status, &Pos);
            busy = (bool)(Status & 1); // last bit from Status is busy variable
        } while (busy);
    }
    int overhead = 0;
    while (AlazarBusy(Board.GetBoardHandle())) // Check how many triggers are missing (should be 0)
    {
        Board.SoftwareTrigger();
        std::this_thread::sleep_for(std::chrono::microseconds(1000));
        overhead++;
    }
    if (overhead > 0)
        std::cout << "Warning: " << overhead << " Trigger events for a line are missing" << endl;
    release_wait(); releaseCounter++;
    auto end_scanning = chrono::steady_clock::now();
    double time_scanning = chrono::duration_cast<chrono::milliseconds>(end_scanning - start_scanning).count();

    // readout data and write to Intensity array
    //std::cout << "Reading Data..." << endl;
    auto start_readout = chrono::steady_clock::now();
    for (int i = 0; i < pixels; i++) // Each record stands for one line
    {
        int n;
        U16* data = Board.ReadDatafromBuffer(n, DetectorChannel, i + 1);
        for (int j = 0; j < pixels; j++) // add current line at the end of picture
        {
            picture[i * pixels + j] = data[j + offset];
        }
        free(data); // since ReadDatafromBuffer calls malloc, i need to call free here to plug memory leak
        data = NULL; 
        //delete[] data;

    }
    auto end_readout = chrono::steady_clock::now();
    double time_readout = chrono::duration_cast<chrono::milliseconds>(end_readout - start_readout).count();

    //std::cout << "Time for scanning:     " << time_scanning << " ms" << endl;
    //std::cout << "Time for reading data: " << time_readout << " ms" << endl;

    Board.ConfigureTriggerEngine(TRIG_ENGINE_J, TriggerChannel, (U32)0, TRIGGER_SLOPE_POSITIVE); // reset Trigger Engine
}

void Microscope::TakeImageColumns()
{
    int waitCounter = 0;
    int releaseCounter = 0;
    Tiff.setAttribute(ID_MeasurementMode, "Measurement mode", "Fast Mode");

    // Configuring Board
    Board.SetSampleRate(SAMPLE_RATE_200KSPS); 
    int offset = 36; // correct shift 
    int pretrigger = 0;
    Board.ConfigureBufferRecord(pretrigger, pixels + offset - pretrigger, pixels); // "pixels" records with "pixels-offset-pretrigger" samples each
    Board.SinglePortAcquisition(false, DetectorChannel, false);      // "wait" = false to continue in programm
    Board.ConfigureTriggerEngine(TRIG_ENGINE_J, TriggerChannel, (U32)150, TRIGGER_SLOPE_POSITIVE); // Trigger for synchronisation

    // calculate time for one line with given sample rate (TODO: arbritrary sample rate)
    int samplerate = 200000; // S/s
    int Time = pixels * 1e6 / samplerate + 9; //(+9 from jump delay default value) // 1e6/samplerate gives time in microseconds
    if (FOV * galvoPixelCalib * samplerate / pixels > 112350000) // jump speed higher than possible for Scanner
        std::cout << "Warning: Velicity too high. Reduce pixelsize" << endl;    //????????????? Reduce? Rather increase pixelsize or reduce amount of pixels, right?

    // Create List for Galvo Scanner
    set_start_list(1);
    for (int i = 0; i < pixels; i++)
    {
        micro_vector_abs(gCoords[i], gCoords[0], -1, -1); // fastest way to get back to beginning of a column (so far)
        set_wait(i + 1); waitCounter++;
        timed_jump_abs(gCoords[i], gCoords[pixels - 1], Time); // Try timed_mark_abs if precision is neccessary 
    }
    jump_abs(0, 0);
    set_end_of_list();
    execute_list(1);

    // For readout of xy position
    // 05: SetMode (readout); 01: Is position
    control_command(1, 1, 0x0501); // x position
    control_command(1, 2, 0x0501); // y position

    int signal[4] = { 1, 2, 0, 0 }; // Tells which variables should be read out (here x, y, 0, 0)

    bool busy;
    UINT Status;
    UINT Pos;

    //std::cout << "Scanning..." << endl;
    //std::this_thread::sleep_for(std::chrono::microseconds(100000));// time that needs to be waited or the images are weirdly distorted..
    std::this_thread::sleep_for(std::chrono::microseconds(10000));// 
    auto start_scanning = chrono::steady_clock::now();
    for (int i = 0; i < pixels; i++) // Loop over all lines
    {
        do
        {   // Check actual position until continue
            get_values((ULONG_PTR)signal, (ULONG_PTR)gPosResult); // Convert Adresses to ULONG 
        } while (gPosResult[1] >= gCoords[0]); // Mirror is at the beginning of a line again
        release_wait(); releaseCounter++; // Start scanning the line
        do
        {
            get_status(&Status, &Pos);
            busy = (bool)(Status & 1); // last bit from Status is busy variable
        } while (busy);
    }
    int overhead = 0;
    while (AlazarBusy(Board.GetBoardHandle())) // Check how many triggers are missing (should be 0)
    {
        Board.SoftwareTrigger();
        std::this_thread::sleep_for(std::chrono::microseconds(1000));
        overhead++;
    }
    if (overhead > 0)
        std::cout << "Warning: " << overhead << " Trigger events for a line are missing" << endl;
    release_wait(); releaseCounter++;
    auto end_scanning = chrono::steady_clock::now();
    double time_scanning = chrono::duration_cast<chrono::milliseconds>(end_scanning - start_scanning).count();

    // readout data and write to Intensity array
    //std::cout << "Reading Data..." << endl;
    auto start_readout = chrono::steady_clock::now();
    for (int i = 0; i < pixels; i++) // Each record stands for one column
    {
        int n;
        U16* data = Board.ReadDatafromBuffer(n, DetectorChannel, i + 1);
        for (int j = 0; j < pixels; j++) // add current line at the end of picture
        {
            picture[j * pixels + i] = data[j + offset];
        }
        free(data); // since ReadDatafromBuffer calls malloc, i need to call free here to plug memory leak
        data = NULL;
        //delete[] data;
    }
    auto end_readout = chrono::steady_clock::now();
    double time_readout = chrono::duration_cast<chrono::milliseconds>(end_readout - start_readout).count();

    //std::cout << "Time for scanning:     " << time_scanning << " ms" << endl;
    //std::cout << "Time for reading data: " << time_readout << " ms" << endl;

    Board.ConfigureTriggerEngine(TRIG_ENGINE_J, TriggerChannel, (U32)0, TRIGGER_SLOPE_POSITIVE); // reset Trigger Engine
}



void Microscope::TakeImageMeanZ(unsigned int n, float dz)
{
    unsigned int NumPixels = pixels * pixels;
    unsigned int* buffer = new unsigned int[NumPixels]; // buffer for the sum of all images
    for (int i = 0; i < NumPixels; i++)
        buffer[i] = 0;
    double meanZ = zPosition; // Start position of the piezo stage

    SetzPosition(meanZ - double(n-1)/2 * dz/1000); // lowest position for stack, works for even and odd n

    for (int i = 0; i < n; i++)
    {
        TakeImageRows();
        for (int i = 0; i < NumPixels; i++)
            buffer[i] += picture[i];
        SetzPosition(zPosition + dz/1000);
    }

    for (int i = 0; i < NumPixels; i++)
        picture[i] = WORD(double(buffer[i]) / n + 0.5);

    delete[] buffer;
    SetzPosition(meanZ);
}

unsigned int Microscope::GetPixels()
{
    return pixels;
}

double Microscope::GetFOV()
{
    return FOV;
}

void Microscope::SaveAsTiff(WORD* pic, const char* filename)
{
    if (maxImage() >= ImagSatThresh)
    {
        //std::cout << "Warning: Image shows saturation! Maybe you want to reduce the Input Range." << endl;
    }
    Tiff.setAttribute(ID_APDGain, "APD Gain M", microscopeAPDM);
    Tiff.saveData(pic, filename);
}

void Microscope::SaveAsDat(WORD* pic, const char* filename)
{
    ofstream fileout(filename);

    for (int i = 0; i < pixels; i++)
    {
        for (int j = 0; j < pixels; j++)
            fileout << pic[i * pixels + j] << " ";
        fileout << endl;
    }

    fileout.close();
}


//bool Microscope::ConfSingleCvibro(uint64 samples, uint64 smplRt, string trigChoice, int trigEdge,  int chnlSel, int32 inptR, int trgLvl, bool inpt50vs1M, bool ACvsDC)
bool Microscope::ConfSingleCvibro(uint64 samples, uint64 smplRt, char trigChoice, int trigEdge,  int chnlSel, int32 inptR, int trgLvl, bool inpt50vs1M, bool ACvsDC)
{
    // all samples post trigger!

    int trigChnl = chnlSel-1; // only used for channel trigger!

    if (chnlSel == CHANNEL0) { 

        std::cout << "Channel trigger on Channel 0..\n";
    }
    else
    {
        std::cout << "Channel trigger on Channel 1..\n";
    }
     // set samples and sampling rate
    spcmADC.confSampling(smplRt, samples, samples);

    // configure trigger source/mode
    if (trigChoice == 's' || trigChoice == 'S') {
        std::cout << "Software trigger chosen\n";
        spcmADC.confSoftTrigger();
    }
    else if (trigChoice == 'c' || trigChoice == 'C') {
        std::cout << "Channel trigger on ";
        if (trigEdge == SPC_TM_POS)
            std::cout << "rising edge";
        else if (trigEdge == SPC_TM_NEG)
            std::cout << "falling edge";
        std::cout << "\n";
        spcmADC.confChanTrigger(trigChnl, trigEdge, trgLvl);

    }
    else if (trigChoice == 'e' || trigChoice == 'E')
    {
        std::cout << "External trigger on ";
        if (trigEdge == SPC_TM_POS)
            std::cout << "rising edge";
        else if (trigEdge == SPC_TM_NEG)
            std::cout << "falling edge";
        std::cout << "\n";
        spcmADC.confExtTrigger(trigEdge);
    }
    else {
        std::cout << "Invalid parameter for trigger mode.. Exiting\n";
        return 1;
    }
    
    // configure channel and prepare buffer
    spcmADC.confChannel(chnlSel, 0, inptR, inpt50vs1M, ACvsDC);
    spcmADC.prepareBuffer();
    //cout << "Buffer prepared..\n";
    return 0;
}

bool Microscope::ConfSingleCvibro() {
    uint64 sa, sr;
    //string tc;
    char tc;
    int te, cs, tl;
    int32 ir;
    bool it, ic;
    std::cout << "Please enter parameters for vibrometry configuration" << endl;
    std::cout << "Enter number of samples:" << endl;
    std::cin >> sa;

    std::cout << "Enter samplerate:" << endl;
    std::cin >> sr;

    std::cout << "Enter trigger choice: s <-> software, c <-> channel, e <-> external" << endl;
    std::cin >> tc;

    std::cout << "Enter trigger edge: 1 <-> rising edge, 2 <-> falling edge\n";
    std::cin >> te;

    std::cout << "Enter channel select: 1 <-> Chn0, 2 <-> Chn1\n";
    std::cin >> cs;

    std::cout << "Enter input range\n";
    std::cin >> ir;

    std::cout << "Enter trigger level between [-8191,8191] over input range.\n";
    std::cin >> tl;

    std::cout << "Enter input termination (1/0) <-> (50Ohm/1MOhm)\n";
    std::cin >> it;

    std::cout << "Enter input coupling (1/0) <-> (AC/DC)\n";
    std::cin >> ic;

    return ConfSingleCvibro(sa,sr,tc,te,cs,ir,tl,it,ic);
}

bool Microscope::RunSingleVibro(int Nsamples, char trigChoice, int trigEdge, bool timeStamp, bool showPlot) {
    long inputrange = 500;
    int showSamp    = 128;
    int trigLvl     = 0;


    //// generate galvo list 2 to toggle 8bit-port to trigger gate for transfer of sync signal through gate box
    ////set_start_list(1);
    //set_start_list_1();
    //set_wait(1);
    //jump_abs(0, 0); // make sure galvo is in center (should usually be the case but who knows)
    //write_8bit_port_list(0xff);  // writing to RTC6 card "EXTENSION 2 Stiftleiste" => Opens the gate for data aqcuisition
    //set_wait(2);
    //write_8bit_port_list(0);
    //set_end_of_list();

    //// For readout of xy position
    //// 05: SetMode (readout); 01: Is position
    //control_command(1, 1, 0x0501); // x position
    //control_command(1, 2, 0x0501); // y position

    //int signal[4] = { 1, 2, 0, 0 }; // Tells which variables should be read out (here x, y, 0, 0)
    //bool onTargetX = false;  // bool used to calculate if galvo is on designated postition from jump_abs(gCoords[k], gCoords[i]);
    //bool onTargetY = false;


    if (trigChoice == 'c' || trigChoice == 'C') {
        trigLvl = 205; // i chose those values from experience.. for channel trigger, this seems to be in ADC-code (NOT mV)
    }
    else if (trigChoice == 'e' || trigChoice == 'E')    {
        trigLvl = 800; // for an external trigger, the value seems to be in mV... manual p101f
    }

    string directory = "Motions\\Vibrometry_data\\data"; 
    string timestamp = "";
    if (timeStamp)
        timestamp = create_timestamp();
    //string timestamp = "123";

    string metaFile = directory + "\\" + timestamp  + "_meta_Data.txt";
    string dataFile = directory + "\\" + timestamp  + "_ASCII_Data.txt";
    

    ConfSingleCvibro(Nsamples, 500, trigChoice, trigEdge, 1, inputrange, trigLvl,true,true);
    int16* locData = new int16[Nsamples];

    // start measurement
    //execute_list(1);
    //execute_list_1();
    //release_wait(); cout << "wait released\n";
    //int posCountX = 0;
    //int posCountY = 0;
    //do
    //{   // Check actual position of galvo until continue
    //    get_values((ULONG_PTR)signal, (ULONG_PTR)gPosResult); // Convert Adresses to ULONG 
    //    onTargetX = (abs(gPosResult[0]) < 4);
    //    onTargetY = (abs(gPosResult[1]) < 4);
    //    //if (onTarget)  // Since scanlab measures distances in "bit-values" 600 = 1mu (for Nikon50x), 1 = 1/600 mu as tolerance for "galvo overswing"
    //    posCountX += onTargetX;
    //    posCountY += onTargetY;
    //    std::cout << "gPosResult[0] = " << gPosResult[0] << endl;
    //} while (posCountX < 4 || posCountY < 4);
    spcmADC.measureSingleRecord(0, locData, Nsamples);
    //release_wait(); cout << "wait released\n";


    //spcmADC.safeASCIItoDisk(locData);
    spcmADC.safeASCIItoDisk(metaFile, dataFile, locData);
    
    if (showPlot)
    {
        if (showSamp > Nsamples)
            showSamp = Nsamples;
        // prepare plotter
        gnuPlotter.do_setTitle("ADC vibrometry signal data");
        gnuPlotter.do_sendCMD("set xlabel 'Samples'\n");
        gnuPlotter.do_setPlotRanges(0, showSamp, -8192, 8192);
        gnuPlotter.do_sendCMD("set ylabel 'ADC Code'\n");
        gnuPlotter.do_showLegend(true);
        gnuPlotter.set_dataSize(showSamp); // only allocate memory once for internal vector of plotter
        gnuPlotter.do_sendCMD("set lt 1 lc rgb 'blue' lw 2\n");
        if (gnuPlotter.do_updateData(locData))
            std::cout << "Error loading data\n";
        if (gnuPlotter.do_showData())
            std::cout << "Error showing data\n";
    }

    // free up system memory
    delete[] locData;
    DSP.do_reset();
    spcmADC.freeUpTheBuffer();
    return 0;
}

bool Microscope::RunSingleVibroDemod(int Nsamples, char trigChoice, int trigEdge, int smooth, bool timeStamp, bool showPlot) {
    float  Fs = 500 * pow(10, 6);    //std::cout << "Fs =     " << Fs << std::endl;
    float  Fc = 80 * pow(10, 6);     //std::cout << "Fc =     " << Fc << std::endl;
    float  Ts = 1 / Fs;              //std::cout << "Ts =     " << Ts << std::endl;
    float  lambda_LAS = hwParams.s_lasWavel;    //std::cout << "lambda = " << lambda_LAS << std::endl;
    
    long inputrange = 500;
    int showSamp    = 128;
    int trigLvl     = 0;

    std::vector<float> dspOutVec;
    dspOutVec.resize(Nsamples / smooth);
    DSP.do_initDSP(Fs, Fc, lambda_LAS, Nsamples, smooth);

    if (trigChoice == 'c' || trigChoice == 'C') {
        trigLvl = 205; // i chose those values from experience.. for channel trigger, this seems to be in ADC-code (NOT mV)
    }
    else if (trigChoice == 'e' || trigChoice == 'E') {
        trigLvl = 800; // for an external trigger, the value seems to be in mV... manual p101f
    }

    string directory = "Motions\\Vibrometry_data";
    string timestamp = "";
    if (timeStamp)
        timestamp = create_timestamp();
    //string timestamp = "123";

    string metaFile = directory + "\\dataDem\\" + timestamp + "_meta_Data.txt";
    string dataFile = directory + "\\dataDem\\" + timestamp + "_ASCII_disp_Data.txt";

    ConfSingleCvibro(Nsamples, 500, trigChoice, trigEdge, 1, inputrange, trigLvl, true, true);

    Fgen.OutPutEnable(1);

    int16* locData = new int16[Nsamples];
    spcmADC.measureSingleRecord(0, locData, Nsamples);
    //spcmADC.safeASCIItoDisk(locData);

    std::cout << "Measurement complete..\n";
    Fgen.OutPutDisable(1);

    DSP.do_updateData(Nsamples, locData);
    DSP.do_demodulate(dspOutVec, 1);
    DSP.do_pinToZero(dspOutVec);

    std::cout << "Demodulation complete..\n";

    printf("Storing %6d samples\n", Nsamples/smooth);
    //system("pause");

    FILE* hFileData;
    fopen_s(&hFileData, dataFile.c_str(), "wt");
    FILE* hFileMeta;
    fopen_s(&hFileMeta, metaFile.c_str(), "wt");

    for (float x : dspOutVec) {
        fprintf_s(hFileData, "%09f\n", x);
    };

    // print metadata
    //fprintf_s(hFileMeta, "%d\n%d\n%d\n%d\n", (int64)Fs, Nsamples, smooth, Nsamples/smooth);
    fprintf_s(hFileMeta, "%d,%d,%d,%d\n", (int64)Fs, Nsamples, smooth, Nsamples/smooth);
    fprintf_s(hFileMeta, "Sample rate, total Samples, decimation, resulting samples\n");
    fprintf_s(hFileMeta, timestamp.c_str());

    fclose(hFileData); fclose(hFileMeta);

    // don't care right now..
    //if (showPlot)
    //{
    //    if (showSamp > Nsamples)
    //        showSamp = Nsamples;
    //    // prepare plotter
    //    gnuPlotter.do_setTitle("ADC vibrometry signal data");
    //    gnuPlotter.do_sendCMD("set xlabel 'Samples'\n");
    //    gnuPlotter.do_setPlotRanges(0, showSamp, -8192, 8192);
    //    gnuPlotter.do_sendCMD("set ylabel 'ADC Code'\n");
    //    gnuPlotter.do_showLegend(true);
    //    gnuPlotter.set_dataSize(showSamp); // only allocate memory once for internal vector of plotter
    //    gnuPlotter.do_sendCMD("set lt 1 lc rgb 'blue' lw 2\n");
    //    if (gnuPlotter.do_updateData(locData))
    //        std::cout << "Error loading data\n";
    //    if (gnuPlotter.do_showData())
    //        std::cout << "Error showing data\n";
    //}


    // free up system memory
    delete[] locData;
    DSP.do_reset(); 
    spcmADC.freeUpTheBuffer();
    return 0;
}


void Microscope::LiveModeImag()
{
    bool dynamicRange;
    std::cout << "Should dynamic range be adjusted? (0/1) ";
    std::cin >> dynamicRange;

    bool linewise;
    std::cout << "Scan linewise? (0/1)" << endl;
    std::cin >> linewise;

    bool crosshair;
    std::cout << "Show crosshair? (0/1) ";
    std::cin >> crosshair;

    std::cout << "Press any key to abort..." << endl;

    void (Microscope:: * ImageFunction)();
    if (linewise)
        //ImageFunction = &Microscope::TakeImageRows;
        ImageFunction = &Microscope::TakeImageRows;
    else
        ImageFunction = &Microscope::TakeImageColumns;

    const char* filename = "temp.tif";
    cv::Mat myImage = cv::Mat::zeros(pixels, pixels, CV_16UC1);
    cv::namedWindow("Window Name", cv::WINDOW_AUTOSIZE); // does nothing if window with same name already exists

    while (!_kbhit()) // means while no KeyBoardHit...
    {
        isScanning = true;
        ((*this).*ImageFunction)(); // Image is taken at this point
        //TakeImageRows();
        //TakeImageColumns();
        //for (int i = 0; i < pixels * pixels; i++)
        //{
        //    if (picture[i] >= ImagSatThresh ) {
        //        std::cout << "Pixel value exceeded set saturation threshold of " << ImagSatThresh << endl;
        //        std::cout << "Pixel value is = " << picture[i] << endl;
        //    }
        //}
        int maxInt = maxImage();
        //std::cout << "Maximum pixel value is = " << maxInt << endl;
        //std::cout << "Voltage =  " << (maxInt - 32768) * 100/ 32768 << " mV" << endl; // INPUT RANGE MUST BE ADAPTED TO ACTUAL VALUE TO GET CORRECT CONSOLE READINGS!!
        checkImageSatPxNo();
        if (dynamicRange)
        {
            CorrectBackground();
            increaseDynamicRange();
        }
        else
        {
            CorrectZeroValue(picture);
            for (int i = 0; i < pixels * pixels; i++) // double value to compensate loss of dynamic range
                picture[i] *= 2;
        }
        SaveAsTiff(picture,filename);
        ifstream input(filename);
        if (!input) 
        {
            std::cout << "Oops! file does not exist...\n";
        }
        //myImage = imread(filename, IMREAD_ANYDEPTH); // Read the file  
        myImage = cv::imread(filename, cv::IMREAD_COLOR); // Read the file
        input.close(); //close file

        if (crosshair)
        {
            BYTE* ImagePtr = (BYTE*)myImage.data; // BGR, ImagePtr[myImage.channels() * (i*pixels + j) + channel]
            int size = 10;
            for (int i = pixels/2 - size/2; i < pixels / 2 + size / 2; i++)
            {
                ImagePtr[myImage.channels() * (pixels + 1) * i + 0] = 0;
                ImagePtr[myImage.channels() * (pixels + 1) * i + 1] = 0;
                ImagePtr[myImage.channels() * (pixels + 1) * i + 2] = 255; // full red
            }
            for (int i = pixels / 2 - size / 2 + 1; i <= pixels / 2 + size / 2; i++)
            {
                ImagePtr[myImage.channels() * (pixels - 1) * i + 0] = 0;
                ImagePtr[myImage.channels() * (pixels - 1) * i + 1] = 0;
                ImagePtr[myImage.channels() * (pixels - 1) * i + 2] = 255; // full red
            }   
        }
        //moveWindow("Window Name", 1281, 0);
        imshow("Window Name", myImage);
        cv::waitKey(1); // Necessary for event handling or sth. idk ask stackoverflow
    }
    cv::destroyWindow("Window Name");
    isScanning = false;
    std::cout << endl;
}
void Microscope::LiveModeImag(const bool* cont, bool dynamicRange, bool linewise, bool crosshair)
{
    void (Microscope:: * ImageFunction)();
    if (linewise)
        //ImageFunction = &Microscope::TakeImageRows;
        ImageFunction = &Microscope::TakeImageRows;
    else
        ImageFunction = &Microscope::TakeImageColumns;

    const char* filename = "temp.tif";
    cv::Mat myImage = cv::Mat::zeros(pixels, pixels, CV_16UC1);
    cv::namedWindow("Window Name", cv::WINDOW_AUTOSIZE); // does nothing if window with same name already exists

    while (*cont) // means while cont = true
    {
        isScanning = true;
        ((*this).*ImageFunction)(); // Image is taken at this point
        //std::this_thread::sleep_for(100ms);
        if (dynamicRange)
        {
            CorrectBackground();
            increaseDynamicRange();
        }
        else
        {
            CorrectZeroValue(picture);
            for (int i = 0; i < pixels * pixels; i++) // double value to compensate loss of dynamic range
                picture[i] *= 2;
        }
        SaveAsTiff(picture, filename);
        ifstream input(filename);
        if (!input)
        {
            std::cout << "Oops! file does not exist...\n";
        }
        myImage = cv::imread(filename, cv::IMREAD_COLOR); // Read the file
        input.close(); //close file

        if (crosshair)
        {
            BYTE* ImagePtr = (BYTE*)myImage.data; // BGR, ImagePtr[myImage.channels() * (i*pixels + j) + channel]
            int size = 10;
            for (int i = pixels / 2 - size / 2; i < pixels / 2 + size / 2; i++)
            {
                ImagePtr[myImage.channels() * (pixels + 1) * i + 0] = 0;
                ImagePtr[myImage.channels() * (pixels + 1) * i + 1] = 0;
                ImagePtr[myImage.channels() * (pixels + 1) * i + 2] = 255; // full red
            }
            for (int i = pixels / 2 - size / 2 + 1; i <= pixels / 2 + size / 2; i++)
            {
                ImagePtr[myImage.channels() * (pixels - 1) * i + 0] = 0;
                ImagePtr[myImage.channels() * (pixels - 1) * i + 1] = 0;
                ImagePtr[myImage.channels() * (pixels - 1) * i + 2] = 255; // full red
            }
        }
        imshow("Window Name", myImage);
        cv::waitKey(1); // Necessary for event handling or sth. idk ask stackoverflow
    }
    isScanning = false;
    cv::destroyWindow("Window Name");
    std::cout << endl;
}

void Microscope::LiveModeVibro(bool* cont, int Nsamples, double* locDat, int lChannel, bool conv2Volt) {
    int totalSampleNumber;
    int cardErr = 0;

    while (*cont)
    {
        if (conv2Volt)
        {
            spcmADC.measureSingleRecordVolt(0, locDat, Nsamples);
            if (gnuPlotter.do_updateData(locDat))
                std::cout << "Error loading data\n";
        }
        else
        {
            spcmADC.measureSingleRecord(0, locDat, Nsamples);
            if (gnuPlotter.do_updateData(locDat))
                std::cout << "Error loading data\n";
        }
        if (gnuPlotter.do_showData())
            std::cout << "Error showing data\n";
        //this_thread::sleep_for(50ms);
    }
}

void Microscope::LiveModeVibro(bool* cont, int Nsamples, int16* locDat, int lChannel) {
    while (*cont){
        spcmADC.measureSingleRecord(0, locDat, Nsamples);
        if (gnuPlotter.do_updateData(locDat))
            std::cout << "Error loading data\n";

        if (gnuPlotter.do_showData())
            std::cout << "Error showing data\n";
        //this_thread::sleep_for(50ms);
    }
}

void Microscope::LiveModeVibroDemod(bool* cont, int Nsamples, int16* locDat,std::vector<float> dspOutVec, int lChannel) {
    while(*cont){
        spcmADC.measureSingleRecord(0, locDat, Nsamples);
        // demodulation goes here
        DSP.do_updateData(Nsamples, locDat);
        DSP.do_demodulate(dspOutVec, 1);
        DSP.do_pinToZero(dspOutVec);
        // plotting follows here
        if (gnuPlotter.do_updateData(dspOutVec))
            std::cout << "Error loading data\n";

        if (gnuPlotter.do_showData())
            std::cout << "Error showing data\n";
        //this_thread::sleep_for(50ms);
    }
}

bool Microscope::InteractiveMode()
{
    // set livemode parameters
    bool dynRng, linW, crossH;
    bool scanS          = false;
    bool vibroS         = false;
    bool lm_cont        = true;
    bool defSetup       = false;
    bool* ptr_scanS     = &scanS;    // pointers used to stop thread performing imaging/vibrometry
    bool* ptr_vibroS    = &vibroS;

    bool demodSignal        = false;
    bool validSmooth        = false;
    int vibNsampAcqu        = 0;
    int vibNsampShow        = 0;
    int vibInptRng          = 500; // mV
    int vibChanChoice       = CHANNEL0;
    int16* localData        = NULL;

    // parameters for digital signal demodulation
    float  Fs = 500 * pow(10, 6);    //std::cout << "Fs =     " << Fs << std::endl;
    float  Fc = 80 * pow(10, 6);     //std::cout << "Fc =     " << Fc << std::endl;
    float  Ts = 1 / Fs;              //std::cout << "Ts =     " << Ts << std::endl;
    int    Ns = pow(2, 19);          //std::cout << "Ns =     " << Ns << std::endl;
    float  lambda_LAS = hwParams.s_lasWavel;    //std::cout << "lambda = " << lambda_LAS << std::endl;
    size_t smooth = 64;              //std::cout << "smooth = " << smooth << std::endl;

    std::vector<float> dspOutput;

    void(Microscope:: * liveImaging)(const bool*, bool, bool, bool) 
        = &Microscope::LiveModeImag; //use pointer to resolve function overload

    void(Microscope:: * liveVibro)(bool*, int, int16*, int)         
        = &Microscope::LiveModeVibro;

    void(Microscope:: * liveVibroDem)(bool*, int, int16*, std::vector<float>, int) 
        = &Microscope::LiveModeVibroDemod;

    thread t_imag; // if no scanning was used, the thread remains default constructed and should be deleted when it goes out of scope since it is stack allocated.
    thread t_vibr;

    unsigned int choice;
    string choice_str;
    while (lm_cont)
    {
        std::cout << std::endl;
        std::cout << "Further actions:" << endl;
        std::cout << "1: Toggle scanning on/off" << endl;
        std::cout << "2: Toggle vibrometry on/off" << endl;
        std::cout << "3: Move Sample / Set Markers" << endl;
        std::cout << "4: Change APD Gain" << endl;
        std::cout << "5: Set Excitation parameters " << endl;
        std::cout << "6: Move to specific marker " << endl;
        std::cout << "7: Clear Marker Log file " << endl;
        std::cout << "8: Connect Stages" << endl;
        std::cout << "9: Disconnect Stages " << endl;
        std::cout << "99: Move all Stages to 0" << endl;
        std::cout << "0: Abort and quit" << endl;
        std::cout << "Your choice: ";


        std::cin >> choice_str;
        if (isNumeric(choice_str))
            choice = stoi(choice_str);
        else
            choice = 999999;

        switch (choice)
        {
        case 1:
        {
            std::cout << "Toggle scanning on/off..\n";
            if (scanS == true)
            {
                scanS = false;
                t_imag.join();
                std::cout << "Scanning stopped!" << endl;
            }
            else
            {
                std::cout << "Starting scanning!" << endl;
                scanS = true;
                std::cout << "Use default 011 setup? (y/n) <-> (1/0)" << endl;
                std::cin >> defSetup;
                if (defSetup == 1) {
                    dynRng = 0; linW = 1; crossH = 1;
                }
                else {
                    std::cout << "Should dynamic range be adjusted? (0/1) ";
                    std::cin >> dynRng;
                    std::cout << "Scan linewise? (0/1)" << endl;
                    std::cin >> linW;
                    std::cout << "Show crosshair? (0/1) " << endl;
                    std::cin >> crossH;
                }
                thread t_temp(liveImaging, this, ptr_scanS, dynRng, linW, crossH);
                t_imag = move(t_temp);
                defSetup = false; // reset variable here
            }
            break;
        }
        case 2:
        {
            std::cout << "Toggling vibrometry on/off..\n";
            if (vibroS == true) {
                vibroS = false;
                t_vibr.join();
                delete[] localData;
                localData = NULL;
                std::cout << "Vibrometry stopped!" << endl;
            }
            else {
                vibNsampAcqu = pow(2, 19);
                std::cout << "Starting vibrometry!" << endl;
                std::cout << "Demodulate Signal or display raw signal? (1/0)\n";
                std::cin >> demodSignal;
                vibroS = true;

                // allocate buffer here
                localData = new int16[Ns];

                // prepare common plotter specs
                gnuPlotter.do_showLegend(true);
                gnuPlotter.do_sendCMD("set lt 1 lc rgb 'blue' lw 2\n");
                gnuPlotter.do_sendCMD("set xlabel 'Samples'\n");

                if (demodSignal == true) {
                    vibNsampShow = 1024;
                    validSmooth = false;
                    do {
                        std::cout << "Set movAvg window length\n";
                        std::cout << "Chose power of 2 and integer divider of " << vibNsampAcqu << "\n";
                        std::cin >> smooth;
                        // check for valid power of two - otherwise DSP will fail
                        if (!((smooth != 0) && !((smooth & (smooth - 1)) == 0)))
                            validSmooth = true;
                        else
                            validSmooth = false;
                        // check for integer divisor of total record length - otherwise DSP will fail
                        if (vibNsampAcqu % smooth != 0)
                            validSmooth = false;
                    } while (!validSmooth);
                    ConfSingleCvibro(vibNsampAcqu, 500, 'e', SPC_TM_NEG, vibChanChoice, vibInptRng, 205, 1, 1);

                    dspOutput.resize(Ns / smooth);
                    DSP.do_initDSP(Fs, Fc, lambda_LAS, Ns, smooth);

                    gnuPlotter.do_setTitle("Displacement data");
                    gnuPlotter.do_setPlotXrange(0, vibNsampShow);
                    gnuPlotter.do_setPlotYrange(-60, 60);
                    gnuPlotter.do_showLegend(true);
                    gnuPlotter.set_dataSize(vibNsampShow); // only allocate memory once for internal vector
                    gnuPlotter.do_sendCMD("set ylabel 'Displacement in nm'\n");

                    thread t_temp2(liveVibroDem, this, ptr_vibroS, (int)Ns, localData, dspOutput, vibChanChoice - 1);
                    t_vibr = move(t_temp2);
                }
                if (demodSignal == false) {
                    vibNsampShow = 128;
                    ConfSingleCvibro(vibNsampAcqu, 500, 'c', SPC_TM_NEG, vibChanChoice, vibInptRng, 205, 1, 1);

                    gnuPlotter.do_setTitle("Raw vibrometry signal");
                    gnuPlotter.do_setPlotXrange(0, vibNsampShow);
                    gnuPlotter.do_setPlotYrange(-8192, 8192);
                    gnuPlotter.do_showLegend(true);
                    gnuPlotter.set_dataSize(vibNsampShow); // only allocate memory once for internal vector
                    gnuPlotter.do_sendCMD("set ylabel 'ADC Code'\n");

                    thread t_temp3(liveVibro, this, ptr_vibroS, vibNsampShow, localData, vibChanChoice - 1);
                    t_vibr = move(t_temp3);
                }
            }
            break;
        }
        case 3:
        {
            MoveSampleAround();
            break;
        }
        case 4:
        {
            std::cout << "Setting new APD gain selected.." << endl;
            if (APD0.SetGain(&microscopeAPDM) == 0)
                std::cout << "Setting gain failed - check COM-port vacancy" << endl;
            break;
        }
        case 5:
        {
            SetExcitParam();
            break;
        }
        case 6:
        {
            std::cout << "Moving to individual markers" << endl;
            MoveToMarker();
            break;
        }
        case 7:
        {
            bool clearFile;
            std::cout << "Do you want to clear the MarkerLog.dat file (1/0)?" << endl;
            std::cin >> clearFile;
            if (clearFile) {
                ofstream filestream;
                filestream.open("MarkerLog.dat", ofstream::out | ofstream::trunc);
                filestream.close();
                std::cout << "MarkerLog.dat was emptied." << endl;
            }
            else
                std::cout << "Aborted -> MarkerLog.dat remained unchanged." << endl;
            break;
        }
        case 8:
        {
            if (conAllStages()) {
                std::cout << "Check controller bus." << endl;
            }
            break;
        }
        case 9:
        {
            if (!disconAllStages()) {
                std::cout << "Check controller bus." << endl;
            }
            break;
        }
        case 99:
        {
            bool moveToZero = false;
            std::cout << "Move all stages to zero? (1/0)" << endl;
            std::cin >> moveToZero;
            if (moveToZero) {
                thread movX(&Microscope::SetxPosition, this, 0);
                thread movY(&Microscope::SetyPosition, this, 0);
                thread movZ(&Microscope::SetzPosition, this, 0);

                movX.join();
                movY.join();
                movZ.join();

                movX.~thread();
                movY.~thread();
                movZ.~thread();
            }
            else
                std::cout << "Aborted.." << endl;
            break;
        }
        case 0:
        {
            std::cout << "Aborting.." << endl;
            lm_cont = false;
            //scanS = false;
            //vibroS = false;


            if (scanS == true) {
                scanS = false;
                t_imag.join();
                t_imag.~thread();
                std::cout << "Scanning stopped!" << endl;
            }

            if (vibroS == true) {
                vibroS = false;
                t_vibr.join();
                t_vibr.~thread();
                if (localData != NULL)
                    delete[] localData;
                std::cout << "Vibrometry stopped!" << endl;
            }

            break;
        }
        default:
        {
            std::cout << "This was an invalid choice!\n";
            cout << "Press Enter to Continue\n";
            getch();
            break;
        };
        std::cout << endl;
        }
    }
    return 0;
}

void Microscope::CorrectBackground(){
    WORD min = 0xffff;
    bool saturated = false;
    for (int i = 0; i < pixels * pixels; i++)
    {
        if ((picture[i] >= ImagSatThresh) && (saturated == false)) {
            //std::cout << "Pixel value exceeded set saturation threshold of " << ImagSatThresh << endl;
            saturated = true;
        }
        if (picture[i] < min)
            min = picture[i];
    }
    for (int i = 0; i < pixels * pixels; i++)
        picture[i] -= min;
}
void Microscope::increaseDynamicRange()
{
    WORD max = 0;
    for (int i = 0; i < pixels * pixels; i++)
    {
        if (picture[i] > max)  // search for maximum pixel value
            max = picture[i];
    }
    WORD factor = (WORD)(0xffff / max); // since 0xffff is the maximum value for 16bit, max will be smaller than 0xfff
    for (int i = 0; i < pixels * pixels; i++)
        picture[i] *= factor; // pixel value "picture[i]" is upscaled
}
// since AlazarTec Card measures from +- a certain voltage at 16-bit,
// this function corrects the value to a solely positive measurement range
void Microscope::CorrectZeroValue(WORD * pic){
    int numpixels = pixels * pixels;
    bool saturated = false;
    for (int i = 0; i < numpixels; i++)
    {
        if ((pic[i] >= ImagSatThresh) && (saturated == false)) {
            //std::cout << "Pixel value exceeded set saturation threshold of " << ImagSatThresh << endl;
            saturated = true;
        }
        
        if (pic[i] > (32768))
            pic[i] -= (32768);
        else
            pic[i] = 0;
    }
}
void Microscope::CalibrationRoutine()
{
    string folder = "Calibration_FOV" + to_string((int) FOV) + "_pixels" + to_string(pixels);
    string filename;

    if (!CreateDirectoryA(folder.c_str(), NULL))
        std::cout << "Could not create folder" << endl;
    else
        std::cout << "Created Directory: " << folder << endl;

    int number;
    std::cout << "How many images should be taken? ";
    cin >> number;

    TakeImageSinglePixel();
    filename = ".\\" + folder + "\\" + "reference.tif";
    SaveAsTiff(picture, (filename).c_str() );

    for (int i = 0; i < number; i++)
    {
        TakeImageRows();
        filename = ".\\" + folder + "\\" + "PIC" + to_string(i+1) + ".tif";
        SaveAsTiff(picture, (filename).c_str() );
    }
}

bool Microscope::conAllStages() {
    bool allAvailable = true;
    if (allStagesConnected == false) {
        if (xStage.ConnectAndStartUpFixedAxis(hwParams.s_xStageAddr.c_str(), hwParams.s_xAxis)) {
            xPosition = xStage.currentPos;
            std::cout << "x-Stage connected" << endl;
        }
        else {
            std::cout << "x-Stage unavailable" << endl;
            allAvailable = false;
        }
        if (yStage.ConnectAndStartUpFixedAxis(hwParams.s_yStageAddr.c_str(), hwParams.s_yAxis)) {
            yPosition = yStage.currentPos;
            std::cout << "y-Stage connected" << endl;;
        }
        else {
            allAvailable = false;
            std::cout << "y-Stage unavailable" << endl;
        }
        if (zStage.ConnectAndStartUpFixedAxis(hwParams.s_zStageAddr.c_str(), hwParams.s_zAxis)) {
            zPosition = zStage.currentPos;
            std::cout << "z-Stage connected" << endl;
        }
        else {
            std::cout << "z-Stage unavailable" << endl;
            allAvailable = false;
        }
        if (allAvailable) {
            allStagesConnected = true;
            return 0;
        }
        return 1;
    }
    else if(allStagesConnected){
        std::cout << "Stages already connected" << endl;
        return 1;
    }

}
bool Microscope::disconAllStages() {
    bool allAvailable = true;
    //allStagesConnected = false;
    if (!allStagesConnected) {
        std::cout << "Stages already disconnected" << endl;
        return 1;
    }
    else if (allStagesConnected) {
        if (xStage.DisconStage())
            std::cout << "x-Stage disconnected" << endl;
        else {
            std::cout << "Coudln't disconnect x-Stage" << endl;
            allAvailable = false;
        }
        if (yStage.DisconStage())
            std::cout << "y-Stage disconnected" << endl;
        else {
            std::cout << "Coudln't disconnect y-Stage" << endl;
            allAvailable = false;
        }
        if (zStage.DisconStage())
            std::cout << "z-Stage disconnected" << endl;
        else {
            std::cout << "Coudln't disconnect z-Stage" << endl;
            allAvailable = false;
        }
        allStagesConnected = false;
        if (!allAvailable) {
            return 0;
        }
        return 1;
    }
}
void Microscope::SetzPosition(double z)
{
    zPosition = z;
    zStage.MoveStageTo(zPosition);
}
void Microscope::SetxPosition(double x)
{
    xPosition = x;
    xStage.MoveStageTo(xPosition);
}
void Microscope::SetyPosition(double y)
{
    yPosition = y;
    yStage.MoveStageTo(yPosition);
}
bool Microscope::UpdateStagePositions(double& x, double& y, double& z) {
    bool gotAllParams = true;

    if (!allStagesConnected) {

        if (xStage.ConnectAndStartUpFixedAxis(hwParams.s_xStageAddr.c_str(), hwParams.s_xAxis)) {
            x = xStage.currentPos;
            xStage.DisconStage();
            std::cout << "x-Pos readout success" << endl;
        }
        else {
            std::cout << "x-Pos readout FAILED" << endl;
            gotAllParams = false;
        }

        if (yStage.ConnectAndStartUpFixedAxis(hwParams.s_yStageAddr.c_str(), hwParams.s_yAxis)) {
            y = yStage.currentPos;
            yStage.DisconStage();
            std::cout << "y-Pos readout success" << endl;
        }
        else {
            std::cout << "y-Pos readout FAILED" << endl;
            gotAllParams = false;
        }
        if (zStage.ConnectAndStartUpFixedAxis(hwParams.s_zStageAddr.c_str(), hwParams.s_zAxis)) {
            z = zStage.currentPos;
            zStage.DisconStage();
            std::cout << "z-Pos readout success" << endl;
        }
        else {
            std::cout << "z-Pos readout FAILED" << endl;
            gotAllParams = false;
        }
    }
    else if (allStagesConnected) {
        z = zStage.currentPos;
        std::cout << "z-Pos readout success" << endl;
        x = xStage.currentPos;
        std::cout << "x-Pos readout success" << endl;
        y = yStage.currentPos;
        std::cout << "y-Pos readout success" << endl;
    }

    if (!gotAllParams)
        return 1;
    return 0;
}
void Microscope::MovezStage(double zStep)
{
    zStage.MoveStageRel(zStep);
    zPosition = zStage.currentPos;
}
void Microscope::MovexStage(double xStep)
{
    xStage.MoveStageRel(xStep);
    xPosition = xStage.currentPos;
}
void Microscope::MoveyStage(double yStep)
{
    yStage.MoveStageRel(yStep);
    yPosition = yStage.currentPos;
}
bool Microscope::MoveSampleAround() {
    // ptr to select function moving the x,y,z stage
    void(Microscope:: * selectedStgPtr)(double);

    bool keepMoving = true;
    bool markerSet  = false;
    bool fine       = false; // switch from µm to nm movement

    std::string             input_str;
    std::string             moveOrder;
    std::string             moveOrderStep;
    std::string::size_type  sz;

    int     mvOrdrLgth;
    double  moveStep;

    ofstream fileout;

    std::cout << "Step magnitude (nm/mu) (1/0)" << endl;
    std::cin >> input_str;
    if (isNumeric(input_str))
        fine = (bool)stoi(input_str);
    else {
        std::cout << "Invalid input! Exiting!\n";
        return 1;
    }

    std::cout << "Format: <xyz><+-><step> / setm to set Marker /  0 to abort " << endl << endl;
    while (keepMoving) {
       /* std::cout << "Format: <xyz><+-><step> / setm to set Marker /  0 to abort " << endl;*/
        std::cin >> moveOrder;
        mvOrdrLgth = moveOrder.size();
        if (mvOrdrLgth > 6) {
            std::cout << "Maximum step exceeded" << endl;
            keepMoving = false;
            break;
        }

        // select axis by setting pointer
        if (moveOrder == "0") {
            keepMoving = false;
            break;
        }
        else if (moveOrder == "setm") {
            markerSet = true; // required so programm doesn't try to move stage and runs into pointer trouble
            fileout.open("MarkerLog.dat", ofstream::out | ofstream::app);
            fileout << std::fixed << xPosition << " " << yPosition << " " << zPosition << std::endl;
            fileout.close();
            std::cout << "                                                     Marker set" << std::endl;
            selectedStgPtr = &Microscope::MovezStage; // pointer neets to be initialized!
            moveOrder = "z+000"; // overwrite moveOrder with dummy zero move 
            //break;
        }
        else if (moveOrder[0] == 'z') {
            selectedStgPtr = &Microscope::MovezStage;
        }
        else if (moveOrder[0] == 'x') {
            selectedStgPtr = &Microscope::MovexStage;
        }
        else if (moveOrder[0] == 'y') {
            selectedStgPtr = &Microscope::MoveyStage;
        }
        else {
            std::cout << "Invalid command.. retry" << endl;
            keepMoving = false;
            break;
        }

        // markerSet == false means this loop iteration wasn't used to store a marker
        // hence the stage will be moved
        if (markerSet == false) {
            moveOrderStep = moveOrder.substr(1);
            // convert string to double
            moveStep = std::stod(moveOrderStep, &sz);
            // scale acording to fine or coarse movement
            if (fine) {
                moveStep = moveStep / 1000 / 1000; // go from mm to nm
            }
            else {
                moveStep = moveStep / 1000; // go from mm to mu
            }

            //actually move the stage
            ((*this).*selectedStgPtr)(moveStep);
        }
        markerSet = false;

        // extract step size from string as substring
        //moveOrderStep = moveOrder.substr(1, mvOrdrLgth);

        std::cout << "Stages now at: ";
        std::cout << "x-Pos = " << xPosition << " mm" << "   ";
        std::cout << "y-Pos = " << yPosition << " mm" << "   ";
        std::cout << "z-Pos = " << zPosition << " mm." << endl << endl;

    }
    return 0;
}
void Microscope::MoveToXYZpos(double xPos, double yPos, double zPos) {
    // start threads to move sample
    thread movX(&Microscope::SetxPosition, this, xPos);
    thread movY(&Microscope::SetyPosition, this, yPos);
    thread movZ(&Microscope::SetzPosition, this, zPos);

    // wait till they finished
    movX.join();
    movY.join();
    movZ.join();

    // kill them all
    movX.~thread();
    movY.~thread();
    movZ.~thread();
}
void Microscope::CopyDatFile(std::string originPath, std::string measurementFilePath) {
    
    ifstream input;
    ofstream output;
    string line;

    input.open(originPath,std::ios::in);
    output.open(measurementFilePath, std::ios::out | std::ios::trunc);

    // count lines i.e. number of markers
    int numPositions = 0;
    while (std::getline(input, line) and input.good())
    {
        output << line << std::endl;
    }
    input.close();
    output.close();
}

bool Microscope::ReadMarkerCoords(string filename, MarkerCoordList& MarkerStruct) {
    //std::cout << "Reading marker coordinates from file" << endl;
    //std::cout << "Marker file filename is = " << filename << endl;
    string line;
    ifstream fin;     
    ofstream fout;
    size_t lineCnt = 0;

    //fin.open("MarkerLog.dat");
    fin.open(filename);
    fout.open(filename, ofstream::out | ofstream::app);
    if (!fin.is_open()) {
        std::cout << "ERROR OPENING FILE" << std::endl;
        return 1;
    }

    // count lines in file
    while (std::getline(fin, line)) {
        lineCnt++;
    }
    if (line.empty() == false) // append empty line at end of marker log file if none found
        fout << std::fixed << std::endl;

    // reset filestream after counting lines
    fin.clear();
    fin.seekg(0);

    // count non-empty lines i.e. number of markers
    size_t numPositions = 0;
    bool emptyline = true;
    //while (std::getline(fin, line) and fin.good()){
    while (std::getline(fin, line)){
        emptyline = line.empty();
        if(emptyline == false)
            ++numPositions;
    }

    std::cout << std::endl;
    std::cout << " " << numPositions << " non-empty lines found in file" << std::endl;
 
    // reset filestream after counting lines
    fin.clear();
    fin.seekg(0);

    //allocate memory according to number of markers i.e. numPositions;
    double* xPosArray = new double[numPositions];
    double* yPosArray = new double[numPositions];
    double* zPosArray = new double[numPositions];

    string xPos, yPos, zPos; // substrings
    size_t firstWhite, lastWhite;

    lineCnt = 0; // reset to zero
    while (getline(fin, line)) {
        if (line.empty() == false)
        {
            firstWhite = line.find_first_of(' ');
            lastWhite = line.find_last_of(' ');

            // cut coordinates out of current line
            xPos = line.substr(0, firstWhite);
            yPos = line.substr(firstWhite + (size_t)1, size_t(lastWhite - firstWhite));
            zPos = line.substr(lastWhite + (size_t)1);

            // cast to double and store in array..
            xPosArray[lineCnt] = std::stod(xPos);
            yPosArray[lineCnt] = std::stod(yPos);
            zPosArray[lineCnt] = std::stod(zPos);

            lineCnt++;
        }
    }
    fin.close();
    fout.close();

    // pass pointers to memory into the struct for storing the marker positions
    MarkerStruct.Nmarkers    = lineCnt;
    MarkerStruct.xPosMarkers = xPosArray;
    MarkerStruct.yPosMarkers = yPosArray;
    MarkerStruct.zPosMarkers = zPosArray;
    MarkerStruct.s_filled    = true;
    return 0;

}

void Microscope::MoveThroughMarkers() {

    // read the markers from the file
    ReadMarkerCoords("MarkerLog.dat",MarkerPosStrct);
    bool returnToOrigin = false;

    std::cout << "Return to origin position (1/0)?" << endl;
    cin >> returnToOrigin;

    int numPositions = MarkerPosStrct.Nmarkers;
    
    double xPosBuff = xPosition;
    double yPosBuff = yPosition;
    double zPosBuff = zPosition;

    for (int i = 0; i < numPositions; i++) {

        MoveToXYZpos(MarkerPosStrct.xPosMarkers[i], MarkerPosStrct.yPosMarkers[i], MarkerPosStrct.zPosMarkers[i]);

        // take image fast mode linewise
        TakeImageRows();

        // safe image
        string filePath;
        string folder = "MotionsSeriesSample";
        char imagenumber[20]; // buffer for sprintf_s to write down current image number
        string tiffName;

        sprintf_s(imagenumber, "%02d", i);
        filePath = ".\\" + folder + "\\" +"Pos_" + imagenumber + ".tif";

        SaveAsTiff(picture, filePath.c_str());

        std::cout << "Reached Marker position " << i << " at:" << endl;
        std::cout << "xPos = " << xPosition << endl;
        std::cout << "yPos = " << yPosition << endl;
        std::cout << "zPos = " << zPosition << endl;

        this_thread::sleep_for(chrono::milliseconds(200));
    }
    if (returnToOrigin == 1) {
        std::cout << "Moving back to start position" << endl;
        MoveToXYZpos(xPosBuff, yPosBuff, zPosBuff);
    }

    this_thread::sleep_for(chrono::milliseconds(500));
}

bool Microscope::MoveToMarker() {
    /*bool reloadM;
    if (MarkerPosStrct.s_filled) {
        std::cout << "Reload marker positions? (y/n)(1/0)\n";
        cin >> reloadM;
    }
    if (reloadM) {
        ReadMarkerCoords("MarkerLog.dat", MarkerPosStrct);
    }*/
    int MOI; // Marker Of Interest
    ReadMarkerCoords("MarkerLog.dat", MarkerPosStrct);

    std::cout << endl;
    std::cout << "Keep in mind, marker numbering starts at 0!!!\n";
    std::cout << "Enter marker number / line to which you want to go:\n";
    cin >> MOI;

    // if Nmarkers = 0, MOI = 0 is not valid, since MOI is the number of A marker
    // and Nmarkers is the total number OF markers!
    if (MOI > MarkerPosStrct.Nmarkers-1) {         
        std::cout << "Maximum number of markers exceeded!\n";
        return 1;
    }

    std::cout << "Moving to marker number " << MOI  << endl;
    MoveToXYZpos(
        MarkerPosStrct.xPosMarkers[MOI], 
        MarkerPosStrct.yPosMarkers[MOI], 
        MarkerPosStrct.zPosMarkers[MOI]);

    // update global class variables for position
    xPosition = MarkerPosStrct.xPosMarkers[MOI];
    yPosition = MarkerPosStrct.yPosMarkers[MOI];
    zPosition = MarkerPosStrct.zPosMarkers[MOI];

    std::cout << "Stages now at: ";
    std::cout << "x-Pos = " << xPosition << " mm" << "   ";
    std::cout << "y-Pos = " << yPosition << " mm" << "   ";
    std::cout << "z-Pos = " << zPosition << " mm." << endl << endl;

    return 0;
}

bool Microscope::GeneratePiezoDriftCalib(bool validate, int delay, bool linewise) {
    bool  manual, paramsOK = false;
    float time, Vpp, startFreq, freqStep, NfreqSteps, endFreq;
    float actualFreq = 0;

    double xStart, yStart, zStart; // initial stages-position buffers
    double xNext, yNext, zNext; // initial stages-position buffers

    string  prefix;
    string  folder = "PiezoDriftCalib";

    if (validate) {
        std::cout << "Validating piezo drift compensation!" << endl;
        prefix = "val_";
        // load piezo calibration file
        if (ReadMarkerCoords("PiezoDriftCalib\\driftCalib.dat", PiezoDriftCorr))
            return 1; // exit if file couldn't be read
        UpdateStagePositions(xStart, yStart, zStart); // reads out and stores stage positions from PI-drivers
    }
    else {
        std::cout << "Generating piezo drift calibration!" << endl;
        prefix = "cal_";
    }

    if (!CreateDirectoryA(folder.c_str(), NULL)) {
        std::cout << "Could not create folder" << endl;
        std::cout << "Foulder may already exist." << endl;
    }
    else
        std::cout << "Created Directory: " << folder << endl;

    char    frequencyChars[20];
    string  filepath;

    std::cout << "Manual frequency range selection? (1/0) (y/n)" << endl;

    cin >> manual;

    if (manual == true) {
        cout << "Enter Vpp in V" << endl;
        cin >> Vpp;
        cout << "Enter start frequency in Hz" << endl;
        cin >> startFreq;
        cout << "Enter frequency step size in Hz" << endl;
        cin >> freqStep;
        cout << "Enter Number of frequency steps" << endl;
        cin >> NfreqSteps;
    }
    else {
        while(paramsOK == false){
            readMeasurementConfigFile(measurementConfFileName, dynMeasParams);
            std::cout << dynMeasParams;
            std::cout << "Parameters ok? (y/n)(1/0)\n";
            cin >> paramsOK;
        }
        Vpp         = dynMeasParams.s_Vpp;
        startFreq   = dynMeasParams.s_startFreq;
        freqStep    = dynMeasParams.s_freqStep;
        NfreqSteps  = dynMeasParams.s_NfreqSteps;
        endFreq     = startFreq + freqStep * NfreqSteps;
    }

    // Before starting the measurement, check at least if number of drift compensation shifts
    // is equal to number of measured frequencies
    if (validate and NfreqSteps+1 != PiezoDriftCorr.Nmarkers) {
        std::cout << "Number of frequencies does not equal number of frequency shifts! \n";
        std::cout << "Check if piezo drift compensation data is up to date!\n";
        std::cout << "Aborting procedure!\n";
        return 1;
    }

    // configure Fgen
    for (size_t i = 0; i <= NfreqSteps; i++) {

        if (validate) { // move to new position
            xNext = xStart + PiezoDriftCorr.xPosMarkers[i];
            yNext = yStart + PiezoDriftCorr.yPosMarkers[i];
            zNext = zStart + PiezoDriftCorr.zPosMarkers[i];
            MoveToXYZpos(xNext, yNext, zNext);
        }

        actualFreq = startFreq + i * freqStep;
        std::cout << "Current frequency is " << actualFreq << "Hz\n";
        Fgen.OutPutEnable(1);
        Fgen.ConfigureSine(Vpp, 0, actualFreq, 0);
        this_thread::sleep_for(chrono::seconds(delay)); // allow piezo to heat/settle
        if (linewise)
            TakeImageSinglePixel();
        else if (!linewise)
            TakeImageSinglePixelCol();
        sprintf_s(frequencyChars, "%06d", (int)actualFreq);
        filepath = folder + "\\" + prefix + frequencyChars + "Hz.tif";
        cout << "filepath = " << filepath << endl;
        SaveAsTiff(picture, filepath.c_str());
        Fgen.OutPutDisable(1);
    }    

    // return to start position
    if(validate)
        MoveToXYZpos(xStart, yStart, zStart);

    return 0;
}

//bool Microscope::GeneratePiezoDriftCalib(bool validate, int delay) {
//    return GeneratePiezoDriftCalib(validate, delay, true);
//}

void Microscope::TakeStack(double stepsize, int n, bool fastMode)
{
    //void (Microscope::*ImageFunction)();
    //if (fastMode)
    //    ImageFunction = &Microscope::TakeImageRows;
    //else
    //    ImageFunction = &Microscope::TakeImageSinglePixel;

    string folder = "Stack";
    string filename;

    if (!CreateDirectoryA(folder.c_str(), NULL))
        std::cout << "Could not create folder" << endl;
    else
        std::cout << "Created Directory: " << folder << endl;

    std::cout << "Connecting z-stage.." << endl;
    zPosition = zStage.currentPos;
    double startZ = zStage.currentPos;
        char imagenumber[50]; // buffer for sprintf_s to write down current image number
    for (int i = 0; i < n; i++)
    {
       SetzPosition(startZ + i * stepsize / 1000); // Move Piezo to new position (note i starts from 0!)

        //( (*this).*ImageFunction )(); // Image is taken at this point
        if (fastMode)
            TakeImageRows();
        else
            TakeImageSinglePixel();


        sprintf_s(imagenumber, "%06d", (int)(((n - 1) * stepsize - stepsize * i) * 1000));
        //filename = ".\\" + folder + "\\" + to_string((int)(((n-1)*stepsize - stepsize * i)* 1000)) + "nm.tif";
        filename = ".\\" + folder + "\\" + imagenumber + "nm.tif";
        //filename = ".\\" + folder + "\\" + to_string((int)(stepsize*i*1000)) + "nm.tif";
        CorrectZeroValue(picture); // correct Zero value before safing the image
        for (int i = 0; i < pixels * pixels; i++) // double value to compensate loss of dynamic range
            picture[i] *= 2;
        SaveAsTiff(picture,filename.c_str());
        std::cout <<  "\r" << "Image " << i+1 << " / " << n << endl;
    }
    SetzPosition(startZ);// reset piezo to z position of the beginning
    //zStage.DisconStage();
}

void Microscope::TakeStackGainAdj(double stepsize, int n, bool fastMode)
{

    //void (Microscope:: * ImageFunction)();
    //if (fastMode)
    //    ImageFunction = &Microscope::TakeImageRows;
    //else
    //    ImageFunction = &Microscope::TakeImageSinglePixel;

    string folder = "Stack";
    string filename;
    char imagenumber[50]; // buffer for sprintf_s to write down current image number

    if (!CreateDirectoryA(folder.c_str(), NULL))
        std::cout << "Could not create folder" << endl;
    else
        std::cout << "Created Directory: " << folder << endl;

    int maxCount = 20; // integer that aborts gain adjustment to avoid endless loop
    float satPixPerc = 0.4; // Percentage (so 0.2 = 0.2%) of tolerated pixels for which saturation can occur befor gain is adjusted.

    std::cout << "Connecting z-stage.." << endl;
    zStage.ConnectAndStartUpFixedAxis(hwParams.s_zStageAddr.c_str(), hwParams.s_zAxis,true);
    zPosition = zStage.currentPos;
    double startZ = zStage.currentPos;
    bool abort = false;
    std::cout << "starting stack loop" << endl;

    int i = 0;
    while (i < n and !abort) // means while no KeyBoardHit...)
    {

        std::cout << "Slice number " << i+1 << " of " << n << endl;
        SetzPosition(startZ + i * stepsize / 1000); // Move Piezo to new position (note i starts from 0!) // + Plus for out of the sample

        //((*this).*ImageFunction)(); // Image is taken at this point
        if (fastMode)
            TakeImageRows();
        else
            TakeImageSinglePixel();

        SaveAndShowTempImage("temp.tif");

        if (checkImageSat(satPixPerc)) {  // check if image exceedes image saturation threshold set by satPixPerc
            //SeekGainSimple(&maxCount, &satPixPerc, ImageFunction);
            //std::cout << "Using divide and conquer" << endl;   
            SeekGainDivideAndConquer(&maxCount, &satPixPerc, fastMode);
        }

        sprintf_s(imagenumber, "%06d", (int)(((n - 1) * stepsize - stepsize * i) * 1000));
        //filename = ".\\" + folder + "\\" + to_string((int)(((n-1)*stepsize - stepsize * i)* 1000)) + "nm.tif";
        filename = ".\\" + folder + "\\" + imagenumber + "nm.tif";
        
        CorrectZeroValue(picture); // correct Zero value before safing the image
        for (int i = 0; i < pixels * pixels; i++) // double value to compensate loss of dynamic range
            picture[i] *= 2;
        SaveAsTiff(picture,filename.c_str());

        if (_kbhit()) {// means while no KeyBoardHit...)
            std::cout << "Abort stack? (1/0)" << endl;
            cin >> abort;
        }
        i++;
    }
    cv::destroyWindow("Window Name");
    SetzPosition(startZ);
    std::cout << "Moving Stage back." << endl;
    zStage.DisconStage();
}


void Microscope::TakeAndSaveMotionImages_V2(size_t NpointPerPx)
{
    std::cout << endl << "TakeAndSaveMotionImages_V2" << endl;

    int samplerate = 0; // S/s  // copy from microscope attributes

    std::cout << "Choose DIC/imaging sample rate in kSps" << endl;
    cin >> samplerate;

    samplerate = samplerate * 1000; // convert from kSps to Sps

    // (Re)configer board channels
    Board.ConfigureChannel(CHANNEL_A, DC_COUPLING, InputRange, IMPEDANCE_1M_OHM);
    Board.ConfigureChannel(CHANNEL_B, DC_COUPLING, INPUT_RANGE_PM_2_V, IMPEDANCE_1M_OHM);
    // Configuring Board (external trigger)

    //Board.SetSampleRate(SAMPLE_RATE_10MSPS);
    Board.SetSampleRateInt(samplerate);
    Board.ConfigureTriggerEngine(TRIG_ENGINE_J, TRIG_EXTERNAL, (U32)200, TRIGGER_SLOPE_NEGATIVE); // Trigger for synchronisation

    int waitCounter = 0;
    int releaseCounter = 0;

    size_t samples = NpointPerPx; // 
    float timePx = 1 / float(samplerate) * samples; // time in seconds
    int timePx_ms = (int)(1000 * timePx + 0.5); // time converted and rounded to milliseconds
    int NwaitCommands = int(timePx / 20e-6 + 1);
    bool longwait = false;

    std::cout << endl;
    std::cout << "Number of samples         per pixel = " << samples        << endl;
    std::cout << "Number of wait commands   per pixel = " << NwaitCommands  << endl;

    if (NwaitCommands * pixels * pixels > 4194304) {
        std::cout << "WARNING, GALVO LIST MEMORY DEPTH EXCEEDED!" << std::endl;
        std::cout << "MAXIMUM COMMAND NUMBER IS 4194304!" << std::endl << std::endl;

        std::cout << "DEMANDED NUMBER IS " << NwaitCommands * pixels * pixels << std::endl;
        std::cout << "Longwait mode using CPU-sleep enabled!" << std::endl << std::endl;
        std::cout << "Desired dwell time per pixel is " << timePx_ms << "ms" << std::endl;

        longwait = true;
    }

    // Create List in Galvo Scanner
    //set_start_list(1);
    set_start_list(1);
    waitCounter++; set_wait(waitCounter);
    //int waitnumber = 0; // to enlarge gate in galvo-toggle on EXTENSION 2 Stiftleiste
    for (int i = 0; i < pixels; i++)
    {
        for (int j = 0; j < pixels; j++)
        {
            jump_abs(gCoords[j], gCoords[i]);
            waitCounter++; set_wait(waitCounter);

            write_8bit_port_list(0xff);  // writing to RTC6 card "EXTENSION 2 Stiftleiste" => Opens the gate for data aqcuisition
            waitCounter++; set_wait(waitCounter);
            //waitCounter++; set_wait(waitCounter);
            write_8bit_port_list(0);
            //set_wait((i * pixels + j) * 30 + 5);
            if (longwait == false) {
                for (int k = 0; k < NwaitCommands; k++) //increase pixel dwell time depending on samples per pixel
                {
                    waitCounter++; set_wait(waitCounter);
                }
            }
            else if(longwait == true) {
                waitCounter++; set_wait(waitCounter);
                // set a single wait. this wait will be paired with a "this_thread::sleep_for(XXXms)" for XXX milliseconds down below
            }
        }
    }

    jump_abs(0, 0); // Jump back to the centre when finished
    set_end_of_list();
    execute_list(1);

    // Allocate Memory for data
    WORD* PixelValues = new WORD[size_t(pixels) * pixels * samples];
    if (PixelValues == NULL)
    {
        std::cout << "ERROR: Out of Memory. Try to take less samples." << endl;
        return;
    }

    //Fgen.OutPutEnable(1);

    // Activate AutoDMA measurement
    Board.StartNPTAutoDMAAcquisition(samples, pixels, pixels, CHANNEL_A);

    // For readout of xy position
  // 05: SetMode (readout); 01: Is position
    control_command(1, 1, 0x0501); // x position
    control_command(1, 2, 0x0501); // y position

    int signal[4] = { 1, 2, 0, 0 }; // Tells which variables should be read out (here x, y, 0, 0)
    //int* result = new int[4];       // Pointer for the results from readout
    bool onTargetX = false;  // bool used to calculate if galvo is on designated postition from jump_abs(gCoords[k], gCoords[i]);
    bool onTargetY = false;

    bool success = true;
    bool busy;
    UINT Status; UINT Pos;

    size_t NumPixels = size_t(pixels) * pixels;
    //cout << "NumPixels = NumThreads = " << NumPixels << endl;

    // Timing variables
    double lineDuration, toggleTime, moveGalvoTime, pixelduration, time_scanning;

    auto start_scanning = chrono::steady_clock::now();

    for (int line = 0; line < pixels; line++)
    {
        auto startLine = chrono::steady_clock::now();
        std::cout << "\r" << "Scanning... " << int(double(line) / pixels * 100 + 0.5) << "%";
        //printf("starting line %u \r", i + 1);
        for (int col = 0; col < pixels; col++)
        {
            auto start = chrono::steady_clock::now();
            release_wait(); releaseCounter++; // make galvo jump to next position
            int posCountX = 0; int posCountY = 0;
            do
            {   // Check actual position until continue
                get_values((ULONG_PTR)signal, (ULONG_PTR)gPosResult); // Convert Adresses to ULONG 
                onTargetX = (abs(gPosResult[0] - gCoords[col])  < 4); // standard is 4
                onTargetY = (abs(gPosResult[1] - gCoords[line]) < 4);// standard is 4
                //if (onTarget)  // Since scanlab measures distances in "bit-values" 600 = 1mu (for Nikon50x), 1 = 1/600 mu as tolerance for "galvo overswing"
                //    posCount++;

                posCountX += onTargetX;
                posCountY += onTargetY;
            } while (posCountX < 4 || posCountY < 4); // Mirror is at the beginning of a line again. The "threshold" value for posCount is defined rather arbitrarily to give good images. 
            // it is chosen > 1 to account for multiple overswings..
            //posCount = 0; //Reset

            auto startToggle = chrono::steady_clock::now();
            release_wait(); releaseCounter++;// Make Galvo toggle  // open and close gate for triggering here
            //release_wait(); releaseCounter++;// Make Galvo toggle
            //////release_wait(); // Make Galvo toggle

            // extra waits increase the duration of the time where the "gate" for the triggers is open. may be important for low frequencies < 50kHz!!
            // additional waits - DELETE LATER

            auto endToggle = chrono::steady_clock::now();
            //printf("Toggle for pixel %u completed\r", k + 1);
            //this_thread::sleep_for(chrono::milliseconds(1));  // since rtc6 card internal clock works in 10micros intervals, 100microseconds already seems long..
            //release_wait();
            if (longwait == false) {
                for (int k = 0; k < NwaitCommands; k++) { //increase pixel dwell time depending on samples per pixel
                    release_wait(); releaseCounter++;
                }
            }
            else if (longwait == true) {
                release_wait(); releaseCounter++;
                this_thread::sleep_for(std::chrono::milliseconds(timePx_ms));
            }
            //this_thread::sleep_for(100ms);
            auto end = chrono::steady_clock::now();
            pixelduration   = chrono::duration_cast<chrono::microseconds>(end - start).count();
            moveGalvoTime   = chrono::duration_cast<chrono::microseconds>(startToggle - start).count();
            toggleTime      = chrono::duration_cast<chrono::microseconds>(endToggle - startToggle).count();
            //std::cout << "timeduration = " << timeduration << " micros" << endl;
            //std::cout << "toggleTime = " << toggleTime << " micros" << endl;
        }
        success = Board.ReadDataNPTAutoDmALine(PixelValues, 5000, line); // timeout in ms, add some buffer (of 20ms) to avoid errors
        /*this_thread::sleep_for(500ms);*/
                                                                  // Display progress
        


        auto endLine = chrono::steady_clock::now();
        lineDuration = chrono::duration_cast<chrono::microseconds>(endLine - startLine).count();
        if (!success) // break up
        {
            std::cout << endl << "An Error occured - stopping measurement";
            set_start_list(1);
            jump_abs(0, 0); // move galvo back to the centre
            set_end_of_list();
            execute_list(1);
            break;
        }
    }
    release_wait(); releaseCounter++;

    auto end_scanning = chrono::steady_clock::now();
    time_scanning = chrono::duration_cast<chrono::milliseconds>(end_scanning - start_scanning).count();

    std::cout << endl;
    //cout << "lineDuration = " << lineDuration << "microseconds" << endl;
    //cout << "toggleTime = " << toggleTime << "microseconds" << endl;
    //cout << "moveGalvoTime = " << moveGalvoTime << "microseconds" << endl;
    std::cout << "time_scanning = " << time_scanning << "milliseconds" << endl;
    std::cout << endl;

    Board.AbortNPTAutoDMA(); // end Auto DMA measurement (important!!)

    // Save Data as tif images
    string folder = "Motions";

    if (!CreateDirectoryA(folder.c_str(), NULL))
        std::cout << "Could not create folder" << endl;
    else
        std::cout << "Created Directory: " << folder << endl;

    auto start_saving = chrono::steady_clock::now();

    // Multi threaded image formation
    //-----------------------------------------------
    if (success)
    {
        int px = pixels;
        const int threads = 12;
        size_t perThread = samples / threads; // number of images saved by each thread
        thread t[threads];
        int fov = FOV;
        double exFreq = excitFreq;
        int gain = APD0.QuerryCurrentGain();
        float IR = Board.translateInputRange(InputRange);
        for (int i = 0; i < threads; i++)
        {
            if (i < threads - 1)
            {
                thread t_temp([i, perThread, PixelValues, samples, NumPixels, folder, px, fov, gain, IR, exFreq] // create thread that overwrites a part of the buffer
                    {
                        WORD* image = new WORD[NumPixels];
                        CTiffGrey T(px, px); // Tiff object could not be used in lambda function, so create a new one
                        T.setAttribute(ID_FieldOfView, "Field of View [µm]", fov);
                        T.setAttribute(ID_PixelSize, "Pixel size [µm]", double(fov) / px);
                        T.setAttribute(ID_APDGain, "APD Gain M", gain);
                        T.setAttribute(ID_InputRange, "Input range [mV]", IR);
                        T.setAttribute(ID_ExcitationFreq, "Excitation freq [Hz]", exFreq);
                        char imagenumber[50]; // buffer for sprintf_s to write down current image number
                        string filename;
                        for (size_t j = i * perThread; j < (i + 1) * perThread; j++)
                        {
                            for (size_t k = 0; k < NumPixels; k++)
                            {
                                image[k] = PixelValues[k * samples + j];
                            }
                            sprintf_s(imagenumber, "%05d", j);
                            filename = ".\\" + folder + "\\PIC" + imagenumber + ".tif";
                            T.saveData(image, filename.c_str());
                        }
                        delete[] image;
                    });
                t[i] = move(t_temp); // move task into array of threads
            }
            else
            {
                thread t_temp([i, perThread, PixelValues, samples, NumPixels, folder, px, fov, gain, IR, exFreq] // last thread maybe has to work more if samples and number of threads are relative primes
                    {
                        WORD* image = new WORD[NumPixels];
                        CTiffGrey T(px, px); // Tiff object could not be used in lambda function, so create a new one
                        T.setAttribute(ID_FieldOfView, "Field of View [µm]", fov);
                        T.setAttribute(ID_PixelSize, "Pixel size [µm]", double(fov) / px);
                        T.setAttribute(ID_APDGain, "APD Gain M", gain);
                        T.setAttribute(ID_InputRange, "Input range [mV]", IR);
                        T.setAttribute(ID_ExcitationFreq, "Excitation freq [Hz]", exFreq);
                        char imagenumber[50]; // buffer for sprintf_s to write down current image number
                        string filename;
                        for (size_t j = i * perThread; j < samples; j++)
                        {
                            for (size_t k = 0; k < NumPixels; k++)
                            {
                                image[k] = PixelValues[k * samples + j];
                            }
                            sprintf_s(imagenumber, "%05d", j);
                            filename = ".\\" + folder + "\\PIC" + imagenumber + ".tif";
                            T.saveData(image, filename.c_str());
                        }
                        delete[] image;
                    });
                t[i] = move(t_temp);
            }
        }

        for (int i = 0; i < threads; i++) // wait until all threads are finished
            t[i].join();
        //-----------------------------------------------

        if (!success)
        {
            std::cout << "!success <-> Error!" << std::endl;
            for (int i = 0; i < pixels * pixels; i++)
            {
                this_thread::sleep_for(chrono::microseconds(100));
                release_wait(); releaseCounter++;
                this_thread::sleep_for(chrono::microseconds(100));
                release_wait(); releaseCounter++;
            }
        }

        auto end_saving = chrono::steady_clock::now();
        double time_saving = chrono::duration_cast<chrono::milliseconds>(end_saving - start_saving).count();

        //std::cout << "Time for taking pictures " << time_scanning << "ms" << endl;
        //std::cout << "Time for saving pictures " << time_saving << "ms" << endl;
    }

    //Board.ConfigureAuxIO(AUX_IN_AUXILIARY, TRIGGER_SLOPE_POSITIVE); // Resetting AUX IO
    delete[] PixelValues;

    std::cout << "       Wait commands issued: " << waitCounter     << "\n";
    std::cout << "    Release commands issued: " << releaseCounter  << "\n";


    //Fgen.OutPutDisable(1);
}


void Microscope::TakeAndSaveMotionImagesColumn_V2(size_t NpointPerPx)
{

    std::cout << endl << "TakeAndSaveMotionImagesColumn_V2" << std::endl;

    int samplerate = 0; // S/s  // copy from microscope attributes
    std::cout << "Choose DIC/imaging sample rate in kSps" << endl;
    cin >> samplerate;

    samplerate = samplerate * 1000; // convert from kSps to Sps


    // (Re)configer board channels
    Board.ConfigureChannel(CHANNEL_A, DC_COUPLING, InputRange, IMPEDANCE_1M_OHM);
    Board.ConfigureChannel(CHANNEL_B, DC_COUPLING, INPUT_RANGE_PM_2_V, IMPEDANCE_1M_OHM);
    // Configuring Board (external trigger)

    Board.SetSampleRateInt(samplerate);
    Board.ConfigureTriggerEngine(TRIG_ENGINE_J, TRIG_EXTERNAL, (U32)200, TRIGGER_SLOPE_NEGATIVE); // Trigger for synchronisation
    //Board.ConfigureAuxIO(AUX_IN_TRIGGER_ENABLE, TRIGGER_SLOPE_POSITIVE); // Put Toggle signal on AuxIO port to enable trigger when galvo is ready

    int waitCounter = 0;
    int releaseCounter = 0;

    // calculate number of samples per pixel to reach given time
    size_t  samples = NpointPerPx; // 
    float   timePx = 1 / float(samplerate) * samples; // time in seconds
    int     timePx_ms = (int)(1000 * timePx + 0.5); // time converted and rounded to milliseconds
    int     NwaitCommands = int(timePx / 20e-6 + 1);
    bool    longwait = false;

    std::cout << endl;
    std::cout << "Number of samples         per pixel = " << samples << endl;
    std::cout << "Number of wait commands   per pixel = " << NwaitCommands << endl;

    if (NwaitCommands * pixels * pixels > 4194304) {
        std::cout << "WARNING, GALVO LIST MEMORY DEPTH EXCEEDED!" << std::endl;
        std::cout << "MAXIMUM COMMAND NUMBER IS 4194304!" << std::endl << std::endl;

        std::cout << "DEMANDED NUMBER IS " << NwaitCommands * pixels * pixels << std::endl;
        std::cout << "Longwait mode using CPU-sleep enabled!" << std::endl << std::endl;
        std::cout << "Desired dwell time per pixel is " << timePx_ms << "ms" << std::endl;

        longwait = true;
    }

    // Create List in Galvo Scanner
    set_start_list(1);
    waitCounter++; set_wait(waitCounter);

    for (int i = 0; i < pixels; i++)
    {
        for (int j = 0; j < pixels; j++)
        {
            jump_abs(gCoords[i], gCoords[j]);
            waitCounter++; set_wait(waitCounter);
            //periodic_toggle_list(2, // 8-Bit digital Output
            //    0xff,   // toggle all bits
            //    10,     // Stay on for 100 us
            //    1,      // Stay off for 10 us
            //    1,      // Toggle only one time
            //    0);     // Start immediatly
            write_8bit_port_list(0xff);
            waitCounter++; set_wait(waitCounter);
            //waitCounter++; set_wait(waitCounter);
            write_8bit_port_list(0);
            if (longwait == false) {
                for (int k = 0; k < NwaitCommands; k++) //increase pixel dwell time depending on samples per pixel
                {
                    waitCounter++; set_wait(waitCounter);
                }
            }
            else if (longwait == true) {
                waitCounter++; set_wait(waitCounter);
                // set a single wait. this wait will be paired with a "this_thread::sleep_for(XXXms)" for XXX milliseconds down below
            }
        }
    }

    jump_abs(0, 0); // Jump back to the centre when finished
    set_end_of_list();
    execute_list(1);

    // Allocate Memory for data
    WORD* PixelValues = new WORD[size_t(pixels) * pixels * NpointPerPx];
    if (PixelValues == NULL)
    {
        std::cout << "ERROR: Out of Memory. Try to take less samples." << endl;
        return;
    }

    //Fgen.OutPutEnable(1);

    // Activate AutoDMA measurement
    Board.StartNPTAutoDMAAcquisition(samples, pixels, pixels, CHANNEL_A);

    // For readout of xy position
  // 05: SetMode (readout); 01: Is position
    control_command(1, 1, 0x0501); // x position
    control_command(1, 2, 0x0501); // y position

    int signal[4] = { 1, 2, 0, 0 }; // Tells which variables should be read out (here x, y, 0, 0)
    //int* result = new int[4];       // Pointer for the results from readout
    bool onTargetX = false;  // bool used to calculate if galvo is on designated postition from jump_abs(gCoords[k], gCoords[i]);
    bool onTargetY = false;

    bool success = true;
    bool busy;
    UINT Status;
    UINT Pos;

    size_t NumPixels = size_t(pixels) * pixels;

    //std::cout << "Scanning..." << endl;
    auto start_scanning = chrono::steady_clock::now();
    for (int i = 0; i < pixels; i++)
    {
        std::cout << "\r" << "Scanning... " << int(double(i) / pixels * 100 + 0.5) << "%";
        for (int k = 0; k < pixels; k++)
        {
            auto start = chrono::steady_clock::now();
            release_wait(); releaseCounter++; // make galvo jump to next position

            int posCountX = 0;
            int posCountY = 0;
            do
            {   // Check actual position until continue
                get_values((ULONG_PTR)signal, (ULONG_PTR)gPosResult); // Convert Adresses to ULONG 
                onTargetX = (abs(gPosResult[0] - gCoords[i]) < 4);
                onTargetY = (abs(gPosResult[1] - gCoords[k]) < 4);
                //if (onTarget)  // Since scanlab measures distances in "bit-values" 600 = 1mu (for Nikon50x), 1 = 1/600 mu as tolerance for "galvo overswing"
                //    posCount++;
                posCountX += onTargetX;
                posCountY += onTargetY;
            } while (posCountX < 4 || posCountY < 4); // Mirror is at the beginning of a line again. The "threshold" value for posCaount is defined rather arbitrarily to give good images. 
            // it is chosen > 1 to account for multiple overswings..
            //posCount = 0; //Reset


            release_wait(); releaseCounter++; // Make Galvo toggle
            //release_wait(); releaseCounter++;
            //this_thread::sleep_for(chrono::microseconds(100));
            if (longwait == false) {
                for (int k = 0; k < NwaitCommands; k++) //increase pixel dwell time depending on samples per pixel
                {
                    release_wait(); releaseCounter++;
                }
            }
            else if (longwait == true) {
                release_wait(); releaseCounter++;
                //this_thread::sleep_for(100ms);
                this_thread::sleep_for(std::chrono::milliseconds(timePx_ms));
            }

            // read out one buffer which should be measured now
            //success = Board.ReadDataNPTAutoDmA(data, int(time) + 20); // timeout in ms, add some buffer (of 20ms) to avoid errors
            //for (int j = 0; j < NpointPerPx; j++)
                //PixelValues[(i * pixels + k) * NpointPerPx + j] = data[j];

            if (!success)
                break;

            auto end = chrono::steady_clock::now();
            double timeduration = chrono::duration_cast<chrono::microseconds>(end - start).count();
            //std::cout << "Time = " << time << endl;
        }
        success = Board.ReadDataNPTAutoDmALine(PixelValues, 5000, i); // timeout in ms, add some buffer (of 20ms) to avoid errors

        if (!success) // break up
        {
            std::cout << endl << "An Error occured - stopping measurement";
            set_start_list(1);
            jump_abs(0, 0); // move galvo back to the centre
            set_end_of_list();
            execute_list(1);
            break;
        }

    }
    release_wait(); releaseCounter++;

    auto end_scanning = chrono::steady_clock::now();
    double time_scanning = chrono::duration_cast<chrono::milliseconds>(end_scanning - start_scanning).count();

    std::cout << endl;
    Board.AbortNPTAutoDMA(); // end Auto DMA measurement (important!!)

    // Save Data as tif images
    string folder = "Motions";

    if (!CreateDirectoryA(folder.c_str(), NULL))
        std::cout << "Could not create folder" << endl;
    else
        std::cout << "Created Directory: " << folder << endl;

    auto start_saving = chrono::steady_clock::now();

    // Multi threaded
    //-----------------------------------------------
    if (success)
    {
        int px = pixels;
        const int threads = 12;
        size_t perThread = NpointPerPx / threads; // number of images saved by each thread 
        thread t[threads];
        int fov = FOV;
        double exFreq = excitFreq;
        int gain = APD0.QuerryCurrentGain();
        float IR = Board.translateInputRange(InputRange);
        for (int i = 0; i < threads; i++)
        {

            if (i < threads - 1)
            {
                thread t_temp([i, perThread, PixelValues, NpointPerPx, NumPixels, folder, px, fov, gain, IR, exFreq] // create thread that overwrites a part of the buffer
                    {
                        WORD* image = new WORD[NumPixels];
                        //WORD* imageROT = new WORD[NumPixels];
                        CTiffGrey T(px, px); // Tiff object could not be used in lambda function, so create a new one
                        T.setAttribute(ID_FieldOfView, "Field of View [µm]", fov);
                        T.setAttribute(ID_PixelSize, "Pixel size [µm]", double(fov) / px);
                        T.setAttribute(ID_APDGain, "APD Gain M", gain);
                        T.setAttribute(ID_InputRange, "Input range [mV]", IR);
                        T.setAttribute(ID_ExcitationFreq, "Excitation freq [Hz]", exFreq);
                        char imagenumber[50]; // buffer for sprintf_s to write down current image number
                        string filename;
                        for (size_t j = i * perThread; j < (i + 1) * perThread; j++) // i * perThread gives the image/slice at which the i-th thread start it's work
                        {
                            for (size_t k = 0; k < (size_t)px; k++)
                            {
                                for (size_t l = 0; l < (size_t)px; l++) {
                                    image[l * px + k] = PixelValues[(l + k * px) * size_t(NpointPerPx) + j];
                                } //image[k] = PixelValues[k * samples + j];
                            }
                            //for (size_t k = 0; k < NumPixels; k++)
                            //{
                            //    image[k] = PixelValues[k * samples + j]; //k * samples iterates through the k-th pixel within the j-th slice
                            //}
                            //for (size_t a = 0; a < (size_t)px; a++) {
                            //    for (size_t b = 0; b < (size_t)px; b++) {
                            //        imageROT[a + b * px] = image[a * px + b];
                            //    }
                            //}
                            sprintf_s(imagenumber, "%05d", j);
                            filename = ".\\" + folder + "\\PIC" + imagenumber + ".tif";
                            T.saveData(image, filename.c_str());
                            //T.saveData(imageROT, filename.c_str());
                        }
                        delete[] image;
                        //delete[] imageROT;
                    });
                t[i] = move(t_temp); // move task into array of threads
            }
            else
            {
                thread t_temp([i, perThread, PixelValues, NpointPerPx, NumPixels, folder, px, fov, gain, IR, exFreq] // last thread maybe has to work more if samples and number of threads are relative primes
                    {
                        WORD* image = new WORD[NumPixels];
                        CTiffGrey T(px, px); // Tiff object could not be used in lambda function, so create a new one
                        T.setAttribute(ID_FieldOfView, "Field of View [µm]", fov);
                        T.setAttribute(ID_PixelSize, "Pixel size [µm]", double(fov) / px);
                        T.setAttribute(ID_APDGain, "APD Gain M", gain);
                        T.setAttribute(ID_InputRange, "Input range [mV]", IR);
                        T.setAttribute(ID_ExcitationFreq, "Excitation freq [Hz]", exFreq);
                        char imagenumber[50]; // buffer for sprintf_s to write down current image number
                        string filename;
                        for (size_t j = i * perThread; j < NpointPerPx; j++)
                        {
                            for (size_t k = 0; k < (size_t)px; k++)
                            {
                                for (size_t l = 0; l < (size_t)px; l++) {
                                    image[l * px + k] = PixelValues[(l + k * px) * size_t(NpointPerPx) + j];
                                }
                            }
                            sprintf_s(imagenumber, "%05d", j);
                            filename = ".\\" + folder + "\\PIC" + imagenumber + ".tif";
                            T.saveData(image, filename.c_str());
                        }
                        delete[] image;
                    });
                t[i] = move(t_temp);
            }
        }

        for (int i = 0; i < threads; i++) // wait until all threads are finished
            t[i].join();
        //-----------------------------------------------

        if (!success)
        {
            for (int i = 0; i < pixels * pixels; i++)
            {
                this_thread::sleep_for(chrono::microseconds(100));
                release_wait();
                this_thread::sleep_for(chrono::microseconds(100));
                release_wait();
            }
        }

        auto end_saving = chrono::steady_clock::now();
        double time_saving = chrono::duration_cast<chrono::milliseconds>(end_saving - start_saving).count();

        std::cout << "Time for taking pictures " << time_scanning << "ms" << endl;
        std::cout << "Time for saving pictures " << time_saving << "ms" << endl;
    }

    delete[] PixelValues;
    //Fgen.OutPutDisable(1);
}

bool Microscope::DynamicMeasurement() {
    size_t NsampPx = 0;
    int vibExp = 0;
    int vibSamples = 0;
    char trigC = 'e'; // always trigger vibrometry on external (i.e. sync signal) also, since DIC measurement does the same
    bool linewise, timeStamp = false, dicMode = false, vibMode = false, showPlot = false;

    void (Microscope:: * ImageFunction)(size_t); // measurement function pointer

    std::cout << "Include DIC (y/n) (1/0)?" << endl;
    std::cin >> dicMode;

    std::cout << "Include Vibrometry (y/n) (1/0)?" << endl;
    std::cin >> vibMode;

    if (dicMode == false and vibMode == false) {
        std::cout << "Aborted!\n"; return 0;
    }

    if (dicMode) {
        std::cout << "Set parameters for DIC data" << std::endl;
        std::cout << "Numbers of samples N stored per pixel (should follow 256 + N * 16): ";
        std::cin >> NsampPx;

        std::cout << "Scan linewise? (0/1)" << std::endl;
        std::cin >> linewise;
    }

    if (vibMode) {
        std::cout << "Select Vibrometry parameters:" << std::endl;
        std::cout << "Enter number n of 2^n samples to be acquired:" << std::endl;
        std::cin >> vibExp;
        vibSamples = pow(2, vibExp);
        std::cout << "Timestamp in Filename? (y/n)->(1/0)" << std::endl;
        std::cin >> timeStamp;
    }

    // make sure sample excitation is ON
    Fgen.OutPutEnable(1);
    //std::this_thread::sleep_for(20ms);

    if (dicMode) {
        if (linewise)
            ImageFunction = &Microscope::TakeAndSaveMotionImages_V2;
        else
            ImageFunction = &Microscope::TakeAndSaveMotionImagesColumn_V2;

        ((*this).*ImageFunction)(NsampPx); // DIC measurement is performed at this point
    }
    if (vibMode) {
        //RunSingleVibro(vibSamples, trigC, SPC_TM_NEG, showPlot);
        RunSingleVibro(vibSamples, trigC, SPC_TM_NEG, timeStamp, showPlot);
    }


    Fgen.OutPutDisable(1); //disable Fgen output on Channel1 

}

void Microscope::TakeAndSaveMotionImagesMeanZ(double time, unsigned int n, float dz)
{
    // Open connection to z-Stage
    zStage.ConnectAndStartUpFixedAxis(hwParams.s_zStageAddr.c_str(), hwParams.s_zAxis,true);
    bool filter;
    std::cout << "Use Fourier filter to cut out high frequencies (> 500kHz) and low frequencies (< 50kHz)? (1/0) ";
    cin >> filter;
    // Configuring Board (external trigger)
    Board.SetSampleRate(SAMPLE_RATE_10MSPS);
    Board.ConfigureTriggerEngine(TRIG_ENGINE_J, TRIG_EXTERNAL, (U32)150, TRIGGER_SLOPE_POSITIVE); // Trigger for synchronisation
    Board.ConfigureAuxIO(AUX_IN_TRIGGER_ENABLE, TRIGGER_SLOPE_POSITIVE); // Put Toggle signal on AuxIO port to enable trigger when galvo is ready

    // calculate number of samples per pixel to reach given time
    int samplerate = 10e6; // S/s
    size_t samples = int(time * samplerate / 1000 + 0.5); // note that time is in ms

    // Allocate Memory for data
    WORD* PixelValues = new WORD[size_t(pixels) * pixels * samples];
    if (PixelValues == NULL)
    {
        std::cout << "ERROR: Out of Memory. Try to take less samples." << endl;
        return;
    }
    unsigned int* buffer = new unsigned int[size_t(pixels) * pixels * samples]; // buffer to sum up all the images
    memset(buffer, 0, size_t(pixels) * pixels * samples * 4); // set all values in the buffer to 0

    // save mean z position an move piezo to lowest position
    double meanZ = zPosition; // Start position of the piezo stage
    SetzPosition(meanZ - double(n - 1) / 2 * dz / 1000); // lowest position for stack, works for even and odd n

    bool success = true;

    for (int i = 0; i < n; i++)
    {
        // Create List in Galvo Scanner
        set_start_list(1);
        set_wait(0);

        for (int k = 0; k < pixels; k++)
        {
            for (int j = 0; j < pixels; j++)
            {
                jump_abs(gCoords[j], gCoords[k]);
                set_wait((k * pixels + j) * 3 + 1);
                write_8bit_port_list(0xff);
                set_wait((k * pixels + j) * 3 + 2);
                write_8bit_port_list(0);
                set_wait((k * pixels + j) * 3 + 3);
            }
        }

        jump_abs(0, 0); // Jump back to the centre when finished
        set_end_of_list();
        execute_list(1);

        // prepare fourier filter
        double fL = 50e3;
        double fH = 500e3;
        fftw_complex* in, * out;
        fftw_plan pFW, pBW;

        in = (fftw_complex*)fftw_malloc(sizeof(fftw_complex) * samples);
        out = (fftw_complex*)fftw_malloc(sizeof(fftw_complex) * samples);
        pFW = fftw_plan_dft_1d(samples, in, out, FFTW_FORWARD, FFTW_MEASURE);
        pBW = fftw_plan_dft_1d(samples, out, in, FFTW_BACKWARD, FFTW_MEASURE);

        // Activate AutoDMA measurement
        Board.StartNPTAutoDMAAcquisition(samples, 1, pixels * pixels, DetectorChannel);

        // sacnning part
        bool busy;
        UINT Status;
        UINT Pos;
        U16* data = new U16[samples];

        //std::cout << "Scanning..." << endl;
        auto start_scanning = chrono::steady_clock::now();
        for (int j = 0; j < pixels; j++)
        {
            std::cout << "\r" << "Scanning Image " << i+1 << "/" << n << ": " << int(double(j) / pixels * 100 + 0.5) << "% ";
            for (int k = 0; k < pixels; k++)
            {
                //auto start = chrono::steady_clock::now();
                release_wait(); // make galvo jump to next position
                do
                {
                    get_status(&Status, &Pos);
                    busy = (bool)(Status & 1);
                } while (busy);

                this_thread::sleep_for(chrono::microseconds(100)); // Test: Ensure that Galvo has stopped moving before fireing Trigger

                release_wait(); // Make Galvo toggle
                this_thread::sleep_for(chrono::microseconds(100));
                release_wait();

                // read out one buffer which should be measured now
                success = Board.ReadDataNPTAutoDmA(data, int(time) + 20); // timeout in ms, round up to guarantee that the card has enought time for measurement
                if (filter)
                {
                    for (int l = 0; l < samples; l++)
                    {
                        in[l][0] = double(data[l]);
                        in[l][1] = 0;
                    }
                    FTfilter(in, out, samples, pFW, pBW, samplerate, fL, fH);
                    for (int l = 0; l < samples; l++)
                        buffer[(j * size_t(pixels) + k) * samples + l] += U16(in[l][0] + 0.5); // add 32769 if DC Peak is lost
                }
                else
                {
                    for (int l = 0; l < samples; l++)
                        buffer[(j * size_t(pixels) + k) * samples + l] += data[l];
                }

                if (!success)
                    break;
            }
            if (!success) // break up
            {
                std::cout << endl << "An Error occured - stopping measurement";
                set_start_list(1);
                jump_abs(0, 0); // move galvo back to the centre
                set_end_of_list();
                execute_list(1);
                break;
            }

        }
        if (!success)
            break;
        release_wait();
        auto end_scanning = chrono::steady_clock::now();
        double time_scanning = chrono::duration_cast<chrono::milliseconds>(end_scanning - start_scanning).count();

        std::cout << endl;
        Board.AbortNPTAutoDMA(); // end Auto DMA measurement (important!!)
        SetzPosition(zPosition + dz / 1000);
    }

    // Save Data as tif images
    string folder = "Motions";

    if (!CreateDirectoryA(folder.c_str(), NULL))
        std::cout << "Could not create folder" << endl;
    else
        std::cout << "Created Directory: " << folder << endl;

    size_t NumPixels = size_t(pixels) * pixels;

    auto start_saving = chrono::steady_clock::now();

    // Multi threaded
    //-----------------------------------------------
    if (success)
    {
        int px = pixels;
        const int threads = 12;
        size_t perThread = samples / threads; // number of images saved by each thread
        thread t[threads];
        int fov = FOV;
        double exFreq = excitFreq;
        int gain = APD0.QuerryCurrentGain();
        float IR = Board.translateInputRange(InputRange);
        for (int i = 0; i < threads; i++)
        {

            if (i < threads - 1)
            {
                thread t_temp([i, perThread, buffer, samples, NumPixels, folder, px, fov, gain, IR, n, exFreq] // create thread that overwrites a part of the buffer
                    {
                        WORD* image = new WORD[NumPixels];
                        CTiffGrey T(px, px); // Tiff object could not be used in lambda function, so create a new one
                        T.setAttribute(ID_FieldOfView, "Field of View [µm]", fov);
                        T.setAttribute(ID_PixelSize, "Pixel size [µm]", double(fov) / px);
                        T.setAttribute(ID_APDGain, "APD Gain M", gain);
                        T.setAttribute(ID_InputRange, "Input range [mV]", IR);
                        T.setAttribute(ID_ExcitationFreq, "Excitation freq [Hz]", exFreq);
                        char imagenumber[50]; // buffer for sprintf_s to write down current image number
                        string filename;
                        for (size_t j = i * perThread; j < (i + 1) * perThread; j++)
                        {
                            for (size_t k = 0; k < NumPixels; k++)
                            {
                                image[k] = WORD(double(buffer[k * samples + j]) / n + 0.5);
                            }
                            sprintf_s(imagenumber, "%05d", j);
                            filename = ".\\" + folder + "\\PIC" + imagenumber + ".tif";
                            T.saveData(image, filename.c_str());
                        }
                    });
                t[i] = move(t_temp); // move task into array of threads (?within?)
            }
            else
            {
                thread t_temp([i, perThread, buffer, samples, NumPixels, folder, px, fov, gain, IR, n, exFreq] // last thread maybe has to work more if samples and number of threads are relative primes
                    {
                        WORD* image = new WORD[NumPixels];
                        CTiffGrey T(px, px); // Tiff object could not be used in lambda function, so create a new one
                        T.setAttribute(ID_FieldOfView, "Field of View [µm]", fov);
                        T.setAttribute(ID_PixelSize, "Pixel size [µm]", double(fov) / px);
                        T.setAttribute(ID_APDGain, "APD Gain M", gain);
                        T.setAttribute(ID_InputRange, "Input range [mV]", IR);
                        T.setAttribute(ID_ExcitationFreq, "Excitation freq [Hz]", exFreq);
                        char imagenumber[50]; // buffer for sprintf_s to write down current image number
                        string filename;
                        for (size_t j = i * perThread; j < samples; j++)
                        {
                            for (size_t k = 0; k < NumPixels; k++)
                            {
                                image[k] = WORD(double(buffer[k * samples + j]) / n + 0.5);
                            }
                            sprintf_s(imagenumber, "%05d", j);
                            filename = ".\\" + folder + "\\PIC" + imagenumber + ".tif";
                            T.saveData(image, filename.c_str());
                        }
                    });
                t[i] = move(t_temp);
            }
        }

        for (int i = 0; i < threads; i++) // wait until all threads are finished
            t[i].join();
    }
    //-----------------------------------------------

    delete[] buffer;


    auto end_saving = chrono::steady_clock::now();
    double time_saving = chrono::duration_cast<chrono::seconds>(end_saving - start_saving).count();

    Board.ConfigureAuxIO(AUX_IN_AUXILIARY, TRIGGER_SLOPE_POSITIVE); // Resetting AUX IO
    SetzPosition(meanZ); // Go back to initial z Position
    // Disconnect stage
    zStage.DisconStage(); 
}

void Microscope::TakeSeveralImages(int number, int time, bool fastMode)
{
    // The thing here is, that "TakeSeveralImages" calls other member functions of the class Microscope. Function pointers are required.
    // For more information: https://www.learncpp.com/cpp-tutorial/78-function-pointers/
    // Therefore at first, a pointer to the "void" member functions of the class "Microscope" has to be generated -> "ImageFunction".
    //void (Microscope:: * ImageFunction)(); // I guess the "Microscope::" behaves like a data type (int/etc) but in this case the data type is the member functions of the class "Microscope"
    //if (fastMode)
    //    ImageFunction = &Microscope::TakeImageRows;    // Depending on the value of "fastMode", the address of either the "TakeImageRows" or "TakeImageSinglePixel"
    //else                                               // is stored in the pointer "ImageFunction".
    //    ImageFunction = &Microscope::TakeImageSinglePixel;

    string folder = "MultipleImages";
    string filename;

    if (!CreateDirectoryA(folder.c_str(), NULL)) {
        std::cout << "Could not create folder" << endl;
        std::cout << "Foulder may already exist." << endl;
    }
    else
        std::cout << "Created Directory: " << folder << endl;

    for (int i = 0; i < number; i++)
    {
        thread t([time] { this_thread::sleep_for(chrono::seconds(time)); }); // start timer
        // In this case, the object "M" of the class Microscope, or more precicely THIS object "M" (hence, "*this")
        // should execute the function whose address is stored in the "ImageFunction" pointer.
        //((*this).*ImageFunction)(); // Image is taken at this point    // * dereferences the "ImageFunction" pointer.
        if (fastMode)
            TakeImageRows();
        else
            TakeImageSinglePixel();
        
        
        
        filename = ".\\" + folder + "\\" + to_string(i * time) + "s.tif";
        SaveAsTiff(picture,filename.c_str());
        t.join(); // wait until timer is finished
    }
}

void Microscope::TakeConsecutiveImages(int number, bool fastMode, bool scanDir) 
{

string folder = "MultipleImages";
string filename;

if (!CreateDirectoryA(folder.c_str(), NULL)) {
    std::cout << "Could not create folder" << endl;
    std::cout << "Foulder may already exist." << endl;
}
else
    std::cout << "Created Directory: " << folder << endl;

char imagenumber[50];
for (int i = 0; i < number; i++)
{
    // In this case, the object "M" of the class Microscope, or more precicely THIS object "M" (hence, "*this")
    // should execute the function whose address is stored in the "ImageFunction" pointer.
    //((*this).*ImageFunction)(); // Image is taken at this point    // * dereferences the "ImageFunction" pointer.
    if (fastMode && scanDir) {
        //std::cout << "Fast line wise imaging." << endl;
        TakeImageRows();    // Depending on the value of "fastMode", the address of either the "TakeImageRows" or "TakeImageSinglePixel"
    }
    else if (fastMode && !scanDir) {
        //std::cout << "Fast column wise imaging." << endl;
        TakeImageColumns();
    }
    else if (!fastMode && scanDir) {
        //std::cout << "Slow line wise imaging." << endl;
        TakeImageSinglePixel();
    }
    else {
        //std::cout << "Slow column wise imaging." << endl;
        TakeImageSinglePixelCol();
    }
     
    
    sprintf_s(imagenumber, "%03d", i);
    filename = ".\\" + folder + "\\" + "img_num_"+ imagenumber + ".tif";
    SaveAsTiff(picture,filename.c_str());
}
std::cout << "All images taken." << endl;

}

void Microscope::TakeZLineScan(const double distance, const unsigned int samples)
{
    if(!allStagesConnected)
        zStage.ConnectAndStartUpFixedAxis(hwParams.s_zStageAddr.c_str(), hwParams.s_zAxis,true);
    zPosition = zStage.currentPos;

    double startZ = zPosition; // z position at beginning
    double stepsize = distance / samples;
    WORD* Intensity = new WORD[samples]; // Array to fill with intensities at different z positions

    // Configuring Board
    Board.SetSampleRate(SAMPLE_RATE_50MSPS);
    int Nmean = 26; // number of samples from card to build a mean
    Board.ConfigureBufferRecord(0, Nmean, samples);
    Board.SinglePortAcquisition(false, DetectorChannel, false);      // "wait" = false

    std::cout << endl;

    for (int i = 0; i < samples; i++)
    {
        SetzPosition(startZ + i * stepsize / 1000); // Move Piezo to new position (note i starts from 0!) // + Plus for out of the sample
        Board.SoftwareTrigger(); // Trigger to take Intensity
        std::cout << "\r" << "Progress: " << i << "/" << samples << " measured";
    }
    SetzPosition(startZ);// reset piezo to z position of the beginning
    if (!allStagesConnected)
        zStage.DisconStage();

    // readout data
    int n;
    for (int i = 0; i < samples; i++)
    {
        WORD* data = Board.ReadDatafromBuffer(n, DetectorChannel, i + 1);
        float temp = 0;
        for (int j = 0; j < n; j++)
            temp += data[j];
        Intensity[i] = (WORD)(temp / n + 0.5);
    }

    // Save data as .dat file
    ofstream fileout("zStack.dat");

    for (int i = 0; i < samples; i++)
        fileout << stepsize * i << " " << Intensity[i] << endl;

    fileout.close();
}

void Microscope::TakeAxialImage(const double NpxAxial, bool orient) {
    WORD* AxialPic = new WORD[NpxAxial * pixels]; // allocate memory for axial image
    Tiff.setBitmapSize(pixels, NpxAxial); // set image width and height accordingly
    Tiff.setAttribute(ID_MeasurementMode, "Measurement mode", "Axial Image");

    // configure zStage
    if (!allStagesConnected)
        zStage.ConnectAndStartUpFixedAxis(hwParams.s_zStageAddr.c_str(), hwParams.s_zAxis,true);
    zPosition = zStage.currentPos;
    double startZ = zPosition; // z position at beginning
    double stepsize = double(FOV / pixels); // Stepsize for the z-stage i.e. axial pixel size
  

    // Configuring Board
    Board.SetSampleRate(SAMPLE_RATE_200KSPS);
    int offset = 36; // correct shift 
    int pretrigger = 0;
    Board.ConfigureBufferRecord(pretrigger, pixels + offset - pretrigger, NpxAxial); // "NpxAxial" records with "pixels" samples each
    Board.SinglePortAcquisition(false, DetectorChannel, false);      // "wait" = false to continue in programm
    Board.ConfigureTriggerEngine(TRIG_ENGINE_J, TriggerChannel, (U32)150, TRIGGER_SLOPE_POSITIVE); // Trigger for synchronisation

    // calculate time for one line with given sample rate (TODO: arbritrary sample rate)
    int samplerate = 200000; // S/s
    int Time = pixels * 1e6 / samplerate + 9; //(+9 from jump delay default value) // 1e6/samplerate gives time in microseconds
    if (FOV * galvoPixelCalib * samplerate / pixels > 112350000) // jump speed higher than possible for Scanner
        std::cout << "Warning: Velocity too high. Increase pixelsize" << endl;    //????????????? Reduce? Rather increase pixelsize or reduce amount of pixels, right?

    // Create List for Galvo Scanner
    set_start_list(1);
    for (int i = 0; i < NpxAxial; i++)
    {
        if (orient) {
            micro_vector_abs(gCoords[0], 0, -1, -1); // fastest way to get back to beginning of a line (so far)
            set_wait(i);
            timed_jump_abs(gCoords[pixels - 1], 0, Time); // Try timed_mark_abs if precision is neccessary 
        }
        else
        {
            micro_vector_abs(0, gCoords[0], -1, -1); // fastest way to get back to top of a column (so far)
            set_wait(i);
            timed_jump_abs(0, gCoords[pixels - 1], Time); // Try timed_mark_abs if precision is neccessary 
        }
    }
    jump_abs(0, 0);
    set_end_of_list();
    execute_list(1);

    // For readout of xy position
    // 05: SetMode (readout); 01: Is position
    control_command(1, 1, 0x0501); // x position
    control_command(1, 2, 0x0501); // y position

    int signal[4] = { 1, 2, 0, 0 }; // Tells which variables should be read out (here x, y, 0, 0)
    //int* result = new int[4];       // Pointer for the results from readout

    bool busy;
    UINT Status;
    UINT Pos;
    bool onTarget = false;

    //std::cout << "Scanning..." << endl;
    std::this_thread::sleep_for(std::chrono::microseconds(100000));
    auto start_scanning = chrono::steady_clock::now();
    for (int i = 0; i < NpxAxial; i++) // Loop over all lines
    {
        do
        {   // Check actual position until continue
            //get_values((ULONG_PTR)signal, (ULONG_PTR)result); // Convert Adresses to ULONG 
            get_values((ULONG_PTR)signal, (ULONG_PTR)gPosResult); // Convert Adresses to ULONG 
            if (orient)
                onTarget = gPosResult[0] >= gCoords[0];
            else
                onTarget = gPosResult[1] >= gCoords[0];
        } while (onTarget); // Mirror is at the beginning of a line again
        release_wait(); // Start scanning the line
        do
        {
            get_status(&Status, &Pos);
            busy = (bool)(Status & 1); // last bit from Status is busy variable
        } while (busy);

        SetzPosition(startZ - i * stepsize / 1000); // Move Piezo to new position (note i starts from 0!) // - Minus for moving INTO the sample
    }

    int overhead = 0;
    while (AlazarBusy(Board.GetBoardHandle())) // Check how many triggers are missing (should be 0)
    {
        Board.SoftwareTrigger();
        std::this_thread::sleep_for(std::chrono::microseconds(1000));
        overhead++;
    }
    if (overhead > 0)
        std::cout << "Warning: " << overhead << " Trigger events for a line are missing" << endl;
    release_wait();
    auto end_scanning = chrono::steady_clock::now();
    double time_scanning = chrono::duration_cast<chrono::milliseconds>(end_scanning - start_scanning).count();

    // Move Stage back to starting position
    SetzPosition(startZ);// reset piezo to z position of the beginning
    if (!allStagesConnected)
        zStage.DisconStage();


    // readout data and write to Intensity array
    //std::cout << "Reading Data..." << endl;
    auto start_readout = chrono::steady_clock::now();
    for (int i = 0; i < NpxAxial; i++) // Each record stands for one line
    {
        int n;
        U16* data = Board.ReadDatafromBuffer(n, DetectorChannel, i + 1);
        for (int j = 0; j < pixels; j++) // add current line at the end of picture
        {
            AxialPic[i * pixels + j] = data[j + offset];
        }
    }
    auto end_readout = chrono::steady_clock::now();
    double time_readout = chrono::duration_cast<chrono::milliseconds>(end_readout - start_readout).count();

    SaveAsTiff(AxialPic,"axImag.tif");

    delete[] AxialPic;  // free up memory again

    Tiff.setBitmapSize(pixels, pixels); // reset to default
};

void Microscope::determineAPDSaturation(int startgain, int gainsteps, bool increase) {

    WORD* Intensity = new WORD[gainsteps]; // Array to fill with intensities for different gain M values

    // Configuring Board
    Board.SetSampleRate(SAMPLE_RATE_50MSPS);
    int Nmean = 26; // number of samples from card to build a mean
    Board.ConfigureBufferRecord(0, Nmean, gainsteps); 
    Board.SinglePortAcquisition(false, DetectorChannel, false);      // "wait" = false

    if (!APD0.SetGain(startgain, &microscopeAPDM))
        std::cout << "Error setting gain 1" << endl;

    int newGain = startgain;
    for (int i = 0; i < gainsteps; i++) {

        std::cout << "Maxium of Image = " << maxImage() << endl;
        Board.SoftwareTrigger(); // Trigger to take Intensity

        if (increase) newGain = newGain + 1; 
        if (!increase) newGain = newGain - 1;
        if (!APD0.SetGain(newGain, &microscopeAPDM))
            std::cout << "Error setting gain 2" << endl;
    }

    // readout data
    int n;
    for (int i = 0; i < gainsteps; i++)
    {
        WORD* data = Board.ReadDatafromBuffer(n, DetectorChannel, i + 1);
        float temp = 0;
        for (int j = 0; j < n; j++)
            temp += data[j];
        Intensity[i] = (WORD)(temp / n + 0.5);
    }

    // Save data as .dat file
    ofstream fileout("GainScan.dat");

    for (int i = 0; i < gainsteps; i++)
        fileout << double(startgain + i) << " " << Intensity[i] << endl;

    fileout.close();

}

bool Microscope::SetExcitParam(){// this version querries for parameters
    float ampl, offs, freq, phase;
    bool amplitudeLimit = false;
    bool enabOutp       = false;

    //default so far..
    phase = 0;
    offs  = 0;

    std::cout << "Enter parameters." << endl;
    std::cout << "Enter excitation amplitude Vpp in Volt (0.001V <= Vpp <= " << hwParams.s_Vpp_thresh << " V)" << endl;
    cin >> ampl;

    if (ampl > hwParams.s_Vpp_thresh) {
        std::cout << "Amplitude Vpp exceeds max of " << hwParams.s_Vpp_thresh << " V " << endl;
        std::cout << "Amplitude set Vpp = 1mV!" << endl;
        amplitudeLimit = true;
        ampl = 0.001;
        excitAmp = ampl; // set in microscope attribute
    }

    std::cout << "Enter frequency in Hz." << endl;
    cin >> freq;
    excitFreq = freq;// set in microscope attribute

    std::cout << "Enable frequency generator output (1/0)?" << endl;
    cin >> enabOutp;


    if (enabOutp)
        Fgen.OutPutEnable(1);
    if (!enabOutp)
        Fgen.OutPutDisable(1);

    Fgen.ConfigureSine(ampl, offs, freq, phase);

    if (amplitudeLimit)
        return 1;
    return 0;
}

bool Microscope::SetExcitParam(float ampl, float offs, float freq, float phase) {//this version doesn't
    bool amplitudeLimit = false;
    float setAmp = ampl;
    if (setAmp > hwParams.s_Vpp_thresh) {
        std::cout << "Amplitude Vpp exceeds max of " << hwParams.s_Vpp_thresh << " V " << endl;
        std::cout << "Amplitude set Vpp = 1mV!" << endl;
        amplitudeLimit = true;
        setAmp = 0.001;
    }
    excitFreq = freq;
    Fgen.ConfigureSine(setAmp, offs, freq, phase);

    if (amplitudeLimit)
        return 1;
    return 0;
}

WORD* Microscope::TakeMotionImages_V2(size_t NpointPerPx)
{
    int waitCounter     = 0;
    int releaseCounter  = 0;
    auto initialize_meas = chrono::steady_clock::now();

    int samplerate = dynMeasParams.s_imagSampleR; //in kSps
    //std::cout << std::endl<< "DIC samplerate is set to " << samplerate << "kSps";
    samplerate = 1000 * samplerate; // now in Sps

    size_t  samples         = NpointPerPx;
    float   timePx          = 1 / float(samplerate) * samples; 
    int     timePx_ms       = (int)(1000 * timePx + 0.5);   // time converted and rounded to milliseconds
    int     NwaitCommands   = int(timePx / 20e-6 + 1);      // calculate how many set_wait() commands are required for the appropriate pixel dwell time
    bool    longwait        = false;

    // (Re)configer board channels
    Board.ConfigureChannel(CHANNEL_A, DC_COUPLING, InputRange, IMPEDANCE_1M_OHM);
    Board.ConfigureChannel(CHANNEL_B, DC_COUPLING, INPUT_RANGE_PM_2_V, IMPEDANCE_1M_OHM);

    // Configuring Board (external trigger)
    //Board.SetSampleRate(SAMPLE_RATE_10MSPS);
    Board.SetSampleRateInt(samplerate);
    Board.ConfigureTriggerEngine(TRIG_ENGINE_J, TRIG_EXTERNAL, (U32)200, TRIGGER_SLOPE_NEGATIVE); // Trigger for synchronisation

    if (NwaitCommands * pixels * pixels > 4194304) { //switch into mode where pixel dwell time is NOT set by several wait-commands of the galvo but by CPU sleep time
        longwait = true;                             //this only makes sense for low freq measurements when the dwell time is on the order of several dozens of milliseconds!
    }

    auto create_galvo_list = chrono::steady_clock::now();
    // Create List in Galvo Scanner
    set_start_list(1);
    waitCounter++; set_wait(waitCounter);

    for (int i = 0; i < pixels; i++)
    {
        for (int j = 0; j < pixels; j++)
        {
            jump_abs(gCoords[j], gCoords[i]);
            set_wait((i * pixels + j) * 30 + 2);
            write_8bit_port_list(0xff);  // writing to RTC6 card "EXTENSION 2 Stiftleiste" => Opens the gate for data aqcuisition
            waitCounter++; set_wait(waitCounter);
            //waitCounter++; set_wait(waitCounter);
            write_8bit_port_list(0);
            if (longwait == false) {
                for (int k = 0; k < NwaitCommands; k++) //increase pixel dwell time depending on samples per pixel
                {
                    waitCounter++; set_wait(waitCounter);
                }
            }
            else if (longwait == true) {
                waitCounter++; set_wait(waitCounter); 
            }
        }
    }

    jump_abs(0, 0); // Jump back to the centre when finished
    set_end_of_list();
    execute_list(1);

    auto finish_galvo_list_gen = chrono::steady_clock::now();
    double time_galvo_list_gen = chrono::duration_cast<chrono::milliseconds>(finish_galvo_list_gen - create_galvo_list).count();
    //cout << endl << endl << "Gen galvo list was " << time_galvo_list_gen << "millisec" << endl << endl;

    // Allocate Memory for data
    WORD* PixelValues = new WORD[size_t(pixels) * pixels * NpointPerPx];
    if (PixelValues == NULL)
    {
        std::cout << "ERROR: Out of Memory. Try to take less samples." << endl;
        return NULL;
    }

    // Activate AutoDMA measurement
    Board.StartNPTAutoDMAAcquisition(samples, pixels, pixels, CHANNEL_A); // buffer is allocated here and needs to have the actual size of the data i.e. samples = 4200 for 10MSps and Tpx = 0.42ms

    // For readout of xy position
  // 05: SetMode (readout); 01: Is position
    control_command(1, 1, 0x0501); // x position
    control_command(1, 2, 0x0501); // y position

    int signal[4] = { 1, 2, 0, 0 }; // Tells which variables should be read out (here x, y, 0, 0)
    //int* result = new int[4];       // Pointer for the results from readout
    bool onTargetX = false;  // bool used to calculate if galvo is on designated postition from jump_abs(gCoords[k], gCoords[i]);
    bool onTargetY = false;

    bool success = true;
    bool busy;
    UINT Status;
    UINT Pos;
    U16* data = new U16[samples]; // see Board.StartNPTAutoDMAAcquisition(..) above

    size_t NumPixels = size_t(pixels) * pixels;

    //std::cout << "Scanning..." << endl;
    auto start_scanning = chrono::steady_clock::now();

    double time_init_meas = chrono::duration_cast<chrono::milliseconds>(start_scanning - initialize_meas).count();
    //cout << endl << endl << "initializing measurement was " << time_init_meas << " milliseconds" << endl << endl;
    for (int i = 0; i < pixels; i++)
    {
        std::cout << "\r" << "Scanning... " << int(double(i) / pixels * 100 + 0.5) << "%";
        for (int k = 0; k < pixels; k++)
        {
            auto start = chrono::steady_clock::now();
            release_wait(); releaseCounter++; // make galvo jump to next position

            int posCountX = 0;
            int posCountY = 0;
            do
            {   // Check actual position until continue
                get_values((ULONG_PTR)signal, (ULONG_PTR)gPosResult); // Convert Adresses to ULONG 
                onTargetX = (abs(gPosResult[0] - gCoords[k]) < 4);
                onTargetY = (abs(gPosResult[1] - gCoords[i]) < 4);
                //if (onTarget)  // Since scanlab measures distances in "bit-values" 600 = 1mu (for Nikon50x), 1 = 1/600 mu as tolerance for "galvo overswing"
                //    posCount++;
                posCountX += onTargetX;
                posCountY += onTargetY;
            } while (posCountX < 4 || posCountY < 4); // Mirror is at the beginning of a line again. The "threshold" value for posCount is defined rather arbitrarily to give good images. 
            // it is chosen > 1 to account for multiple overswings..
            //posCount = 0; //Reset

            release_wait(); releaseCounter++; // Make Galvo toggle
            //release_wait(); releaseCounter++;  // keep gate open

            //this_thread::sleep_for(chrono::microseconds(100));  // since rtc6 card internal clock works in 10micros intervals, 100micros seems long..
            if (longwait == false) {
                for (int k = 0; k < NwaitCommands; k++) //increase pixel dwell time depending on samples per pixel
                {
                    release_wait(); releaseCounter++;
                }
            }
            else if (longwait == true) {
                release_wait(); releaseCounter++;
                this_thread::sleep_for(std::chrono::milliseconds(timePx_ms));
            }

            if (!success)
                break;

            auto end = chrono::steady_clock::now();
            double timeduration = chrono::duration_cast<chrono::microseconds>(end - start).count();
            //std::cout << "Time = " << time << endl;
        }
        success = Board.ReadDataNPTAutoDmALine(PixelValues, 5000, i); // timeout in ms, add some buffer (of 20ms) to avoid errors

        if (!success) // break up
        {
            std::cout << endl << "An Error occured - stopping measurement";
            set_start_list(1);
            jump_abs(0, 0); // move galvo back to the centre
            set_end_of_list();
            execute_list(1);
            break;
        }

    }
    release_wait(); releaseCounter++;

    delete[] data;

    auto end_scanning = chrono::steady_clock::now();
    double time_scanning = chrono::duration_cast<chrono::milliseconds>(end_scanning - start_scanning).count();

    if (!success) // send release waits to RTC6 Scanner Card to make sure list is finished
    {
        for (int i = 0; i < pixels * pixels; i++)
        {
            this_thread::sleep_for(chrono::microseconds(100));
            release_wait(); releaseCounter++;
            this_thread::sleep_for(chrono::microseconds(100));
            release_wait(); releaseCounter++;
        }
    }
    std::cout << endl;
    Board.AbortNPTAutoDMA(); // end Auto DMA measurement (important!!)
    //Board.ConfigureAuxIO(AUX_IN_AUXILIARY, TRIGGER_SLOPE_POSITIVE); // Resetting AUX IO

    std::cout << "Time for taking pictures " << time_scanning << "ms" << endl;

    return PixelValues;
}

WORD* Microscope::TakeMotionImagesColumn_V2(size_t NpointPerPx) 
{
    //int waitCounter     = 0;
    //int releaseCounter  = 0;
    //// (Re)configer board channels
    //Board.ConfigureChannel(CHANNEL_A, DC_COUPLING, InputRange, IMPEDANCE_1M_OHM);
    //Board.ConfigureChannel(CHANNEL_B, DC_COUPLING, INPUT_RANGE_PM_2_V, IMPEDANCE_1M_OHM);
    //// Configuring Board (external trigger)
    //Board.SetSampleRate(SAMPLE_RATE_10MSPS);
    //Board.ConfigureTriggerEngine(TRIG_ENGINE_J, TRIG_EXTERNAL, (U32)200, TRIGGER_SLOPE_NEGATIVE); // Trigger for synchronisation
    //
    //// calculate number of samples per pixel to reach given time
    //const int samplerate    = dynSampleRate; // S/s
    //const float time        = timeDynPx;
    //size_t samples          = NpointPerPx; // 

    //float timePx        = 1 / float(samplerate) * samples;
    //int NwaitCommands   = int(timePx / 20e-6 + 1);
    int waitCounter = 0;
    int releaseCounter = 0;
    auto initialize_meas = chrono::steady_clock::now();

    int samplerate = dynMeasParams.s_imagSampleR; //in kSps
    //std::cout << std::endl<< "DIC samplerate is set to " << samplerate << "kSps";
    samplerate = 1000 * samplerate; // now in Sps

    size_t  samples = NpointPerPx;
    float   timePx = 1 / float(samplerate) * samples;
    int     timePx_ms = (int)(1000 * timePx + 0.5);   // time converted and rounded to milliseconds
    int     NwaitCommands = int(timePx / 20e-6 + 1);      // calculate how many set_wait() commands are required for the appropriate pixel dwell time
    bool    longwait = false;

    // (Re)configer board channels
    Board.ConfigureChannel(CHANNEL_A, DC_COUPLING, InputRange, IMPEDANCE_1M_OHM);
    Board.ConfigureChannel(CHANNEL_B, DC_COUPLING, INPUT_RANGE_PM_2_V, IMPEDANCE_1M_OHM);

    // Configuring Board (external trigger)
    //Board.SetSampleRate(SAMPLE_RATE_10MSPS);
    Board.SetSampleRateInt(samplerate);
    Board.ConfigureTriggerEngine(TRIG_ENGINE_J, TRIG_EXTERNAL, (U32)200, TRIGGER_SLOPE_NEGATIVE); // Trigger for synchronisation

    if (NwaitCommands * pixels * pixels > 4194304) { //switch into mode where pixel dwell time is NOT set by several wait-commands of the galvo but by CPU sleep time
        longwait = true;                             //this only makes sense for low freq measurements when the dwell time is on the order of several dozens of milliseconds!
    }

    // Create List in Galvo Scanner
    set_start_list(1);
    waitCounter++; set_wait(waitCounter);

    for (int i = 0; i < pixels; i++)
    {
        for (int j = 0; j < pixels; j++)
        {
            jump_abs(gCoords[i], gCoords[j]);
            waitCounter++; set_wait(waitCounter);
            //periodic_toggle_list(2, // 8-Bit digital Output
            //    0xff,   // toggle all bits
            //    10,     // Stay on for 100 us
            //    1,      // Stay off for 10 us
            //    1,      // Toggle only one time
            //    0);     // Start immediatly
            write_8bit_port_list(0xff);
            waitCounter++; set_wait(waitCounter);
            //waitCounter++; set_wait(waitCounter);
            write_8bit_port_list(0);
            //set_wait((i * pixels + j) * 3 + 3);
            if (longwait == false) {
                for (int k = 0; k < NwaitCommands; k++) //increase pixel dwell time depending on samples per pixel
                {
                    waitCounter++; set_wait(waitCounter);
                }
            }
            else if (longwait == true) {
                waitCounter++; set_wait(waitCounter);
            }
        }
    }

    jump_abs(0, 0); // Jump back to the centre when finished
    set_end_of_list();
    execute_list(1);

    // Allocate Memory for data
    WORD* PixelValues = new WORD[size_t(pixels) * pixels * NpointPerPx];
    if (PixelValues == NULL)
    {
        std::cout << "ERROR: Out of Memory. Try to take less samples." << endl;
        return NULL;
    }

    // Activate AutoDMA measurement
    Board.StartNPTAutoDMAAcquisition(samples, pixels, pixels, CHANNEL_A); // buffer size MUST MATCH the size of the acutal acquired amount of data!!! Hence "samples" instead of "NpointPerPx" is required

    // For readout of xy position
    // 05: SetMode (readout); 01: Is position
    control_command(1, 1, 0x0501); // x position
    control_command(1, 2, 0x0501); // y position

    int signal[4] = { 1, 2, 0, 0 }; // Tells which variables should be read out (here x, y, 0, 0)
    bool onTargetX = false;  // bool used to calculate if galvo is on designated postition from jump_abs(gCoords[k], gCoords[i]);
    bool onTargetY = false;

    bool success = true;
    bool busy;
    UINT Status;
    UINT Pos;

    size_t NumPixels = size_t(pixels) * pixels;

    //std::cout << "Scanning..." << endl;
    auto start_scanning = chrono::steady_clock::now();
    for (int i = 0; i < pixels; i++)
    {
        std::cout << "\r" << "Scanning... " << int(double(i) / pixels * 100 + 0.5) << "%";
        for (int k = 0; k < pixels; k++)
        {
            auto start = chrono::steady_clock::now();
            release_wait(); releaseCounter++; // make galvo jump to next position

            int posCountX = 0;
            int posCountY = 0;
            do
            {   // Check actual position until continue
                get_values((ULONG_PTR)signal, (ULONG_PTR)gPosResult); // Convert Adresses to ULONG 
                onTargetX = (abs(gPosResult[0] - gCoords[i]) < 4);
                onTargetY = (abs(gPosResult[1] - gCoords[k]) < 4);
                //if (onTarget)  // Since scanlab measures distances in "bit-values" 600 = 1mu (for Nikon 50x), 1 = 1/600 mu as tolerance for "galvo overswing"
                //    posCount++;
                posCountX += onTargetX;
                posCountY += onTargetY;
            } while (posCountX < 4 || posCountY < 4); // Mirror is at the beginning of a line again. The "threshold" value for posCaount is defined rather arbitrarily to give good images. 
            // it is chosen > 1 to account for multiple overswings..
            //posCount = 0; //Reset

            release_wait(); releaseCounter++; // Make Galvo toggle
            //release_wait(); releaseCounter++; // Keep gate open

            //this_thread::sleep_for(chrono::microseconds(100));
            if (longwait == false) {
                for (int k = 0; k < NwaitCommands; k++) //increase pixel dwell time depending on samples per pixel
                {
                    release_wait(); releaseCounter++;
                }
            }
            else if (longwait == true) {
                release_wait(); releaseCounter++;
                this_thread::sleep_for(std::chrono::milliseconds(timePx_ms));
            }

            if (!success)
                break; 

            auto end = chrono::steady_clock::now();
            double timeduration = chrono::duration_cast<chrono::microseconds>(end - start).count();
            //std::cout << "Time = " << time << endl;
        }
        success = Board.ReadDataNPTAutoDmALine(PixelValues, 5000, i); // timeout in ms, add some buffer (of 5000ms) to avoid errors

        if (!success) // break up
        {
            std::cout << endl << "An Error occured - stopping measurement";
            set_start_list(1);
            jump_abs(0, 0); // move galvo back to the centre
            set_end_of_list();
            execute_list(1);
            break;
        }

    }
    release_wait(); releaseCounter++;

    auto end_scanning = chrono::steady_clock::now();
    double time_scanning = chrono::duration_cast<chrono::milliseconds>(end_scanning - start_scanning).count();

    if (!success) // send release waits to RTC6 Scanner Card to make sure list is finished
    {
        for (int i = 0; i < pixels * pixels; i++)
        {
            this_thread::sleep_for(chrono::microseconds(100));
            release_wait(); releaseCounter++;
            this_thread::sleep_for(chrono::microseconds(100));
            release_wait(); releaseCounter++;
        }
    }

    std::cout << endl;
    Board.AbortNPTAutoDMA(); // end Auto DMA measurement (important!!)
  
    std::cout << "Time for taking pictures " << time_scanning << "ms" << endl;

    return PixelValues;
}

void Microscope::SaveMotionDataTiffs(WORD* PixelValues, size_t Nsamples, string folderPath, bool linOrCol) {
    
    std::cout << "Starting image-saving process" << endl;
    size_t NumPixels = size_t(pixels) * size_t(pixels);
    size_t samples = Nsamples; // get number of samples per pixel from variable outside the function scope via pointer
    WORD* pxVal = PixelValues;

    // Save Data as tif images
    string folder = folderPath;

    auto start_saving = chrono::steady_clock::now();

    // Multi threaded image formation
    //-----------------------------------------------

    int px = pixels;
    const int threads = 12;
    size_t perThread = samples / threads; // number of images saved by each thread
    thread t[threads];
    bool rowOrCol = linOrCol;
    int fov = FOV;
    double exFreq = excitFreq;
    int gain = APD0.QuerryCurrentGain();
    float IR = Board.translateInputRange(InputRange);
    for (int i = 0; i < threads; i++)
    {

        if (i < threads - 1)
        {
            thread t_temp([i, perThread, pxVal, samples, NumPixels, folder, px, fov, gain, IR, rowOrCol,exFreq] // create thread that overwrites a part of the buffer
                {
                    WORD* image = new WORD[NumPixels];
                    CTiffGrey T(px, px); // Tiff object could not be used in lambda function, so create a new one
                    T.setAttribute(ID_FieldOfView, "Field of View [µm]", fov);
                    T.setAttribute(ID_PixelSize, "Pixel size [µm]", double(fov) / px);
                    T.setAttribute(ID_APDGain, "APD Gain M", gain);
                    T.setAttribute(ID_InputRange, "Input range [mV]", IR);
                    T.setAttribute(ID_ExcitationFreq, "Excitation freq [Hz]", exFreq);
                    char imagenumber[50]; // buffer for sprintf_s to write down current image number
                    string filename;
                    for (size_t j = i * perThread; j < (i + 1) * perThread; j++)
                    {
                        if (rowOrCol) {
                            for (size_t k = 0; k < NumPixels; k++)
                            {
                                image[k] = pxVal[k * samples + j];
                            }
                        }
                        else if (!rowOrCol) {
                            for (size_t k = 0; k < (size_t)px; k++)
                            {
                                for (size_t l = 0; l < (size_t)px; l++) {
                                    image[l * px + k] = pxVal[(l + k * px) * samples + j];
                                }
                            }
                        }
                        sprintf_s(imagenumber, "%05d", j);
                        //filename = ".\\" + folder + "\\PIC" + imagenumber + ".tif";
                        filename = folder + "\\PIC" + imagenumber + ".tif";
                        T.saveData(image, filename.c_str());
                    }
                    delete[] image;
                });
            t[i] = move(t_temp); // move task into array of threads
        }
        else
        {
            thread t_temp([i, perThread, pxVal, samples, NumPixels, folder, px, fov, gain, IR, rowOrCol,exFreq] // last thread maybe has to work more if samples and number of threads are relative primes
                {
                    WORD* image = new WORD[NumPixels];
                    CTiffGrey T(px, px); // Tiff object could not be used in lambda function, so create a new one
                    T.setAttribute(ID_FieldOfView, "Field of View [µm]", fov);
                    T.setAttribute(ID_PixelSize, "Pixel size [µm]", double(fov) / px);
                    T.setAttribute(ID_APDGain, "APD Gain M", gain);
                    T.setAttribute(ID_InputRange, "Input range [mV]", IR);
                    T.setAttribute(ID_ExcitationFreq, "Excitation freq [Hz]", exFreq);
                    char imagenumber[50]; // buffer for sprintf_s to write down current image number
                    string filename;
                    for (size_t j = i * perThread; j < samples; j++)
                    {
                        if (rowOrCol) {
                            for (size_t k = 0; k < NumPixels; k++)
                            {
                                image[k] = pxVal[k * samples + j];
                            }
                        }
                        else if (!rowOrCol) {
                            for (size_t k = 0; k < (size_t)px; k++)
                            {
                                for (size_t l = 0; l < (size_t)px; l++) {
                                    image[l * px + k] = pxVal[(l + k * px) * samples + j];
                                }
                            }
                        }
                        sprintf_s(imagenumber, "%05d", j);
                        //filename = ".\\" + folder + "\\PIC" + imagenumber + ".tif";
                        filename = folder + "\\PIC" + imagenumber + ".tif";
                        T.saveData(image, filename.c_str());
                    }
                    delete[] image;
                });
            t[i] = move(t_temp);
        }
    }

    for (int i = 0; i < threads; i++) // wait until all threads are finished
        t[i].join();
    //-----------------------------------------------

    auto end_saving = chrono::steady_clock::now();
    double time_saving = chrono::duration_cast<chrono::milliseconds>(end_saving - start_saving).count();

    std::cout << "Time for saving pictures " << time_saving << "ms" << endl;

    delete[] pxVal;  //deallocate memory, however turning PixelValues into a dangling pointer I guess..
   }

void Microscope::SaveMotionDataDats(WORD* PixelValues, size_t Nsamples, string fileName) {
    std::cout << "Start saving data" << endl;
    size_t NumPixels = size_t(pixels) * size_t(pixels);
    size_t samples = Nsamples; // get number of samples per pixel from variable outside the function scope via pointer
    WORD* pxVal = PixelValues;

    FILE* pFile; //FAST FILE STORING
    //pFile = fopen(fullName.c_str(),"wb");
    fopen_s(&pFile, fileName.c_str(),"wb"); // MS VC doesn't like fopen..
    streampos dataSize = NumPixels * samples; // specifies how many entries are to be written
    auto start_saving = chrono::steady_clock::now();
    
    fwrite(pxVal, sizeof(WORD), dataSize, pFile);

    auto end_saving = chrono::steady_clock::now();
    double time_saving = chrono::duration_cast<chrono::milliseconds>(end_saving - start_saving).count();

    fclose(pFile);

    //cout << "Time for saving data " << time_saving << "ms" << endl;

    delete[] pxVal;  //deallocate memory, however turning PixelValues into a dangling pointer I guess..
}

void Microscope::SaveMotionMetaData(std::string metaFilePath, size_t Nsamples, bool linOrCol, bool dynMode, size_t NsamplesVib, size_t sampleRvib) {
    std::cout << "Start saving Metadata" << endl;

    string fileName = "metaData.dat";
    string fullPath;

    if (metaFilePath == "") {
        fullPath = fileName;
    }
    else {
        fullPath = metaFilePath + "\\" + fileName;
    }

    ofstream wrMetaData;
    wrMetaData.open(fullPath, std::fstream::out | std::fstream::trunc);

    //auto start_saving_meta = chrono::steady_clock::now();
    // pixelsX pixelsY FOV[µm] samples linewise microscopeAPDM excitAmp 
    wrMetaData << pixels << endl;
    wrMetaData << pixels << endl;
    wrMetaData << GetFOV() << endl;
    wrMetaData << Nsamples << endl;
    wrMetaData << linOrCol << endl;
    wrMetaData << microscopeAPDM << endl;
    wrMetaData << to_string(excitAmp) << endl;

    if (dynMode == 0)
        wrMetaData << "Dynamic Mode 1" << endl;
    else
        wrMetaData << "Dynamic Mode 2" << endl;

    wrMetaData << NsamplesVib << endl;
    wrMetaData << sampleRvib << endl;

    wrMetaData << endl << endl;

    wrMetaData << "Legend:" << endl;
    wrMetaData << "Pixels X, Pixels Y, FOV in 1e-6m" << endl;
    wrMetaData << "N samples per Px, scan direc lines/columns (1/0), APD Gain," << endl;
    wrMetaData << "Excitation Amplitude in V, Dynamic measurement Mode" << endl;
    wrMetaData << "N samples vibrometry, Sample rate vibrometry" << endl;

    wrMetaData << endl << "Timestamp is " << create_timestamp() << endl;

    auto end_saving_meta = chrono::steady_clock::now();
    //double time_saving_meta = chrono::duration_cast<chrono::milliseconds>(end_saving_meta - start_saving_meta).count();
    //cout << "Time for saving Metadata was " << time_saving_meta << "ms" << endl;
    wrMetaData.close();
}

void Microscope::SaveMotionMetaDataAndDirs(std::string metaFilePath, std::string* directories, int Ndirectories, size_t Nsamples,  bool linOrCol) {
    std::cout << "Start saving Metadata" << endl;

    string fileName = "metaDataAndDirs.dat";
    string fullPath;

    if (metaFilePath == ""){
        fullPath = fileName;
    }
    else {
        fullPath =  metaFilePath + "\\" + fileName;
    }

    ofstream wrMetaData;
    wrMetaData.open(fullPath, std::fstream::out | std::fstream::trunc);
   
    auto start_saving_meta = chrono::steady_clock::now();
    // pixelsX pixelsY FOV[µm] samples linewise microscopeAPDM excitAmp 
    wrMetaData << pixels << endl;
    wrMetaData << pixels << endl;
    wrMetaData << GetFOV() << endl;
    wrMetaData << Nsamples << endl;
    wrMetaData << linOrCol << endl;
    wrMetaData << microscopeAPDM << endl;
    wrMetaData << to_string(excitAmp) << endl;

    for (int i = 0; i < Ndirectories; i++)
    {
        wrMetaData << directories[i] << endl;
    }

    wrMetaData << Ndirectories << endl; // write number of directories as last entry for ease of readout later


    auto end_saving_meta = chrono::steady_clock::now();
    double time_saving_meta = chrono::duration_cast<chrono::milliseconds>(end_saving_meta - start_saving_meta).count();
    std::cout << "Time for saving Metadata was " << time_saving_meta <<"ms" << endl;
    wrMetaData.close();
}

void Microscope::SaveMotionDataDirs(const std::string& dirsFilePaths, std::string* directories, int Ndirectories) {
    std::cout << "Start saving Metadata" << endl;

    string fileName = "dataDirsDIC.dat";
    string fullPath;

    if (dirsFilePaths == "") {
        fullPath = fileName;
    }
    else {
        fullPath = dirsFilePaths + "\\" + fileName;
    }

    ofstream wrMetaData;
    wrMetaData.open(fullPath, std::fstream::out | std::fstream::app);

    auto start_saving_meta_dirs = chrono::steady_clock::now();


    for (int i = 0; i < Ndirectories; i++)
    {
        wrMetaData << directories[i] << endl;
    }

    auto end_saving_meta_dirs = chrono::steady_clock::now();
    double time_saving_meta_dirs = chrono::duration_cast<chrono::milliseconds>(end_saving_meta_dirs - start_saving_meta_dirs).count();
    std::cout << "Time for saving Metadata was " << time_saving_meta_dirs << "ms" << endl;
    wrMetaData.close();
}
// mode == false => DIC, mode == true => VIB
void Microscope::SaveMotionDataDirs(const std::string& dirsFilePaths, std::string* directories, int Ndirectories, bool mode) {
    std::cout << "Start saving Metadata" << endl;

    string fullPath;
    string fileName = "";

    if (mode == false)
        fileName = (string)"dataDirsDIC.dat";
    if (mode == true)
        fileName = (string)"dataDirsVIB.dat";


    if (dirsFilePaths == "") {
        fullPath = fileName;
    }
    else {
        fullPath = dirsFilePaths + "\\" + fileName;
    }

    ofstream wrMetaData;
    wrMetaData.open(fullPath, std::fstream::out | std::fstream::app);

    //auto start_saving_meta_dirs = chrono::steady_clock::now();

    for (int i = 0; i < Ndirectories; i++)
    {
        if(mode == false)
            wrMetaData << directories[i] + "Hz_dic.dat" << endl;
        if(mode == true)
            wrMetaData << directories[i] + "Hz_vib.txt" << endl;
    }

    //auto end_saving_meta_dirs = chrono::steady_clock::now();
    //double time_saving_meta_dirs = chrono::duration_cast<chrono::milliseconds>(end_saving_meta_dirs - start_saving_meta_dirs).count();
    //std::cout << "Time for saving Metadata was " << time_saving_meta_dirs << "ms" << endl;
    wrMetaData.close();
}

void Microscope::ClearDatFile(const std::string& pathAndFileName) {
    ofstream fileToClear;
    fileToClear.open(pathAndFileName, std::fstream::out | std::fstream::trunc);
    fileToClear.close();
}


bool Microscope::readMeasurementConfigFile(std::string confFile, DynMeasParameters& mesParams) {
    string line;
    ifstream readConf;
    readConf.open(confFile);
    if (!readConf.is_open()) {
        std::cout << "ERROR OPENING FILE" << endl;
        return 1;
    }
    // count lines i.e. number of markers
    int lineN = 0;
    while (std::getline(readConf, line) and readConf.good())
        ++lineN;

    // reset filestream after counting lines
    readConf.clear();
    readConf.seekg(0);

    string* lines = new string[lineN];

    for (int i = 0; i < lineN; i++)
        std::getline(readConf, lines[i]);

    readConf.close();

    mesParams.s_dynMeasSeriesPath = lines[0].substr(3);
    mesParams.s_clearDirs         = stoi(lines[1].substr(3));
    mesParams.s_saveMeta          = stoi(lines[2].substr(3));
    mesParams.s_saveAsDat         = stoi(lines[3].substr(3));
    mesParams.s_linewise          = stoi(lines[4].substr(3));
    mesParams.s_fftFiltering      = stoi(lines[5].substr(3));
    mesParams.s_apdGain           = stoi(lines[6].substr(3));
    mesParams.s_pixelsX           = stoi(lines[7].substr(3));
    mesParams.s_pixelsY           = stoi(lines[8].substr(3));
    mesParams.s_FOV               = stof(lines[9].substr(3));
    mesParams.s_NsampPx           = stoi(lines[10].substr(3)); // samples per pixel to be used for saving.. 
    mesParams.s_Vpp               = stod(lines[11].substr(3));
    mesParams.s_startFreq         = stof(lines[12].substr(3));
    mesParams.s_freqStep          = stof(lines[13].substr(3));
    mesParams.s_NfreqSteps        = stoi(lines[14].substr(3));
    mesParams.s_vibrSamples       = stoi(lines[15].substr(3)); // 65536 = 2^16 @ 500MSps <-> 1024 = 2^10 @ 10MSps
    mesParams.s_vibSampleR        = stoi(lines[16].substr(3)); //Sps
    mesParams.s_trigLvl           = stoi(lines[17].substr(3)); //Trigger levels for the external (analog) trigger to be programmed in mV
    mesParams.s_inptRng           = stoi(lines[18].substr(3));
    mesParams.s_imagSampleR       = stoi(lines[19].substr(3));

    mesParams.s_filled = true;

    delete[] lines;
    return 0;
}

// dynMode1
bool Microscope::DynMeasSample(const std::string& markerLogPath, int firstLayerNumber, bool acquireOverview, bool compensateDrift) {
    
    bool compDrift = compensateDrift;

    if (!dynMeasParams.s_filled) {
        std::cout << "No measurement parameters loaded!\n Load parameters first!\n";
        std::cout << "Aborting procedure!\n";
        return 1;
    }
    if (!PiezoDriftCorr.s_filled and compDrift) {
        std::cout << "No drift compensation parameters loaded!\n Load parameters first!\n";
        std::cout << "Aborting procedure!\n";
        return 1;
    }

    std::string dynMeasSeriesPath = dynMeasParams.s_dynMeasSeriesPath;
    
    // set parameters for the measurement // DYNPARSET 1
    bool clearDirs      = dynMeasParams.s_clearDirs;
    bool saveMeta       = dynMeasParams.s_saveMeta;
    bool saveAsDat      = dynMeasParams.s_saveAsDat;
    bool linewise       = dynMeasParams.s_linewise;
    bool fftFiltering   = dynMeasParams.s_fftFiltering;
    int  px             = dynMeasParams.s_pixelsX; // so far pixelsX = pixelsY since only quadratic FOVs are supported
    double FOV          = dynMeasParams.s_FOV;
    size_t NsampPx      = dynMeasParams.s_NsampPx; // samples per pixel to be used for saving.. 
    size_t samples      = NsampPx;

    int gainBuff        = microscopeAPDM;

    float Vpp           = dynMeasParams.s_Vpp;
    float startFreq     = dynMeasParams.s_startFreq;
    float freqStep      = dynMeasParams.s_freqStep;
    int NfreqSteps      = dynMeasParams.s_NfreqSteps;
    double endFreq      = startFreq + freqStep * (double)NfreqSteps;

    size_t vibrSamples  = dynMeasParams.s_vibrSamples; // 65536 = 2^16 @ 500MSps <-> 1024 = 2^10 @ 10MSps
    size_t vibSampleR   = dynMeasParams.s_vibSampleR;  //Sps

    int dicSampleR = dynMeasParams.s_imagSampleR; //KSps

    int trigLvl = dynMeasParams.s_trigLvl; //Trigger levels for the external (analog) trigger to be programmed in mV
    int inptRng = dynMeasParams.s_inptRng;

    if (compDrift and NfreqSteps + 1 != PiezoDriftCorr.Nmarkers) {
        std::cout << "Number of frequencies does not equal number of frequency shifts! \n";
        std::cout << "Check if piezo drift compensation data is up to date!\n";
        std::cout << "Aborting procedure!\n";
        return 1; 
    }

    // Set FOV and Resolution to values from config file
    AskParameters(FOV, px);

    // Set APD gain
    APD0.SetGain(dynMeasParams.s_apdGain, &microscopeAPDM);

    // clear directories so new ones are not appended to possible old file from previous measurement
    if (clearDirs == true) {
        ClearDatFile(dynMeasSeriesPath + "\\" + "dataDirsDIC.dat");
        ClearDatFile(dynMeasSeriesPath + "\\" + "dataDirsVIB.dat");
    }

    excitAmp = Vpp; // copy to microscope attribute
    if (saveMeta == true) {
        SaveMotionMetaData(dynMeasSeriesPath, samples, linewise, dynModeSelect, vibrSamples, 1000 * 1000 * vibSampleR);
    }

    string fileName  = "MarkerLog.dat";
    //string fileName2 = "driftCalib.dat";
    // gets MarkerLog.dat from correct location
    if (markerLogPath != "")
        ReadMarkerCoords(markerLogPath + "\\" + fileName, MarkerPosStrct);
    else {
        std::cout << "Default path for MarkerLog.dat used" << endl;
        ReadMarkerCoords(fileName, MarkerPosStrct);
    }

    //copy marker log into directory where measurement data is stored
    CopyDatFile(fileName, dynMeasSeriesPath + "\\" + fileName);

    if (compDrift) {
        //copy drift compensation directory where measurement data is stored
        CopyDatFile("PiezoDriftCalib\\driftCalib.dat", dynMeasSeriesPath + "\\" + "driftCalib.dat");
        CopyDatFile("PiezoDriftCalib\\xdriftMM.dat"  , dynMeasSeriesPath + "\\" + "xdriftMM.dat");
        CopyDatFile("PiezoDriftCalib\\ydriftMM.dat"  , dynMeasSeriesPath + "\\" + "ydriftMM.dat");
    }

    //configure vibrometer
    int16* vibData = new int16[vibrSamples];

    if (vibrSamples > 0) {
        ConfSingleCvibro(vibrSamples, vibSampleR, 'e', SPC_TM_NEG, CHANNEL0, inptRng, trigLvl, true, true);
    }

    int numPositions = MarkerPosStrct.Nmarkers;
    bool cont = true;

    //string folderName = "side_";

    char    layerChars[20];// buffer for sprintf_s to write down current layer number
    int     freqToPath;
    char    frequencyChars[20]; // buffer for sprintf_s to write down current excitation frequency
    string  fullPath;
    WORD*   dataPtr;

    float actualFreq;

    for (int i = 0; i < numPositions; i++){
        string folderName = "Pos_";
        string subFolderName;

        MoveToXYZpos(MarkerPosStrct.xPosMarkers[i], MarkerPosStrct.yPosMarkers[i], MarkerPosStrct.zPosMarkers[i]);

        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        sprintf_s(layerChars, "%02d", i + firstLayerNumber);

        microscopeAPDM = APD0.QuerryCurrentGain(); // Make sure APD gain M is up to date

        folderName = folderName + layerChars + "_" + to_string((int)(startFreq / 1e3));
        folderName = folderName + "kHz_" + to_string((int)(freqStep / 1e3)) + "kHz_" + to_string((int)(endFreq / 1e3)) + "kHz";

        // Create Directory for overview image
        fullPath = dynMeasSeriesPath + "\\" + folderName;
        std::cout << "overview image directory = " << fullPath << endl;
        if (!CreateDirectoryA(fullPath.c_str(), NULL))
            std::cout << "Could not create folder" << endl;
        else
            std::cout << "Created Directory: " << fullPath << endl;
        // Take overview Image
        if(acquireOverview)
            TakeOverviewImage(linewise, 400, 800, layerChars, fullPath);

        // Create Directory for Data i.e. DataDir
        if (!CreateDirectoryA(fullPath.c_str(), NULL))
            std::cout << "Could not create folder" << endl;
        else
            std::cout << "Created Directory: " << fullPath << endl;

        // generate array of file paths as strings and store in array
        string* directories     = new string[2 * (1 + NfreqSteps)];
        string* directoriesVib  = new string[2 * (1 + NfreqSteps)];
        for (int j = 0; j < (1 + NfreqSteps); j++){  
            freqToPath = startFreq + j * freqStep;
            // Build filepath
            sprintf_s(frequencyChars, "%06d", freqToPath);
            directories[j] = fullPath + "\\" + frequencyChars;
        }
        if (saveAsDat == true){
            SaveMotionDataDirs(dynMeasSeriesPath, directories, NfreqSteps + 1, 0); // saves directories for DIC
            SaveMotionDataDirs(dynMeasSeriesPath, directories, NfreqSteps + 1, 1); // saves directories for VIB
        }

        // create function pointer for correct scanning mode
        //WORD* (Microscope:: * ImageFunction)(size_t, bool, size_t*);// for old version
        WORD* (Microscope:: * ImageFunction)(size_t);
        if (linewise)
            ImageFunction = &Microscope::TakeMotionImages_V2;
        else
            ImageFunction = &Microscope::TakeMotionImagesColumn_V2;

        std::cout << "Starting measurement series" << endl;

        //auto start_meas = chrono::steady_clock::now();

        for (int k = 0; k < NfreqSteps + 1; k++) {
            actualFreq = startFreq + k * freqStep; // set frequency
            std::cout << "Current frequency = " << actualFreq << "Hz\n";
            excitFreq = actualFreq; // copy to microscope object attribute for storing in tiff image
            //auto set_freq = chrono::steady_clock::now();
            Fgen.OutPutEnable(1); //enable output on Channel1 
            Fgen.ConfigureSine(excitAmp, 0, excitFreq, 0);

            if (compDrift) {
                std::cout << "Compensating drift..\n";
                std::cout << "shifting x,y by " << PiezoDriftCorr.xPosMarkers[i] * 1000. << "microm, " << PiezoDriftCorr.yPosMarkers[i] * 1000. << "microm\n";
                MoveToXYZpos(
                    xPosition + PiezoDriftCorr.xPosMarkers[k], //  k => index for current frequency
                    yPosition + PiezoDriftCorr.yPosMarkers[k],
                    zPosition + PiezoDriftCorr.zPosMarkers[k]);
                UpdateStagePositions(xPosition, yPosition, zPosition);
            }

            dataPtr = ((*this).*ImageFunction)(NsampPx); // Image is taken at this point // function returns pointer to measurement data in memory

            if (vibrSamples > 0) {
                spcmADC.measureSingleRecord(0, vibData, vibrSamples);
            }

            // Build total filepath
            sprintf_s(frequencyChars, "%06d", (int)actualFreq);
            string actualPath = fullPath + "\\" + frequencyChars + "Hz";

            if (saveAsDat == false) {
                // Create Directory
                if (!CreateDirectoryA(actualPath.c_str(), NULL))
                    std::cout << "Could not create folder" << endl;
                else
                    std::cout << "Created Directory: " << actualPath << endl;
                // Save Data
                SaveMotionDataTiffs(dataPtr, samples, actualPath, linewise);
            }
            else {
                // Save Data
                //SaveMotionDataDats(dataPtr, samples, directories[i], linewise);
                SaveMotionDataDats(
                    dataPtr, 
                    samples, 
                    (directories[k]) + "Hz_dic.dat");
                spcmADC.safeASCIItoDisk(
                    (directories[k]) + "Hz_vib.txt",
                    vibData);
            }
            Fgen.ConfigureSine(excitAmp, 0, startFreq, 0);
            std::cout << endl;

            // Reset ADC board
            //ResetADCboard();  // prevents bugs
        }
        delete[] directories;
        //delete[] directoriesVib;
        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    }

    Fgen.OutPutDisable(1); //disable output on Channel1 
    delete[] vibData;
    return 0;
}

// dynMode2
bool Microscope::DynMeasSample2(const std::string& markerLogPath, int firstLayerNumber, bool acquireOverview, bool compensateDrift) {
    
    bool compDrift = compensateDrift;

    if (!dynMeasParams.s_filled) {
        std::cout << "No measurement parameters loaded!\n Load parameters first!\n";
        std::cout << "Aborting procedure!\n";
        return 1;
    }    
    if (!PiezoDriftCorr.s_filled and compDrift) {
        std::cout << "No drift compensation parameters loaded!\n Load parameters first!\n";
        std::cout << "Aborting procedure!\n";
        return 1;
    }

    std::string dynMeasSeriesPath = dynMeasParams.s_dynMeasSeriesPath;
    
    // set parameters for the measurement // DYNPARSET 2
    bool clearDirs      = dynMeasParams.s_clearDirs;
    bool saveMeta       = dynMeasParams.s_saveMeta;
    bool saveAsDat      = dynMeasParams.s_saveAsDat;
    bool linewise       = dynMeasParams.s_linewise;
    bool fftFiltering   = dynMeasParams.s_fftFiltering;
    int  px             = dynMeasParams.s_pixelsX; // so far pixelsX = pixelsY since only quadratic FOVs are supported
    double FOV          = dynMeasParams.s_FOV;
    size_t NsampPx      = dynMeasParams.s_NsampPx; // samples per pixel to be used for saving.. 
    size_t samples      = NsampPx; 

    int gainBuff        = microscopeAPDM;

    float Vpp           = dynMeasParams.s_Vpp;
    float startFreq     = dynMeasParams.s_startFreq;
    float freqStep      = dynMeasParams.s_freqStep;
    int NfreqSteps      = dynMeasParams.s_NfreqSteps;
    double endFreq      = startFreq + freqStep * (double)NfreqSteps;

    size_t vibrSamples  = dynMeasParams.s_vibrSamples; // 65536 = 2^16 @ 500MSps <-> 1024 = 2^10 @ 10MSps
    size_t vibSampleR   = dynMeasParams.s_vibSampleR; //Sps

    int trigLvl = dynMeasParams.s_trigLvl; //Trigger levels for the external (analog) trigger to be programmed in mV
    int inptRng = dynMeasParams.s_inptRng;

  
    if (compDrift and NfreqSteps + 1 != PiezoDriftCorr.Nmarkers) {
        std::cout << "Number of frequencies does not equal number of frequency shifts! \n";
        std::cout << "Check if piezo drift compensation data is up to date!\n";
        std::cout << "Aborting procedure!\n";
        return 1; 
    }

    // Set FOV and Resolution to values from config file
    AskParameters(FOV, px);

    // Set APD gain
    APD0.SetGain(dynMeasParams.s_apdGain, &microscopeAPDM);


    //configure vibrometer
    int16* vibData = new int16[vibrSamples];

    if (vibrSamples > 0) {
        ConfSingleCvibro(vibrSamples, vibSampleR, 'e', SPC_TM_NEG, CHANNEL0, inptRng, trigLvl, true, true);
    }
     
    WORD* dataPtr;

    excitAmp = Vpp; // copy to microscope attribute
    SaveMotionMetaData(dynMeasSeriesPath, samples, linewise, dynModeSelect, vibrSamples, 1000*1000*vibSampleR);

    string markerFileName = "MarkerLog.dat";
    // gets MarkerLog.dat from correct location
    if (markerLogPath != "")
        ReadMarkerCoords(markerLogPath + "\\" + markerFileName, MarkerPosStrct);
    else {
        std::cout << "Default path for MarkerLog.dat used" << endl;
        ReadMarkerCoords(markerFileName, MarkerPosStrct);
    }

    //copy marker log into directory where measurement data is stored
    CopyDatFile(markerFileName, dynMeasSeriesPath + "\\" + markerFileName);

    if (compDrift) {
        //copy drift compensation directory where measurement data is stored
        CopyDatFile("PiezoDriftCalib\\driftCalib.dat", dynMeasSeriesPath + "\\" + "driftCalib.dat");
        CopyDatFile("PiezoDriftCalib\\xdriftMM.dat", dynMeasSeriesPath + "\\" + "xdriftMM.dat");
        CopyDatFile("PiezoDriftCalib\\ydriftMM.dat", dynMeasSeriesPath + "\\" + "ydriftMM.dat");
    }

    int     numPositions = MarkerPosStrct.Nmarkers;
    bool    cont = true;
    int     Ndirs;
    Ndirs = numPositions * (NfreqSteps + 1);

    //////////////////////////////////////////////////////////////////////////////////////////////////
    // generate directories
    char layerChars[20];// buffer for sprintf_s to write down current layer number
    char frequencyChars[20]; // buffer for sprintf_s to write down current excitation frequency

    string* overViewImagPaths = new string[numPositions]; // array that stores paths for overview images
    string* fullPaths  = new string[Ndirs]; // array that stores the complete file path to where each <<XXXXXHz.dat>> file will be safed and from which the dataDirs.dat will be constructed

    //string folderName = "side_";
    string folderName = "Pos_";
    string sampleMarkerPosPath;
    std::cout << "numPositions = " << numPositions << endl;
    for (int i = 0; i < numPositions; i++)
    {
        sprintf_s(layerChars, "%02d", (i+firstLayerNumber));
        sampleMarkerPosPath = dynMeasSeriesPath + "\\" +folderName + layerChars + "_" + to_string((int)(startFreq / 1e3)) + "kHz_" + to_string((int)(freqStep / 1e3)) + "kHz_" + to_string((int)(endFreq / 1e3)) + "kHz";
        overViewImagPaths[i] = sampleMarkerPosPath;
        // Create Directory for overview image
        std::cout << "overview image directory = " << sampleMarkerPosPath << endl;
        if (!CreateDirectoryA(sampleMarkerPosPath.c_str(), NULL))
            std::cout << "Could not create folder" << endl;
        else
            std::cout << "Created Directory: " << sampleMarkerPosPath << endl;
        std::cout << sampleMarkerPosPath << endl;
        for (int j = 0; j < (NfreqSteps+1); j++)
        {
            sprintf_s(frequencyChars, "%06d", int(startFreq + j * freqStep)); // convert freq value to zero padded char array
            //fullPaths[i * (NfreqSteps+1) + j] = sampleMarkerPosPath +  "\\" + frequencyChars + "Hz.dat";
            fullPaths[i * (NfreqSteps+1) + j] = sampleMarkerPosPath +  "\\" + frequencyChars;
        }
    }
   
    // clear directories so new ones are not appended to possible old file from previous measurement
    if (clearDirs == true) {
        ClearDatFile(dynMeasSeriesPath + "\\" + "dataDirsDIC.dat");
        ClearDatFile(dynMeasSeriesPath + "\\" + "dataDirsVIB.dat");
    }

    // Save directories
    if (saveAsDat == true) {
        SaveMotionDataDirs(dynMeasSeriesPath, fullPaths, Ndirs, 0); // saves directories for DIC
        SaveMotionDataDirs(dynMeasSeriesPath, fullPaths, Ndirs, 1); // saves directories for VIB
    }

    //////////////////////////////////////////////////////////////////////////////////////////////////
    // acquire overview of sample
    std::cout << endl << endl << "Starting overview imaging " << endl;
    if (acquireOverview == true) {
        for (int i = 0; i < numPositions; i++)        {
            sprintf_s(layerChars, "%02d", (i + firstLayerNumber));
            MoveToXYZpos(MarkerPosStrct.xPosMarkers[i], MarkerPosStrct.yPosMarkers[i], MarkerPosStrct.zPosMarkers[i]);
            TakeOverviewImage(linewise, 400, 800, layerChars, overViewImagPaths[i]);
            std::cout << "\r acquiring overview " << i + 1 << "/" << numPositions << endl;
        }
        // move back to first marker position
        MoveToXYZpos(MarkerPosStrct.xPosMarkers[0], MarkerPosStrct.yPosMarkers[0], MarkerPosStrct.zPosMarkers[0]);
    }
    std::cout << endl << endl << "Overview imaging finished" << endl;

    //////////////////////////////////////////////////////////////////////////////////////////////////
    // perform dynamic measurement

    // create function pointer for correct scanning mode
    WORD* (Microscope:: * ImageFunction)(size_t);
    if (linewise)
        ImageFunction = &Microscope::TakeMotionImages_V2;
    else
        ImageFunction = &Microscope::TakeMotionImagesColumn_V2;

    float actualFreq;
    for (int i = 0; i < (NfreqSteps+1); i++)
    {
        actualFreq = startFreq + i * freqStep; // set frequency
        std::cout << "Current frequency = " << actualFreq << "Hz\n";
        excitFreq = actualFreq; // copy to microscope object attribute for storing in tiff image
        Fgen.OutPutEnable(1); //enable output on Channel1 
        Fgen.ConfigureSine(Vpp, 0, excitFreq, 0);
        std::this_thread::sleep_for(100ms); // wait for 1s for piezo actuation to settle

        for (int j = 0; j < numPositions; j++) {
            //if (j == numPositions-1) {
            //    APD0.SetGain(80 ,&microscopeAPDM);
            //}
            //else if( j == numPositions-2) {
            //    APD0.SetGain(60, &microscopeAPDM);
            //}            
            //else if( j == numPositions-3) {
            //    APD0.SetGain(30, &microscopeAPDM);
            //}
            //else {
            //    APD0.SetGain(gainBuff, &microscopeAPDM);
            //}

            if (compDrift) {
                std::cout << "Compensating drift..\n";
                std::cout << "shifting x,y by " << PiezoDriftCorr.xPosMarkers[i] * 1000. << "microm, " << PiezoDriftCorr.yPosMarkers[i] * 1000. << "microm\n";

                MoveToXYZpos(  // j => index for marker number, i => index for current frequency
                    MarkerPosStrct.xPosMarkers[j] + PiezoDriftCorr.xPosMarkers[i], 
                    MarkerPosStrct.yPosMarkers[j] + PiezoDriftCorr.yPosMarkers[i],
                    MarkerPosStrct.zPosMarkers[j] + PiezoDriftCorr.zPosMarkers[i]);
            }
            else {
                MoveToXYZpos(
                    MarkerPosStrct.xPosMarkers[j],
                    MarkerPosStrct.yPosMarkers[j],
                    MarkerPosStrct.zPosMarkers[j]);
            }
            std::cout << endl;
            
            dataPtr = ((*this).*ImageFunction)(samples); // Image is taken at this point // function returns pointer to measurement data in memory
            //cout << "fullPaths[i*(NfreqSteps+1)+j] = " << fullPaths[j * (NfreqSteps + 1) + i] << endl;
            //APD0.SetGain(gainBuff, &microscopeAPDM);

            if (vibrSamples > 0) {
                spcmADC.measureSingleRecord(0, vibData, vibrSamples);
            }
            if (saveAsDat == false) {  // we never used this so I never implemented it..
                std::cout << "Not implemented, sorry you will get nothing here\n";
            }
            else {                // Save Data
                SaveMotionDataDats(
                    dataPtr, 
                    samples, 
                    (fullPaths[j*(NfreqSteps+1)+i])+"Hz_dic.dat");
                spcmADC.safeASCIItoDisk(
                    (fullPaths[j * (NfreqSteps + 1) + i] + "Hz_vib.txt"),
                    vibData);
            }
            std::cout << endl;
        }
    }

    delete[] overViewImagPaths;
    delete[] fullPaths;
    delete[] vibData;

    Fgen.OutPutDisable(1); //disable output on Channel1 

    return 0;
}

WORD const Microscope::maxImage()
{
    WORD max = 0;
    size_t num = pixels * pixels;
    for (size_t i = 0; i < num; i++)
        if (picture[i] > max)
            max = picture[i];
    return max;
}
// Checks if Image saturates. Return 1, if maximum pixel value >= "ImagSatThres", otherwise 0  FUNCTION OVERLOADED, SEE BELOW
bool const Microscope::checkImageSat() {
    WORD maxInt = maxImage();
    if (maxInt >= ImagSatThresh) {
        std::cout << "Image saturated!" << endl;
        return 1;
    }
    else {
        std::cout << "Maximum pixel value is = " << maxInt << endl;
        //std::cout << "Voltage =  " << (maxInt - 32768) * 2000 / 32768 << " mV" << endl;
        return 0;
    }
}
// Checks if "satPixelPercentage * pixel * pixel" number of pixels are saturated. Returns 1 if number is exceeded, otherwise 0
bool const Microscope::checkImageSat(float satPixelPercentage) {
    int count = 0;
    int satPixelN = int(satPixelPercentage / 100 * pixels * pixels + 0.5) + 1; // +1 so value is >= 1, important for low res pics. (could go with only > instead in the following loops, but more intuitive this way)
    size_t num = pixels * pixels;
    WORD currentPx;
    bool onlyPosVolt = true; //Check for APD voltage overswing
    for (size_t i = 0; i < num; i++) {
        currentPx = picture[i];
        if (currentPx > ImagSatThresh)
            count++;
        if (currentPx < 29491 and onlyPosVolt) {
            onlyPosVolt = false;
            std::cout << "!!Warning!! Negative voltage overswing detected !!Warning!!" << endl;
        }
        if (count > satPixelN) {
            std::cout << "Image saturated! More than " << satPixelPercentage << " % pixels saturated." << endl;
            return 1;
        }
    }
    //std::cout << "Number of saturated pixels is " << count << endl;
    return 0;
}
bool const Microscope::checkImageSatPxNo() {
    int count = 0;
    size_t num = pixels * pixels;
    WORD maxInt = maxImage();
    WORD currentPx;
    bool onlyPosVolt = true; //Check for APD voltage overswing
    for (size_t i = 0; i < num; i++) {
        currentPx = picture[i];
        if (currentPx > ImagSatThresh)
            count++;
        if (currentPx < 29491 and onlyPosVolt){
            onlyPosVolt = false;
            std::cout << "!!Warning!! Negative voltage overswing detected !!Warning!!" << endl;
        }
    }
    float percentage = 100.* (1. * count) / (num * 1.); 
    //std::cout << "Pixels saturated " << (count) << endl;
    std::cout << "Saturation percentage = " << percentage << endl;
    std::cout << "Maximum pixel value is = " << maxInt << endl;
    return 0;
}

WORD const Microscope::getImageSatThresh() {
    return ImagSatThresh;
}

WORD const * Microscope::getPicture() {
    WORD const* p = picture;
    return p;
}

////////////////////////////////////////////////////////////////////
// --------------------- private functions -------------------------
////////////////////////////////////////////////////////////////////

double Microscope::max(double a, double b) // maximum between a and b
{
    return(a < b ? b : a);
}
double Microscope::min(double a, double b) // minimum between a and b
{
    return(a < b ? a : b);
}
void Microscope::FTfilter(fftw_complex* in, fftw_complex* out, int n, fftw_plan pFW, fftw_plan pBW, double samplerate, double fL, double fH)
{
    fftw_execute(pFW);
    int N = int(double(n - 1) / 2); // = n/2 - 1 if n is even and (n-1)/2 if n is odd

    // Start cutting unwanted frequencies, but keep DC Peak
    double end1 = min(n / samplerate * fL, N + 0.5);
    for (int i = 1; i < end1; i++) // cutting low positive freqs, start from 1 to keep DC peak
    {
        out[i][0] = 0;
        out[i][1] = 0;
    }
    double start2 = max(1, n / samplerate * fH); // if fH < 0 this will evade access violation
    double end2 = min(n * (1 - fH / samplerate), n);
    for (int i = int(start2); i < int(end2); i++) // cutting all high frequencies (positive and negative)
        // note that if fH is too high this loop will automatically be skipped because int(n / samplerate * fH) > n * (1 - fH / samplerate)
    {
        out[i][0] = 0;
        out[i][1] = 0;
    }
    double start3 = max(n * (1 - fL / samplerate), N);
    for (int i = int(start3); i < n; i++) // cutting low negative freqs
    {
        out[i][0] = 0;
        out[i][1] = 0;
    }

    fftw_execute(pBW);
    for (int i = 0; i < n; i++)
    {
        in[i][0] /= n;
        in[i][1] /= n;
    }
}
bool Microscope::SeekGainSimple(int const* maxIter, float const* imSatPxPerc, void(Microscope::* locImagFctPtr)()) {
    bool retakeImage = true; // flag to decide if image should be taken again
    bool gainFailed = false; // flag to stop taking images if setting a new gain value failed
    int count = 0;


    while (retakeImage and (count < *maxIter) and !gainFailed) {
        ((*this).*locImagFctPtr)(); // image is taken at this point
        if (checkImageSat(*imSatPxPerc)) {
            int newGain = APD0.QuerryCurrentGain();
            std::cout << " => adjusting gain." << endl;
            if (newGain > 5) {  // if gain is larger than minimum gain, then reduce gain
                newGain = newGain - 5;
            }
            if (APD0.SetGain(newGain, &microscopeAPDM) == 0) { // set new gain and check for errors
                std::cout << "gain adjust failed." << endl;
                return 0;  // with this, no more images are taken.
            }
            count++;
            if (count >= *maxIter) {// debugging
                std::cout << "count limit of " << count << " reached" << endl;
            }
        }
        else {
            retakeImage = false;
            count = 0;
        };
    };
    return 1;
};
bool Microscope::SaveAndShowTempImage(const char* fname) {
    WORD* currentPic;
    currentPic = new WORD[pixels * pixels];
    for (int i = 0; i < pixels*pixels; i++)
    {
        *(currentPic + i) = *(picture + i);//copy data to <<pictureCopy>> for the memes use pointer arithmetic
    }

    CorrectZeroValue(currentPic); // make preview look pretty
    for (int i = 0; i < pixels * pixels; i++) // double value to compensate loss of dynamic range
        currentPic[i] *= 2;

    SaveAsTiff(currentPic, fname); // Saves the (manipulated) copy of the current "picture" in the temp.tiff file or whatever is given via <<fname>>. Thereby I avoid altering the data unintentionally
    cv::namedWindow("Window Name", cv::WINDOW_AUTOSIZE); // does nothing if window with same name already exists
    ifstream input(fname);
    if (!input)
    {
        std::cout << "Oops! file does not exist...\n";
    }
    //myImage = imread(filename, IMREAD_ANYDEPTH); // Read the file  
    cv::Mat myImage = cv::Mat::zeros(pixels, pixels, CV_16UC1);
    myImage = cv::imread(fname, cv::IMREAD_COLOR); // Read the file for live preview
    input.close(); //close file
    imshow("Window Name", myImage);
    cv::waitKey(1); // Necessary for event handling or sth. idk ask stackoverflow
    delete[] currentPic;
    return 1;
}
bool Microscope::SeekGainDivideAndConquer(int const* maxIter, float const* imSatPxPerc, bool fastMode) {
    int count = 0;
    int countGainStepZero = 0;
    bool retakeImage = true;
    bool tooHigh = true;
    int gainStep;
    int maxGain = 400 - 5;

    // when this function is used, the picture is already saturating
    int previousGain = 0;
    int currentGain = int(APD0.QuerryCurrentGain(false) - 5);
    int lowerLim = 0; // gain range is from 5 to 400 so implement Divide and Conquer in this range
    int upperLim = currentGain; // when this function is used, the picture is already saturating
    int newGain;

    while (retakeImage and count < *maxIter) {

        count++;
        std::cout << endl << endl << endl << "Gain optimization iteration number " << count << endl << endl;

        // Determine new gain value
        if (tooHigh) {
            gainStep = int(abs(lowerLim - currentGain) / 2. + 0.5);
            if (gainStep < 1)
                countGainStepZero++; // increase counter 
            newGain = currentGain - gainStep; // decrease gain
            std::cout << "Decreasing gain.." << endl;
        }
        if (!tooHigh) {
            gainStep = int(abs(upperLim - currentGain) / 2. + 0.5);
            if (gainStep < 1) {
                gainStep++; // artificially increase so gain is lowered again by 1
                countGainStepZero++; // increase counter nonetheless
            }
            newGain = currentGain + gainStep; // increase gain
            std::cout << "Increasing gain.." << endl;
        }

        std::cout << "Gain step is = " << gainStep << endl;

        // Set new gain value
        if (APD0.SetGain(newGain + 5, &microscopeAPDM) == 0) { // set new gain and check for errors
            std::cout << "Gain adjust failed." << endl;
            return 0;  // with this, no more images are taken.
        }
        // Update gain value history
        previousGain = currentGain;
        currentGain = newGain;

        // Take picture with this new gain value
        std::cout << "Retaking picture.." << endl << endl;


        //((*this).*locImagFctPtr)(); // image is taken at this point
        if (fastMode)
            TakeImageRows();
        else
            TakeImageSinglePixel();

        SaveAndShowTempImage("temp.tiff");


        // Check for image saturation
        if (checkImageSat(*imSatPxPerc)) {
            tooHigh = true;
            upperLim = currentGain - 1;
        }
        else {
            tooHigh = false;
            lowerLim = currentGain + 1;
        }

        if (upperLim-lowerLim <= 1) { // make sure last image did not show saturation // abort if gainStep == 0 for the e.g. third or second time
            retakeImage = false;
            // Set new gain value
            if (APD0.SetGain(lowerLim + 5, &microscopeAPDM) == 0) { // set new gain and check for errors
                std::cout << "Gain adjust failed." << endl;
                return 0;  // with this, no more images are taken.
            }
            // Take picture with this new final gain value
            std::cout << "Retaking picture.." << endl << endl;
            //((*this).*locImagFctPtr)(); // image is taken at this point
            if (fastMode)
                TakeImageRows();
            else
                TakeImageSinglePixel();
            SaveAndShowTempImage("temp.tiff");
        }

        //std::cout << endl << "*******************" << endl;
        //std::cout << "retakeImage = " << retakeImage << endl;
        //std::cout << "upper Limit = " << upperLim << endl;
        //std::cout << "lower Limit = " << lowerLim << endl;
        //std::cout << "previousGain = " << previousGain << endl;
        //std::cout << "currentGain = " << currentGain << endl;
        //std::cout << "newGain = " << newGain << endl;
        //std::cout << "gainStep = " << gainStep << endl;
        //std::cout << "tooHigh = " << tooHigh << endl;
        //std::cout << "countGainStepZero = " << countGainStepZero << endl;
        //std::cout << endl << "*******************" << endl;

    }
    return 1;
}

// https://www.tutorialspoint.com/how-to-check-if-an-input-is-an-integer-using-c-cplusplus
bool Microscope::isNumeric(string str) {
    for (int i = 0; i < str.length(); i++)
        if (isdigit(str[i]) == false)
            return false; //when one non numeric value is found, return false
    return true;
}