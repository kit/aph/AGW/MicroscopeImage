// configFileReader.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>
#include <string>
#include <chrono> 
#include <fstream>
//#include <conio.h>
//#include <memory>
//#include <Windows.h>

#include "DataStructs.h"

using namespace std;

bool readMeasurementConfigFile(string filename, DynMeasParameters& mesPars, int NreadLines = 19) {
    string line;
    ifstream readMeta;
    readMeta.open(filename);
    if (!readMeta.is_open()) {
        std::cout << "ERROR OPENING FILE" << endl;
        return 1;
    }
    // count lines i.e. number of markers
    int lineN = 0;
    while (std::getline(readMeta, line) and readMeta.good())
    {
        ++lineN;
    }
    // reset filestream after counting lines
    readMeta.clear();
    readMeta.seekg(0);

    string* lines = new string[lineN];

    for (int i = 0; i < lineN; i++)
    {
        //std::getline(readMeta, line);
        std::getline(readMeta, lines[i]);
        //lines[i] = line;
        if (i < NreadLines)
            cout << "line " << i << " = " << lines[i].substr(3) << endl;
    }

    readMeta.close();

    mesPars.s_dynMeasSeriesPath = lines[0].substr(3);
    mesPars.s_clearDirs         = stoi(lines[1].substr(3));
    mesPars.s_saveMeta          = stoi(lines[2].substr(3));
    mesPars.s_saveAsDat         = stoi(lines[3].substr(3));
    mesPars.s_linewise          = stoi(lines[4].substr(3));
    mesPars.s_fftFiltering      = stoi(lines[5].substr(3));
    mesPars.s_apdGain           = stoi(lines[6].substr(3));
    mesPars.s_pixelsX           = stoi(lines[7].substr(3));
    mesPars.s_pixelsY           = stoi(lines[8].substr(3));
    mesPars.s_FOV               = stof(lines[9].substr(3));
    mesPars.s_NsampPx           = stoi(lines[10].substr(3)); // samples per pixel to be used for saving.. 
    mesPars.s_Vpp               = stod(lines[11].substr(3));
    mesPars.s_startFreq         = stof(lines[12].substr(3));
    mesPars.s_freqStep          = stof(lines[13].substr(3));
    mesPars.s_NfreqSteps        = stoi(lines[14].substr(3));
    mesPars.s_vibrSamples       = stoi(lines[15].substr(3)); // 65536 = 2^16 @ 500MSps <-> 1024 = 2^10 @ 10MSps
    mesPars.s_vibSampleR        = stoi(lines[16].substr(3)); //Sps
    mesPars.s_trigLvl           = stoi(lines[17].substr(3)); //Trigger levels for the external (analog) trigger to be programmed in mV
    mesPars.s_inptRng           = stoi(lines[18].substr(3));

    mesPars.s_filled = true;

    delete[] lines;
    return 0;
}

bool readHardwareConfigFile(string filename, MicroscopeHardware& hwPars, int NreadLines = 13) {
    string line;
    ifstream readMeta;
    readMeta.open(filename);
    if (!readMeta.is_open()) {
        std::cout << "ERROR OPENING FILE" << endl;
        return 1;
    }
    // count lines i.e. number of markers
    int lineN = 0;
    while (std::getline(readMeta, line) and readMeta.good())
    {
        ++lineN;
    }
    // reset filestream after counting lines
    readMeta.clear();
    readMeta.seekg(0);

    string* lines = new string[lineN];

    for (int i = 0; i < lineN; i++)
    {
        //std::getline(readMeta, line);
        std::getline(readMeta, lines[i]);
        //lines[i] = line;
        if (i < NreadLines)
            cout << "line " << i << " = " << lines[i].substr(3) << endl;
    }

    readMeta.close();

    hwPars.s_objLens    = stoi( lines[0].substr(3));
    hwPars.s_jump_speed = stoi( lines[1].substr(3));
    hwPars.s_mark_speed = stoi( lines[2].substr(3));
    hwPars.s_FG_VISA    =       lines[3].substr(3);
    hwPars.s_Vpp_thresh = stof( lines[4].substr(3));

    hwPars.s_xStageAddr =       lines[5].substr(3);
    hwPars.s_yStageAddr =       lines[6].substr(3);
    hwPars.s_zStageAddr =       lines[7].substr(3);

    hwPars.s_xAxis[0] =         lines[8].substr(3)[0];
    hwPars.s_yAxis[0] =         lines[9].substr(3)[0];
    hwPars.s_zAxis[0] =         lines[10].substr(3)[0];

    hwPars.s_lasWavel       =  stof(  lines[11].substr(3));
    hwPars.s_gateT          =  stoi(  lines[12].substr(3));
    hwPars.s_gateCOMport    = lines[13].substr(3);
    hwPars.s_gateCOMbaud    = stoi(lines[14].substr(3));

    hwPars.s_filled = true;

    delete[] lines;
    return 0;
}

int main()
{
    std::cout << "Hello World!\n";

    DynMeasParameters myParameters;
    MicroscopeHardware myHWparams;

    // read data
    string configMeasurementFilename = "_ConfigMeasurement.txt";
    string configHardwareFilename = "_ConfigHardware.txt";

    //readMeasurementConfigFile(configMeasurementFilename, myParameters);
    readHardwareConfigFile(configHardwareFilename, myHWparams);

    //cout << myParameters << std::endl;
    cout << myHWparams << std::endl;

}

