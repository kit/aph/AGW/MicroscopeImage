#pragma once
#define int16 short int
#define _USE_MATH_DEFINES

#include <iostream>;
#include <vector>

class dspUnit
{
public:
	dspUnit();

	dspUnit(
		float smplR,
		float carrF);

	~dspUnit();

	// allocates arrays for LO and results after multiplication operation
	bool set_dataLength(int inLength);
	bool set_wWidth(int wWidth);
	bool set_smplR(float smplR);
	bool set_carrF(float carrF);

	bool do_generateLO(
		std::vector<float>& In,
		std::vector<float>& Qu);
	bool do_generateLO();

	bool do_loadData(
		int inLength,
		int16* inputData);

	bool do_updateData(
		int inLength, 
		int16* inputData);

	bool do_initDSP(
		float smplR,
		float carrF,
		float lambda,
		int inLength,
		//int16 *inputData = NULL ,
		int wWidth = 1);

	bool do_mixSignal(
		std::vector<float>& A,
		std::vector<float>& B,
		std::vector<float>& C);

	bool do_decimate(
		std::vector<float>& initial,
		std::vector<float>& decimated);

	bool do_atan2d(
		std::vector<float>& I,
		std::vector<float>& Q,
		std::vector<float>& result);

	bool do_unwrapVec(std::vector<float>& inoutData);

	bool do_scaleDispData(
		std::vector<float>& inoutData,
		float				scale,
		bool				average);

	bool do_pinToZero(std::vector<float>& inoutData);

	bool do_pinToMiddleVpp(std::vector<float>& inoutData);

	bool do_demodulate(
		std::vector<float>& dataOut,
		bool				subAvg = true);

	bool do_resetAllVectors();
	//bool do_resetVectors();

	bool do_reset(); // resets everything to zero. DSP unit needs to be initialized again!


private:
	float	v_LOfreq, v_LOamp, v_smplR, v_laserLambda, v_phaseScale;
	size_t	v_dataLength, v_wWidth;

	//float* ptr_LOsin		= 0; // ptr to LO sin array
	//float* ptr_LOcos		= 0; // ptr to LO cos array

	//float* ptr_Inph			= 0;
	//float* ptr_Quad			= 0;

	//float* ptr_locData		= 0; // pointer to data allocated outside of this class
	//float* ptr_locDatDem	= 0; // pointer to result data allocated by thsi class

	std::vector<float> vec_LOsin, vec_LOcos, vec_Inph, vec_Quad, vec_data;
	std::vector<float> vec_InphDec, vec_QuadDec, vec_dataDem;

	float	unwrapPhase(float prev_angle, float new_angle);

};

