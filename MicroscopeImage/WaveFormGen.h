#pragma once
#include <Ag3352x.h>
#include <iostream>

class WaveFormGen
{
private:
	ViStatus		error;
	ViSession		vi;
	ViRsrc			RsrcName;
	ViConstString	activeChannel;

	bool CheckErr(ViStatus err);

public:

	WaveFormGen();

	bool InitWFG(const char * VISAaddr);

	bool ResetWFG();
	bool SelectChannel(int selChnl);

	bool OutPutEnable();
	bool OutPutEnable(int chnlNo);

	bool OutPutDisable();
	bool OutPutDisable(int chnlNo);

	bool ConfigureSine(
		double amplitude,
		double offset,
		double freq,
		double phase);

	bool CloseDevice();
};

// Comment

/*
This class uses IVI-C driver for the function generator. For this class to work one needs to install:

1. the Keysight "IOLibSuite_18_1_26209" or similar from https://www.keysight.com/en/pd-1985909/io-libraries-suite?nid=-33002.977662&cc=IN&lc=eng
2. the instrument driver (in this case Ag3352x) from https://www.ivifoundation.org/registered_drivers/driver_registry.aspx

then proceed to the VS project properties and for a 64bit application add:

VC++ Directories -> Include Directories -> $(VXIPNPPATH)WinNT\Include;C:\Program Files\IVI Foundation\IVI\Include
VC++ Directories -> Library Directories -> C:\Program Files\IVI Foundation\IVI\Bin;C:\Program Files\IVI Foundation\IVI\Lib_x64\msc
Linker-> Input -> Additional Dependencies -> Ag3352x.lib

(for 32bit use Program Files (x86) respectively and possibly Lib instead of Lib_x64)
(I think the inclusion of "C:\Program Files\IVI Foundation\IVI\Bin" is unnecessary but well.. if it works, it works..)
Paths may change if other install directories are chosen - obviously.
*/