#pragma once

// For Displaying of images
#include <opencv2/core.hpp>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/highgui.hpp>

#include "GalvoScanner.h"
#include "DigitizerBoard.h"
#include "spcmDigitizer.h" //"spcmDigitizer.h" must to be included AFTER openCV, otherwise, openCV typedef __int64 int64 in "interface.h" collides with #define int64 __int64 of dlltyp.h of spcm-card.. both want to do the same i.e. that long long <-> __int64 <-> int64
#include "plotter.h"
#include "dspUnit.h"
#include "TiffGrey.h"
#include "LinStage.h"

#include "fftw3.h"
#include "APD.h"
#include "mySerialPort.h"
#include "WaveFormGen.h"
#include <string>
#include "DataStructs.h"


class Microscope
{
public:
	Microscope(); // Constructor
	~Microscope(); // Destructor

	bool RunningStatus();

	bool readHardwareConfigFile(
		std::string			confFile,
		MicroscopeHardware& hwParams);

	std::string create_timestamp();

	void SetJumpSpeed(int JS);
	void SetMarkSpeed(int MS);
	void ResetADCboard();	// resets ADC board by calling the same functions of class 
							// "DigitizerBoard" also called in the constructor "Microscope()"..
							// seems to get rid of bugs when switching between different 
							// imaging/measurement modes..

	void AskParameters();
	void AskParameters(
		double newFOV,
		int newPx);

	void AskTask();
	//void TakeImageSinglePixel(); 
	void TakeImageSinglePixel(bool	trig = 0);
	//void TakeImageSinglePixelCol(); 
	void TakeImageSinglePixelCol(bool	trig = 0);

	void TakeOverviewImage(
		bool	linewise,
		double	oV_FOV,
		int		oV_px,
		char*	sideNo,
		std::string path = "");

	void TakeImageRows();  
	void TakeImageColumns();

	void TakeImageMeanZ(
		unsigned int n,
		float dz); // means several images around current z position


	unsigned int GetPixels();
	double GetFOV();

	void SaveAsTiff(
		WORD * pic,
		const char* filename);

	void SaveAsDat(
		WORD* pic,
		const char* filename);

	bool ConfSingleCvibro( // configures single channel vibrometry measurement
		uint64			samples,
		uint64			smplRt,
		//std::string		trigChoice,
		char			trigChoice,
		int				trigEdge,
		int				chnlSel,
		int32			inptR,
		int				trgLvl = 0,
		bool			inpt50vs1M = 1,
		bool			ACvsDC = 1);

	bool ConfSingleCvibro();

	bool RunSingleVibro(  // includes configuration of the ADC card
		int		Nsamples,
		char	trigChoice,
		int		trigEdge,
		bool	timeStamp = false,
		bool	showPlot = false);

	bool RunSingleVibroDemod(  // includes configuration of the ADC card and demodulation
		int		Nsamples,
		char	trigChoice,
		int		trigEdge,
		int     decimate = 1,
		bool	timeStamp = false,
		bool	showPlot = false);

	// Some functions for manual control including image-acquisition / vibrometry acquisition and display of the data
	void LiveModeImag(); // Images and displays continuously until keyboardhit

	void LiveModeImag(
		const bool*		cont, 
		bool			dynamicRange, 
		bool			linewise, 
		bool			crosshair); // Overload to image and display until cont = false 


	void LiveModeVibro(
		bool*		cont, 
		int			Nsamples, 
		double*		locDat,
		int			lChannel = 0,
		bool		conv2Volt = false); // This method will get some parameters for the desired acquisition and plot-parameters

	void LiveModeVibro(
		bool*		cont,
		int			Nsamples,
		int16*		locDat,
		int			lChannel = 0);

	void LiveModeVibroDemod(
		bool*				cont,
		int					Nsamples,
		int16*				locDat,
		std::vector<float>  dspOutVec,// this function is designed to be passed to a thread. hence, I copy the vector instead of passing a reference (also the compiler had an issue with it..)
		int					lChannel = 0);

	bool InteractiveMode(); // menue to examine a sample with imaging and vibrometry to set marker positions for later measurements

	void CorrectBackground();
	void CorrectZeroValue(WORD * pic); // Corrects the fact, that a value of 32768 equals 0V
	void increaseDynamicRange();
	void CalibrationRoutine();

	bool conAllStages();
	bool disconAllStages();

	void SetzPosition(double z);
	void SetxPosition(double x);
	void SetyPosition(double y);

	bool UpdateStagePositions(
		double& x, 
		double& y, 
		double& z); // chose passing by reference here for practice and hopefully ease of reading the code..

	void MovezStage(double zStep);
	void MovexStage(double xStep);
	void MoveyStage(double yStep);

	bool MoveSampleAround(); // allows for movement of the sample and setting marker positions which will be logged in "MarkerLog.dat"

	void MoveToXYZpos(
		double xPos, 
		double yPos, 
		double zPos);

	void CopyDatFile(  // copies marker log file into the <<measurementFilePath>> directory
		std::string originPath, 
		std::string measurementFilePath); 

	//void MoveThroughMarkers(void(Microscope::*fkt)(double)); // function that does a certain action at each xyz stage position given in the "MarkerLog.dat" file
	bool ReadMarkerCoords(
		std::string			filename, 
		MarkerCoordList		&MarkerStruct);

	void MoveThroughMarkers(); // function that does a certain action at each xyz stage position given in the "MarkerLog.dat" file

	bool MoveToMarker();
	
	bool GeneratePiezoDriftCalib(bool validate = false, int delay = 10, bool linewise = 1);
	//bool GeneratePiezoDriftCalib(bool validate = false, int delay = 10);

	void TakeStack(
		double	stepsize, 
		int		n, 
		bool	fastMode);

	void TakeStackGainAdj(
		double	stepsize, 
		int		n, 
		bool	fastMode);

	void TakeAndSaveMotionImages_V2(size_t NpointPerPx); // time in ms 
	void TakeAndSaveMotionImagesColumn_V2(size_t NpointPerPx); // same as above but scans columnwise

	// wrapper for TakeMotionImages() and TakeMotionImagesColumn() so user can switch between the two
	// will also querry for vibrometry mode
	bool DynamicMeasurement(); 

	void TakeAndSaveMotionImagesMeanZ(
		double			time, 
		unsigned int	n, 
		float			dz); // time in ms, ds in um

	void TakeSeveralImages(
		int		number, 
		int		time, 
		bool	fastMode);

	void TakeConsecutiveImages(
		int		number, 
		bool	fastMode, 
		bool	scanDir); // simply takes "number" images consecutively

	void TakeZLineScan(
		const double		distance,
		const unsigned int	samples); // distance in um

	void TakeAxialImage(
		const double	NpxAxial,
		bool			orient); // take a slice along the Z axis with "NpxAxial" pixels while "orient" specifies wether in XZ or YZ

	void determineAPDSaturation(
		int		startgain, 
		int		gainsteps, 
		bool	increase);

	bool SetExcitParam(); // this version querries for parameters on user input

	bool SetExcitParam(  //this version doesn't
		float ampl, 
		float offs, 
		float freq, 
		float phase); 


	WORD* TakeMotionImages_V2(size_t NpointPerPx);
	WORD* TakeMotionImagesColumn_V2(size_t NpointPerPx);

	void SaveMotionDataTiffs(// putting the multi threaded write-data-to-disc method which was previously contained in "TakeMotionImages*Column" in a seperate function 
		WORD*			PixelValues,
		size_t			Nsamples, 
		std::string		folderPath, 
		bool			linOrCol);	

	void SaveMotionDataDats(
		WORD*			PixelValues,
		size_t			Nsamples, 
		std::string		fileName);	//  No Multithreading but not really needed since storing binary data of about 7MB is fast compared to image generation...
		//bool			linOrCol);	//  No Multithreading but not really needed since storing binary data of about 7MB is fast compared to image generation...

	//void SaveMotionMetaData(// stores ONLY the metadata like pixels, FOV, etc.. but no directories
	//	std::string		metaFilePath,
	//	size_t			Nsamples,  
	//	bool			linOrCol, 
	//	bool			dynMode); 

	void SaveMotionMetaData(// stores ONLY the metadata like pixels, FOV, etc.. but no directories
		std::string		metaFilePath,
		size_t			Nsamples,
		bool			linOrCol,
		bool			dynMode, 
		size_t			NsamplesVib		= 0,
		size_t			sampleRvib		= 500);

	void SaveMotionMetaDataAndDirs(
		std::string		metaFilePath,
		std::string*	directories, 
		int				Ndirectories, 
		size_t			Nsamples,  
		bool			linOrCol); // stores metadata AND directories

	void SaveMotionDataDirs( // I should discard this function soon!!
		const std::string&	dirsFilePaths, 
		std::string*		directories, 
		int					Ndirectories); // creates or appends directories to metaDataDir file

	void SaveMotionDataDirs(
		const std::string&	dirsFilePaths,
		std::string*		directories,
		int					Ndirectories,
		bool				mode); //(dic/vib) (0/1)); // creates or appends directories to metaDataDir file

	void ClearDatFile(const std::string& pathAndFileName); // basically opens file with trunc flag to clear content

	bool readMeasurementConfigFile(
		std::string			confFile, 
		DynMeasParameters&  mesParams);

	bool DynMeasSample(  // function that performs dynamic measurement for several frequencies at each position specified in "MarkerLog.dat"
		const std::string&	markerLogPath, 
		int					firstLayerNumber,
		bool				acquireOverview,
		bool				compensateDrift); 

	bool DynMeasSample2(// function that does the same thing but goes through all markers first before switching the frequency
		const std::string&	markerLogPath, 
		int					firstLayerNumber, 
		bool				acquireOverview,
		bool				compensateDrift);

	WORD const maxImage(); // returns the maxmimum value of the current image saved in 'picture'
	bool const checkImageSat(); // Checks if Image saturates. Return 1, if maximum pixel value >= "ImagSatThres", otherwise 0
	bool const checkImageSat(float satPixelPercentage); // Checks if "satPixelPercentage * pixel * pixel" number of pixels are saturated. Returns 1 if number is exceeded, otherwise 0
	bool const checkImageSatPxNo();  // displays percentage of saturated pixels

	WORD const getImageSatThresh();  //returns saturation threshold value 
	WORD const * getPicture();  // returns const pointer to picture so it can not be changed..

private:
	GalvoScanner		Scanner;
	APD					APD0;
	mySerialPort        GateControl;  // Controls length of gate for dynamic measurements which is AND-connected with the F
	WaveFormGen			Fgen;
	DigitizerBoard		Board; //parameters set in member-init-list.. old here with: DigitizerBoard Board = DigitizerBoard(1, 1, SAMPLE_RATE_50MSPS);
	spcmDigitizer		spcmADC;
	plotter				gnuPlotter;
	dspUnit				DSP;
	LinStage			xStage, yStage, zStage;
	CorrectionPattern*	Patterns;
	MarkerCoordList		MarkerPosStrct;     // Stores coordinate list for PI-stages for measurement points on sample
	MarkerCoordList		PiezoDriftCorr;     // Stores coordinate shifts due to Piezoactuator drift for different excitation frequencies
	DynMeasParameters   dynMeasParams;		// Stores dynamic measurement parameters and is filled from config file
	MicroscopeHardware  hwParams;			// Stores hardware parameters and is filled from hw config file

	bool isScanning; // Variable mainly used for LiveMode to check if imaging is currently in progress
	//unsigned int jump_speed;
	//unsigned int mark_speed;

	float lasLamda; // in nanometer


	int				galvoPixelCalib; // converts galvo position encoder into micrometers
	unsigned int	pixels;
	double			FOV;
	double			pixelsizeMetric; // pixelsize in �m/px floating number!!
	double			pixelsizeGalvo; // pixelsize in bitlevel/px.. STILL a floating number!!
	int				microscopeAPDM;

	int			dynSampleRate; // sample rate chosen for dynamic measurement
	float		timeDynPx; // illumination time per pixel
	//std::string dynMeasSeriesPath; // specifies where data for dynamic measurement series will be stored

	bool dynModeSelect;
	
	float excitAmp;
	//float excitAmpThresh;
	float excitFreq;


	// VISA address of function generator
	//char myVISAaddr[100] = { 0 };
	std::string myVISAaddr;


	// Positions of PI piezo-linear stages
	double xPosition;
	double yPosition;
	double zPosition;

	bool allStagesConnected;

	int* gCoords	= NULL; // will be a vector serving als coordinate lists for the galvo scanner
	int* gPosResult	= NULL; // will be a buffer for the galvo position readout
	WORD* picture	= NULL;
	WORD ImagSatThresh;

	CTiffGrey Tiff;

	ALAZAR_CHANNELS			DetectorChannel;
	ALAZAR_TRIGGER_SOURCES	TriggerChannel;
	ALAZAR_INPUT_RANGES		InputRange;

	bool running = true;
	bool optimized = false;

	std::string hardwareConfFileName = "_ConfigHardware.txt";
	std::string measurementConfFileName = "_ConfigMeasurement.txt";

	/////////////////////////////////////////////////////////////////////////////////////////
	// Private functions start here
	/////////////////////////////////////////////////////////////////////////////////////////

	void FTfilter(
		fftw_complex* in, 
		fftw_complex* out, 
		int n, fftw_plan pFW, 
		fftw_plan pBW, 
		double samplerate, 
		double fL, 
		double fH);

	double max(double a, double b); // maximum between a and b
	double min(double a, double b); // minimum between a and b

	bool SaveAndShowTempImage(const char* fname);
	bool SeekGainSimple(
		int const*			maxIter, 
		float const*		imSatPxPerc, 
		void(Microscope::*	locImagFctPtr)());

	bool SeekGainDivideAndConquer(
		int const*			maxIter, 
		float const*		imSatPxPerc, 
		bool 				fastMode); //Number guessing algorithm..

	// https://www.tutorialspoint.com/how-to-check-if-an-input-is-an-integer-using-c-cplusplus
	bool isNumeric(std::string str);

};

