#pragma once
#include <string>
class mySerialPort
{
private:
	//int gain; // Multiplication factor of the APD
	int bdrate;
	int cport_nr;
	std::string modeString;
	//const char* mode;
	int flowcontrol;
	unsigned char buf[128] = { 0 }; // buffer to store answer from apd module after e.g. sending a command



public:
	mySerialPort(); // Constructor
	mySerialPort(
		std::string COMportStr,
		std::string modeStr, // Constructor
		int baudR, 
		int flwCntrl = 0);

	bool    ConfigPort(
		std::string COMportStr,
		std::string modeStr, // Constructor
		int baudR,
		int flwCntrl = 0);

	bool	SendCommand(
		std::string command,
		bool CR		= false,
		bool LF		= false,
		bool silent = false); // sends command specified by "command" and listens for the response
	
	bool	ClearBuffer(); // clears buffer buf variable
};

