clear all;

listing = dir;
filenamelist = [];

% load images
for i = 1:length(listing)% Gehe alle Elemente in Ordner durch
    if length(listing(i).name) > 6 % checke alle Elemente mit mehr als 6 Symbolen im Dateinamen
        if listing(i).name(1:3) == 'img'  & listing(i).name(end-2:end) == 'tif' % identifiziere img_XXXXX.tif Files
      
            filenamelist = [filenamelist; listing(i).name];
            %H?nge Name der Bilddatei an Liste an
            
        end
    end
end

InputRange = 1000; %mV
Npx = 400;
FOV = 300; %mu
pixelsize = FOV/Npx;
Npic = length(filenamelist(:,1));
x = (0:(Npx-1)) * pixelsize + pixelsize/2;
y = (0:(Npx-1)) * pixelsize + pixelsize/2;
%y = fliplr(y);  % important, since imagesc flips y-axis.. 
                % otherwise image appears upside down!


Images = [];
average = zeros(Npx,Npx);
for i = 1:Npic
   Images = cat(3,Images,double(imread(filenamelist(i,:))) * InputRange / 2^15 - InputRange);
   average = average + double(imread(filenamelist(i,:)))* InputRange / 2^15 - InputRange;
end

average = 1/Npic .* average;

figure(1)
colormap('gray')
imagesc(Images(:,:,1))
hc=colorbar;
title(hc,'mV');
pbaspect([1 1 1])

figure(2)
colormap('gray')
imagesc(average)
hc=colorbar;
title(hc,'mV');
pbaspect([1 1 1])







