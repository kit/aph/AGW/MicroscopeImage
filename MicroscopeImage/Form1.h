#pragma once


namespace C10508 {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;
	using namespace System::Diagnostics;

	/// <summary>
	/// Summary for Form1
	///
	/// WARNING: If you change the name of this class, you will need to change the
	///          'Resource File Name' property for the managed resource compiler tool
	///          associated with all .resx files this class depends on.  Otherwise,
	///          the designers will not be able to interact properly with localized
	///          resources associated with this form.
	/// </summary>
	public ref class Form1 : public System::Windows::Forms::Form
	{
	public:
		Form1(void)
		{
			InitializeComponent();
			//
			//TODO: Add the constructor code here
			//
		}

	protected:
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		~Form1()
		{
			if (components)
			{
				delete components;
			}
		}
	private: System::Windows::Forms::TextBox^  textBox3;
	protected: 
	private: System::Windows::Forms::Button^  button6;
	private: System::Windows::Forms::Label^  label1;
	private: System::Windows::Forms::Label^  label5;
	private: System::Windows::Forms::Label^  label4;
	private: System::Windows::Forms::Label^  label7;
	private: System::Windows::Forms::TextBox^  textBox2;
	private: System::Windows::Forms::GroupBox^  groupBox3;
	private: System::Windows::Forms::Label^  label2;
	private: System::IO::Ports::SerialPort^  serialPort1;
	private: System::Windows::Forms::GroupBox^  groupBox2;
	private: System::Windows::Forms::TextBox^  textBox1;
	private: System::Windows::Forms::ComboBox^  comboBox2;
	private: System::Windows::Forms::Button^  button5;
	private: System::Windows::Forms::Button^  button4;
	private: System::Windows::Forms::Button^  button3;
	private: System::Windows::Forms::Label^  label6;
	private: System::Windows::Forms::ComboBox^  comboBox1;
	private: System::Windows::Forms::Button^  button2;
	private: System::Windows::Forms::GroupBox^  groupBox1;
	private: System::Windows::Forms::Button^  button1;
	private: System::Windows::Forms::Label^  label3;
	private: System::ComponentModel::IContainer^  components;

	private:
		/// <summary>
		/// Required designer variable.
		/// </summary>


#pragma region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		void InitializeComponent(void)
		{
			this->components = (gcnew System::ComponentModel::Container());
			this->textBox3 = (gcnew System::Windows::Forms::TextBox());
			this->button6 = (gcnew System::Windows::Forms::Button());
			this->label1 = (gcnew System::Windows::Forms::Label());
			this->label5 = (gcnew System::Windows::Forms::Label());
			this->label4 = (gcnew System::Windows::Forms::Label());
			this->label7 = (gcnew System::Windows::Forms::Label());
			this->textBox2 = (gcnew System::Windows::Forms::TextBox());
			this->groupBox3 = (gcnew System::Windows::Forms::GroupBox());
			this->label2 = (gcnew System::Windows::Forms::Label());
			this->serialPort1 = (gcnew System::IO::Ports::SerialPort(this->components));
			this->groupBox2 = (gcnew System::Windows::Forms::GroupBox());
			this->textBox1 = (gcnew System::Windows::Forms::TextBox());
			this->comboBox2 = (gcnew System::Windows::Forms::ComboBox());
			this->button5 = (gcnew System::Windows::Forms::Button());
			this->button4 = (gcnew System::Windows::Forms::Button());
			this->button3 = (gcnew System::Windows::Forms::Button());
			this->label6 = (gcnew System::Windows::Forms::Label());
			this->comboBox1 = (gcnew System::Windows::Forms::ComboBox());
			this->button2 = (gcnew System::Windows::Forms::Button());
			this->groupBox1 = (gcnew System::Windows::Forms::GroupBox());
			this->button1 = (gcnew System::Windows::Forms::Button());
			this->label3 = (gcnew System::Windows::Forms::Label());
			this->groupBox3->SuspendLayout();
			this->groupBox2->SuspendLayout();
			this->groupBox1->SuspendLayout();
			this->SuspendLayout();
			// 
			// textBox3
			// 
			this->textBox3->Location = System::Drawing::Point(6, 105);
			this->textBox3->Margin = System::Windows::Forms::Padding(3, 4, 3, 4);
			this->textBox3->Name = L"textBox3";
			this->textBox3->ReadOnly = true;
			this->textBox3->Size = System::Drawing::Size(98, 21);
			this->textBox3->TabIndex = 12;
			this->textBox3->TextAlign = System::Windows::Forms::HorizontalAlignment::Right;
			// 
			// button6
			// 
			this->button6->Location = System::Drawing::Point(353, 160);
			this->button6->Margin = System::Windows::Forms::Padding(3, 4, 3, 4);
			this->button6->Name = L"button6";
			this->button6->Size = System::Drawing::Size(75, 25);
			this->button6->TabIndex = 8;
			this->button6->Text = L"Exit";
			this->button6->UseVisualStyleBackColor = true;
			this->button6->Click += gcnew System::EventHandler(this, &Form1::button6_Click);
			// 
			// label1
			// 
			this->label1->AutoSize = true;
			this->label1->ForeColor = System::Drawing::Color::Red;
			this->label1->Location = System::Drawing::Point(18, 165);
			this->label1->Name = L"label1";
			this->label1->Size = System::Drawing::Size(266, 15);
			this->label1->TabIndex = 7;
			this->label1->Text = L"Please check rotary switch configuration. (No.9)";
			// 
			// label5
			// 
			this->label5->AutoSize = true;
			this->label5->Location = System::Drawing::Point(115, 20);
			this->label5->Name = L"label5";
			this->label5->Size = System::Drawing::Size(49, 15);
			this->label5->TabIndex = 2;
			this->label5->Text = L"module";
			// 
			// label4
			// 
			this->label4->AutoSize = true;
			this->label4->Location = System::Drawing::Point(6, 20);
			this->label4->Name = L"label4";
			this->label4->Size = System::Drawing::Size(31, 15);
			this->label4->TabIndex = 1;
			this->label4->Text = L"gain";
			// 
			// label7
			// 
			this->label7->AutoSize = true;
			this->label7->Location = System::Drawing::Point(2, 86);
			this->label7->Name = L"label7";
			this->label7->Size = System::Drawing::Size(43, 15);
			this->label7->TabIndex = 11;
			this->label7->Text = L"Output";
			// 
			// textBox2
			// 
			this->textBox2->Location = System::Drawing::Point(6, 58);
			this->textBox2->Margin = System::Windows::Forms::Padding(3, 4, 3, 4);
			this->textBox2->Name = L"textBox2";
			this->textBox2->ReadOnly = true;
			this->textBox2->Size = System::Drawing::Size(98, 21);
			this->textBox2->TabIndex = 10;
			this->textBox2->TextAlign = System::Windows::Forms::HorizontalAlignment::Right;
			// 
			// groupBox3
			// 
			this->groupBox3->Controls->Add(this->textBox3);
			this->groupBox3->Controls->Add(this->label7);
			this->groupBox3->Controls->Add(this->textBox2);
			this->groupBox3->Controls->Add(this->label2);
			this->groupBox3->Enabled = false;
			this->groupBox3->Location = System::Drawing::Point(322, 13);
			this->groupBox3->Name = L"groupBox3";
			this->groupBox3->Size = System::Drawing::Size(110, 139);
			this->groupBox3->TabIndex = 9;
			this->groupBox3->TabStop = false;
			this->groupBox3->Text = L"Command (DebugMonitor)";
			// 
			// label2
			// 
			this->label2->AutoSize = true;
			this->label2->Location = System::Drawing::Point(2, 39);
			this->label2->Name = L"label2";
			this->label2->Size = System::Drawing::Size(34, 15);
			this->label2->TabIndex = 9;
			this->label2->Text = L"Input";
			// 
			// serialPort1
			// 
			this->serialPort1->DataReceived += gcnew System::IO::Ports::SerialDataReceivedEventHandler(this, &Form1::serialPort1_DataReceived);
			// 
			// groupBox2
			// 
			this->groupBox2->Controls->Add(this->textBox1);
			this->groupBox2->Controls->Add(this->comboBox2);
			this->groupBox2->Controls->Add(this->button5);
			this->groupBox2->Controls->Add(this->button4);
			this->groupBox2->Controls->Add(this->button3);
			this->groupBox2->Controls->Add(this->label6);
			this->groupBox2->Controls->Add(this->label5);
			this->groupBox2->Controls->Add(this->label4);
			this->groupBox2->Location = System::Drawing::Point(112, 13);
			this->groupBox2->Margin = System::Windows::Forms::Padding(3, 4, 3, 4);
			this->groupBox2->Name = L"groupBox2";
			this->groupBox2->Padding = System::Windows::Forms::Padding(3, 4, 3, 4);
			this->groupBox2->Size = System::Drawing::Size(204, 139);
			this->groupBox2->TabIndex = 6;
			this->groupBox2->TabStop = false;
			this->groupBox2->Text = L"User Gain";
			// 
			// textBox1
			// 
			this->textBox1->Location = System::Drawing::Point(119, 40);
			this->textBox1->Margin = System::Windows::Forms::Padding(3, 4, 3, 4);
			this->textBox1->Name = L"textBox1";
			this->textBox1->ReadOnly = true;
			this->textBox1->Size = System::Drawing::Size(75, 21);
			this->textBox1->TabIndex = 8;
			this->textBox1->TextAlign = System::Windows::Forms::HorizontalAlignment::Right;
			// 
			// comboBox2
			// 
			this->comboBox2->FormattingEnabled = true;
			this->comboBox2->Location = System::Drawing::Point(7, 40);
			this->comboBox2->Margin = System::Windows::Forms::Padding(3, 4, 3, 4);
			this->comboBox2->Name = L"comboBox2";
			this->comboBox2->Size = System::Drawing::Size(75, 23);
			this->comboBox2->TabIndex = 7;
			// 
			// button5
			// 
			this->button5->ForeColor = System::Drawing::Color::Red;
			this->button5->Location = System::Drawing::Point(6, 105);
			this->button5->Margin = System::Windows::Forms::Padding(3, 4, 3, 4);
			this->button5->Name = L"button5";
			this->button5->Size = System::Drawing::Size(188, 25);
			this->button5->TabIndex = 6;
			this->button5->Text = L"Update [#UW]";
			this->button5->UseVisualStyleBackColor = true;
			this->button5->Click += gcnew System::EventHandler(this, &Form1::button5_Click);
			// 
			// button4
			// 
			this->button4->Location = System::Drawing::Point(7, 71);
			this->button4->Margin = System::Windows::Forms::Padding(3, 4, 3, 4);
			this->button4->Name = L"button4";
			this->button4->Size = System::Drawing::Size(75, 25);
			this->button4->TabIndex = 5;
			this->button4->Text = L"Set [#US]";
			this->button4->UseVisualStyleBackColor = true;
			this->button4->Click += gcnew System::EventHandler(this, &Form1::button4_Click);
			// 
			// button3
			// 
			this->button3->Location = System::Drawing::Point(119, 71);
			this->button3->Margin = System::Windows::Forms::Padding(3, 4, 3, 4);
			this->button3->Name = L"button3";
			this->button3->Size = System::Drawing::Size(75, 25);
			this->button3->TabIndex = 4;
			this->button3->Text = L"Get [#UG]";
			this->button3->UseVisualStyleBackColor = true;
			this->button3->Click += gcnew System::EventHandler(this, &Form1::button3_Click);
			// 
			// label6
			// 
			this->label6->AutoSize = true;
			this->label6->Location = System::Drawing::Point(87, 42);
			this->label6->Name = L"label6";
			this->label6->Size = System::Drawing::Size(21, 15);
			this->label6->TabIndex = 3;
			this->label6->Text = L">>";
			// 
			// comboBox1
			// 
			this->comboBox1->DropDownStyle = System::Windows::Forms::ComboBoxStyle::DropDownList;
			this->comboBox1->FormattingEnabled = true;
			this->comboBox1->Location = System::Drawing::Point(9, 40);
			this->comboBox1->Margin = System::Windows::Forms::Padding(3, 4, 3, 4);
			this->comboBox1->Name = L"comboBox1";
			this->comboBox1->Size = System::Drawing::Size(75, 23);
			this->comboBox1->TabIndex = 3;
			// 
			// button2
			// 
			this->button2->Location = System::Drawing::Point(9, 105);
			this->button2->Margin = System::Windows::Forms::Padding(3, 4, 3, 4);
			this->button2->Name = L"button2";
			this->button2->Size = System::Drawing::Size(75, 25);
			this->button2->TabIndex = 2;
			this->button2->Text = L"Close";
			this->button2->UseVisualStyleBackColor = true;
			this->button2->Click += gcnew System::EventHandler(this, &Form1::button2_Click);
			// 
			// groupBox1
			// 
			this->groupBox1->Controls->Add(this->comboBox1);
			this->groupBox1->Controls->Add(this->button2);
			this->groupBox1->Controls->Add(this->button1);
			this->groupBox1->Controls->Add(this->label3);
			this->groupBox1->Location = System::Drawing::Point(12, 13);
			this->groupBox1->Margin = System::Windows::Forms::Padding(3, 4, 3, 4);
			this->groupBox1->Name = L"groupBox1";
			this->groupBox1->Padding = System::Windows::Forms::Padding(3, 4, 3, 4);
			this->groupBox1->Size = System::Drawing::Size(94, 139);
			this->groupBox1->TabIndex = 5;
			this->groupBox1->TabStop = false;
			this->groupBox1->Text = L"COM port";
			// 
			// button1
			// 
			this->button1->Location = System::Drawing::Point(9, 71);
			this->button1->Margin = System::Windows::Forms::Padding(3, 4, 3, 4);
			this->button1->Name = L"button1";
			this->button1->Size = System::Drawing::Size(75, 25);
			this->button1->TabIndex = 1;
			this->button1->Text = L"Open";
			this->button1->UseVisualStyleBackColor = true;
			this->button1->Click += gcnew System::EventHandler(this, &Form1::button1_Click);
			// 
			// label3
			// 
			this->label3->AutoSize = true;
			this->label3->Location = System::Drawing::Point(6, 20);
			this->label3->Name = L"label3";
			this->label3->Size = System::Drawing::Size(48, 15);
			this->label3->TabIndex = 0;
			this->label3->Text = L"Port list";
			// 
			// Form1
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(7, 15);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->ClientSize = System::Drawing::Size(444, 198);
			this->Controls->Add(this->button6);
			this->Controls->Add(this->label1);
			this->Controls->Add(this->groupBox3);
			this->Controls->Add(this->groupBox2);
			this->Controls->Add(this->groupBox1);
			this->Font = (gcnew System::Drawing::Font(L"Arial", 9, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point, 
				static_cast<System::Byte>(0)));
			this->Margin = System::Windows::Forms::Padding(3, 4, 3, 4);
			this->MaximizeBox = false;
			this->MinimizeBox = false;
			this->Name = L"Form1";
			this->Text = L"Form1";
			this->Load += gcnew System::EventHandler(this, &Form1::Form1_Load);
			this->groupBox3->ResumeLayout(false);
			this->groupBox3->PerformLayout();
			this->groupBox2->ResumeLayout(false);
			this->groupBox2->PerformLayout();
			this->groupBox1->ResumeLayout(false);
			this->groupBox1->PerformLayout();
			this->ResumeLayout(false);
			this->PerformLayout();

		}
#pragma endregion
	private:
        delegate void DelegateAnalysisReceiveData(String^ str);
        enum class myCONST {
			DATA_FORMAT_SIZE = 9,
            GAIN_MIN = 5,
            GAIN_MAX = 400
        };

	private: 
		System::Void Form1_Load(System::Object^  sender, System::EventArgs^  e) 
		{
            int i;
            array<int>^ gainList = {
                safe_cast<int>(myCONST::GAIN_MIN), 10, 30, 50, 100, 150, 250, safe_cast<int>(myCONST::GAIN_MAX)
            };
            String^ portName;
            array<String^>^ ports = gcnew array<String^>(10);

            button1->Enabled = true;
            button2->Enabled = false;
            groupBox2->Enabled = false;

			// Title
			this->Text = "APD module C10508-01";

            // search serial port
            ports = System::IO::Ports::SerialPort::GetPortNames();
            //for each (portName in ports) {
            //    comboBox1->Items->Add(portName);
            //}
            //comboBox1->DropDownStyle = System::Windows::Forms::ComboBoxStyle::DropDownList;
            //comboBox1->SelectedIndex = 0;   //this->comboBox1->Items->Count - 1;
			for (int i = 0; i < 10; i++) {
				comboBox1->Items->Add(ports[i]);
			}
			comboBox1->DropDownStyle = System::Windows::Forms::ComboBoxStyle::DropDownList;
			comboBox1->SelectedIndex = 0;   //this->comboBox1->Items->Count - 1;

            // User Gain
            for (i = 0; i < gainList->Length; i++) {
                comboBox2->Items->Add(gainList[i]);
            }
            comboBox2->Text = gainList[1].ToString();

            // Serial port
            serialPort1->BaudRate = 9600;
            serialPort1->DataBits = 8;
            serialPort1->Parity = System::IO::Ports::Parity::None;
            serialPort1->StopBits = System::IO::Ports::StopBits::One;
            serialPort1->Handshake = System::IO::Ports::Handshake::None;
            serialPort1->RtsEnable = false;
            serialPort1->DiscardNull = false;
			serialPort1->NewLine = "\r\n";		
		}

	private: 
		System::Void button6_Click(System::Object^  sender, System::EventArgs^  e) 
		{
            this->Close();
		}

	private: 
		System::Void button1_Click(System::Object^  sender, System::EventArgs^  e) 
		{
            if (serialPort1->IsOpen == false) {
                serialPort1->PortName = this->comboBox1->Text;
                serialPort1->Open();

                if (serialPort1->IsOpen == true) {
                    serialPort1->DiscardInBuffer();
                    button1->Enabled = false;
                    button2->Enabled = true;
                    groupBox2->Enabled = true;
					groupBox3->Enabled = true;
                }
            }
		}

	private: 
		System::Void button2_Click(System::Object^  sender, System::EventArgs^  e) 
		{
            if (serialPort1->IsOpen == true) {
                serialPort1->Close();
                button1->Enabled = true;
                button2->Enabled = false;
                groupBox2->Enabled = false;
				groupBox3->Enabled = false;
            }
		}

	private: 
		System::Void button3_Click(System::Object^  sender, System::EventArgs^  e) 
		{
			// TODO : get user gain
			SendCommand("#UG0000");
		}

	private: 
		System::Void button4_Click(System::Object^  sender, System::EventArgs^  e) 
		{
            // TODO : set user gain
            String^ cmd;
            int gain;

            if (comboBox1->Text != "") {
                gain = System::UInt32::Parse(comboBox2->Text);
                if (gain >= safe_cast<int>(myCONST::GAIN_MIN) && gain <= safe_cast<int>(myCONST::GAIN_MAX)) {
                    // Ex. Gain = 123 : "#US0123"
                    cmd = "#US" + gain.ToString("0000");
					SendCommand(cmd);
                } else {
                    MessageBox::Show("Invalid parameter", "Error : #US", MessageBoxButtons::OK, MessageBoxIcon::Error);
                }
            } else {
                MessageBox::Show("Invalid parameter (non-numeric)", "Error : #US", MessageBoxButtons::OK, MessageBoxIcon::Error);
            }
		}

	private: 
		System::Void button5_Click(System::Object^  sender, System::EventArgs^  e) 
		{
            // TODO : update user gain
            String^ cmd;
            int gain;

            if (comboBox1->Text != "") {
                gain = System::UInt32::Parse(comboBox2->Text);
                if (gain >= safe_cast<int>(myCONST::GAIN_MIN) && gain <= safe_cast<int>(myCONST::GAIN_MAX)) {
                    // Ex. Gain = 123 : "#UW0123"
                    cmd = "#UW" + gain.ToString("0000");
					SendCommand(cmd);
                } else {
                    MessageBox::Show("Invalid value", "Error : #UW", MessageBoxButtons::OK, MessageBoxIcon::Error);
                }
            } else {
                MessageBox::Show("Invalid parameter (non-numeric)", "Error : #UW", MessageBoxButtons::OK, MessageBoxIcon::Error);
            }
        }

	private: 
		System::Void serialPort1_DataReceived(System::Object^  sender, System::IO::Ports::SerialDataReceivedEventArgs^  e) 
		{
			// TODO : update user gain
            DelegateAnalysisReceiveData^ func = gcnew DelegateAnalysisReceiveData(this, &C10508::Form1::AnalysisReceiveData);
            array<unsigned char>^ buffer = gcnew array<unsigned char>(11);
            int receiveSize;
            String^ receiveStr;

			// 1 byte = 1 start + 8 data + 1 stop bit
			// 90 bit / 9600 baud = 9.36ms
			System::Threading::Thread::Sleep(20);
			Application::DoEvents();

            receiveSize = serialPort1->BytesToRead;
			if (receiveSize == safe_cast<int>(myCONST::DATA_FORMAT_SIZE)) {
				try {
					serialPort1->Read(buffer, 0, 9);
					receiveStr = System::Text::Encoding::GetEncoding("utf-8")->GetString(buffer, 0, 9);
					Invoke(func, receiveStr);
				} catch (System::Exception^ err) {
					MessageBox::Show(err->ToString(), "Error : serialPort1_DataReceived", MessageBoxButtons::OK, MessageBoxIcon::Error);
				}
			} 
			serialPort1->DiscardInBuffer();
        }

	private:
        System::Void AnalysisReceiveData(String^ str)
        {
            String^ commandStr;
            int dat;

            commandStr = str->Substring(0, 3);
            dat = System::UInt32::Parse(str->Substring(3, 4));

			if (!commandStr->CompareTo("*US")) {
                textBox1->Text = dat.ToString();
			} else if (!commandStr->CompareTo("*UG")) {
				textBox1->Text = dat.ToString();
			} else if (!commandStr->CompareTo("*UW")) {
				textBox1->Text = dat.ToString();
			} else {
				MessageBox::Show(str, "Error : Serial", MessageBoxButtons::OK, MessageBoxIcon::Error);
			}
			// DebugMonitor 
			textBox3->Text = str;
        }

	private:
		System::Void SendCommand(String^ str)
		{
			// Send
			serialPort1->WriteLine(str);
			// DebugMonitor
			textBox2->Text = str;		
		}

};
}

