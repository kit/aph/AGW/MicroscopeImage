//APD-class implementation

/*
https://orxor.wordpress.com/2014/10/03/rs232-programmierung-mit-c-auf-die-schnelle/
*/
#include "APD.h"
#include <fstream>
#include <iostream>
#include <string>

//#include <stdlib.h>
//#include <stdio.h>

#ifdef _WIN32
#include <Windows.h>
#else
#include <unistd.h>
#endif

#include "rs232.h"

using namespace std;

APD::APD() {
    bdrate = 9600;
    cport_nr = RS232_GetPortnr("COM4");;
    mode = "8N1";
    flowcontrol = 0;
};

bool APD::SendCommand(string command, bool silent) {

    if (RS232_OpenComport(cport_nr, bdrate, mode, flowcontrol) != 0 and !silent) {
        cerr << "Error while opening serial interface" << endl;
        return 1;
    }
    else if (!silent) {
        cout << "Com port opened." << endl;
    }

    RS232_cputs(cport_nr, command.c_str());
    if (!silent) {
        cout << "Sent command was " << command.substr(0, command.length() - 2) << endl; // cut CR and LF Bytes
    }

#ifdef _WIN32
    Sleep(100);
#else
    usleep(100000);  /* sleep for 0.1 Second */
#endif

     // Listen for answer from APD
    int i;
    int n;

    ClearBuffer(); // clears buffer to receive new debug response
    n = RS232_PollComport(cport_nr, buf, 128);

    if (n > 0) {
        buf[n] = 0;   /* always put a "null" at the end of a string! */

        for (i = 0; i < n; i++) {
            if (buf[i] < 32) {  /* replace unreadable control-codes by dots */
                buf[i] = '.';
            }
        }
        if (!silent) {
            printf("APD responding with %i received bytes: %s\n", n, (char*)buf);
        }
    }
    if (RS232_CloseComport(cport_nr) != 0 and !silent) {
        cout << "COM port closed" << endl;
    }
    return 0;
};

int APD::QuerryCurrentGain(bool silent) {
    if (SendCommand("#UG0000\r\n", true)) {
        cout << "Error while querying gain." << endl;
        return 0;
    }
    gain = 100 * (int(buf[4]) - 48) + 10 * (int(buf[5]) - 48) + 1 * (int(buf[6]) - 48); // ugly as hell pls don't judge lol

    if(!silent)
        cout << "Current APD Gain is M = " << gain << "."<< endl;

    return gain;
};

bool APD::SetDefaultGain() {
    if (SendCommand("#UW0010\r\n")) {
        cout << "Error while setting default gain." << endl;
        return 0;
    }
    return 1;
};

bool APD::ResetGain() {
    if (SendCommand("#US0010\r\n")) {
        cout << "Error while resetting gain." << endl;
        return 0;
    }
    gain = 100 * (int(buf[4]) - 48) + 10 * (int(buf[5]) - 48) + 1 * (int(buf[6]) - 48); // ugly as hell pls don't judge lol
    return 1;
};

bool APD::ClearBuffer() {
    for (int i = 0; i < 128; i++) {
        *(buf + i) = 0; // reset all buffer elements to zero
    }
    return 1;
};

bool APD::SetGain(int* apdM) {
    int msg;
    QuerryCurrentGain();
    cout << "Enter integer value for M from 005 to 400." << endl;
    cin >> msg;

    while (msg < 5 and msg > 400) {
        cout << "Invalid integer value. Remain within 5 to 400." << endl;
    };

    string cmd = to_string(msg);
    while (cmd.size() < 4) {
        cmd = "0" + cmd;
    };
    cmd = "#US" + cmd + "\r\n"; // Carriage Return \r and Line Feed \n are required!!!
    if (SendCommand(cmd)) {
        cout << "Error while setting gain." << endl;
        return 0;
    }
    gain = 100 * (int(buf[4]) - 48) + 10 * (int(buf[5]) - 48) + 1 * (int(buf[6]) - 48); // ugly as hell pls don't judge lol
    *apdM = msg;
    return 1;
};

bool APD::SetGain(float newGain, int* apdM) {
    int msg;
    msg = int(newGain + 0.5);
    if (msg < 5 and msg > 400) {
        cout << "Invalid integer gain value." << endl;
        cout << "Gain set to 5." << endl;
        if (SendCommand("#US0005\r\n")) {
            cout << "Error while resetting gain." << endl;
                return 0;
        }
        SetDefaultGain();
        *apdM = 10;
        return 0;
    }
    else {
        string cmd = to_string(msg);
        while (cmd.size() < 4) {
            cmd = "0" + cmd;
        };
        cmd = "#US" + cmd + "\r\n"; // Carriage Return \r and Line Feed \n are required!!!
        SendCommand(cmd,false);
        gain = 100 * (int(buf[4]) - 48) + 10 * (int(buf[5]) - 48) + 1 * (int(buf[6]) - 48); // ugly as hell pls don't judge lol
        *apdM = msg;
        return 1;
    }
};

