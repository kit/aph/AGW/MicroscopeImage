// Header for APD-class

#pragma once
//#include <fstream>
//#include <iostream>
#include <string>

//#include <stdlib.h>
//#include <stdio.h>

//#ifdef _WIN32
//#include <Windows.h>
//#else
//#include <unistd.h>
//#endif

//#include "rs232.h"


class APD
{

private:
	int gain; // Multiplication factor of the APD
	int bdrate;
	int cport_nr;
	const char* mode;
	int flowcontrol;
	unsigned char buf[128] = { 0 }; // buffer to store answer from apd module after e.g. sending a command
	


public:
	APD(); // Constructor
	bool SendCommand(std::string command, bool silent = false); // sends command specified by "command" and listens for the debug response of the apd
	int QuerryCurrentGain(bool silent = false); // Querrys Gain 
	bool SetDefaultGain(); // Sets gain on power-up or rotary switch position 8
	bool ResetGain();
	bool ClearBuffer(); // clears buffer buf variable
	bool SetGain(int* apdM); // takes a pointer to the gain variable of the Microscope class object
	bool SetGain(float newGain, int* adpM);
};