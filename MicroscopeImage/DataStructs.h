#pragma once
#include "DigitizerBoard.h"
#include <string>
#include <iostream>


struct SegmentIntensity
{
	WORD* I_pi2;
	WORD* I_pi;
};

struct CorrectionPattern
{
	BYTE* SegmentValues;
};

struct MarkerCoordList
{

    MarkerCoordList();
    MarkerCoordList(int N, double* xP, double* yP, double* zP);
    MarkerCoordList(const MarkerCoordList& mcl); // copy constructor

    ~MarkerCoordList();

    //https://www.learncpp.com/cpp-tutorial/overloading-the-assignment-operator/
    MarkerCoordList& operator= (const MarkerCoordList& mcl); // assignment operator needs to be member function

    bool s_filled = false;

	int Nmarkers;
	// pointers for the coordinate lists read from "MarkerLog.dat" file
	double* xPosMarkers;
	double* yPosMarkers;
	double* zPosMarkers;


};

struct DynMeasParameters 
{

    DynMeasParameters();

    bool s_filled = false;

    std::string s_dynMeasSeriesPath; // specifies where data for dynamic measurement series will be stored
    bool        s_clearDirs, s_saveMeta, s_saveAsDat, s_linewise, s_fftFiltering;
    int         s_apdGain, s_pixelsX, s_pixelsY;
    double      s_FOV;
    size_t      s_NsampPx; // samples per pixel to be used for saving.. 
    float       s_Vpp, s_startFreq, s_freqStep;
    int         s_NfreqSteps;
    size_t      s_vibrSamples; // 65536 = 2^16 @ 500MSps <-> 1024 = 2^10 @ 10MSps
    size_t      s_vibSampleR; //Sps
    int         s_imagSampleR;
    int         s_trigLvl, s_inptRng; //Trigger levels for the external (analog) trigger to be programmed in mV
};

struct MicroscopeHardware {

    MicroscopeHardware();

    bool s_filled = false;

    int         s_objLens; // 1 <-> Wetzlar Plan L50x/0.6, 2 <-> Wetzlar Plan L25x/0.4, 3 <-> Wetzlar PL8x/0.18, 4 <-> Nikon CFI60 TU Plan Epi ELWD 50x/0.6
    int         s_jump_speed, s_mark_speed;
    std::string s_FG_VISA;
    float       s_Vpp_thresh;
    std::string s_xStageAddr, s_yStageAddr, s_zStageAddr;
    char        s_xAxis[2] = { 0 }, s_yAxis[2] = { 0 }, s_zAxis[2] = { 0 };
    float       s_lasWavel;
    std::string s_gateT, s_gateCOMport;
    int         s_gateCOMbaud;

};


std::ostream& operator<<(std::ostream& os, const DynMeasParameters& other);

std::ostream& operator<<(std::ostream& os, const MicroscopeHardware& other);


