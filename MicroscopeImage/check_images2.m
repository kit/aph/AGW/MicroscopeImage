close all;

InputRange = 1000; %mV
FOV = 300; %um
pixelanzahl = 400;
pixelsize = FOV/pixelanzahl;

x = (0:(pixelanzahl-1)) * pixelsize + pixelsize/2;
y = (0:(pixelanzahl-1)) * pixelsize + pixelsize/2;
yfl = fliplr(y);  % important, since imagesc flips y-axis.. 
                  % otherwise image appears upside down!

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%  
%% As tiff

I = double(imread('axImag2_y.tiff') - 2^15) * InputRange / 2^15; %mV

figure(1)
hold on
colormap('gray')
imagesc(x, yfl, I)
title("Displacements");
xlim([0 FOV])
ylim([0 FOV])
xlabel(['x-Coordinate in ',char(181),'m'])
ylabel(['y-Coordinate in ',char(181),'m'])
hc=colorbar;
title(hc,'mV');
pbaspect([1 1 1])

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%  
%% As bmp

% Ibmp1 = double(imread('fluo_bottom_run1_post.bmp'))./255.*100; %mV
% 
% subplot(1,2,1)
% hold on
% title('bmp')
% imagesc(x, yfl, Ibmp1)
% xlim([0 FOV])
% ylim([0 FOV])
% xlabel("x-Coordinate in \mum")
% ylabel("y-Coordinate in \mum")
% hc=colorbar;
% title(hc,'mV');
% pbaspect([1 1 1])
% 
% hold off
% 
% 
% 
% Ibmp1 = double(imread('fluo_bottom_run1_post.bmp'))./255.*100; %mV
% 
% subplot(1,2,2)
% hold on
% title('bmp')
% imagesc(x, yfl, Ibmp1)
% xlim([0 FOV])
% ylim([0 FOV])
% xlabel("x-Coordinate in \mum")
% ylabel("y-Coordinate in \mum")
% hc=colorbar;
% title(hc,'mV');

% pbaspect([1 1 1])
% 
% hold off