
// simple tiff writer for 16bit grey bitmaps
// Author: Rudi Weinacker
// 1. Version 07.05.2020
// Rev. 08.05.2020:
//  - faster
//  - needs less memory
//  - add possibility to change:
//   - top-down behavior
//   - bitmap size
//   - bpp (8 or 16 bit)
//   - bitmap size


#include "TiffGrey.h"
#include <cstring>
#include <stdio.h>
#include <stdlib.h>


#ifndef BYTE
typedef unsigned char BYTE;
#endif
#ifndef WORD
typedef unsigned short WORD;
#endif
#ifndef DWORD
typedef unsigned long DWORD;
#endif



////////////////////////////////////////////////////////////////////////////////////////////////////////
// Header structs
////////////////////////////////////////////////////////////////////////////////////////////////////////


//#pragma pack(push,1)
#pragma pack(2)
namespace
{
	enum ETiffType
	{
		TT_Tiff ,
		TT_BigTiff ,
		TT_Unknown ,
	};

	enum EMarvin : WORD
	{
		Marvin32 = 42 ,
		Marvin64 = 43 ,
	};

	enum EBYTEORDER : WORD
	{
		byteorder_Intel = 0x4949 ,
		byteorder_Motorola = 0x4D4D ,
	};

	struct TIFF_HEADER
	{
	private:
		EBYTEORDER Byteorder ; // Byteorder: 0x4949=Intel, 0x4D4D=Motorola
		EMarvin Marvin ;	   // Tiff-Kennwert
		DWORD pIFD ;           // Pointer auf erstes IFD bei Tiff, ist 8 bei BigTiff
	public:
		TIFF_HEADER ()
			: Byteorder ( byteorder_Intel ) // intel Byteorder
			, Marvin ( Marvin32 )
			, pIFD ( sizeof ( *this ) )
		{}
		void setIntel ()    { Byteorder = byteorder_Intel; }
		void setMotorola () { Byteorder = byteorder_Motorola; }
		void setDefaultValues ()
		{
			Byteorder = byteorder_Intel ;
			Marvin = Marvin32 ;
			pIFD = sizeof ( *this ) ;
		}
	};

	enum FieldType : WORD
	{
		FT_BYTE = 1 , //  8-bit unsigned integer.
		FT_ASCII = 2 , //  8-bit byte that contains a 7-bit ASCII code; the last byte must be NUL (binary zero).
		FT_SHORT = 3 , //  16-bit (2-byte) unsigned integer.
		FT_LONG = 4 , //  32-bit (4-byte) unsigned integer.
		FT_RATIONAL = 5 , // 
		FT_SBYTE = 6 , //  An 8-bit signed (twos-complement) integer.
		FT_UNDEFINED = 7 , //  An 8-bit byte that may contain anything, depending on the definition of the field.
		FT_SSHORT = 8 , //  A 16-bit (2-byte) signed (twos-complement) integer.
		FT_SLONG = 9 , //  A 32-bit (4-byte) signed (twos-complement) integer.
		FT_SRATIONAL = 10 , //  Two SLONG�s: the first represents the numerator of a fraction, the second the denominator.
		FT_FLOAT = 11 , //  Single precision (4-byte) IEEE format.
		FT_DOUBLE = 12 ,  // 
		TIFF_LONG8 = 16 , // being unsigned 8byte integer
		TIFF_SLONG8 = 17 , // being signed 8byte integer
		TIFF_IFD8 = 18 , // being a new unsigned 8byte IFD offset
	};
	static int getTypeSize ( FieldType ft )
	{
		const BYTE numbytes[] = 
		{
			0 , // not defined
			1 , // FT_BYTE      =  1, //  8-bit unsigned integer.
			1 , // FT_ASCII     =  2, //  8-bit byte that contains a 7-bit ASCII code; the last byte must be NUL (binary zero).
			2 , // FT_SHORT     =  3, //  16-bit (2-byte) unsigned integer.
			4 , // FT_LONG      =  4, //  32-bit (4-byte) unsigned integer.
			8 , // FT_RATIONAL  =  5, // 
			1 , // FT_SBYTE     =  6, //  An 8-bit signed (twos-complement) integer.
			1 , // FT_UNDEFINED =  7, //  An 8-bit byte that may contain anything, depending on the definition of the field.
			2 , // FT_SSHORT    =  8, //  A 16-bit (2-byte) signed (twos-complement) integer.
			4 , // FT_SLONG     =  9, //  A 32-bit (4-byte) signed (twos-complement) integer.
			8 , // FT_SRATIONAL = 10, //  Two SLONG�s: the first represents the numerator of a fraction, the second the denominator.
			4 , // FT_FLOAT     = 11, //  Single precision (4-byte) IEEE format.
			8 , // FT_DOUBLE    = 12  // 
			0 , // unknown = 13
			0 , // unknown = 14
			0 , // unknown = 15
			8 , // 16 : TIFF_LONG8   being unsigned 8byte integer
			8 , // 17 : TIFF_SLONG8  being signed 8byte integer
			8 , // 18 : TIFF_IFD8    being a new unsigned 8byte IFD offset
		};
		return numbytes [ ft ] ;
	}
	enum TIFFTags : WORD
	{
		TIFFTAG_NewSubfileType = 254 , // FE LONG 1
		TIFFTAG_SubfileType = 255 , // FF SHORT 1
		TIFFTAG_ImageWidth = 256 , // 100 SHORT or LONG 1
		TIFFTAG_ImageLength = 257 , // 101 SHORT or LONG 1
		TIFFTAG_BitsPerSample = 258 , // 102 SHORT SamplesPerPixel
		TIFFTAG_Compression = 259 , // 103 SHORT 1
		TIFFTAG_PhotometricInterpretation = 262 , // 106 SHORT 1
		TIFFTAG_Threshholding = 263 , // 107 SHORT 1
		TIFFTAG_CellWidth = 264 , // 108 SHORT 1
		TIFFTAG_CellLength = 265 , // 109 SHORT 1
		TIFFTAG_FillOrder = 266 , // 10A SHORT 1
		TIFFTAG_DocumentName = 269 , // 10D ASCII
		TIFFTAG_ImageDescription = 270 , // 10E ASCII
		TIFFTAG_Make = 271 , // 10F ASCII
		TIFFTAG_Model = 272 , // 110 ASCII
		TIFFTAG_StripOffsets = 273 , // 111 SHORT or LONG StripsPerImage
		TIFFTAG_Orientation = 274 , // 112 SHORT 1
		TIFFTAG_SamplesPerPixel = 277 , // 115 SHORT 1
		TIFFTAG_RowsPerStrip = 278 , // 116 SHORT or LONG 1
		TIFFTAG_StripByteCounts = 279 , // 117 LONG or SHORT StripsPerImage
		TIFFTAG_MinSampleValue = 280 , // 118 SHORT SamplesPerPixel
		TIFFTAG_MaxSampleValue = 281 , // 119 SHORT SamplesPerPixel
		TIFFTAG_XResolution = 282 , // 11A RATIONAL 1
		TIFFTAG_YResolution = 283 , // 11B RATIONAL 1
		TIFFTAG_PlanarConfiguration = 284 , // 11C SHORT 1
		TIFFTAG_PageName = 285 , // 11D ASCII
		TIFFTAG_XPosition = 286 , // 11E RATIONAL
		TIFFTAG_YPosition = 287 , // 11F RATIONAL
		TIFFTAG_FreeOffsets = 288 , // 120 LONG
		TIFFTAG_FreeByteCounts = 289 , // 121 LONG
		TIFFTAG_GrayResponseUnit = 290 , // 122 SHORT 1
		TIFFTAG_GrayResponseCurve = 291 , // 123 SHORT 2**BitsPerSample
		TIFFTAG_T4Options = 292 , // 124 LONG 1
		TIFFTAG_T6Options = 293 , // 125 LONG 1
		TIFFTAG_ResolutionUnit = 296 , // 128 SHORT 1
		TIFFTAG_PageNumber = 297 , // 129 SHORT 2
		TIFFTAG_TransferFunction = 301 , // 12D SHORT {1 or SamplesPerPixel}* 2** BitsPerSample
		TIFFTAG_Software = 305 , // 131 ASCII
		TIFFTAG_DateTime = 306 , // 132 ASCII 20
		TIFFTAG_Artist = 315 , // 13B ASCII
		TIFFTAG_HostComputer = 316 , // 13C ASCII
		TIFFTAG_Predictor = 317 , // 13D SHORT 1
		TIFFTAG_WhitePoint = 318 , // 13E RATIONAL 2
		TIFFTAG_PrimaryChromaticities = 319 , // 13F RATIONAL 6
		TIFFTAG_ColorMap = 320 , // 140 SHORT 3 * (2**BitsPerSample)
		TIFFTAG_HalftoneHints = 321 , // 141 SHORT 2
		TIFFTAG_TileWidth = 322 , // 142 SHORT or LONG 1
		TIFFTAG_TileLength = 323 , // 143 SHORT or LONG 1
		TIFFTAG_TileOffsets = 324 , // 144 LONG TilesPerImage
		TIFFTAG_TileByteCounts = 325 , // 145 SHORT or LONG TilesPerImage
		TIFFTAG_InkSet = 332 , // 14C SHORT 1
		TIFFTAG_InkNames = 333 , // 14D ASCII total number of characters in all ink name strings, including zeros
		TIFFTAG_NumberOfInks = 334 , // 14E SHORT 1
		TIFFTAG_DotRange = 336 , // 150 BYTE or SHORT 2, or 2*NumberOfInks
		TIFFTAG_TargetPrinter = 337 , // 151 ASCII any
		TIFFTAG_ExtraSamples = 338 , // 152 BYTE number of extra components per pixel
		TIFFTAG_SampleFormat = 339 , // 153 SHORT SamplesPerPixel
		TIFFTAG_SMinSampleValue = 340 , // 154 Any SamplesPerPixel
		TIFFTAG_SMaxSampleValue = 341 , // 155 Any SamplesPerPixel
		TIFFTAG_TransferRange = 342 , // 156 SHORT 6
		TIFFTAG_JPEGProc = 512 , // 200 SHORT 1
		TIFFTAG_JPEGInterchangeFormat = 513 , // 201 LONG 1
		TIFFTAG_JPEGInterchangeFormatLngth = 514 , // 202 LONG 1
		TIFFTAG_JPEGRestartInterval = 515 , // 203 SHORT 1
		TIFFTAG_JPEGLosslessPredictors = 517 , // 205 SHORT SamplesPerPixel
		TIFFTAG_JPEGPointTransforms = 518 , // 206 SHORT SamplesPerPixel
		TIFFTAG_JPEGQTables = 519 , // 207 LONG SamplesPerPixel
		TIFFTAG_JPEGDCTables = 520 , // 208 LONG SamplesPerPixel
		TIFFTAG_JPEGACTables = 521 , // 209 LONG SamplesPerPixel
		TIFFTAG_YCbCrCoefficients = 529 , // 211 RATIONAL 3
		TIFFTAG_YCbCrSubSampling = 530 , // 212 SHORT 2
		TIFFTAG_YCbCrPositioning = 531 , // 213 SHORT 1
		TIFFTAG_ReferenceBlackWhite = 532 , // 214 LONG 2*SamplesPerPixel
		TIFFTAG_Copyright = 33432 , // 8298 ASCII Any


		TIFFTAG_ModelPixelScaleTag = 33550 , // (SoftDesk)
		TIFFTAG_ModelTransformationTag = 34264 , // (JPL Carto Group)
		TIFFTAG_ModelTiepointTag = 33922 , // (Intergraph)
		TIFFTAG_GeoKeyDirectoryTag = 34735 , // (SPOT)
		TIFFTAG_GeoDoubleParamsTag = 34736 , // (SPOT)
		TIFFTAG_GeoAsciiParamsTag = 34737 , // (SPOT)
		TIFFTAG_GDAL_NODATA = 42113 , // (Ascii)
	};
	enum TiffOrientation : WORD
	{
		to_TopDown  = 1 ,
		to_BottomUp = 4 ,
	};
	struct SIFDEntry
	{
		//static const int DoNotWriteDirectly = 1 ;
		union
		{
			DWORD TagType ;
			struct
			{
				TIFFTags  Tag   ;
				FieldType Type  ;
			};
		};
		DWORD     Count ;
		union
		{
			DWORD Value  ;
			float ValueF ;
		};
		DWORD getPos() const { return Value ; }
		int  getValueNumBytes() const { return getTypeSize ( Type ) ; }
		int getDataSize () const { return Count * getValueNumBytes() ; }
		static int  getValueNumBytes ( FieldType type ) { return getTypeSize ( type ) ; }
		static bool isSimple ( FieldType type , DWORD count ) ;
		bool isSimple() const ;
		SIFDEntry() {}
		SIFDEntry ( TIFFTags tag , FieldType type , DWORD count , DWORD value )
			: Tag   ( tag   )
			, Type  ( type  )
			, Count ( count )
			, Value ( value )
		{}
	};
	bool SIFDEntry::isSimple ( FieldType type , DWORD count )
	{
		if ( count != 1 ) return false ;
		return getValueNumBytes ( type ) <= 4 ;
	}

	bool SIFDEntry::isSimple() const
	{
		return isSimple ( Type , Count ) ;
	}
	struct SResolution
	{
		const int z , n ;
		SResolution() : z ( 960000 ) , n ( 10000 ) {}
		SResolution ( int dpi ) : z ( 10000 * dpi ) , n ( 10000 ) {}
		operator DWORD () const { return z ; }
	};
}


//#pragma pack(pop)
#pragma pack()

////////////////////////////////////////////////////////////////////////////////////////////////////////
// struct CGreyBMP::SImpl
////////////////////////////////////////////////////////////////////////////////////////////////////////

struct CTiffGrey::SImpl
{
	DWORD wx , wy ;
	DWORD m_bpp ;
	TiffOrientation m_Orientation ;
	DWORD m_NumBytesPerPixel ;
	DWORD m_RowLength ;
	DWORD m_FileSize ;
	DWORD m_IfdStartPointer ;
	DWORD m_IfdEntryPointer ;
	DWORD m_DataPointer ;

	DWORD m_PointerWidth ;
	DWORD m_PointerHeight ;
	DWORD m_PointerBPP ;
	DWORD m_PointerOrientation ;
	DWORD m_PointerRowsPerStrip ;
	DWORD m_PointerImageByteCount ;

	DWORD m_ImageStart ;
	DWORD m_ImageByteCount ;
	BYTE* m_Bits ;
	BYTE* m_FileImage ;
	bool m_bValid ;
	~SImpl () ;
	SImpl () ;
	SImpl ( int x , int y ) ;
	SImpl ( const char* filename ) ;
	bool init () ;
	bool init ( FILE *pf ) ;
	bool save ( const void* buffer , const char* filename ) const ;
	bool save ( const void* buffer , FILE *pf ) const ;
	BYTE* getLine ( int line ) ;
	static DWORD clacPixelImageSize ( int wx , int wy , int bpp ) ;
	DWORD addEntry ( TIFFTags tag , FieldType type , DWORD count , DWORD value ) ;
	DWORD addEntry ( TIFFTags tag , FieldType type , DWORD count , const void* data ) ;
	template < class T >
	DWORD addEntry ( TIFFTags tag , FieldType type , DWORD count , const T& value )
	{
		if ( SIFDEntry::isSimple ( type , count ) )
		{
			return addEntry ( tag , type , count , ( DWORD ) value ) ;
		}
		else
		{
			return addEntry ( tag , type , count , ( const void* ) &value ) ;
		}
	}
	void addHeader () ;
	void addIFD () ;
	DWORD addToImage ( const void* data , DWORD offset , DWORD size ) ;

	template < class T >
	DWORD addToImage ( const T& object , DWORD offset )
	{
		return addToImage ( &object , offset , sizeof ( object ) ) ;
	}
	void align ( DWORD& offset ) { if ( offset & 1 ) ++offset ; }
	static DWORD getAligned ( DWORD offset ) { return offset + ( offset & 1 ) ; }
	template < class T >
	T& updateImageField ( DWORD offset ) { return * ( T* ) ( m_FileImage + offset ) ; }
	void setBitmapSize ( int width , int height ) ;
	void setBPP ( int bpp ) ;
	void setOrientation ( TiffOrientation o ) ;
};

CTiffGrey::SImpl::~SImpl ()
{
	if ( m_FileImage ) free ( m_FileImage ) ;
}

CTiffGrey::SImpl::SImpl ()
{
	std::memset ( this , 0 , sizeof ( *this ) ) ;
	m_bpp = 16 ;
}

void CTiffGrey::SImpl::setBitmapSize ( int width , int height )
{
	wx = width ;
	wy = height ;
	m_RowLength = m_NumBytesPerPixel * wx ;
	m_ImageByteCount = m_RowLength * wy ;
	if ( !m_FileImage ) return ;
	updateImageField < DWORD > ( m_PointerWidth          ) = width  ;
	updateImageField < DWORD > ( m_PointerHeight         ) = height ;
	updateImageField < DWORD > ( m_PointerRowsPerStrip   ) = height ;
	updateImageField < DWORD > ( m_PointerImageByteCount ) = m_ImageByteCount ;
}
void CTiffGrey::SImpl::setBPP ( int bpp )
{
	m_bpp = bpp ;
	m_NumBytesPerPixel = m_bpp / 8 ;
	m_RowLength = m_NumBytesPerPixel * wx ;
	m_ImageByteCount = m_RowLength * wy ;
	if ( !m_FileImage ) return ;
	updateImageField < DWORD > ( m_PointerBPP            ) = bpp              ;
	updateImageField < DWORD > ( m_PointerImageByteCount ) = m_ImageByteCount ;
}
void CTiffGrey::SImpl::setOrientation ( TiffOrientation o )
{
	if ( !m_FileImage ) return ;
	updateImageField < DWORD > ( m_PointerOrientation ) = o ;
}

CTiffGrey::SImpl::SImpl ( int x , int y )
{
	std::memset ( this , 0 , sizeof ( *this ) ) ;
	wx = x ;
	wy = y ;
	m_bpp = 16 ;
	m_Orientation = TiffOrientation::to_TopDown ;
	m_NumBytesPerPixel = m_bpp / 8 ;
	m_RowLength = m_NumBytesPerPixel * x ;
	m_ImageByteCount = m_RowLength * y ;
	init () ; // first init for getting tiff pointers
	DWORD DataSize = m_DataPointer ;
	m_DataPointer = getAligned ( m_IfdEntryPointer + 4 ) ; // 4 bytes zero after ifd
	m_ImageStart = getAligned ( m_DataPointer + DataSize ) ;
	//m_FileSize = m_ImageStart + m_ImageByteCount ;
	m_FileSize = m_ImageStart ;
	m_FileImage = ( BYTE* ) malloc ( m_FileSize ) ;
	memset ( m_FileImage , 0 , m_FileSize ) ;
	m_Bits = m_FileImage + m_ImageStart ;
	init () ; // second init for setup tiff image
	m_bValid = true ;
}

DWORD CTiffGrey::SImpl::addToImage ( const void* data , DWORD offset , DWORD size )
{
	if ( m_FileImage ) memcpy ( m_FileImage + offset , data , size ) ;
	return offset + size ;
}

void CTiffGrey::SImpl::addHeader ()
{
	TIFF_HEADER Header ;
	m_IfdStartPointer = addToImage ( Header , 0 ) ;
}

void CTiffGrey::SImpl::addIFD ()
{
	WORD NumEntries = 0 ;
	m_IfdEntryPointer = addToImage ( NumEntries , m_IfdStartPointer ) ;
}

DWORD CTiffGrey::SImpl::addEntry ( TIFFTags tag , FieldType type , DWORD count , DWORD value )
{
	DWORD Result = m_IfdEntryPointer + 8 ;
	SIFDEntry Entry ( tag , type , count , value ) ;
	m_IfdEntryPointer = addToImage ( Entry , m_IfdEntryPointer ) ;
	if ( m_FileImage ) updateImageField < WORD > ( m_IfdStartPointer ) ++ ;
	return Result ;
}

DWORD CTiffGrey::SImpl::addEntry ( TIFFTags tag , FieldType type , DWORD count , const void* data )
{
	SIFDEntry Entry ( tag , type , count , m_DataPointer ) ;
	DWORD Result = m_IfdEntryPointer + 8 ;
	m_IfdEntryPointer = addToImage ( Entry , m_IfdEntryPointer ) ;
	if ( m_FileImage ) updateImageField < WORD > ( m_IfdStartPointer ) ++ ;
	m_DataPointer = addToImage ( data , m_DataPointer , Entry.getDataSize() ) ;
	align ( m_DataPointer ) ;
	return Result ;
}

bool CTiffGrey::SImpl::init ()
{
	addHeader () ;
	
	addIFD () ;

	m_PointerWidth          = addEntry ( TIFFTAG_ImageWidth                , FT_LONG     , 1 , wx                 ) ;
	m_PointerHeight         = addEntry ( TIFFTAG_ImageLength               , FT_LONG     , 1 , wy                 ) ;
	m_PointerBPP            = addEntry ( TIFFTAG_BitsPerSample             , FT_SHORT    , 1 , m_bpp              ) ;
	                          addEntry ( TIFFTAG_Compression               , FT_SHORT    , 1 , 1                  ) ; // uncompressed
	                          addEntry ( TIFFTAG_PhotometricInterpretation , FT_SHORT    , 1 , 1                  ) ; // 0 is black
	                          addEntry ( TIFFTAG_StripOffsets              , FT_LONG     , 1 , m_ImageStart       ) ;
	m_PointerOrientation    = addEntry ( TIFFTAG_Orientation               , FT_SHORT    , 1 , m_Orientation      ) ;
	m_PointerRowsPerStrip   = addEntry ( TIFFTAG_RowsPerStrip              , FT_LONG     , 1 , wy                 ) ; // single strip
	m_PointerImageByteCount = addEntry ( TIFFTAG_StripByteCounts           , FT_LONG     , 1 , m_ImageByteCount   ) ;
	                          addEntry ( TIFFTAG_XResolution               , FT_RATIONAL , 1 , SResolution ( 96 ) ) ; 
	                          addEntry ( TIFFTAG_YResolution               , FT_RATIONAL , 1 , SResolution ( 96 ) ) ;
	                          addEntry ( TIFFTAG_ResolutionUnit            , FT_SHORT    , 1 , 2                  ) ;
	return true ;
}

CTiffGrey::SImpl::SImpl ( const char* filename )
{
	std::memset ( this , 0 , sizeof ( *this ) ) ;
	m_bValid = false ;
	FILE *pf ;
	if ( fopen_s ( &pf , filename , "rb" ) ) return ;
	m_bValid = init ( pf ) ;
	fclose ( pf ) ;
}

BYTE* CTiffGrey::SImpl::getLine ( int line )
{
	return m_Bits + line * m_RowLength ;
}

bool CTiffGrey::SImpl::init ( FILE *pf )
{
	// loading not implemented yet
	return false ;
}

bool CTiffGrey::SImpl::save ( const void* buffer , const char* filename ) const
{
	if ( !m_bValid ) return false ;
	FILE *pf ;
	if ( fopen_s ( &pf , filename , "wb" ) ) return false ;
	bool sucess = save ( buffer , pf ) ;
	fclose ( pf ) ;
	return sucess ;
}
bool CTiffGrey::SImpl::save ( const void* buffer , FILE *pf ) const
{
	if ( m_FileSize       != fwrite ( m_FileImage , 1 , m_FileSize       , pf ) ) return false ;
	if ( m_ImageByteCount != fwrite ( buffer      , 1 , m_ImageByteCount , pf ) ) return false ;
	return true ;
}



////////////////////////////////////////////////////////////////////////////////////////////////////////
// class CGreyBMP
////////////////////////////////////////////////////////////////////////////////////////////////////////

CTiffGrey::~CTiffGrey ()
{
	delete m_pImpl ;
}

//CTiffGrey::CTiffGrey ( const char* filename )
//	: m_pImpl ( new SImpl ( filename ) )
//{}

CTiffGrey::CTiffGrey ( int width , int height )
	: m_pImpl ( new SImpl ( width , height ) )
{}

bool CTiffGrey::isValid () const
{
	return m_pImpl->m_bValid ;
}

//bool CTiffGrey::getData ( void* buffer ) const
//{
//	if ( !isValid () ) return false ;
//	BYTE* BufferOut = ( BYTE* ) buffer ;
//
//	const int NumLines = m_pImpl->wy ;
//	const int RowLengthOut = m_pImpl->wx * getNumBytesPerPixel () ;
//	for ( int iLine = 0 ; iLine < NumLines ; iLine++ )
//	{
//		const BYTE* LineBmp = m_pImpl->getLine ( iLine ) ;
//		std::memcpy ( BufferOut , LineBmp , RowLengthOut ) ;
//		BufferOut += RowLengthOut ;
//	}
//	return true ;
//}
bool CTiffGrey::saveData ( const void* buffer , const char* filename )
{
	return m_pImpl->save ( buffer , filename ) ;
}
void CTiffGrey::setTopDown ( bool td )
{
	m_pImpl->setOrientation ( ( td ? to_TopDown : to_BottomUp ) ) ;
}
void CTiffGrey::setBitmapSize ( int width , int height )
{
	m_pImpl->setBitmapSize ( width , height ) ;
}
void CTiffGrey::setBPP ( int bpp )
{
	m_pImpl->setBPP ( bpp ) ;
}

int CTiffGrey::getNumBytesPerPixel () const
{
	if ( !isValid () ) return 0 ;
	return m_pImpl->m_NumBytesPerPixel ;
}
int CTiffGrey::getBufferSize () const
{
	if ( !isValid () ) return 0 ;
	return m_pImpl->wx * m_pImpl->wy * getNumBytesPerPixel() ;
}
int CTiffGrey::getNumPixelsX () const
{
	if ( !isValid () ) return 0 ;
	return m_pImpl->wx ;
}
int CTiffGrey::getNumPixelsY () const
{
	if ( !isValid () ) return 0 ;
	return m_pImpl->wy ;
}


////////////////////////////////////////////////////////////////////////////////////////////////////////
// simple test
////////////////////////////////////////////////////////////////////////////////////////////////////////


;/* comment this line out for activating test

static bool doTest ()
{
	const char* filename = "H://TestTif.tif" ;
	{
		CTiffGrey TestTiff ( 3 , 4 ) ;
		const WORD TestData1a [] = 
		{
			10000 , 11000 , 12000 ,
			20000 , 21000 , 22000 ,
			30000 , 31000 , 32000 ,
			40000 , 41000 , 42000 ,
		} ;
		const WORD TestData1b [] = 
		{
			10000 , 11000 , 12000 ,
			40000 , 41000 , 42000 ,
			20000 , 21000 , 22000 ,
			30000 , 31000 , 32000 ,
		} ;
		const WORD TestData1c [] = 
		{
			10000 , 11000 , 12000 ,
			20000 , 21000 , 22000 ,
			40000 , 41000 , 42000 ,
			30000 , 31000 , 32000 ,
		} ;
		TestTiff.saveData ( TestData1a , "H://TestTif1a.tif" ) ;
		TestTiff.saveData ( TestData1b , "H://TestTif1b.tif" ) ;
		TestTiff.saveData ( TestData1c , "H://TestTif1c.tif" ) ;

		const BYTE TestData2 [] =
		{
			50  , 55  , 60  ,
			100 , 105 , 110 ,
			150 , 155 , 160 ,
			200 , 205 , 210
		} ;
		TestTiff.setBPP ( 8 ) ;
		TestTiff.setTopDown ( false ) ;
		TestTiff.saveData ( TestData2 , "H://TestTif2.tif" ) ;

		const WORD TestData3 [] = 
		{
			10000 , 11000 , 12000 , 13000 , 14000 ,
			20000 , 21000 , 22000 , 23000 , 24000 ,
			30000 , 31000 , 32000 , 33000 , 34000 ,
			40000 , 41000 , 42000 , 43000 , 44000 ,
			50000 , 51000 , 52000 , 53000 , 54000 ,
			60000 , 61000 , 62000 , 63000 , 64000 ,
		} ;
		TestTiff.setTopDown ( true ) ;
		TestTiff.setBitmapSize ( 5 , 6 ) ;
		TestTiff.setBPP ( 16 ) ;
		TestTiff.saveData ( TestData3 , "H://TestTif3.tif" ) ;
	}
	return true ;
}

static const bool bTest = doTest () ;

// */
