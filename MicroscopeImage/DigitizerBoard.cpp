#include "DigitizerBoard.h"
#include <cstdio>
#include <iostream>
#include <chrono>

using namespace std;

// Constructor
DigitizerBoard::DigitizerBoard(U32 systemId, U32 boardId, U32 sampleRateIdOrValue)
{
	// Get a handle to the board
	boardHandle = AlazarGetBoardBySystemID(systemId, boardId);
    if (boardHandle == NULL)
    {
        printf("Error: Unable to open board system Id %u board Id %u\n", systemId, boardId);
    }

    sampleRate = sampleRateIdOrValue; // set samplerate member data

    bytesPerSample = 2; // standard package

    RETURN_CODE retCode;
    retCode = AlazarSetCaptureClock(boardHandle, INTERNAL_CLOCK, sampleRate, CLOCK_EDGE_RISING, 0); //define Clock (changeable with methode SetCaptureClock)
    //retCode = AlazarSetCaptureClock(boardHandle, FAST_EXTERNAL_CLOCK, SAMPLE_RATE_USER_DEF, CLOCK_EDGE_RISING, 0); //define Clock (changeable with methode SetCaptureClock)

    if (retCode != ApiSuccess)
    {
        printf("Error: AlazarSetCaptureClock failed -- %s\n", AlazarErrorToText(retCode));
    }

    long temp;

    retCode = AlazarGetParameter(boardHandle, CHANNEL_ALL, GET_CHANNELS_PER_BOARD, &temp); // determine number of channels
    if (retCode != ApiSuccess)
    {
        printf("Error: AlazarGetParameter failed -- %s\n", AlazarErrorToText(retCode));
    }

    numberOfChannels = (U32)temp;

    //cout << "Card has " << numberOfChannels << " channels" << endl;

    channels = new channel[numberOfChannels];

    for (int i = 0; i < numberOfChannels; i++)
    {
        channels[i].channelID  = i + 1;                 // CHANNEL_A = 1, CHANNEL_B = 2, ... (see enum ALAZAR_CHANNELS in AlazarCmd.h)
        channels[i].couplingID = DC_COUPLING;           // DC_Coupling as default value, changeable with method (TODO)
        channels[i].impedance  = IMPEDANCE_50_OHM;      // Impedance of 50 Ohm as default value (fits for most BNC cables)
        channels[i].inputRange = INPUT_RANGE_PM_20_V;   // High input range with +/- as default, changeable with method (TODO)
    }

    // Set default values to Trigger Engines (disable)
    EngineJ = {TRIG_ENGINE_J, TRIG_DISABLE, 128, TRIGGER_SLOPE_POSITIVE};
    EngineK = {TRIG_ENGINE_K, TRIG_DISABLE, 128, TRIGGER_SLOPE_POSITIVE};
    // Board will react to Trigger Engine J or K (changeable in methode SetTriggerOperation)
    TriggerOperation = TRIG_ENGINE_OP_J_OR_K;
    ActivateTriggerOperation();

    bufferRecord.postTriggerSamples = 0;
    bufferRecord.preTriggerSamples  = 0;
    bufferRecord.recordsPerCapture  = 0;

    currentDMABufferIndex        = 0;
    NPT_DMA.preTriggerSamples    = 0;
    NPT_DMA.postTriggerSamples   = 0;
    NPT_DMA.RecordsPerBuffer     = 0;
    NPT_DMA.BufferPerAcquisition = 0;
    NPT_DMA.bytesPerBuffer       = 0;
    //BufferArray = new U16*[BUFFER_COUNT];

    U32 buffersCompleted = 0;

    TriggerTimeout = 0;
    AlazarSetTriggerTimeOut(boardHandle, TriggerTimeout);

}

// public functions

HANDLE DigitizerBoard::GetBoardHandle()
{
    return (boardHandle);
}

U32 DigitizerBoard::GetSampleRate()
{
    return (sampleRate);
}

void DigitizerBoard::SetSampleRate(U32 sampleRateIdOrValue)
{
    sampleRate = sampleRateIdOrValue;
    SetCaptureClock(INTERNAL_CLOCK, CLOCK_EDGE_RISING);
}

void DigitizerBoard::SetSampleRateInt(int sampleRateInt)
{
    sampleRate = translateSampleRate(sampleRateInt);
    SetCaptureClock(INTERNAL_CLOCK, CLOCK_EDGE_RISING);
}

void DigitizerBoard::TestLED(const DWORD& time)
{
    RETURN_CODE retCode;
    retCode = AlazarSetLED(boardHandle, LED_ON);
    if (retCode != ApiSuccess)
    {
        printf("Error: AlazarSetLED failed -- %s\n", AlazarErrorToText(retCode));
    }

    Sleep(time);

    retCode = AlazarSetLED(boardHandle, LED_OFF);
    if (retCode != ApiSuccess)
    {
        printf("Error: AlazarSetLED failed -- %s\n", AlazarErrorToText(retCode));
    }
}

void DigitizerBoard::ConfigureChannel(U8 channelID, U32 coupling, ALAZAR_INPUT_RANGES inputRange, U32 impedance)
{
    U32 channelIndex = (U32)channelID - 1;
    //cout << "configuering channel " << channelIndex << endl;
    channels[channelIndex].couplingID = coupling;
    channels[channelIndex].impedance = impedance;
    channels[channelIndex].inputRange = inputRange;
    AlazarConfigureChannel(channels[channelIndex]);
}

void DigitizerBoard::SetCaptureClock(const U32& source, const U32& edgeID)
{
    RETURN_CODE retCode = AlazarSetCaptureClock(boardHandle, source, sampleRate, edgeID, 0);
    if (retCode != ApiSuccess)
    {
        printf("Error: AlazarSetCaptureClock failed -- %s\n", AlazarErrorToText(retCode));
    }
}

void DigitizerBoard::ActivateExternalClock(unsigned int decimation)
{
    RETURN_CODE retCode = AlazarSetCaptureClock(boardHandle, FAST_EXTERNAL_CLOCK, SAMPLE_RATE_USER_DEF, CLOCK_EDGE_RISING, decimation);
    if (retCode != ApiSuccess)
    {
        printf("Error: AlazarSetCaptureClock failed -- %s\n", AlazarErrorToText(retCode));
    }
}

void DigitizerBoard::DeactivateExternalClock()
{
    SetCaptureClock(INTERNAL_CLOCK, CLOCK_EDGE_RISING);
}

void DigitizerBoard::SetTriggerOperation(U32 TO)
{
    TriggerOperation = TO;
    ActivateTriggerOperation();
}

void DigitizerBoard::ConfigureTriggerEngine(U32 TriggerEngine, U32 Source, U32 TriggerLevelCode, U32 Slope)
{
    switch (TriggerEngine)
    {
    case TRIG_ENGINE_J:
        EngineJ.Source = Source;
        EngineJ.TriggerLevelCode = TriggerLevelCode;
        EngineJ.Slope = Slope;
        break;
    case TRIG_ENGINE_K:
        EngineK.Source = Source;
        EngineK.TriggerLevelCode = TriggerLevelCode;
        EngineK.Slope = Slope;
        break;
    default:
        cout << "ERROR: No valid Trigger engine!" << endl;
        break;
    }
    ActivateTriggerOperation();
}

void DigitizerBoard::ConfigureTriggerEngine(U32 TriggerEngine, U32 Source, float TriggerLevelVolts, U32 Slope)
{
    if (Source > numberOfChannels)
    {
        cout << "ERROR: Channel ID to high" << endl;
        return;
    }
    float InputRangeVolts;
    switch (Source)
    {
    case TRIG_CHAN_A:
        InputRangeVolts = translateInputRange(channels[0].inputRange);
        break;
    case TRIG_CHAN_B:
        InputRangeVolts = translateInputRange(channels[1].inputRange);
        break;
    case TRIG_CHAN_C:
        InputRangeVolts = translateInputRange(channels[2].inputRange);
        break;
    case TRIG_CHAN_D:
        InputRangeVolts = translateInputRange(channels[3].inputRange);
        break;
    case TRIG_CHAN_E:
        InputRangeVolts = translateInputRange(channels[4].inputRange);
        break;
    case TRIG_CHAN_F:
        InputRangeVolts = translateInputRange(channels[5].inputRange);
        break;
    case TRIG_CHAN_G:
        InputRangeVolts = translateInputRange(channels[6].inputRange);
        break;
    case TRIG_CHAN_H:
        InputRangeVolts = translateInputRange(channels[7].inputRange);
        break;
    case TRIG_CHAN_I:
        InputRangeVolts = translateInputRange(channels[8].inputRange);
        break;
    case TRIG_CHAN_J:
        InputRangeVolts = translateInputRange(channels[9].inputRange);
        break;
    case TRIG_CHAN_K:
        InputRangeVolts = translateInputRange(channels[10].inputRange);
        break;
    case TRIG_CHAN_L:
        InputRangeVolts = translateInputRange(channels[11].inputRange);
        break;
    case TRIG_CHAN_M:
        InputRangeVolts = translateInputRange(channels[12].inputRange);
        break;
    case TRIG_CHAN_N:
        InputRangeVolts = translateInputRange(channels[13].inputRange);
        break;
    case TRIG_CHAN_O:
        InputRangeVolts = translateInputRange(channels[14].inputRange);
        break;
    case TRIG_CHAN_P:
        InputRangeVolts = translateInputRange(channels[15].inputRange);
        break;
    default:
        cout << "ERROR: invalid Source for Trigger level in volts. Use trigger level code (U32)" << endl;
        return;
        break;
    }
    U32 TriggerLevelCode = (U32)(128 + 127 * TriggerLevelVolts / InputRangeVolts);
    ConfigureTriggerEngine(TriggerEngine, Source, TriggerLevelCode, Slope);
}

void DigitizerBoard::ConfigureBufferRecord(U32 preTriggersamples, U32 postTriggerSamples, U32 recordsPerCapture)
{
    bufferRecord.preTriggerSamples = preTriggersamples;
    bufferRecord.postTriggerSamples = postTriggerSamples;
    bufferRecord.recordsPerCapture = recordsPerCapture;
}

void DigitizerBoard::SinglePortAcquisition(bool SingleChannel, U8 ChannelID, bool wait)
{
    RETURN_CODE retCode;
    retCode = AlazarSetRecordSize(boardHandle, bufferRecord.preTriggerSamples, bufferRecord.postTriggerSamples);
    if (retCode != ApiSuccess)
    {
        printf("Error: AlazarSetRecordSize failed -- %s\n", AlazarErrorToText(retCode));
    }

    retCode = AlazarSetRecordCount(boardHandle, bufferRecord.recordsPerCapture);
    if (retCode != ApiSuccess)
    {
        printf("Error: AlazarSetRecordSize failed -- %s\n", AlazarErrorToText(retCode));
    }

    if (SingleChannel)
    {
        retCode = AlazarSetParameter(boardHandle, 0, SET_SINGLE_CHANNEL_MODE, ChannelID);
        if (retCode != ApiSuccess)
        {
            printf("Error: AlazarSetRecordSize failed -- %s\n", AlazarErrorToText(retCode));
        }
    }
        
    retCode = AlazarStartCapture(boardHandle);
    if (retCode != ApiSuccess)
    {
        printf("Error: AlazarStartCapture failed -- %s\n", AlazarErrorToText(retCode));
    }

    if (wait)
        while (AlazarBusy(boardHandle)) {}
}

U16* DigitizerBoard::ReadDatafromBuffer(int& samplesPerRecord, U8 ChannelID, long record)
{
    samplesPerRecord = bufferRecord.preTriggerSamples + bufferRecord.postTriggerSamples;
    U32 allocBytes = bytesPerSample * (samplesPerRecord + 16);
    void* buffer = malloc(allocBytes); // allocate memory
    if (!buffer) // check if memory is available
    {
        cout << "ERROR: Out of Memory" << endl;
        return(NULL);
    }

    AlazarRead(boardHandle, ChannelID, buffer, bytesPerSample, record, ((long)bufferRecord.preTriggerSamples), samplesPerRecord);

    //U16* pSamples = (U16*)buffer;

    /*for (int i = 0; i < samplesPerRecord; i++)
    {
        U16 sampleValue = pSamples[i];
        printf("sample value = %04X\n", sampleValue);
    }*/

    return ((U16*)buffer);
}

double* DigitizerBoard::ConvertToVoltage(U16* data, const int& n, U8 ChannelID)
{
    double* voltage = new double[n];
    if (!voltage)
    {
        cout << "ERROR: Out of Memory" << endl;
        return (NULL);
    }

    // Since 1930 Card only can measure from minus to plus and not from 0 to plus, only this case is treated here
    int bitshift = 4;
    int bitspersample = 12;
    double codeZero = (1 << (bitspersample - 1)) - 0.5;
    double codeRange = (1 << (bitspersample - 1)) - 0.5;

    double inputRange = translateInputRange(channels[ChannelID - 1].inputRange);

    for (int i = 0; i < n; i++)
    {
        U16 samplecode = data[i] >> bitshift; // convert 16 bit data into 12 bit data, since DAQ Card only has 12 bits
        voltage[i] = inputRange * ((double)(samplecode - codeZero) / codeRange);
    }

    return (voltage);
}

double* DigitizerBoard::GetTimes(int& samplesPerRecord, long record)
{
    samplesPerRecord = bufferRecord.preTriggerSamples + bufferRecord.postTriggerSamples;
    double* times = new double[samplesPerRecord];
    U32 samplerate = translateSampleRate();
    U32 preTriggerSamples = bufferRecord.preTriggerSamples;
    for (int i = 0; i < samplesPerRecord; i++)
    {
        times[i] = ((double)i - preTriggerSamples) / samplerate;
    }
    return (times);
}

U16 DigitizerBoard::RecordSingleValue(U8 ChannelID, U32 avg)
{
    AlazarSetTriggerTimeOut(boardHandle, 1); // forces trigger event after 10us
    ConfigureBufferRecord(0, avg, 1); 
    SinglePortAcquisition();

    int n;
    U16* data = ReadDatafromBuffer(n, ChannelID);

    if (n != avg)
    {
        cout << "Error: incorrect number of samples" << endl;
    }

    double average = 0;
    for (int i = 0; i < n; i++)
        average += data[i];
    average /= n;

    AlazarSetTriggerTimeOut(boardHandle, TriggerTimeout); // reset Trigger Timeout
    return ((U16) average);
}

void DigitizerBoard::SetTriggertimeout(U32 TTO)
{
    TriggerTimeout = TTO;
    AlazarSetTriggerTimeOut(boardHandle, TriggerTimeout);
}

void DigitizerBoard::SoftwareTrigger()
{
    AlazarForceTrigger(boardHandle);
}

void DigitizerBoard::ConfigureAuxIO(U32 mode, U32 parameter)
{
    AuxIOMode = mode;
    AuxIOParameter = parameter;

    RETURN_CODE retCode = AlazarConfigureAuxIO(boardHandle, mode, parameter);
    if (retCode != ApiSuccess)
        printf("Error: AlazarConfigureAuxIO failed -- %s\n", AlazarErrorToText(retCode));
}

void DigitizerBoard::StartNPTAutoDMAAcquisition(U32 samplesPerRecord, U32 RecordsPerBuffer, U32 BuffersPerAcquisition, U8 channel)
{
    NPT_DMA.preTriggerSamples       = 0;
    NPT_DMA.postTriggerSamples      = samplesPerRecord;
    NPT_DMA.RecordsPerBuffer        = RecordsPerBuffer;
    NPT_DMA.BufferPerAcquisition    = BuffersPerAcquisition;

    // Get the sample size in bits, and the on-board memory size in samples per channel
    U8 bitsPerSample;
    U32 maxSamplesPerChannel;
    RETURN_CODE retCode = AlazarGetChannelInfo(boardHandle, &maxSamplesPerChannel, &bitsPerSample);
    if (retCode != ApiSuccess)
    {
        printf("Error: AlazarGetChannelInfo failed -- %s\n", AlazarErrorToText(retCode));
        return;
    }

    // Calculate the size of each DMA buffer in bytes (Copy from ATS9130_NPT.cpp sample code)
    float BytesPerSample = (float)((bitsPerSample + 7) / 8);
    U32 bytesPerRecord = (U32)(BytesPerSample * samplesPerRecord + 0.5); // 0.5 compensates for double to integer conversion 
    U32 bytesPerBuffer = bytesPerRecord * RecordsPerBuffer;

    NPT_DMA.bytesPerBuffer = bytesPerBuffer;

    //cout << "bytesPerRecord = " << bytesPerRecord << endl;

    // Allocate memory for DMA buffers (Copy from ATS9130_NPT.cpp sample code)
    bool success = true;

    buffersCompleted        = 0;
    currentDMABufferIndex   = 0;
    U32 bufferIndex;
    for (bufferIndex = 0; (bufferIndex < BUFFER_COUNT) && success; bufferIndex++)
    {
        // Allocate page aligned memory
        BufferArray[bufferIndex] =
            (U16*)AlazarAllocBufferU16(boardHandle, bytesPerBuffer);
        if (BufferArray[bufferIndex] == NULL)
        {
            printf("Error: Alloc %u bytes failed\n", bytesPerBuffer);
            success = false;
        }
    }

    // Configure the record size (Copy from ATS9130_NPT.cpp sample code)
    if (success)
    {
        retCode = AlazarSetRecordSize(boardHandle, 0, samplesPerRecord);
        if (retCode != ApiSuccess)
        {
            printf("Error: AlazarSetRecordSize failed -- %s\n", AlazarErrorToText(retCode));
            success = false;
        }
    }

    // Configure the board to make an NPT AutoDMA acquisition (Copy from ATS9130_NPT.cpp sample code)
    if (success)
    {
        U32 recordsPerAcquisition = RecordsPerBuffer * BuffersPerAcquisition;

        U32 admaFlags = ADMA_EXTERNAL_STARTCAPTURE | ADMA_NPT | ADMA_FIFO_ONLY_STREAMING;

        retCode = AlazarBeforeAsyncRead(boardHandle, channel, 0,
            samplesPerRecord, RecordsPerBuffer, recordsPerAcquisition,
            admaFlags);
        if (retCode != ApiSuccess)
        {
            printf("Error: AlazarBeforeAsyncRead failed -- %s\n", AlazarErrorToText(retCode));
            success = false;
        }
    }

    // Add the buffers to a list of buffers available to be filled by the board (Copy from ATS9130_NPT.cpp sample code)

    for (bufferIndex = 0; (bufferIndex < BUFFER_COUNT) && success; bufferIndex++)
    {
        U16* pBuffer = BufferArray[bufferIndex];
        retCode = AlazarPostAsyncBuffer(boardHandle, pBuffer, bytesPerBuffer);
        if (retCode != ApiSuccess)
        {
            printf("Error: AlazarPostAsyncBuffer %u failed -- %s\n", bufferIndex,
                AlazarErrorToText(retCode));
            success = false;
        }
    }

    // Arm the board system to wait for a trigger event to begin the acquisition (Copy from ATS9130_NPT.cpp sample code)
    if (success)
    {
        retCode = AlazarStartCapture(boardHandle);
        if (retCode != ApiSuccess)
        {
            printf("Error: AlazarStartCapture failed -- %s\n", AlazarErrorToText(retCode));
            success = false;
        }
    }
}

//bool DigitizerBoard::ReadDataNPTAutoDmA(U16* data, int timeout_ms)
//{
//    RETURN_CODE retCode;
//
//    U16* pBuffer = BufferArray[currentDMABufferIndex];
//    retCode = AlazarWaitAsyncBufferComplete(boardHandle, pBuffer, timeout_ms);
//    if (retCode != ApiSuccess)
//    {
//        printf("Error: AlazarWaitAsyncBufferComplete failed -- %s\n", AlazarErrorToText(retCode));
//        return false;
//    }
//
//    // Copy read out data into data array
//    U32 samples = NPT_DMA.postTriggerSamples;
//    for (int i = 0; i < samples; i++)
//    {
//        data[i] = pBuffer[i];
//    }
//
//    // Add the buffer at the end of the buffer list again
//    retCode = AlazarPostAsyncBuffer(boardHandle, pBuffer, NPT_DMA.bytesPerBuffer);
//    if (retCode != ApiSuccess)
//    {
//        printf("Error: AlazarPostAsyncBuffer failed -- %s\n", AlazarErrorToText(retCode));
//        return false;
//    }
//
//    currentDMABufferIndex++;
//    //cout << pBuffer[0] << endl;
//    currentDMABufferIndex = currentDMABufferIndex % BUFFER_COUNT; // Next buffer can be read out
//    return true;
//}
bool DigitizerBoard::ReadDataNPTAutoDmA(U16* data, int timeout_ms)
{
    bool a = ReadDataNPTAutoDmA(data, timeout_ms, 0);
    return a;
}

bool DigitizerBoard::ReadDataNPTAutoDmA(U16* data, int timeout_ms, int px)
{
    RETURN_CODE retCode;

    U16* pBuffer = BufferArray[currentDMABufferIndex];
    retCode = AlazarWaitAsyncBufferComplete(boardHandle, pBuffer, timeout_ms);
    if (retCode != ApiSuccess)
    {
        printf("Error: AlazarWaitAsyncBufferComplete failed -- %s\n", AlazarErrorToText(retCode));
        return false;
    }

    // Copy read out data into data array
    U32 samples = NPT_DMA.postTriggerSamples;
    for (int i = 0; i < samples; i++)
    {
        data[i+samples*px] = pBuffer[i];
    }

    // Add the buffer at the end of the buffer list again
    retCode = AlazarPostAsyncBuffer(boardHandle, pBuffer, NPT_DMA.bytesPerBuffer);
    if (retCode != ApiSuccess)
    {
        printf("Error: AlazarPostAsyncBuffer failed -- %s\n", AlazarErrorToText(retCode));
        return false;
    }

    currentDMABufferIndex++;
    //cout << pBuffer[0] << endl;
    currentDMABufferIndex = currentDMABufferIndex % BUFFER_COUNT; // Next buffer can be read out
    return true;
}

bool DigitizerBoard::ReadDataNPTAutoDmALine(U16* data, int timeout_ms, int line) {
    //std::cout << "Current DMA Buffer Index = " << currentDMABufferIndex << endl;
    RETURN_CODE retCode;

    U16* pBuffer = BufferArray[currentDMABufferIndex];
    retCode = AlazarWaitAsyncBufferComplete(boardHandle, pBuffer, timeout_ms);
    if (retCode != ApiSuccess){
        printf("Error: AlazarWaitAsyncBufferComplete failed -- %s\n", AlazarErrorToText(retCode));
        return false;
    }

    // Copy read out data into data array
    U32 datapointsPerLine = NPT_DMA.postTriggerSamples * NPT_DMA.RecordsPerBuffer;
    //std::cout << "datapoints per line = " << datapointsPerLine << endl;
    for (int i = 0; i < datapointsPerLine; i++)
    {
        data[i + datapointsPerLine * line] = pBuffer[i];
    }
    buffersCompleted++;
    if (buffersCompleted == NPT_DMA.BufferPerAcquisition) {
        //std::cout << "Card has reached number of completed buffers!\n";
    }
    //printf("Copied buffer for line %u \r",line + 1);
    // Add the buffer at the end of the buffer list again
    retCode = AlazarPostAsyncBuffer(boardHandle, pBuffer, NPT_DMA.bytesPerBuffer);
    if (retCode != ApiSuccess)
    {
        printf("Error: AlazarPostAsyncBuffer failed -- %s\n", AlazarErrorToText(retCode));
        return false;
    }

    currentDMABufferIndex++;
    currentDMABufferIndex = currentDMABufferIndex % BUFFER_COUNT; // Next buffer can be read out
    return true;
}


bool DigitizerBoard::AbortNPTAutoDMA()
{
    RETURN_CODE retCode;
    //printf("buffers completed: %d \n", buffersCompleted);
    // Abort the acquisition (Copy from ATS9130_NPT.cpp sample code)
    retCode = AlazarAbortAsyncRead(boardHandle);
    buffersCompleted = 0;
    if (retCode != ApiSuccess)
    {
        printf("Error: AlazarAbortAsyncRead failed -- %s\n", AlazarErrorToText(retCode));
        return false;
    }

    // Free all memory allocated (Copy from ATS9130_NPT.cpp sample code)
    for (U32 bufferIndex = 0; bufferIndex < BUFFER_COUNT; bufferIndex++)
    {
        if (BufferArray[bufferIndex] != NULL)
        {
            AlazarFreeBufferU16(boardHandle, BufferArray[bufferIndex]);
        }
    }
    return true;
}

// private functions

void DigitizerBoard::AlazarConfigureChannel(channel CH)
{
    U8 channelID = CH.channelID;
    U32 coupling = CH.couplingID;
    U32 inputRange = CH.inputRange;
    U32 impedance = CH.impedance;
    RETURN_CODE retCode = AlazarInputControl(boardHandle, channelID, coupling, inputRange, impedance);
    if (retCode != ApiSuccess)
    {
        printf("Error: AlazarInputControl failed -- %s\n", AlazarErrorToText(retCode));
    }
}

void DigitizerBoard::ActivateTriggerOperation()
{
    RETURN_CODE retCode;
    retCode = AlazarSetTriggerOperation(boardHandle, TriggerOperation, 
        EngineJ.TriggerEngine, EngineJ.Source, EngineJ.Slope, EngineJ.TriggerLevelCode, 
        EngineK.TriggerEngine, EngineK.Source, EngineK.Slope, EngineK.TriggerLevelCode);
    if (retCode != ApiSuccess)
    {
        printf("Error: AlazarSetTriggerOperation failed -- %s\n", AlazarErrorToText(retCode));
    }

    if (EngineJ.Source == TRIG_EXTERNAL || EngineK.Source == TRIG_EXTERNAL)
    {
        //AlazarSetExternalTrigger(boardHandle, DC_COUPLING, ETR_5V_50OHM);
        AlazarSetExternalTrigger(boardHandle, DC_COUPLING, ETR_TTL);
        //AlazarSetExternalTrigger(boardHandle, DC_COUPLING, ETR_2V5_50OHM);
    }
}

float DigitizerBoard::translateInputRange(ALAZAR_INPUT_RANGES IR)
{
    switch (IR)
    {
    // +/- IR
    case INPUT_RANGE_PM_20_MV:
        return(20.0);
    case INPUT_RANGE_PM_40_MV:
        return(40.0);
    case INPUT_RANGE_PM_50_MV:
        return(50.0);
    case INPUT_RANGE_PM_80_MV:
        return(80.0);
    case INPUT_RANGE_PM_100_MV:
        return(100.0);
    case INPUT_RANGE_PM_200_MV:
        return(200.0);
    case INPUT_RANGE_PM_400_MV:
        return(400.0);
    case INPUT_RANGE_PM_500_MV:
        return(500.0);
    case INPUT_RANGE_PM_800_MV:
        return(800.0);
    case INPUT_RANGE_PM_1_V:
        return(1000.0);
    case INPUT_RANGE_PM_2_V:
        return(2000.0);
    case INPUT_RANGE_PM_4_V:
        return(4000.0);
    case INPUT_RANGE_PM_5_V:
        return(5000.0);
    case INPUT_RANGE_PM_8_V:
        return(8000.0);
    case INPUT_RANGE_PM_10_V:
        return(10000.0);
    case INPUT_RANGE_PM_20_V:
        return(20000.0);
    case INPUT_RANGE_PM_40_V:
        return(40000.0);
    case INPUT_RANGE_PM_16_V:
        return(16000.0);
    case INPUT_RANGE_PM_1_V_25:
        return(1250.0);
    case INPUT_RANGE_PM_2_V_5:
        return(2500.0);
    case INPUT_RANGE_PM_125_MV:
        return(125.0);
    case INPUT_RANGE_PM_250_MV:
        return(250.0);
    // 0 -> IR
    case INPUT_RANGE_0_TO_40_MV:
        return(40.0);
    case INPUT_RANGE_0_TO_80_MV:
        return(80.0);
    case INPUT_RANGE_0_TO_100_MV:
        return(100.0);
    case INPUT_RANGE_0_TO_160_MV:
        return(160.0);
    case INPUT_RANGE_0_TO_200_MV:
        return(200.0);
    case INPUT_RANGE_0_TO_250_MV:
        return(250.0);
    case INPUT_RANGE_0_TO_400_MV:
        return(400.0);
    case INPUT_RANGE_0_TO_500_MV:
        return(500.0);
    case INPUT_RANGE_0_TO_800_MV:
        return(800.0);
    case INPUT_RANGE_0_TO_1_V:
        return(1000.0);
    case INPUT_RANGE_0_TO_1600_MV:
        return(1600.0);
    case INPUT_RANGE_0_TO_2_V:
        return(2000.0);
    case INPUT_RANGE_0_TO_2_V_5:
        return(2500.0);
    case INPUT_RANGE_0_TO_4_V:
        return(4000.0);
    case INPUT_RANGE_0_TO_5_V:
        return(5000.0);
    case INPUT_RANGE_0_TO_8_V:
        return(8000.0);
    case INPUT_RANGE_0_TO_10_V:
        return(10000.0);
    case INPUT_RANGE_0_TO_16_V:
        return(16000.0);
    case INPUT_RANGE_0_TO_20_V:
        return(20000.0);
    case INPUT_RANGE_0_TO_80_V:
        return(80000.0);
    case INPUT_RANGE_0_TO_32_V:
        return(32000.0);
    // -IR -> 0
    case INPUT_RANGE_0_TO_MINUS_40_MV:
        return(40.0);
    case INPUT_RANGE_0_TO_MINUS_80_MV:
        return(80.0);
    case INPUT_RANGE_0_TO_MINUS_100_MV:
        return(100.0);
    case INPUT_RANGE_0_TO_MINUS_160_MV:
        return(160.0);
    case INPUT_RANGE_0_TO_MINUS_200_MV:
        return(200.0);
    case INPUT_RANGE_0_TO_MINUS_250_MV:
        return(250.0);
    case INPUT_RANGE_0_TO_MINUS_400_MV:
        return(400.0);
    case INPUT_RANGE_0_TO_MINUS_500_MV:
        return(500.0);
    case INPUT_RANGE_0_TO_MINUS_800_MV:
        return(800.0);
    case INPUT_RANGE_0_TO_MINUS_1_V:
        return(1000.0);
    case INPUT_RANGE_0_TO_MINUS_1600_MV:
        return(1600.0);
    case INPUT_RANGE_0_TO_MINUS_2_V:
        return(2000.0);
    case INPUT_RANGE_0_TO_MINUS_2_V_5:
        return(2500.0);
    case INPUT_RANGE_0_TO_MINUS_4_V:
        return(4000.0);
    case INPUT_RANGE_0_TO_MINUS_5_V:
        return(5000.0);
    case INPUT_RANGE_0_TO_MINUS_8_V:
        return(8000.0);
    case INPUT_RANGE_0_TO_MINUS_10_V:
        return(10000.0);
    case INPUT_RANGE_0_TO_MINUS_16_V:
        return(16000.0);
    case INPUT_RANGE_0_TO_MINUS_20_V:
        return(20000.0);
    case INPUT_RANGE_0_TO_MINUS_80_V:
        return(80000.0);
    case INPUT_RANGE_0_TO_MINUS_32_V:
        return(32000.0);
    default:
        cout << "ERROR: Invalid Input Range ID" << endl;
        return(0);
    }
}

U32 DigitizerBoard::translateSampleRate()
{
    switch (sampleRate)
    {
    case SAMPLE_RATE_1KSPS:
        return(1000);
    case SAMPLE_RATE_2KSPS:
        return(2000);
    case SAMPLE_RATE_5KSPS:
        return(5000);
    case SAMPLE_RATE_10KSPS:
        return(10000);
    case SAMPLE_RATE_20KSPS:
        return(20000);
    case SAMPLE_RATE_50KSPS:
        return(50000);
    case SAMPLE_RATE_100KSPS:
        return(100000);
    case SAMPLE_RATE_200KSPS:
        return(200000);
    case SAMPLE_RATE_500KSPS:
        return(500000);
    case SAMPLE_RATE_1MSPS:
        return(1000000);
    case SAMPLE_RATE_2MSPS:
        return(2000000);
    case SAMPLE_RATE_5MSPS:
        return(5000000);
    case SAMPLE_RATE_10MSPS:
        return(10000000);
    case SAMPLE_RATE_20MSPS:
        return(20000000);
    case SAMPLE_RATE_25MSPS:
        return(25000000);
    case SAMPLE_RATE_50MSPS:
        return(50000000);
    case SAMPLE_RATE_100MSPS:
        return(100000000);
    case SAMPLE_RATE_125MSPS:
        return(125000000);
    case SAMPLE_RATE_160MSPS:
        return(160000000);
    case SAMPLE_RATE_180MSPS:
        return(180000000);
    case SAMPLE_RATE_200MSPS:
        return(200000000);
    case SAMPLE_RATE_250MSPS:
        return(250000000);
    case SAMPLE_RATE_400MSPS:
        return(400000000);
    case SAMPLE_RATE_500MSPS:
        return(500000000);
    case SAMPLE_RATE_800MSPS:
        return(800000000);
    case SAMPLE_RATE_1000MSPS:
        return(1000000000);
    case SAMPLE_RATE_1200MSPS:
        return(1200000000);
    case SAMPLE_RATE_1500MSPS:
        return(1500000000);
    case SAMPLE_RATE_1600MSPS:
        return(1600000000);
    case SAMPLE_RATE_1800MSPS:
        return(1800000000);
    case SAMPLE_RATE_2000MSPS:
        return(2000000000);
    case SAMPLE_RATE_2400MSPS:
        return(2400000000);
    case SAMPLE_RATE_3000MSPS:
        return(3000000000);
    case SAMPLE_RATE_3600MSPS:
        return(3600000000);
    case SAMPLE_RATE_4000MSPS:
        return(4000000000);
    case SAMPLE_RATE_300MSPS:
        return(300000000);
    case SAMPLE_RATE_350MSPS:
        return(350000000);
    case SAMPLE_RATE_370MSPS:
        return(370000000);
    default:
        cout << "ERROR: undefined sample rate" << endl;
        return(0);
    }
}

U32 DigitizerBoard::translateSampleRate(int SR) {
    U32 SR_code;

    if (SR == 1000)
        SR_code = 0X00000001UL;
    else if (SR == 2000)
        SR_code = 0X00000002UL;
    else if (SR == 5000)
        SR_code = 0X00000004UL;
    else if (SR == 10000)
        SR_code = 0X00000008UL;
    else if (SR == 20000)
        SR_code = 0X0000000AUL;
    else if (SR == 50000)
        SR_code = 0X0000000CUL;
    else if (SR == 100000)
        SR_code = 0X0000000EUL;
    else if (SR == 200000)
        SR_code = 0X00000010UL;
    else if (SR == 500000)
        SR_code = 0X00000012UL;
    else if (SR == 1000000)
        SR_code = 0X00000014UL;
    else if (SR == 1000000)
        SR_code = 0X00000014UL;
    else if (SR == 2000000)
        SR_code = 0X00000018UL;
    else if (SR == 5000000)
        SR_code = 0X0000001AUL;
    else if (SR == 10000000)
        SR_code = 0X0000001CUL;
    else if (SR == 20000000)
        SR_code = 0X0000001EUL;
    else if (SR == 25000000)
        SR_code = 0X00000021UL;
    else if (SR == 50000000)
        SR_code = 0X00000022UL;
    else
        SR_code = 0X00000000UL;

    return SR_code;
}