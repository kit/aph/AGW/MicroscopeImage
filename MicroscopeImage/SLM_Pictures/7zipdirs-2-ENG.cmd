echo off

if exist "C:\Program Files\7-Zip\7z.exe" (

  pause

  for /D %%f in (*.*) do (
    echo gefunden == %%f
    "C:\Program Files\7-Zip\7z.exe" a -tzip "%%f".zip "%%f"\
    echo %ERRORLEVEL%
    if %ERRORLEVEL% EQU 0 (
      rd /s /q "%%f" 
    )
  )
) else ( echo !!!!!!! 7z.exe doesn't exist !!!!!!!! )
pause
